/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Lexical Graph.
 * 
 * @author Fernando Berzal (berzal@modelcc.org) & Luis Quesada (lquesada@modelcc.org)
 */
public final class LexicalGraph implements Serializable 
{
    /**
     * Set of tokens for this graph.
     */
    private List<Token> tokens;

    /**
     * List of start tokens for this graph.
     */
    private List<Token> start;

    /**
     * List of preceding tokens.
     */
    private Map<Token,List<Token>> preceding;

    /**
     * List of following tokens.
     */
    private Map<Token,List<Token>> following;

    /**
     * Input start index
     */
    private int inputStart;

    /**
     * Input end index
     */
    private int inputEnd;
    
    /**
     * Default constructor.
     * @param tokens the token set
     * @param start the start symbol
     * @param preceding the preceding map
     * @param following the following map
     */
    public LexicalGraph (Collection<Token> tokens, int inputStart,int inputEnd) 
    {
        this.tokens = new ArrayList<Token>(tokens);
        
        this.start = new ArrayList<Token>();
        this.preceding = new HashMap<Token,List<Token>>();
        this.following = new HashMap<Token,List<Token>>();
       
        this.inputStart = inputStart;
        this.inputEnd = inputEnd;
    }

    /**
     * Get the list of tokens in the graph.
     * @return list of tokens.
     */
    public List<Token> getTokens() 
    {
        return Collections.unmodifiableList(tokens);
    }

    
    /**
     * Add a token to the graph.
     * @param t token to be added
     */
    public void addToken (Token t)
    {
    	tokens.add(t);
    }
    
    /**
     * Add a start token to the graph.
     * @param t start token
     */
    public void addStartToken (Token t)
    {
    	start.add(t);
    }
    
    
    /**
     * Add a link between two consecutive tokens.
     * @param t1 the preceding token.
     * @param t2 the following token.
     */
    public void link (Token t1, Token t2)
    {
        addMapElement(preceding,t2,t1);
        addMapElement(following,t1,t2);    	
    }
    
    private void addMapElement(Map<Token,List<Token>> target, Token t1,Token t2) 
    {
        List<Token> set = target.get(t1);
        if (set == null) {
            set = new ArrayList<Token>();
            target.put(t1,set);
        }
        set.add(t2);
    }
    
    
    /**
     * Get the list of start tokens for this lexical graph.
     * @return the list of start tokens.
     */
    public List<Token> getStart() 
    {
        return Collections.unmodifiableList(start);
    }
    
    public Token getStartToken ()
    {
    	if (start.size()>0)
    		return start.get(0);
    	else
    		return null;
    }

    /**
     * Get the preceding relationship map.
     * @return the map of preceding tokens.
     */
    public Map<Token, List<Token>> getPreceding() 
    {
        return Collections.unmodifiableMap(preceding);
    }
    
    /**
     * Get the list of preceding tokens.
     * @param t Given token
     * @return Tokens immediately preceding t
     */
    public List<Token> getPreceding (Token t)
    {
    	return preceding.get(t);
    }

    /**
     * Get the following relationship map.
     * @return the map of following.
     */
    public Map<Token, List<Token>> getFollowing() 
    {
        return Collections.unmodifiableMap(following);
    }
    
    
    /**
     * Get the list of following tokens.
     * @param t Given token
     * @return Tokens immediately following t
     */
    public List<Token> getFollowing (Token t)
    {
    	return following.get(t);
    }

    /**
     * @return the input start index.
     */
    public int getInputStartIndex() 
    {
        return inputStart;
    }

    /**
     * @return the input end index.
     */
    public int getInputEndIndex() 
    {
        return inputEnd;
    }
}
