/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.lamb;

import java.io.Reader;
import java.io.Serializable;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;

/**
 * Lamb - Lexer with AMBiguity Support.
 *
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class LambLexer extends Lexer implements Serializable 
{
    /**
     * Lexical Specification.
     */
    protected LexicalSpecification ls;
        
    /**
     * Constructor.
     * @param ls the lexical specification.
     */
    public LambLexer(LexicalSpecification ls) 
    {
        this.ls = ls;
    }
    
    /**
     * Scans an input string.
     * @param input the input string.
     * @return the obtained lexical graph.
     */    
    @Override
    public LexicalGraph scan(Reader input) 
    {
        return new Lamb(ls,input).scan();
    }

}