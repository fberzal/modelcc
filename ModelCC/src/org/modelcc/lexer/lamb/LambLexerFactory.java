/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.lamb;

import java.io.Serializable;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.lexer.LexerException;
import org.modelcc.lexer.LexerFactory;

/**
 * Lamb Lexer Generator
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class LambLexerFactory extends LexerFactory implements Serializable 
{
	@Override
    public LambLexer createLexer (LexicalSpecification lexis) throws LexerException 
    {
        return new LambLexer(lexis);
    }

}
