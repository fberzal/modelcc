/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import org.modelcc.ModelCCException;

/**
 * Lexer exception
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class LexerException extends ModelCCException 
{
	public LexerException (String message)
	{
		super(message);
	}
	
	public LexerException (String message, Throwable cause)
	{
		super(message, cause);
	}
}
