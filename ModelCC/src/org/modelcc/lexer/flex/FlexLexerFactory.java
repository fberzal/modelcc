/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.flex;

import java.io.Serializable;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.lexer.LexerException;
import org.modelcc.lexer.LexerFactory;

/**
 * FLex - Fast Lexer
 * 
 * @author Fernando Berzal (fberzal@acm.org)
 */
public class FlexLexerFactory extends LexerFactory implements Serializable 
{
	@Override
    public FlexLexer createLexer (LexicalSpecification lexis) throws LexerException 
    {
        return new FlexLexer(lexis);
    }

}
