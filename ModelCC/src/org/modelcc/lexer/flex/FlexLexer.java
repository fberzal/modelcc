/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.flex;

import java.io.Reader;
import java.io.Serializable;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;

/**
 * FLex - Fast Lexer
 *
 * @author Fernando Berzal (berzal@acm.org)
 */
public class FlexLexer extends Lexer implements Serializable 
{
    /**
     * Lexical Specification.
     */
    protected LexicalSpecification ls;
        
    /**
     * Constructor.
     * @param ls the lexical specification.
     */
    public FlexLexer(LexicalSpecification ls) 
    {
        this.ls = ls;
    }
    
    /**
     * Scans an input string.
     * @param input the input string.
     * @return the obtained lexical graph.
     */    
    @Override
    public LexicalGraph scan(Reader input) 
    {
        return new Flex(ls,input).scan();
    }
}