/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import java.io.Serializable;

import org.modelcc.language.factory.SymbolIdentifier;

/**
 * Token.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class Token implements Serializable 
{
    /**
     * Token type.
     */
    private Object type;

    /**
     * Value.
     */
    private Object value;


    /**
     * Matched text.
     */
    private String string;

    /**
     * Offset.
     */
    private int offset;

    /**
     * Line.
     */
    private int line;

    /**
     * User data.
     */
    private Object userData;
    
    /**
     * Constructor.
     * @param type the token type.
     * @param value the token value.
     * @param string the token-matched string. 
     * @param offset the token input.
     * @param line the token input line number.
     */
    public Token (Object type, Object value, String string, int offset, int line) 
    {
        this.type = type;
        this.value = value;
        this.string = string;
        this.offset = offset;
        this.line = line;
    }

    /**
     * @return the token type.
     */
    public Object getType() 
    {
        return type;
    }
    
    public Class getTokenClass ()
    {
		return ((SymbolIdentifier)getType()).getElement().getElementClass();
    }
    
    /**
     * Token offset 
     * @return the token start offset.
     */
    public int getOffset() 
    {
        return offset;
    }
    
    /**
     * Token line 
     * @return the token input line.
     */
    public int getLine() 
    {
        return line;
    }

    /**
     * Token offset (starting position)
     * @return the token start index.
     */
    public int getStartIndex() 
    {
        return offset;
    }

    /**
     * Token offset (ending position)
     * @return the token end index.
     */
    public int getEndIndex() 
    {
        return offset+string.length()-1;
    }

    /**
     * Get token value.
     * @return the token value.
     */
    public Object getValue() 
    {
        return value;
    }


    /**
     * Set token value.
     * @param value the token value
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Get user data.
     * @return the token user data
     */
    public Object getUserData() {
        return userData;
    }

    /**
     * Set user data.
     * @param userData the token user data
     */
    public void setUserData(Object userData) {
        this.userData = userData;
    }

    /**
     * Token string
     * @return the input string corresponding to this token.
     */
    public String getString() 
    {
        return string;
    }
    
    
    /**
     * Token length
     * @return the length of the input string corresponding to this token 
     */
    public int length ()
    {
    	return ((string!=null)? string.length(): 0);
    }

}
