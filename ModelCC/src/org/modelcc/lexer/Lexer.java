/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;

/**
 * ModelCC Lexer interface
 * @author Luis Quesada (lquesada@modelcc.org)
 * @serial
 */
public abstract class Lexer implements Serializable 
{
    /**
     * Scans an input reader.
     * @param input the input reader.
     * @return the lexical graph.
     */
    public abstract LexicalGraph scan (Reader input);
    
    /**
     * Scans an input string.
     * @param input the input string.
     * @return the lexical graph.
     */
    public final LexicalGraph scan (String input) 
    {
        return scan(new StringReader(input));
    }

}
