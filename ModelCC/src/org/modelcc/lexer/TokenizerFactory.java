/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import java.io.Reader;
import java.io.Serializable;

import org.modelcc.language.LanguageException;
import org.modelcc.language.LanguageSpecification;
import org.modelcc.language.factory.LanguageSpecificationFactory;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.metamodel.LanguageModel;

/**
 * ModelCC Parser Generator: Tokenizer factory.
 * 
 * @author Fernando Berzal (fberzal@modelcc.org)
 */
public class TokenizerFactory implements Serializable 
{   
	/**
	 * Current language model
	 */
	private LanguageModel model;
	
	/**
	 * Current lexical specification
	 */
	private LexicalSpecification lexis;
		
    /**
     * Current tokenizer class
     */
    private Class tokenizerClass = org.modelcc.lexer.lamb.Lamb.class; 
                                 // org.modelcc.lexer.lamb.Lamb.class;
                                 // org.modelcc.lexer.flex.Flex.class;

	
    
	/**
	 * Constructor
	 * @param model Language model
	 * @throws LanguageException 
	 */
	public TokenizerFactory (LanguageModel model) 
		throws LanguageException
	{
		setModel(model);
	}
	
	/**
	 * Constructor
	 * @param model Language model
	 * @param tokenizerClass Tokenizer class
	 * @throws LanguageException 
	 */
	public TokenizerFactory (LanguageModel model, Class tokenizerClass)
			throws LanguageException
	{
		setModel(model);
		setTokenizerClass(tokenizerClass);
	}

	
	public Class getTokenizerClass ()
	{
		return tokenizerClass;
	}

	public void setTokenizerClass (Class tokenizerClass)
	{
		this.tokenizerClass = tokenizerClass;
	}
    
	public LanguageModel getModel ()
	{
		return model;
	}
	
	public void setModel (LanguageModel model)
		throws LanguageException
	{
    	this.model = model;
    	
        LanguageSpecificationFactory lsf = new LanguageSpecificationFactory();
        LanguageSpecification language = lsf.create(model);
        
        this.lexis = language.getLexicalSpecification();
	}
	
	public LexicalSpecification getLexicalSpecification ()
	{
		return lexis;
	}
	
    /**
     * Creates a tokenizer (convenience static method).
	 * 
	 * @param model Language model
	 * @param reader Tokenizer input reader
	 * @return Tokenizer
	 * @throws Exception
	 */
    public static Tokenizer create (LanguageModel model, Reader reader)
    	throws Exception
    {    	
    	TokenizerFactory factory = new TokenizerFactory(model);
    	
    	return factory.create(reader);
    }
    
    /**
     * Creates a tokenizer.
     * 
     * @param reader Tokenizer input reader
     * @return Tokenizer
     * @throws Exception
     */
    public Tokenizer create (Reader reader)
    	throws Exception
    {
        return (Tokenizer) tokenizerClass.getConstructor(LexicalSpecification.class, Reader.class).newInstance(lexis,reader);
    }
    
}
