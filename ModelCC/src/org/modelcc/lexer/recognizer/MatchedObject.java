/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.recognizer;

import java.io.Serializable;

/**
 * A recognized object, as returned by a pattern recognizer.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public final class MatchedObject implements Serializable {

    /**
     * Matched object.
     */
    private Object object;

    /**
     * Matched text.
     */
    private String text;

    /**
     * Default constructor.
     * @param object the matched object.
     * @param text the matched text.
     */
    public MatchedObject(Object object,String text) {
        this.object = object;
        this.text = text;
    }

    /**
     * @return the object
     */
    public Object getObject() {
        return object;
    }

    /**
     * @return the length
     */
    public String getText() {
        return text;
    }
    
}
