/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.recognizer;

import java.io.Serializable;

/**
 * Pattern recognizer abstract class.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public abstract class PatternRecognizer implements Serializable
{
    /**
     * Try to match the pattern in a certain position of a char sequence.
     * @param cs the char sequence in which to match the pattern.
     * @param start the position of the char sequence in which to match the pattern.
     * @return an object that contains the matched subsequence if there was a match, null otherwise.
     */
    public abstract MatchedObject read (CharSequence cs, int start);
    
    /**
     * Pattern recognition.
     * @param cs the input char sequence.
     * @return an object that contains the matched object if there was a match, null otherwise.
     */
    public final MatchedObject read (CharSequence cs)
    {
    	return read(cs,0);
    }
    
}