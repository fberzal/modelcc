/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer.recognizer.regexp;

import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.modelcc.lexer.recognizer.MatchedObject;
import org.modelcc.lexer.recognizer.PatternRecognizer;

/**
 * Regular Expression Based Pattern Recognizer.
 * 
 * WARNING! According to the JDK documentation, 
 * instances of java.util.regex.Pattern are immutable and are safe for use by multiple concurrent threads.
 * However, instances of java.util.regex.Matcher are not safe for use by multiple concurrent threads.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class RegExpPatternRecognizer extends PatternRecognizer
{
    /**
     * Cache of already-compiled patterns.
     */
    private static HashMap<String,Pattern> patterns = new HashMap<String,Pattern>();

    /**
     * Regular expression.
     */
    private String regExp;

    /**
     * Pattern associated to this recognizer.
     */
    private Pattern p;

    /**
     * Regular Expression constructor.
     * @param arg the regular expression that describes this recognizer.
     */
    public RegExpPatternRecognizer(String arg) 
    {
        this.regExp = arg;
        p = patterns.get(arg);
        if (p == null) {
            p = Pattern.compile(arg);
            patterns.put(arg, p);
        }
    }

    /**
     * Try to match the pattern in a certain position of a char sequence.
     * @param scs the char sequence in which to match the pattern.
     * @param start the position of the char sequence in which to match the pattern.
     * @return an object that contains the matched subsequence if there was a match, null otherwise.
     */
    public MatchedObject read (CharSequence cs, int start) 
    {
    	Matcher m = p.matcher(cs.subSequence(start, cs.length())); 
    	boolean result = false;
    	
    	try {
   			result = m.lookingAt();   		
    	} catch (Throwable error) {
    		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "Error while trying to apply regular expression '"+regExp+"'"
    				+" to char sequence (start="+start+", length="+cs.length()+")",error);
    		// throw error;
    	}

    	if (result) {
    		String s = m.group();
    		return new MatchedObject(s,s);
    	} else {
    		return null;
    	}
    }

    /**
     * Equals method
     * @param obj Object to compare with
     * @return true if equals, false if not.
     */
    @Override
    public boolean equals(Object obj) 
    {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RegExpPatternRecognizer other = (RegExpPatternRecognizer) obj;
        if (this.p != other.p && (this.p == null || !this.p.equals(other.p))) {
            return false;
        }
        return true;
    }

    /**
     * Unique hashCode
     * @return a hashCode
     */
    @Override
    public int hashCode() 
    {
        int hash = 5;
        hash = 59 * hash + (this.p != null ? this.p.hashCode() : 0);
        return hash;
    }

    /**
     * @return the regular expression
     */
    public String getRegExp() 
    {
        return regExp;
    }

    /**
     * @return the string
     */
    @Override
    public String toString() 
    {
        return "REGEXP("+regExp+")";
    }
}
