/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.lexer;

import java.io.Reader;

import org.modelcc.language.lexis.LexicalSpecification;

/**
 * ModelCC tokenizer abstract class.
 * 
 * @author Fernando Berzal (berzal@modelcc.org)
 */
public abstract class Tokenizer 
{
    /**
     * Lexical specification
     */
	protected LexicalSpecification lexis;
	
	/** 
	 * Input reader
	 */
	protected Reader reader; 
	
	/**
	 * Constructor
	 * @param lexis Language lexical specification
	 * @param reader Tokenizer input reader
	 */
	protected Tokenizer (LexicalSpecification lexis, Reader reader)
	{
		this.lexis = lexis;
		this.reader = reader;
	}
		
    /**
     * Scans input to obtain a stream of tokens.
     * @return the next token from this tokenizeer.
     */
    public abstract Token nextToken();
}
