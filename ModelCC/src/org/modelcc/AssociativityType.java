/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

/**
 * Associativity type.
 */
public enum AssociativityType
{
	/**
	 * Undefined associativity.
	 */
	UNDEFINED,

	/**
	 * Left to right associativity.
	 */
	LEFT_TO_RIGHT,

	/**
	 * Right to left associativity.
	 */
	RIGHT_TO_LEFT,

	/**
	 * Non associative.
	 */
	NON_ASSOCIATIVE         
}
