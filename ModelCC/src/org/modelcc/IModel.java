/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

/**
 * Marker interface for ModelCC model elements.
 */
public interface IModel 
{
	
}
