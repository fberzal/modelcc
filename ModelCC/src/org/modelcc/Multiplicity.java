/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Multiplicity constraints.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Multiplicity 
{
	public final static int UNDEFINED = -1;

    /**
     * @return the minimum multiplicity value.
     */
    public int minimum() default UNDEFINED;

    /**
     * @return the minimum multiplicity value.
     */
    public int maximum() default UNDEFINED;

}
