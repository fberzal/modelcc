/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.metamodel;

/**
 * Evaluator interface
 * 
 * @author Fernando Berzal (berzal@acm.org)
 *
 * @param <T> Object type
 */

public interface Evaluator<T>  
{
	public double evaluate (T object);
}
