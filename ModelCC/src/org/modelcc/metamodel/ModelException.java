/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.metamodel;

import org.modelcc.ModelCCException;

/**
 * Metamodel exception
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ModelException extends ModelCCException 
{
	public ModelException (String message)
	{
		super(message);
	}
	
	public ModelException (String message, Throwable cause)
	{
		super(message, cause);
	}
}
