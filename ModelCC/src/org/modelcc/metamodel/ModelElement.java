/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.metamodel;

import java.util.ArrayList;
import java.util.List;

/**
 * ModelCC metamodel (composite design pattern)
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ModelElement<M> implements java.io.Serializable
{
	/**
	 * Element ID
	 */
	private String id;
	
    /**
     * ModelElement class.
     */
    private Class elementClass;

    /**
     * ModelElement members.
     */
    private List<M> members;
 
    /**
     * Constructor
     * @param elementClass element class
     * @param id element ID
     */
    public ModelElement (Class elementClass, String id) 
    {
    	this.id = id;
        this.elementClass = elementClass;
    	this.members = new ArrayList<M>();    	
    }
 
    /**
     * Constructor
     * @param elementClass element class
     */    
    public ModelElement (Class elementClass) 
    {
    	this(elementClass, elementClass.getCanonicalName());
    }
    
    /**
     * @return the element class
     */
    public Class getElementClass() 
    {
        return elementClass;
    }

    /**
     * @param elementClass the element class
     */
    public void setElementClass(Class elementClass) 
    {
        this.elementClass = elementClass;
    }

    /**
     * @return the element members
     */
    public List<M> getMembers() 
    {
        return members;
    }

    /**
     * Get element member
     * @param i member index
     * @return The i-th element member
     */
    public M getMember (int i)
    {
    	return members.get(i);
    }
    
    /**
     * @param contents the element members
     */
    public void setMembers(List<M> contents) 
    {
        this.members = contents;
    }
    
    /**
     * @param member the member to be added to this element
     */
    public void addMember (M member)
    {
    	members.add(member);
    }

    /**
     * @return element identifier
     */
    public String getID() 
    {
        return id;
    }    
    
    // toString
    
    public String toString ()
    {
    	return elementClass.toString();
    }
}
