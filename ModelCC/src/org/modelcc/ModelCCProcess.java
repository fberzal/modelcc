/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */
package org.modelcc;

import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.modelcc.io.log.Message;
import org.modelcc.io.log.MessageLogger;

/**
 * ModelCC process
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public abstract class ModelCCProcess 
{
    /**
	 * Logger.
	 */
	private MessageLogger logger;

	/**
	 * Log message
	 * @param level Warning level
	 * @param string Message
	 * @param object Message objects
	 */
	public final void log(Level level, String string, Object[] object) 
	{
		if (logger==null)
			logger = new MessageLogger();
		
	    logger.log(level,string,object);
	    Logger.getLogger(this.getClass().getName()).log(level,string,object);
	}

	/**
	 * Log message
	 * @param level Warning level
	 * @param message Exception message
	 * @param exception Source exception
	 */
	public final void log(Level level, String message, Exception exception) 
	{
		if (logger==null)
			logger = new MessageLogger();
		
	    logger.log(level,message,exception);
	    Logger.getLogger(this.getClass().getName()).log(level,message,exception);
	}

	/**
	 * Retrieve messages
	 * @return the message list
	 */
	public final List<Message> getMessages() 
	{
		if (logger!=null)
			return Collections.unmodifiableList(logger.getMessages());
		else
			return Collections.EMPTY_LIST;
	}
	
	
	/**
	 * Clear logged messages
	 */
	public final void clearMessages()
	{
		if (logger!=null)
			logger.clear();
	}
}
