/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.syntax;

import java.io.Serializable;

/**
 * Syntactic specification.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
s */
public final class SyntaxSpecification implements Serializable {

    /**
     * Grammar.
     */
    private Grammar grammar;

    /**
     * Constraints.
     */
    private SyntaxConstraints constraints;

    /**
     * Constructor.
     */
    public SyntaxSpecification() 
    {
        this.grammar = null;
        this.constraints = null;
    }

    /**
     * Constructor.
     * @param grammar the language grammar.
     * @param constraints the language constraints.
     */
    public SyntaxSpecification(Grammar grammar, SyntaxConstraints constraints) 
    {
        this.grammar = grammar;
        this.constraints = constraints;
    }

    // Getters & setters

    public Grammar getGrammar() 
    {
        return grammar;
    }

    public void setGrammar(Grammar grammar) 
    {
        this.grammar = grammar;
    }

    public SyntaxConstraints getConstraints() 
    {
        return constraints;
    }

    public void setConstraints(SyntaxConstraints constraints) 
    {
        this.constraints = constraints;
    }
}
