/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.syntax;

import java.io.Serializable;

/**
 * Object collection.
 * 
 * @author Fernando Berzal (berzal@modelcc.org)
 */
public class ObjectCollection implements Serializable 
{    
    private Object[] content;
   
    private Symbol symbol;
    private RuleSymbol ruleSymbol;

    
    
    public ObjectCollection () 
    {
    	this(new Object[0],null,null);
    }    

    public ObjectCollection (Object[] content) 
    {
    	this(content,null,null);
    }

    public ObjectCollection (Symbol symbol, RuleSymbol ruleSymbol) 
    {
    	this(new Object[0], symbol, ruleSymbol);
    }
    
    public ObjectCollection (Object[] content, Symbol symbol, RuleSymbol ruleSymbol) 
    {
    	this.content = content;
    	this.symbol = symbol;
    	this.ruleSymbol = ruleSymbol;
    }

    
    public Object[] getContent() 
    {
    	return content;
    }
    
    public Symbol getSymbol() 
    {
    	return symbol;
    }
        
    public void setSymbol (Symbol symbol)
    {
    	this.symbol = symbol;
    }

    public RuleSymbol getRuleSymbol() 
    {
    	return ruleSymbol;
    }
    
    public void setRuleSymbol (RuleSymbol ruleSymbol)
    {
    	this.ruleSymbol = ruleSymbol;
    }
    
    
    public int size ()
    {
    	return content.length;
    }
    
    public Object get (int i)
    {
    	return content[i];
    }
    
    public void set (int i, Object obj)
    {
    	content[i] = obj;
    }
    
    public void resize (int newSize)
    {
    	Object old[] = content;
    	
    	content = new Object[newSize];
    	
    	for (int i=0; (i<old.length) && (i<content.length); i++)
    		content[i] = old[i];
    }
    
    public void add (Object obj)
    {
    	Object old[] = content;
    	
    	content = new Object[old.length+1];
    	
    	for (int i=0; i<old.length; i++)
    		content[i] = old[i];
    	
    	content[old.length] = obj;
    }
    
    public void add (ObjectCollection collection)
    {
    	int original = size();
    	
    	resize(original + collection.size());

    	for (int j=0; j<collection.size(); j++)
    		set(original+j, collection.get(j));    	
    }
}