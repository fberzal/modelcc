package org.modelcc.language.syntax;


import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;

import org.modelcc.language.Builder;
import org.modelcc.language.LanguageException;
import org.modelcc.language.factory.SymbolIdentifier;
import org.modelcc.language.metamodel.CompositeLanguageElement;
import org.modelcc.language.metamodel.LanguageElement;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.metamodel.SimpleLanguageElement;
import org.modelcc.lexer.recognizer.PatternRecognizer;

/**
 * Rule builder: Object instantiation from grammar rules.
 * 
 * @author Fernando Berzal (fberzal@modelcc.org)
 */
public class ObjectBuilder extends Builder 
{
    private ReferencePostBuilder referencePostBuilder;

	/**
	 * Constructor
	 * @param model Language model
	 */
	public ObjectBuilder (LanguageModel model)
	{
		super(model);
        referencePostBuilder = new ReferencePostBuilder(model);
	}
	
	/**
     * Build a symbol, fill it, and validate it.
     * @param t symbol to be built.
     * @param data parser metadata.
     * @return true if the symbol is valid, false if not.
     */
    public boolean build (Rule r, Symbol t, ParserMetadata data)
    {
    	SymbolBuilder builder = r.getBuilder();

    	if (builder!=null) {
    		return r.getBuilder().build(t, data);
    	} else {
    		
    		try {
    			instantiate(r,t,data);
    			return true;
    		} catch (LanguageException error) {
    			// error.printStackTrace(System.err);
    			return false;
    		}
    	}
    }
    
    public Object instantiate (Rule r, Symbol t, ParserMetadata data)
    	throws LanguageException
    {
        Object object = null;
    	
        switch (r.getType()) {

        case COMPOSITION:
        	object = instantiateComposition(t,data);
        	break;

        case SELECTION:
        	object = instantiateSelection(t,data);
        	break;
        	
        case REFERENCE:
        	object = instantiateReference(r,t,data);
        	break;
        	
        case COPY:
        	 object = t.getContent(0).getUserData();
        	 break;
        	
        case REPETITION:
        	object = instantiateCollection(r,t,data);
        	break;

        default:
            if (t.getType() instanceof SymbolIdentifier) {
            	LanguageElement element = languageElement(t);

            	if (element instanceof SimpleLanguageElement) {
                	object = instantiateSelection(t,data);
            		if ((object==null) && ((SimpleLanguageElement) element).matchesEmptyString()) {
            			// Empty symbol builder
            			object = instantiate((SimpleLanguageElement)element, "");
            		}
            	}
            }                
        }
        
        if (r.getType()!=Rule.Type.REFERENCE)
        	t.setUserData(object);

        return object;
    }
    
    
    private LanguageElement languageElement (Symbol t)
    {
    	SymbolIdentifier id = (SymbolIdentifier) t.getType();
    	
    	return id.getElement();
    }
    
    private int symbolIdentifier (Symbol t)
    {
    	int target = -1;
        		
    	for ( int i=0; i<t.size(); i++) {
    		if (t.getContent(i).getType().getClass().equals(SymbolIdentifier.class))
    			if (target==-1)
    				target = i;
    			else
    				target = -2;
    	}
    	
    	return target;
    }
    
    
    
    private Object instantiateSelection (Symbol t, ParserMetadata data)
        	throws LanguageException
    {
    	Object object = null;
    	int    target = symbolIdentifier(t);
    	
    	if (target >=0)
    		object = t.getContent(target).getUserData();
    	
    	return object;
    }

    
    private Object instantiateComposition (Symbol t, ParserMetadata data) 
        	throws LanguageException
    {
        CompositeLanguageElement ce = (CompositeLanguageElement) languageElement(t);
        Object obj = instantiate(ce, t);

        // Keys
        
        if ((obj!=null) && !ce.getKeyMembers().isEmpty()) {
        	Class cls = ce.getElementClass();
            Map<Class,Map<ObjectWrapper,Object>> ids = data.getKeys();
            Map<Object,ObjectWrapper> map = data.getMap();
            Map<ObjectWrapper,Object> idmap = ids.get(cls);
            if (idmap == null) {
                idmap = new HashMap<ObjectWrapper,Object>();
                ids.put(cls,idmap);
            }
            ObjectWrapper kw = ObjectWrapper.createKeyWrapper(obj, getModel(), map);
            if (idmap.containsKey(kw)) {
                log(Level.SEVERE, "Class \"{0}\": Duplicate ID.", new Object[]{cls.getCanonicalName()});
            } else {
                idmap.put(kw,obj);
            }
        }
        
        return obj;
    }    
    
    
	public Object instantiateReference (Rule r, Symbol t, ParserMetadata data)
		throws LanguageException
    {
        CompositeLanguageElement ce = (CompositeLanguageElement) languageElement(t);
        Object object = null;

        if (!ce.getKeyMembers().isEmpty())
        	object = instantiateReference(ce, t);

        if (object!=null) {
        	data.addReference(object, t);
        	r.setPostBuilder(referencePostBuilder);
        }
        
        return object;
    }    
	
	public Object instantiateCollection (Rule r, Symbol t, ParserMetadata data)
	{
        ObjectCollection list = new ObjectCollection();
        
        for (int i=0; i<t.size(); i++) {
        	Symbol s = t.getContent(i);
        	
        	if (s!=t) {

        		if ( (s.getUserData()!=null) && (s.getUserData() instanceof ObjectCollection) ) {

        			// Sublist
    
        			list.add ( (ObjectCollection) s.getUserData() );
       			
        		} else {
        			
        			// List element
        			
        			if (t.getElement(i) == r.getElement()) {
        				list.add ( s.getUserData() );
        			} else if (!PatternRecognizer.class.isAssignableFrom(t.getContent(i).getType().getClass())) {
        				list.setSymbol(t.getContent(i));
        				list.setRuleSymbol(t.getElement(i));
        			}
        		}
        	}
        }
        
        return list;
	}

}
