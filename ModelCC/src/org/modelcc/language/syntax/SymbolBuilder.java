/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.syntax;

import org.modelcc.language.Builder;
import org.modelcc.language.metamodel.LanguageModel;

/**
 * Symbol builder.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class SymbolBuilder extends Builder 
{
	/**
	 * Default constructor
	 */
	protected SymbolBuilder ()
	{
		super(null);
	}
	
	/**
	 * Constructor
	 * @param model Language model
	 */
	public SymbolBuilder (LanguageModel model)
	{
		super(model);
	}
	
    /**
     * Build a symbol, fill it, and validate it.
     * @param t symbol to be built.
     * @param data parser metadata.
     * @return true if the symbol is valid, false if not.
     */
    public boolean build (Symbol t, ParserMetadata data)
    {
    	return true;
    }
}
