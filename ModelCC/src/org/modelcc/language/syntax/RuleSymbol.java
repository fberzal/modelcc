/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.syntax;

import java.io.Serializable;

import org.modelcc.language.metamodel.LanguageMember;

/**
 * Rule symbol.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class RuleSymbol implements Serializable 
{
    /**
     * Rule symbol type.
     */
    private Object type;
    /**
     * Language member.
     */
    private LanguageMember member;

    
    /**
     * Constructor.
     * @param type the type of the rule symbol.
     */
    public RuleSymbol (Object type) 
    {
        this.type = type;
    }

    /**
	 * Constructor.
	 * @param type the type of the rule symbol.
	 * @param member the language member.
	 */
	public RuleSymbol (Object type, LanguageMember member) 
	{
	    this.type = type;
	    this.member = member;
	}

	/**
     * Get rule symbol type
     * @return the type of the rule symbol
     */
    public Object getType() 
    {
        return type;
    }


    /**
     * Get symbol position
     * @return the symbol position
     */
    public LanguageMember getLanguageMember() 
    {
        return member;
    }

    
    
    // Object methods
    
	@Override
	public int hashCode() 
	{
		return ((type == null) ? 0 : type.hashCode());
	}

	@Override
	public boolean equals (Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RuleSymbol other = (RuleSymbol) obj;
		if ((type==null) && (other.type!=null))
			return false;
		else if (!type.equals(other.type))
			return false;
		return true;
	}
}
