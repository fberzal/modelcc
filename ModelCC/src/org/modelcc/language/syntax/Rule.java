/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.syntax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Context-free grammar rule.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public final class Rule implements Serializable 
{
	/**
	 * Rule type.
	 */
	public enum Type { UNDEFINED, COMPOSITION, SELECTION, REPETITION, COPY, REFERENCE };
	
    /**
     * User data.
     */
    private Object userData;

    /**
     * Left hand side element.
     */
    private RuleSymbol left;

    /**
     * Right hand side elements.
     */
    private List<RuleSymbol> right;

    
    /**
     * Rule type
     */
    private Type type;
    
    
    /**
     * Collection element
     */
    private RuleSymbol element;
    
    /**
     * Builder.
     */
    private SymbolBuilder builder;

    /**
     * Post builder.
     */
    private SymbolBuilder postBuilder;

    private static final SymbolBuilder DEFAULT_SYMBOL_BUILDER = new SymbolBuilder(); 
    
    /**
     * Constructor.
     */
    public Rule (RuleSymbol left) 
    {
    	this.left = left;
    	this.right = new ArrayList<RuleSymbol>();
    	this.userData = null;
    	this.type = Type.UNDEFINED;
        this.postBuilder = DEFAULT_SYMBOL_BUILDER;    	
    }
    
    /**
     * Copy constructor
     * @param r Original rule
     */
    public Rule (Rule r)
    {
    	this.left = r.left;
    	this.right = new ArrayList<RuleSymbol>(r.right);
    	this.builder = r.builder;
    	this.postBuilder = r.postBuilder;
    	this.userData = r.userData;
    	this.type = r.type;
    }

    // Getters & setters
    
    public RuleSymbol getLeft() 
    {
        return left;
    }

    public void setLeft(RuleSymbol left) 
    {
        this.left = left;
    }

    public List<RuleSymbol> getRight() 
    {
        return right;
    }

    public void add (RuleSymbol element)
    {
    	right.add(element);
    }
    
    public Type getType ()
    {
    	return type;
    }
    
    public void setType (Type type)
    {
    	this.type = type;
    }
    
    public RuleSymbol getElement ()
    {
    	return element;
    }
    
    public void setElement (RuleSymbol element)
    {
    	this.element = element;
    }
    
    protected SymbolBuilder getBuilder() 
    {
        return builder;
    }

    public void setBuilder(SymbolBuilder builder) 
    {
        this.builder = builder;
    }

    public SymbolBuilder getPostBuilder() 
    {
        return postBuilder;
    }

    public void setPostBuilder(SymbolBuilder postBuilder) 
    {
        this.postBuilder = postBuilder;
        if (this.postBuilder == null)
            this.postBuilder = DEFAULT_SYMBOL_BUILDER;
    }
    
    public Object getUserData() 
    {
        return userData;
    }

    public void setUserData(Object userData) 
    {
        this.userData = userData;
    }
    
    // toString

    @Override
    public String toString() {
        String r = "";
        
        if (left!=null)
        	r += left.getType();
        else
        	r += "<null>";
        
        r += " ::= ";
        
        if (right!=null) {

        	for (int i=0; i<right.size();i++) {        		
        	    if (right.get(i)!=null)        	
        	    	r += right.get(i).getType()+" ";
        	    else
        	    	r+= "<null>";
        	}
        	    
        } else {
        	r += "<null>";
        }
        
        return r + " builder="+builder;
    }
}
