/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language;

import org.modelcc.ModelCCException;

/**
 * ModelCC language model exception.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class LanguageException extends ModelCCException 
{
	public LanguageException (String message)
	{
		super(message);
	}
	
	public LanguageException (String message, Throwable cause)
	{
		super(message, cause);
	}
}
