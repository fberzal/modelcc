/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language;


/**
 * Null rule element exception.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class NullElementException extends LanguageException
{
    /**
     * Default constructor.
     */
    public NullElementException (String message) 
    {
        super(message);
    }
}
