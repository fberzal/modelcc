/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language;

import java.io.Serializable;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.syntax.SyntaxSpecification;

/**
 * Language specification.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class LanguageSpecification implements Serializable {

    /**
     * Lexical specification.
     */
    private LexicalSpecification lexicalSpecification;

    /**
     * Syntactic specification.
     */
    private SyntaxSpecification syntacticSpecification;

    /**
     * Constructor
     * @param lexicalSpecification the lexical specification.
     * @param syntacticSpecification the syntactic specification.
     */
    public LanguageSpecification (LexicalSpecification lexicalSpecification,SyntaxSpecification syntacticSpecification) 
    {
        this.lexicalSpecification = lexicalSpecification;
        this.syntacticSpecification = syntacticSpecification;
    }

    /**
     * @return the lexicalSpecification
     */
    public LexicalSpecification getLexicalSpecification() {
        return lexicalSpecification;
    }

    /**
     * @return the syntacticSpecification
     */
    public SyntaxSpecification getSyntacticSpecification() {
        return syntacticSpecification;
    }

}
