package org.modelcc.language;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import org.modelcc.ModelCCProcess;
import org.modelcc.io.java.Reflection;
import org.modelcc.language.metamodel.CompositeLanguageElement;
import org.modelcc.language.metamodel.LanguageElement;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.metamodel.MemberCollection;
import org.modelcc.language.metamodel.SimpleLanguageElement;
import org.modelcc.language.syntax.ObjectCollection;
import org.modelcc.language.syntax.RuleSymbol;
import org.modelcc.language.syntax.Symbol;

public abstract class Builder extends ModelCCProcess
{
	protected LanguageModel model;

	/**
	 * Constructor
	 * 
	 * @param model Language model
	 */
	public Builder (LanguageModel model)
	{
		this.model = model;
	}
	
	/**
	 * Get language model
	 * 
	 * @return The language model
	 */
	public final LanguageModel getModel() 
	{
		return model;
	}
	
	// Temporary methods
	
	protected Object instantiate (SimpleLanguageElement element, Object value) 
		throws LanguageException
	{
		Class c = element.getElementClass();
        Object obj = null;
        
        try {
            obj = c.newInstance();

            if (element.getValueField() != null) {
            	Field fld = Reflection.findField(c,element.getValueField());
                if (fld != null) {
                    fld.setAccessible(true);
                    fld.set(obj, cast(fld.getType(), value));
                }
            }
        } catch (Exception error) {
            log(Level.SEVERE, "Token instantiation exception", error);
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unable to create token instance", error);
        }            
        
        runSetupMethods(obj,element);

        if (!checkConstraints(obj,element))
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unsatisfied constraints");

        return obj;
	}	
	
	

	protected Object instantiate(CompositeLanguageElement ce, Symbol t) 
    	throws LanguageException	
	{
		Object obj = null;
        Class  c = ce.getElementClass();

        try {
            obj = c.newInstance();
        } catch (Exception error) {
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unable to create instance", error);
        }
            
        Set<Field> filled = fill(c, t, obj);

        fillOptionalFields(obj,getModel(),filled);
    	runSetupMethods(obj,ce);

        if (!checkConstraints(obj,ce))
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unsatisfied constraints");

        
        return obj;
	}
	
	
	protected Object instantiateReference (CompositeLanguageElement ce, Symbol t) 
    	throws LanguageException	
	{
		Object obj = null;
        Class c = ce.getElementClass();

        try {
            obj = c.newInstance();
        } catch (Exception error) {
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unable to create instance");
        }
        
        fill(c, t, obj);
            
		return obj;
	}	

	
	private Set<Field> fill(Class c, Symbol t, Object obj)
    	throws LanguageException	
	{
		Set<RuleSymbol> proc = new HashSet<RuleSymbol>();
		Set<Field> filled = new HashSet<Field>();
		
        try {		
        	
        	for (int i=0; i<t.size(); i++) {
        		Symbol s = t.getContent(i);
        		RuleSymbol re = t.getElement(i);
        		proc.add(re);

        		if (re.getLanguageMember()!=null) {
        			LanguageMember ct = re.getLanguageMember();
        			Field fld = Reflection.findField(c,ct.getID());

        			if (fld != null) {

        				if (!ct.getClass().equals(MemberCollection.class)) {

        					Object content = s.getUserData();

            				fld.setAccessible(true);
        					fld.set(obj,content);
        					if (content!=null) {
        						filled.add(fld);
        					}

        				} else {

        					filled.add(fld);
        					MemberCollection mc = (MemberCollection)ct;
        					ObjectCollection listContents = (ObjectCollection) s.getUserData();
        					if (listContents==null)
        						throw new LanguageException("[ModelCC] Object instantiation exception: Error while processing "+obj);
        					
        					Symbol extra = listContents.getSymbol();
        					RuleSymbol extraRe = listContents.getRuleSymbol();
        					if (extraRe != null) {
        						LanguageMember extraCt = extraRe.getLanguageMember();
        						Field extraFld = Reflection.findField(c,extraCt.getID());
        						filled.add(extraFld);
        						if (extraFld != null) {
        							extraFld.setAccessible(true);
        							extraFld.set(obj,extra.getUserData());
        						}
        					}

        					Object[] listData = listContents.getContent();
 
           					if ( listData.length<mc.getMinimumMultiplicity())
           						throw new LanguageException("[ModelCC] Object instantiation exception: Unsatisfied minimum multiplicity constraint.");
        					if ( mc.getMaximumMultiplicity()!=-1
        							&& listData.length>mc.getMaximumMultiplicity())
           						throw new LanguageException("[ModelCC] Object instantiation exception: Unsatisfied maximum multiplicity constraint.");

                			fillCollectionField(obj, fld, mc, listData);
        				}
        			}
        		}
        	}

        } catch (Exception error) {
        	throw new LanguageException("[ModelCC] Object instantiation exception", error);
        }

        return filled;
	}

	
	private void fillCollectionField (Object obj, Field fld, MemberCollection mc, Object[] listData)
			throws LanguageException 
	{
		Object list;
		Method add;
		
		try {

			fld.setAccessible(true);
			
			switch (mc.getCollection()) {
			
			case LIST:
				if (fld.getType().isInterface())
					list = ArrayList.class.newInstance();
				else
					list = fld.getType().newInstance();
				add = list.getClass().getDeclaredMethod("add",Object.class);
				for (int j=0; j<listData.length; j++)
					add.invoke(list,listData[j]);
				fld.set(obj,list);
				break;
				
			case ARRAY:
				list = Array.newInstance(mc.getElementClass(),listData.length);
				for (int j=0; j<listData.length; j++)
					Array.set(list, j, listData[j]);
				fld.set(obj,list);
				break;
				
			case SET:
				if (fld.getType().isInterface())
					list = HashSet.class.newInstance();
				else
					list = fld.getType().newInstance();
				add = list.getClass().getDeclaredMethod("add",Object.class);
				for (int j=0; j<listData.length; j++)
					add.invoke(list,listData[j]);
				fld.set(obj,list);
				break;
			}
			
		} catch (Exception error) {
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unable to instantiate collection", error);
		}
	}
	

    private void fillOptionalFields (Object o, LanguageModel m, Set<Field> filled) 
        	throws LanguageException	
    {
    	try {		

    		if (m.getClassToElement().get(o.getClass()).getClass().equals(SimpleLanguageElement.class)) {

    			// SimpleLanguageElement
    			Class c = o.getClass();
    			SimpleLanguageElement be = (SimpleLanguageElement) m.getClassToElement().get(c);
    			if (be.getValueField() != null) {
    				Field fld = Reflection.findField(c,be.getValueField());
    				if (fld != null) {
    					fld.setAccessible(true);
    					if (fld.getType().equals(String.class))
    						fld.set(o, cast(fld.getType(), ""));
    				}
    			}

    		} else { 

    			// CompositeLanguageElement
    			CompositeLanguageElement ce = (CompositeLanguageElement)m.getClassToElement().get(o.getClass());
    			Field[] fields = o.getClass().getDeclaredFields();

    			for (int i=0; i<fields.length; i++) {
    				LanguageMember ct = ce.getFieldToContent().get(fields[i].getName());
    				if ( (ct!=null) && !ct.isOptional() ) {
    					Field fld = Reflection.findField(o.getClass(),ct.getID());
    					if (filled == null || !filled.contains(fields[i])) {
    						if (!ct.getClass().equals(MemberCollection.class)) {
    							Class c = fields[i].getType();
    							Object o2;
    							while (Modifier.isAbstract(c.getModifiers()) && m.getDefaultElement().get(m.getClassToElement().get(c)) != null) {
    								c = m.getDefaultElement().get(m.getClassToElement().get(c)).iterator().next().getElementClass();
    							}
    							if (!Modifier.isAbstract(c.getModifiers())) {
    								o2 = c.newInstance();
    								LanguageElement ee = (LanguageElement)m.getClassToElement().get(o2.getClass());
    								runSetupMethods(o2,ee);
    								if (checkConstraints(o2,ee)) {
    									fillOptionalFields(o2,m,null);
    									fields[i].setAccessible(true);
    									fields[i].set(o,o2);
    								}
    							}
    						} else {
    							MemberCollection mc = (MemberCollection)ct;

    							if (mc.getMinimumMultiplicity() == 0) {
        							fillCollectionField(o, fld, mc, new Object[0] );
    							} else {
    								throw new LanguageException("[ModelCC] Object instantiation exception: Unsatisfied minimum multiplicity constraint.");
    							}
    						}
    					}
    				}
    			}
    		}

    	} catch (Exception error) {
    		throw new LanguageException("[ModelCC] Object instantiation exception", error);
    	}
    }
	
    
	/**
	 * Converts and casts an object to a class type
	 * @param type the class type
	 * @param obj the object
	 * @return an instance of the class type.
	 */
    private Object cast (Class type,Object obj) 
    {
        if (obj.getClass().equals(String.class)) {
            String data = (String)obj;
            if (type.equals(double.class) || type.equals(Double.class)) {
                if (data.charAt(0) == '+')
                    return Double.parseDouble(data.subSequence(1,data.length()).toString());
                else
                    return Double.parseDouble(data.toString());
            } else if(type.equals(float.class) || type.equals(Float.class)) {
                if (data.charAt(0) == '+')
                    return Float.parseFloat(data.subSequence(1,data.length()).toString());
                else
                    return Float.parseFloat(data.toString());
            } else if (type.equals(long.class) || type.equals(Long.class)) {
                if (data.charAt(0) == '+')
                    return Long.parseLong(data.subSequence(1,data.length()).toString());
                else
                    return Long.parseLong(data.toString());
            } else if (type.equals(int.class) || type.equals(Integer.class)) {
                if (data.charAt(0) == '+')
                    return Integer.parseInt(data.subSequence(1,data.length()).toString());
                else
                    return Integer.parseInt(data.toString());
            } else if (type.equals(byte.class) || type.equals(Byte.class)) {
                if (data.charAt(0) == '+')
                    return Byte.parseByte(data.subSequence(1,data.length()).toString());
                else
                    return Byte.parseByte(data.toString());
            } else if (type.equals(short.class) || type.equals(Short.class)) {
                if (data.charAt(0) == '+')
                    return Short.parseShort(data.subSequence(1,data.length()).toString());
                else
                    return Short.parseShort(data.toString());
            } else if (type.equals(boolean.class) || type.equals(Boolean.class)) { 
            	return (data.equals("true")?true:false);
            } else if (type.equals(String.class)) {
            	return data;
            } else if (type.equals(Character.class) || type.equals(char.class)) { 
            	return data.charAt(0);
            }
        }
        return obj;
    }    

	// Ancillary methods
	
	/**
	 * Run setup methods
	 * 
	 * @param o Object
	 * @param el Language element
	 */
	protected final void runSetupMethods(Object o, LanguageElement el)
		throws LanguageException
	{    	
    	if (getModel().getSuperelements().get(el) != null) {
    		runSetupMethods(o,getModel().getSuperelements().get(el));
    	}
     
    	try {

        	if (el.getSetupMethod() != null) {
        		Method mtd = el.getElementClass().getDeclaredMethod(el.getSetupMethod(),new Class[]{});
        		if (mtd != null) {
        			try {
            			mtd.setAccessible(true);
        				mtd.invoke(o);
        			} catch (Exception e) {
        				log(Level.SEVERE, "Exception when invoking method \"{0}\" of class class \"{1}\".", new Object[]{mtd.getName(),el.getElementClass().getCanonicalName()});
        				throw e;
        			}
        		}
        	}
        } catch (Exception error) {
        	throw new LanguageException("[ModelCC] Object instantiation exception: Unable to invoke setup methods", error);
        }
	}

	/**
	 * Check object constraints
	 * 
	 * @param o Object 
	 * @param el Language element
	 * @return true if constraints are satisfied
	 */
	protected final boolean checkConstraints(Object o, LanguageElement el)
		throws LanguageException
	{
		boolean valid = true;

		if (getModel().getSuperelements().get(el) != null) {
			valid &= checkConstraints(o,getModel().getSuperelements().get(el));
		}
		
		try {

			for (String constraintMethod: el.getConstraintMethods()) {
				Method mtd = el.getElementClass().getDeclaredMethod(constraintMethod,new Class[]{});
				if (mtd != null) {
					try {
						mtd.setAccessible(true);
						valid &= (Boolean) mtd.invoke(o);
					} catch (Exception e) {
						log(Level.SEVERE, "Exception when invoking method \"{0}\" of class class \"{1}\".", new Object[]{mtd.getName(),el.getElementClass().getCanonicalName()});
						throw e;
					}
				}
			}
		} catch (Exception error) {
			throw new LanguageException("[ModelCC] Object instantiation exception: Unable to invoke constraint methods", error);
		}		

		return valid;
	}
}