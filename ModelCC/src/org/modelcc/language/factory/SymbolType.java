/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.factory;

import java.io.Serializable;

/**
 * Symbol type.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public enum SymbolType implements Serializable {

    /**
     * Element type.
     */
    ELEMENT,

    /**
     * Pattern element.
     */
    BASIC,

    /**
     * List element.
     */
    LIST,

    /**
     * List, before last element.
     */
    LIST_BEFORE_LAST,

    /**
     * List, within elements.
     */
    LIST_WITHIN,

    /**
     * Nullable list.
     */
    LIST_ZERO,

    /**
     * List sep
     */
    LIST_SEP,
    
    /**
     * List element
     */
    LIST_ELEMENT
}
