/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.factory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelcc.language.metamodel.LanguageMember;

/**
 * Member specification
 * 
 * @author Luis Quesada (lquesada@modelcc.org), refactored by Fernando Berzal (berzal@modelcc.org)
 */
public class MemberSpecification implements Serializable 
{
    private List<LanguageMember> members;
    private Map<LanguageMember,MemberContent> contents;

    public MemberSpecification()
    {
    	members = new ArrayList<LanguageMember>();
    	contents = new HashMap<LanguageMember,MemberContent>();
    }
    
    public MemberSpecification(LanguageMember em) 
    {
    	this();
    	members.add(em);
    }

    public MemberSpecification(MemberSpecification mn) 
    {
    	this();
    	add(mn);
    }
    
	public List<LanguageMember> getMembers() 
	{
		return members;
	}

	public void clear()
	{
    	members = new ArrayList<LanguageMember>();
	}
	
	public void add(LanguageMember member)
	{
		members.add(member);
	}
	
	public void addAll(List<LanguageMember> newMembers)
	{
		members.addAll(newMembers);
	}

	public void add(MemberSpecification member)
	{
		members.addAll(member.members);
		contents.putAll(member.contents);
	}
	
	public MemberContent getContent (LanguageMember member)
	{
		return contents.get(member);
	}
	
	public void setContent (LanguageMember member, MemberContent content)
	{
		contents.put(member, content);
	}

	public LanguageMember getFront() {
		if (members.size()==0)
			return null;
		return members.get(0);
	}

	public LanguageMember getBack() {
		if (members.size()==0)
			return null;
		return members.get(members.size()-1);
	}

}
