/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.factory;

import java.io.Serializable;

import org.modelcc.SeparatorPolicy;
import org.modelcc.language.metamodel.LanguageMember;

/**
 * Member content.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class MemberContent implements Serializable 
{
    private int position;
    private SeparatorPolicy separatorPolicy;
    private LanguageMember content;
    
    public MemberContent(int position,SeparatorPolicy separatorPolicy,LanguageMember content) 
    {
    	this.position = position;
    	this.separatorPolicy = separatorPolicy;
    	this.content = content;
    }

	public int getPosition() {
		return position;
	}

	public SeparatorPolicy getSeparatorPolicy() {
		return separatorPolicy;
	}

	public LanguageMember getContent() {
		return content;
	}
}