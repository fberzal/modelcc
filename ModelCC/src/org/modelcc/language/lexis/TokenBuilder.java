/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.lexis;


import org.modelcc.language.Builder;
import org.modelcc.language.LanguageException;
import org.modelcc.language.factory.SymbolIdentifier;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.metamodel.SimpleLanguageElement;
import org.modelcc.lexer.Token;

/**
 * Token builder.
 *
 * @author Fernando Berzal (berzal@modelcc.org)
 */
public class TokenBuilder extends Builder 
{	
	/**
	 * Constructor
	 * @param model Language model
	 */
	public TokenBuilder (LanguageModel model)
	{
		super(model);
	}
	
	
    /**
     * Instantiates an object given a token.
     * @param t token to be built.
     * @return Instantiated object, null if the object could not be instantiated or the object constraints were not met.
     */
	public Object build (Token t) 
	{
		Object value = t.getValue();
        SymbolIdentifier eid = (SymbolIdentifier)t.getType();
        SimpleLanguageElement element = (SimpleLanguageElement) eid.getElement();

        try {
        	return instantiate(element, value);
        } catch (LanguageException error) {
        	return null;
        }
    }


}
