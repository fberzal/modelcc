/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.lexis;

import java.io.Serializable;

import org.modelcc.lexer.Token;
import org.modelcc.lexer.recognizer.PatternRecognizer;

/**
 * Token specification.
 *
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class TokenSpecification implements Serializable 
{
    /**
     * Pattern recognizer.
     */
    private PatternRecognizer recognizer;

    /**
     * Type of this matchable item.
     * Can be used to store anything that identifies this token.
     */
    private Object type;

    /**
     * Whether the recognized tokens have to be considered or skipped.
     */
    private boolean skip;

    /**
     * Token builder.
     */
    private TokenBuilder builder;

    /**
     * Default token builder.
     */
    private static TokenBuilder defaultBuilder = new TokenBuilder(null) 
    {
        @Override
		public Object build(Token t) 
        {
            return "";
        }
    };

    
    /**
     * Constructor.
     * @param type the token type.
     * @param recognizer the pattern recognizer.
     * @param builder the token builder.
     */
    public TokenSpecification (Object type, PatternRecognizer recognizer, TokenBuilder builder) 
    {
        this.type = type;
        this.recognizer = recognizer;
        this.builder = builder;
        this.skip = false;

        if (this.builder == null)
            this.builder = defaultBuilder;
    }
    
    /**
     * Constructor.
     * @param type the token type.
     * @param recognizer the pattern recognizer.
     */
    public TokenSpecification (Object type, PatternRecognizer recognizer) 
    {
        this(type, recognizer, null);
    }

    
    // Getters & Setters

    /**
     * Get token type.
     * @return The token type object.
     */
    public Object getType() 
    {
        return type;
    }
    
    /**
     * Get pattern recognizer.
     * @return The pattern recognizer associated to this token type.
     */

    public PatternRecognizer getRecognizer() 
    {
        return recognizer;
    }

    /**
     * Should tokens of this type be skipped?
     * @return whether tokens of this type have to be skipped (true) or considered (false).
     */
    public boolean isSkipped() 
    {
        return skip;
    }

    /**
     * @param option whether tokens of this type should be skipped (true) or considered (false).
     */
    public void setSkip (boolean skip) 
    {
        this.skip = skip;
    }

    /**
     * @return the token builder
     */
    public TokenBuilder getBuilder() 
    {
        return builder;
    }

    /**
     * @param builder the token builder
     */
    public void setBuilder(TokenBuilder builder) 
    {
        this.builder = builder;
        if (this.builder == null)
            this.builder = defaultBuilder;
    }
}
