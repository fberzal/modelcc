/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.lexis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Lexical specification.
 *
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class LexicalSpecification implements Serializable 
{
    /**
     * Sorted list of token specifications.
     */
    private List<TokenSpecification> tspecs;

    /**
     * Map of token precedences.
     */
    private Map<TokenSpecification,Set<TokenSpecification>> precedences;

    /**
     * Constructor.
     */
    public LexicalSpecification () 
    {
        this.tspecs = new ArrayList<TokenSpecification>();;
        this.precedences = new HashMap<TokenSpecification,Set<TokenSpecification>>();
    }
    
    /**
     * Get token specifications.
     * @return the list of token specifications.
     */
    public List<TokenSpecification> getTokenSpecifications() 
    {
        return Collections.unmodifiableList(tspecs);
    }
    
    /**
     * Set token specifications.
     * @param tokens a list of token specifications.
     */
    public void setTokenSpecifications (List<TokenSpecification> tokens)
    {
    	this.tspecs = tokens;
    }
    
  
    /**
     * Add token specification
     * @param ts Token specification
     */
    public void addTokenSpecification (TokenSpecification ts) 
    {
    	tspecs.add(ts);
    }

    /**
     * Skip token specification
     * @param ts Token specification
     */
    public void skipTokenSpecification (TokenSpecification ts) 
    {
    	ts.setSkip(true);
    	tspecs.add(ts);
    }
    
    /**
     * Remove token specification
     * @param ts Token specification
     */
    public void removeTokenSpecification (TokenSpecification ts)
    {
    	tspecs.remove(ts);
    }

    /**
     * Get token precedences
     * @return the map of token precedences.
     */
    public Map<TokenSpecification, Set<TokenSpecification>> getPrecedences() 
    {
        return Collections.unmodifiableMap(precedences);
    }

    /**
     * Get token precedences
     * @param ts Token
     * @return Set of tokens preceded by the given token
     */
    public Set<TokenSpecification> getPrecedences (TokenSpecification ts) 
    {
        return precedences.get(ts);
    }
    
    /**
     * Add precedence relationship
     * @param ts Token
     * @param set Set of tokens preceded by the given token
     */
    public void addPrecedence (TokenSpecification ts, Set<TokenSpecification> set) 
    {
    	precedences.put(ts, set);
    }

    /**
     * Add precedence relationship between tokens.
     * @param ts1 the token that precedes.
     * @param ts2 the token that is preceded.
     */
    public void addPrecedence (TokenSpecification ts1,TokenSpecification ts2) 
    {
        Set<TokenSpecification> set = precedences.get(ts1);
        if (set == null) {
            set = new HashSet<TokenSpecification>();
            precedences.put(ts1,set);
        }
        set.add(ts2);
    }

    /**
     * Removes precedence relationship between tokens.
     * @param ts1 the token that precedes.
     * @param ts2 the token that is preceded.
     */
    public void removePrecedence(TokenSpecification ts1,TokenSpecification ts2) 
    {
        Set<TokenSpecification> set = precedences.get(ts1);
        if (set == null) {
            return;
        }
        set.remove(ts2);
        if (set.isEmpty())
            precedences.remove(ts1);
    }
    
    /**
     * Token precedence relationships
     * @param precedences Precedence relationship
     * @param ts1 First token type
     * @param ts2 Second token type
     * @return true if ts1 precedes ts2
     */
	public boolean precedes (TokenSpecification ts1, TokenSpecification ts2)
	{
		Set<TokenSpecification> precedes = precedences.get(ts1);
		
		if (precedes!=null)
			return precedes.contains(ts2);
		else
			return false;		
	}    
}
