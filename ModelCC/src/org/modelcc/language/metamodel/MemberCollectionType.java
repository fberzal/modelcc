/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.io.Serializable;

/**
 * Java collection type.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public enum MemberCollectionType implements Serializable 
{
    /**
     * List collection.
     */
  LIST,

    /**
    * Set collection.
    */
  SET,

    /**
     * Array collection.
     */
  ARRAY

}
