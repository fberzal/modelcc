/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.modelcc.AssociativityType;
import org.modelcc.CompositionType;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.metamodel.Evaluator;

/**
 * Composite language element.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class CompositeLanguageElement extends LanguageElement 
{
    /**
     * Key members.
     */
    private List<LanguageMember> keyMembers;

    /**
     * Whether the members can be placed in any order.
     */
    private boolean freeOrder;

    /**
     * Composition type.
     */
    private CompositionType composition;

    /**
     * Field to ElementMember.
     */
    private Map<String,LanguageMember> fieldToContent;

    /**
     * Constructor.
     * @param elementClass the element class
     * @param associativity the associativity
     * @param prefix the prefix
     * @param suffix the suffix
     * @param separator the default separator
     * @param setupMethod the setup method
     * @param constraintMethods the constraint checking methods
     * @param members the element contents
     * @param keyMembers the element ids
     * @param freeOrder whether the element members can be placed in free order
     * @param composition the composition type
     * @param evaluator the evaluator
     */
    public CompositeLanguageElement(
    		Class elementClass,
    		AssociativityType associativity,
    		List<PatternRecognizer> prefix,
    		List<PatternRecognizer> suffix,
    		List<PatternRecognizer> separator,
    		String setupMethod,
    		List<String> constraintMethods,
    		List<LanguageMember> members,
    		List<LanguageMember> keyMembers,
    		boolean freeOrder,
    		CompositionType composition,
    		Evaluator evaluator ) 
    {
        super(elementClass,associativity,prefix,suffix,separator,setupMethod,constraintMethods,evaluator);
        
        setMembers(members);
        
        this.keyMembers = keyMembers;
        this.freeOrder = freeOrder;
        this.composition = composition;

        this.fieldToContent = new HashMap<String,LanguageMember>();
        
        for (LanguageMember ct: members)
            fieldToContent.put(ct.getID(),ct);
    }

    // Getters
    
    public List<LanguageMember> getKeyMembers() {
        return Collections.unmodifiableList(keyMembers);
    }

    public Map<String, LanguageMember> getFieldToContent() {
        return Collections.unmodifiableMap(fieldToContent);
    }

    // Order
    
    public boolean isFreeOrder() {
        return freeOrder;
    }

    public void setFreeOrder(boolean freeOrder) {
    	this.freeOrder = freeOrder;
    }

    // Composition
    
    public CompositionType getComposition() {
        return composition;
    }

	public void setComposition(CompositionType composition) {
		this.composition = composition;
	}	
}
