/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.io.Serializable;
import java.util.List;

import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.metamodel.Evaluator;
import org.modelcc.metamodel.ModelElement;

/**
 * Language model member.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public class LanguageMember extends ModelElement implements Serializable 
{
    /**
     * Whether this member is optional.
     */
    private boolean optional;

	/**
     * Whether this member is part of the element key
     */
    private boolean key;

    /**
     * Whether this member is a reference.
     */
    private boolean reference;

    /**
     * Prefix.
     */
    private List<PatternRecognizer> prefix;

    /**
     * Suffix.
     */
    private List<PatternRecognizer> suffix;

    /**
     * Separator.
     */
    private List<PatternRecognizer> separator;

    /** 
     * Evaluator.
     */
    private Evaluator evaluator;

    /**
     * Constructor
     * @param field the field
     * @param elementClass the element class.
     */
    public LanguageMember(String field,Class elementClass)
    {
    	super(elementClass,field);
    }

    /**
     * Constructor
     * @param field the field
     * @param elementClass the element class.
     * @param optional whether content is optional or not
     * @param key is key
     * @param reference is reference
     * @param prefix the prefix
     * @param suffix the suffix
     * @param separator the separator
     * @param positionTag the position tag
     * @param evaluator the evaluator
     */
    public LanguageMember(
    		String field,
    		Class elementClass,
    		boolean optional,
    		boolean key,
    		boolean reference,
    		List<PatternRecognizer> prefix,
    		List<PatternRecognizer> suffix,
    		List<PatternRecognizer> separator,
    		Evaluator evaluator) 
    {
    	super(elementClass,field);

    	this.optional = optional;
        this.key = key;
        this.reference = reference;
        this.prefix = prefix;
        this.suffix = suffix;
        this.separator = separator;
        this.evaluator = evaluator; 
    }


    // Getters
    
    public boolean isOptional() {
        return optional;
    }

    public boolean isKey() {
        return key;
    }

    public boolean isReference() {
        return reference;
    }

    public List<PatternRecognizer> getPrefix() {
        return prefix;
    }

    public List<PatternRecognizer> getSuffix() {
        return suffix;
    }

    public List<PatternRecognizer> getSeparator() {
        return separator;
    }
    
    public Evaluator getEvaluator() {
    	return evaluator;
    }

    // Setters
    
    public void setOptional(boolean optional) {
		this.optional = optional;
	}

	public void setKey(boolean key) {
		this.key = key;
	}

	public void setReference(boolean reference) {
		this.reference = reference;
	}

	public void setPrefix(List<PatternRecognizer> prefix) {
		this.prefix = prefix;
	}

	public void setSuffix(List<PatternRecognizer> suffix) {
		this.suffix = suffix;
	}

	public void setSeparator(List<PatternRecognizer> separator) {
		this.separator = separator;
	}

	public void setEvaluator(Evaluator evaluator) {
		this.evaluator = evaluator;
	}    
}
