/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.io.Serializable;
import java.util.List;

import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.metamodel.Evaluator;

/**
 * Collection member
 *  
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public final class MemberCollection extends LanguageMember implements Serializable 
{
    /**
     * Collection type.
     */
    private MemberCollectionType collection;

    /**
     * Minimum multiplicity.
     */
    private int minimumMultiplicity;

	/**
     * Maximum multiplicity
     */
    private int maximumMultiplicity;

    /**
     * Constructor
     * @param field the content field
     * @param elementClass the element class
     * @param collection the collection type
     */
    public MemberCollection (String field, Class elementClass, MemberCollectionType collection)
    {
    	super(field,elementClass);
    	this.collection = collection;
    }
    
    /**
     * Constructor
     * @param field the content field
     * @param elementClass the content element class
     * @param optional whether content is optional or not
     * @param id is id
     * @param reference is reference
     * @param prefix the content prefix
     * @param suffix the content suffix
     * @param separator the ad hoc separator
     * @param collection the collection type
     * @param minimumMultiplicity the minimum multiplicity
     * @param maximumMultiplicity the maximum multiplicity
     * @param evaluator the evaluator
     */
    public MemberCollection (
    		String field,
    		Class elementClass,
    		boolean optional,
    		boolean id,
    		boolean reference,
    		List<PatternRecognizer> prefix,
    		List<PatternRecognizer> suffix,
    		List<PatternRecognizer> separator,
    		MemberCollectionType collection,
    		int minimumMultiplicity,
    		int maximumMultiplicity,
    		Evaluator evaluator ) 
    {
        super(field,elementClass,optional,id,reference,prefix,suffix,separator,evaluator);

        this.collection = collection;
        this.minimumMultiplicity = minimumMultiplicity;
        this.maximumMultiplicity = maximumMultiplicity;
    }

    // Getters & setters
    
    public MemberCollectionType getCollection() {
        return collection;
    }

    public int getMinimumMultiplicity() {
        return minimumMultiplicity;
    }

    public int getMaximumMultiplicity() {
        return maximumMultiplicity;
    }

    public void setMinimumMultiplicity(int minimumMultiplicity) {
		this.minimumMultiplicity = minimumMultiplicity;
	}

	public void setMaximumMultiplicity(int maximumMultiplicity) {
		this.maximumMultiplicity = maximumMultiplicity;
	}
}
