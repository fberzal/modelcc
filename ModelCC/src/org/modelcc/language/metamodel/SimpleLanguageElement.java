/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.io.Serializable;
import java.util.List;

import org.modelcc.AssociativityType;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.metamodel.Evaluator;

/**
 * Simple language element.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class SimpleLanguageElement extends LanguageElement implements Serializable 
{
    /**
     * Pattern.
     */
    private PatternRecognizer pattern;

    /**
     * Value field.
     */
    private String valueField;
    

    /**
     * Constructor.
     * @param elementClass the element class
     * @param associativity the associativity
     * @param prefix the prefix
     * @param suffix the suffix
     * @param separator the default separator
     * @param setupMethod the setup method
     * @param constraintMethods the constraint checking methods
     * @param pattern the pattern
     * @param valueField the value field
     * @param evaluator The evaluator
     */
    public SimpleLanguageElement (
    		Class elementClass,
    		AssociativityType associativity,
    		List<PatternRecognizer> prefix,
    		List<PatternRecognizer> suffix,
    		List<PatternRecognizer> separator,
    		String setupMethod,
    		List<String> constraintMethods,
    		PatternRecognizer pattern,
    		String valueField,
    		Evaluator evaluator) 
    {
        super(elementClass,associativity,prefix,suffix,separator,setupMethod,constraintMethods,evaluator);
        
        this.pattern = pattern;
        this.valueField = valueField;
    }

    
    // Pattern recognizer

    public PatternRecognizer getPatternRecognizer() 
    {
        return pattern;
    }

	public void setPatternRecognizer(PatternRecognizer pattern) 
	{
		this.pattern = pattern;
	}

	// Value field

    public String getValueField() 
    {
        return valueField;
    }
    
    
    // Matches empty string?

	private Boolean canBeEmpty;
	    
    public boolean matchesEmptyString() 
    {
    	if (canBeEmpty==null)
    		canBeEmpty = ( getPatternRecognizer().read("")!=null );
    	
    	return canBeEmpty;
    }
    
}
