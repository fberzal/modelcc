/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language.metamodel;

import java.io.Serializable;

import org.modelcc.SeparatorPolicy;

/**
 * Member position metadata.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class MemberPositionMetadata implements Serializable 
{
	private LanguageMember member;
	
	private int[] position;
	
	private SeparatorPolicy separatorPolicy;
	
	public MemberPositionMetadata(LanguageMember member,int[] position,SeparatorPolicy separatorPolicy) 
	{
		this.member = member;
		this.position = position;
		this.separatorPolicy = separatorPolicy;
	}

	public LanguageMember getMember() {
		return member;
	}

	public int[] getPosition() {
		return position;
	}

	public SeparatorPolicy getSeparatorPolicy() {
		return separatorPolicy;
	}

	public boolean contains(int needle) {
		for (int i = 0;i < position.length;i++)
			if (position[i]==needle)
				return true;
		return false;
	}

	public boolean isEmpty() {
		return position.length==0;
	}	  
}
