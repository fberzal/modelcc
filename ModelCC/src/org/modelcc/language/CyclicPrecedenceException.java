/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.language;

/**
 * Cyclic precedence exception.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (berzal@modelcc.org)
 */
public final class CyclicPrecedenceException extends LanguageException
{
	/**
	 * Constuctor.
	 * @param message Exception message.
	 */
    public CyclicPrecedenceException (String message) 
    {
        super(message);
    }
	
}
