/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

import java.io.Serializable;

/**
 * Separator policy.
 */
public enum SeparatorPolicy implements Serializable {
	REPLACE,
	BEFORE,
	AFTER,
	EXTRA
}
