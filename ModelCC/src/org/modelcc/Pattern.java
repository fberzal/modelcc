/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Pattern recognizer
 */

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Pattern 
{
	public static final String UNDEFINED = "]]]]]]]]]]]]]";

	/**
	 * @return the matcher class.
	 */
	public Class matcher() default Pattern.class;

	/**
	 * @return the matcher class arguments.
	 */
	public String args() default "";

	/**
	 * @return the regular expression.
	 */
	public String regExp() default UNDEFINED;
}
