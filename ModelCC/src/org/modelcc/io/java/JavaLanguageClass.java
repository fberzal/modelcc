/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io.java;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.modelcc.AssociativityType;
import org.modelcc.CompositionType;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.metamodel.Evaluator;
import org.modelcc.metamodel.ModelElement;

/**
 * Java model element
 */
public final class JavaLanguageClass extends ModelElement<LanguageMember>
{
    /**
     * Key members.
     */
    private List<LanguageMember> keyMembers;
    
    /**
     * Whether the element can appear in any order.
     */
    private Boolean freeOrder;
    
    /**
     * Associativity type.
     */
    private AssociativityType associativity;
    
    /**
     * Composition type.
     */
    private CompositionType composition;
    
    /**
     * Prefix.
     */
    private List<PatternRecognizer> prefix;
    
    /**
     * Suffix.
     */
    private List<PatternRecognizer> suffix;
    
    /**
     * Separator.
     */
    private List<PatternRecognizer> separator;
    
    /**
     * Pattern.
     */
    private PatternRecognizer pattern;
    
    /**
     * Value field.
     */
    private Field valueField;
    
    /**
     * Setup method.
     */
    private Method setupMethod;
    
    /**
     * Constraint methods.
     */
    private List<Method> constraintMethods;
    
    /** 
     * Evaluator
     */
    private Evaluator evaluator;
    
    /**
     * Constructor
     * @param elementClass the element class
     */
    public JavaLanguageClass(Class elementClass) 
    {
    	super(elementClass);
    	this.keyMembers = new ArrayList<LanguageMember>();
    	this.constraintMethods = new ArrayList<Method>();
    }


    public Boolean isFreeOrder() 
    {
        return freeOrder;
    }

    public AssociativityType getAssociativity() 
    {
        return associativity;
    }

    public CompositionType getComposition() 
    {
        return composition;
    }

    public List<PatternRecognizer> getPrefix() 
    {
        return prefix;
    }

    public List<PatternRecognizer> getSuffix() 
    {
        return suffix;
    }

    public List<PatternRecognizer> getSeparator() 
    {
        return separator;
    }

    public PatternRecognizer getPattern() 
    {
        return pattern;
    }

    public Field getValueField() 
    {
        return valueField;
    }

    public Method getSetupMethod() 
    {
        return setupMethod;
    }

    public List<Method> getConstraintMethods() 
    {
        return constraintMethods;
    }
    
    public void addConstraintMethod (Method method)
    {
    	constraintMethods.add(method);
    }

    public void setFreeOrder(boolean freeOrder) 
    {
        this.freeOrder = freeOrder;
    }

    public void setAssociativity(AssociativityType associativity) 
    {
        this.associativity = associativity;
    }

    public void setComposition(CompositionType composition) 
    {
        this.composition = composition;
    }

    public void setPrefix(List<PatternRecognizer> prefix) 
    {
        this.prefix = prefix;
    }

    public void setSuffix(List<PatternRecognizer> suffix) 
    {
        this.suffix = suffix;
    }

    public void setSeparator(List<PatternRecognizer> separator) 
    {
        this.separator = separator;
    }

    public void setPattern(PatternRecognizer pattern) 
    {
        this.pattern = pattern;
    }

    public void setValueField(Field valueField) 
    {
        this.valueField = valueField;
    }

    public void setSetupMethod(Method method) 
    {
        this.setupMethod = method;
    }


    public List<LanguageMember> getKeyMembers() 
    {
        return keyMembers;
    }
    
    public void addKeyMember (LanguageMember member)
    {
    	keyMembers.add(member);
    }

    public void setKeyMembers(List<LanguageMember> keyMembers) 
    {
        this.keyMembers = keyMembers;
    }

    public Evaluator getEvaluator() 
    {
    	return evaluator;
    }

    public void setEvaluator(Evaluator evaluator) 
    {
    	this.evaluator = evaluator;
    }
}
