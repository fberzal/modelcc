package org.modelcc.io.java;

import java.util.logging.Level;

/**
 * Java model visitor
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public abstract class JavaModelChecker<M extends JavaModelReaderMetadata> 
{
	private JavaModelReader reader;

	public JavaModelChecker(JavaModelReader reader)
	{
		this.reader = reader;
	}
	
    public void log(Level level, String string, Object[] object) 
    {
        reader.log(level,string,object);
    }
    
    public abstract void check (M metadata);
}
