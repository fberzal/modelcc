package org.modelcc.io.java;

import java.util.logging.Level;

/**
 * Java model processor
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public abstract class JavaModelProcessor<M extends JavaModelReaderMetadata> 
{
	private JavaModelReader reader;

	public JavaModelProcessor(JavaModelReader reader)
	{
		this.reader = reader;
	}
	
    public void log(Level level, String string, Object[] object) 
    {
        reader.log(level,string,object);
    }
    
    public abstract void process (M metadata);
}
