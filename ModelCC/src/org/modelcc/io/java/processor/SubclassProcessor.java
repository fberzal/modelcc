package org.modelcc.io.java.processor;

import java.util.HashSet;
import java.util.Set;

import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.io.java.JavaModelProcessor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.language.metamodel.LanguageElement;

/**
 * Process subclasses
 */
public class SubclassProcessor extends JavaModelProcessor<JavaLanguageMetadata>
{	
	public SubclassProcessor (JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void process(JavaLanguageMetadata metadata) 
	{
    	for (JavaLanguageClass k: metadata.getJavaSubclasses().keySet()) {
    		Set<LanguageElement> ns = new HashSet<LanguageElement>();
    		for (JavaLanguageClass sub: metadata.getJavaSubclasses(k))
    			ns.add(metadata.javaElementToElement.get(sub));
    		if (!ns.isEmpty())
    			metadata.subclasses.put(metadata.javaElementToElement.get(k), ns);
    	}		
    }
}
