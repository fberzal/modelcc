package org.modelcc.io.java.processor;

import java.util.Map.Entry;

import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.io.java.JavaModelProcessor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageMetadata;

/**
 * Process superclasses
 */
public class SuperclassProcessor extends JavaModelProcessor<JavaLanguageMetadata>
{	
	public SuperclassProcessor (JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void process(JavaLanguageMetadata metadata) 
	{
    	for (Entry<JavaLanguageClass,JavaLanguageClass> entr: metadata.getJavaSuperclasses().entrySet()) {
    		metadata.superclasses.put(metadata.javaElementToElement.get(entr.getKey()), metadata.javaElementToElement.get(entr.getValue()));
    	}		
    }
}
