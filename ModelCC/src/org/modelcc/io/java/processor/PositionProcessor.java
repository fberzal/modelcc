package org.modelcc.io.java.processor;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import org.modelcc.Position;
import org.modelcc.io.java.JavaModelProcessor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.io.java.Reflection;
import org.modelcc.language.metamodel.MemberCollection;
import org.modelcc.language.metamodel.CompositeLanguageElement;
import org.modelcc.language.metamodel.LanguageElement;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.language.metamodel.MemberPositionMetadata;
import org.modelcc.lexer.recognizer.PatternRecognizer;

/**
 * Position processor
 */
public class PositionProcessor extends JavaModelProcessor<JavaLanguageMetadata>
{	
	public PositionProcessor (JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void process(JavaLanguageMetadata metadata) 
	{
    	Set<LanguageElement> elements = metadata.elements;
    	
    	for (LanguageElement element: elements) {
    		Map<LanguageMember,MemberPositionMetadata> positions = new HashMap<LanguageMember,MemberPositionMetadata>();
    		
    		if (CompositeLanguageElement.class.isAssignableFrom(element.getClass())) { 
    			processComposite((CompositeLanguageElement)element, positions);
    		}
    		
    		element.setPositions(positions);
    	}		
    }

	private void processComposite (CompositeLanguageElement composite,
			                       Map<LanguageMember, MemberPositionMetadata> positions) 
	{
		Field[] fields = Reflection.getAllFields(composite.getElementClass());
		
		for (int i=0; i<fields.length; i++) {
			LanguageMember member = getMember(composite, fields[i].getName());			
			MemberPositionMetadata metadata = processField(composite, member, fields,i);
			
			if (metadata!=null)
				positions.put(member,metadata);
		}
	}


	
	private MemberPositionMetadata processField (CompositeLanguageElement composite, LanguageMember member, Field[] fields, int i)
	{
		Field field = fields[i];
		MemberPositionMetadata metadata = null;

		if (field.isAnnotationPresent(Position.class)) {
			Position positionTag = field.getAnnotation(Position.class);
			Field otherField = referencedField(fields, positionTag);
			LanguageMember otherMember = getMember(composite, positionTag.element());

			boolean empty = canBeEmpty(otherMember);

			if (otherMember==null) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation refers to an undefined field.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (otherMember==member) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation refers to the same field.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (MemberCollection.class.isAssignableFrom(member.getClass()) &&
					(contains(positionTag.position(),Position.BEFORELAST)||contains(positionTag.position(),Position.WITHIN))) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation cannot be applied to a list and have BEFORELAST or WITHIN values.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (!MemberCollection.class.isAssignableFrom(otherMember.getClass()) &&
					(contains(positionTag.position(),Position.BEFORELAST)||contains(positionTag.position(),Position.WITHIN))) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation cannot be applied to BEFORELAST or WITHIN a non-list element.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (otherField.isAnnotationPresent(Position.class)) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation cannot refer to a member annotated with @Position.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (otherMember.isOptional()) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation cannot refer to a member annotated with @Optional.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (MemberCollection.class.isAssignableFrom(otherMember.getClass()) &&
					((MemberCollection)otherMember).getMinimumMultiplicity()==0 && empty) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Position annotation cannot refer to a member annotated with minimum multiplicity 0 and empty prefixes or suffixes.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else if (!compatible(field,fields)) {
				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": @Position clashes with another member.", new Object[]{field.getName(),composite.getElementClass().getCanonicalName()});
			} else {
				metadata = new MemberPositionMetadata(otherMember,positionTag.position(),positionTag.separatorPolicy());
			}
		}

		return metadata;
	}


	private Field referencedField(Field[] fl, Position positionTag) 
	{
		Field otherField = null;
		for (int j = 0;j < fl.length;j++) {
			if (fl[j].getName().equals(positionTag.element()))
				otherField = fl[j];
		}
		return otherField;
	}

	private boolean canBeEmpty(LanguageMember element) 
	{
		boolean empty = true;
		
		if (element != null) {
			if (element.getPrefix() != null) {
				for (PatternRecognizer prx : element.getPrefix()) {
					if (prx.read("")==null) {
						empty = false;
					}
				}
			}
			if (element.getSuffix() != null) {
				for (PatternRecognizer prx : element.getSuffix()) {
					if (prx.read("")==null) {
						empty = false;
					}
				}
			}
		}
		
		return empty;
	}
	
	
    private boolean contains (int[] haystack, int needle) 
    {
    	for (int i = 0;i < haystack.length;i++)
    		if (haystack[i]==needle)
    			return true;
    	return false;
    }


    private boolean compatible(Field field, Field[] fl) {
    	Position positionTag = field.getAnnotation(Position.class);
    	for (int j = 0;j < fl.length;j++) {
    		if (fl[j] != field) {
    			if (fl[j].isAnnotationPresent(Position.class)) {
    				Position otherPositionTag = fl[j].getAnnotation(Position.class);
    				if (positionTag.element().equals(otherPositionTag.element())) {

    					if ( (contains(positionTag.position(),Position.BEFORE) &&  (contains(otherPositionTag.position(),Position.BEFORE))) ||
    						 (contains(positionTag.position(),Position.AFTER) &&  (contains(otherPositionTag.position(),Position.AFTER))) ||
    						 (contains(positionTag.position(),Position.WITHIN) &&  (contains(otherPositionTag.position(),Position.WITHIN) || contains(otherPositionTag.position(),Position.BEFORELAST))) ||
    						 (contains(positionTag.position(),Position.BEFORELAST) &&  (contains(otherPositionTag.position(),Position.WITHIN) || contains(otherPositionTag.position(),Position.BEFORELAST))) )
    						return false;
    				}
    			}
    		}
    	}
    	return true;
    }

	

	private LanguageMember getMember (CompositeLanguageElement composite, String id) 
	{
		LanguageMember member = null;

		int indexOther = searchField(composite.getMembers(),id);    
		if (indexOther != -1)
			member = composite.getMembers().get(indexOther);
	
		return member;
	}	
	
    private int searchField (List<LanguageMember> contents, String name) 
    {
    	int index = 0;
    	while (index<contents.size()) {
    		if (contents.get(index).getID().equals(name)) {
    			return index;
    		}
    		index++;
    	}
    	return -1;
    }
}
