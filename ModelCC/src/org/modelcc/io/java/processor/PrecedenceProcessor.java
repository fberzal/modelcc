package org.modelcc.io.java.processor;

import java.util.HashSet;
import java.util.Set;

import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.io.java.JavaModelProcessor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.language.metamodel.LanguageElement;

/**
 * Process precedences
 */
public class PrecedenceProcessor extends JavaModelProcessor<JavaLanguageMetadata>
{	
	public PrecedenceProcessor (JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void process(JavaLanguageMetadata metadata) 
	{
    	for (JavaLanguageClass k: metadata.prePrecedences.keySet()) {
    		Set<LanguageElement> ns = new HashSet<LanguageElement>();
    		for (JavaLanguageClass pre: metadata.prePrecedences.get(k))
    			ns.add(metadata.javaElementToElement.get(pre));
    		if (!ns.isEmpty())
    			metadata.precedences.put(metadata.javaElementToElement.get(k), ns);
    	}
    }
}
