/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io.java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.modelcc.metamodel.ModelElement;

/**
 * Java model reader metadata
 * 
 * @author Fernando Berzal (berzal@acm.org)
 *
 * @param <J> Java class model element
 */
public class JavaModelReaderMetadata<J extends ModelElement> implements java.io.Serializable
{
	private Set<Class> relevantClasses = new HashSet<Class>();

	private Map<Class,J> classToJavaElement = new HashMap<Class,J>();
	
	private Set<J> javaElements = new HashSet<J>();
	private Map<J,Set<J>> javaSubclasses = new HashMap<J,Set<J>>();
	private Map<J,J> javaSuperclasses = new HashMap<J,J>();
	
	
	// Getters & setters
	
	public Set<Class> getRelevantClasses() 
	{
		return relevantClasses;
	}
	
	public void setRelevantClasses(Set<Class> relevantClasses) 
	{
		this.relevantClasses = relevantClasses;
	}

	
	public J getClassElement (Class type)
	{
		return classToJavaElement.get(type);
	}
	
	public void setClassElement (Class type, J element)
	{
		classToJavaElement.put(type,element);
	}

	public void setClassToJavaElement(Map<Class,J> classToJavaElement) 
	{
		this.classToJavaElement = classToJavaElement;
	}

	
	public Set<J> getJavaElements() 
	{
		return javaElements;
	}

	
	public Map<J,Set<J>> getJavaSubclasses() 
	{
		return javaSubclasses;
	}

	public Set<J> getJavaSubclasses (J element)
	{
		return javaSubclasses.get(element);
	}
	
	public void setJavaSubclasses (J element, Set<J> subclasses)
	{
		javaSubclasses.put(element, subclasses);
	}
	
	public void addJavaSubclass (J element, J subclass)
	{
		Set<J> se = javaSubclasses.get(element);
		
		if (se == null) {
			se = new HashSet<J>();
			javaSubclasses.put(element,se);
		}
		
		se.add(subclass);
		
		setJavaSuperclass(subclass,element);
	}
	

	public Map<J,J> getJavaSuperclasses() 
	{
		return javaSuperclasses;
	}

	public J getJavaSuperclass(J element)
	{
		return javaSuperclasses.get(element);
	}
	
	private void setJavaSuperclass (J element, J superclass)
	{
		javaSuperclasses.put(element, superclass);
	}

}
