package org.modelcc.io.java.checker;


import java.util.Iterator;

import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaModelChecker;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.language.metamodel.MemberCollection;
import org.modelcc.language.metamodel.LanguageMember;

/**
 * Checks optional values
 */
public class OptionalChecker extends JavaModelChecker<JavaLanguageMetadata>
{	
	public OptionalChecker (JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void check(JavaLanguageMetadata metadata) 
	{
    	for (JavaLanguageClass pe: metadata.getJavaElements()) {
    		for (int i = 0;i < pe.getMembers().size();i++) {
    			LanguageMember em = (LanguageMember) pe.getMember(i);
    			if (em.isOptional()) {
    				if (!MemberCollection.class.isAssignableFrom(em.getClass())) {
    					JavaLanguageClass pe2 = metadata.getClassElement(em.getElementClass());
    					boolean allopt = true;
    					if (pe2.getMembers().isEmpty())
    						allopt = false;
    					for (Iterator<LanguageMember> itec2 = pe2.getMembers().iterator();itec2.hasNext() && allopt;) {
    						LanguageMember em2 = itec2.next();
    						if (!em2.isOptional())
    							allopt = false;
    					}
    				}
    			}
    		}
    	}		
    }
}
