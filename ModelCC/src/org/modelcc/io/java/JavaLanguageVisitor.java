/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io.java;

import org.modelcc.language.metamodel.LanguageMember;

/**
 * Java model visitor for ModelCC languages
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class JavaLanguageVisitor extends JavaModelVisitor<JavaLanguageClass, LanguageMember>
{
	public JavaLanguageVisitor (JavaModelReader reader)
	{
		super(reader);
	}

}
