package org.modelcc.io.java.visitor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.modelcc.Suffix;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

/**
 * Java model reader: Class visitor for @Prefix
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class SuffixVisitor extends JavaLanguageVisitor
{	
	private JavaLanguageMetadata metadata;
	
	public SuffixVisitor(JavaModelReader reader, JavaLanguageMetadata metadata)
	{
		super(reader);
		this.metadata = metadata;
	}
	
	@Override
	public void startVisit(Class type, JavaLanguageClass element) 
	{
    	List<PatternRecognizer> suffix = null;
    	
    	if (type.isAnnotationPresent(Suffix.class)) {
    		Suffix an;
    		an = (Suffix) type.getAnnotation(Suffix.class);
    		PatternRecognizer pr;
    		int k;
    		suffix = new ArrayList<PatternRecognizer>();
    		boolean err = false;
    		for (k = 0;k < an.value().length;k++) {
    			if (an.value()[k] == null) {
    				log(Level.SEVERE, "In class \"{0}\": The @Suffix value cannot contain null.", new Object[]{type.getCanonicalName()});
    				err = true;
    			} else if (an.value()[k].equals("")) {
    			} else {
    				if (metadata.pas.containsKey(an.value()[k]))
    					pr = metadata.pas.get(an.value()[k]);
    				else {
    					pr = new RegExpPatternRecognizer(an.value()[k]);
    					metadata.pas.put(an.value()[k],pr);
    				}
    				suffix.add(pr);
    			}
    		}
    		if (err)
    			suffix = null;
    		else
    			metadata.delimiters.addAll(suffix);
    	}

    	element.setSuffix(suffix);
 	}

	// Model element member

	public void visit (Field field, LanguageMember member)
	{
    	List<PatternRecognizer> suffix = null;
    	
    	if (field.isAnnotationPresent(Suffix.class)) {
    		suffix = new ArrayList<PatternRecognizer>();
    		Suffix an;
    		an = field.getAnnotation(Suffix.class);
    		PatternRecognizer pr;
    		
    		for (int k=0; k < an.value().length;k++) {
    			if (an.value()[k] == null) {
    				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Suffix value cannot contain null.", 
    						new Object[]{field.getName(), field.getDeclaringClass().getCanonicalName()});
    		    } else if (an.value()[k].equals("")) {
    			} else {
    				if (metadata.pas.containsKey(an.value()[k]))
    					pr = metadata.pas.get(an.value()[k]);
    				else {
    					pr = new RegExpPatternRecognizer(an.value()[k]);
    					metadata.pas.put(an.value()[k],pr);
    				}
    				suffix.add(pr);
    				metadata.delimiters.add(pr);
    				//log(Level.INFO,"  Field suffix: RegExp \""+allList(an.value())+".");
    			}
    		}
    		
        	member.setSuffix(suffix);    		
    	}
		
	}
}
