package org.modelcc.io.java.visitor;

import org.modelcc.FreeOrder;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaLanguageClass;

/**
 * Java model reader: Class visitor for @FreeOrder
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class FreeOrderVisitor extends JavaLanguageVisitor
{	
	public FreeOrderVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void startVisit(Class type, JavaLanguageClass element) 
	{
    	boolean freeOrder = false;
    	
    	if (type.isAnnotationPresent(FreeOrder.class))
    		freeOrder = ((FreeOrder) type.getAnnotation(FreeOrder.class)).value();
    	
    	element.setFreeOrder(freeOrder);
	}

}
