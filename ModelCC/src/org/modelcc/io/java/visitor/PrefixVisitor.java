package org.modelcc.io.java.visitor;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.modelcc.Prefix;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaLanguageMetadata;
import org.modelcc.io.java.JavaLanguageClass;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

/**
 * Java model reader: Class visitor for @Prefix
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class PrefixVisitor extends JavaLanguageVisitor 
{	
	private JavaLanguageMetadata metadata;
	
	public PrefixVisitor(JavaModelReader reader, JavaLanguageMetadata metadata)
	{
		super(reader);
		this.metadata = metadata;
	}
	
	@Override
	public void startVisit(Class type, JavaLanguageClass element) 
	{
    	List<PatternRecognizer> prefix = null;
    	
    	if (type.isAnnotationPresent(Prefix.class)) {
    		Prefix an;
    		an = (Prefix) type.getAnnotation(Prefix.class);
    		PatternRecognizer pr;
    		int k;
    		prefix = new ArrayList<PatternRecognizer>();
    		boolean err = false;
    		for (k = 0;k < an.value().length;k++) {
    			if (an.value()[k] == null) {
    				log(Level.SEVERE, "In class \"{0}\": The @Prefix value cannot contain null.", new Object[]{type.getCanonicalName()});
    				err = true;
    			} else if (an.value()[k].equals("")) {
    			} else {
    				if (metadata.pas.containsKey(an.value()[k])) {
    					pr = metadata.pas.get(an.value()[k]);
    			    } else {
    					pr = new RegExpPatternRecognizer(an.value()[k]);
    					metadata.pas.put(an.value()[k],pr);
    				}
    				prefix.add(pr);
    			}
    		}
    		if (err)
    			prefix = null;
    		else
    			metadata.delimiters.addAll(prefix);
    	}
    	
    	element.setPrefix(prefix);	
 	}

	
	// Model element member
	
	public void visit (Field field, LanguageMember member)
	{
    	List<PatternRecognizer> prefix;
    	
    	if (field.isAnnotationPresent(Prefix.class)) {
    		prefix = new ArrayList<PatternRecognizer>();
    		Prefix an;
    		an = field.getAnnotation(Prefix.class);
    		PatternRecognizer pr;
    		
    		for (int k=0;k < an.value().length;k++) {
    			if (an.value()[k] == null) {
    				log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": The @Prefix value cannot contain null.",
    						new Object[]{field.getName(), field.getDeclaringClass().getCanonicalName()});
    		    } else if (an.value()[k].equals("")) {
    			} else {
    				if (metadata.pas.containsKey(an.value()[k]))
    					pr = metadata.pas.get(an.value()[k]);
    				else {
    					pr = new RegExpPatternRecognizer(an.value()[k]);
    					metadata.pas.put(an.value()[k],pr);
    				}
    				prefix.add(pr);
    				metadata.delimiters.add(pr);
    				//log(Level.INFO,"  Field prefix: RegExp \""+allList(an.value())+".");
    			}
    		}
    		
    		member.setPrefix(prefix);
    	}
		
	}
}
