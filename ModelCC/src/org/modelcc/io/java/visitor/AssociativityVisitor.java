package org.modelcc.io.java.visitor;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageClass;

/**
 * Java model reader: Class visitor for @Associativity
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class AssociativityVisitor extends JavaLanguageVisitor 
{	
	public AssociativityVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void startVisit(Class type, JavaLanguageClass element) 
	{
    	AssociativityType associativity = null;
    	
    	if (type.isAnnotationPresent(Associativity.class))
    		associativity = ((Associativity) type.getAnnotation(Associativity.class)).value();

    	element.setAssociativity(associativity);
	}

}
