package org.modelcc.io.java.visitor;

import java.lang.reflect.Field;
import java.util.logging.Level;

import org.modelcc.ID;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.language.metamodel.LanguageMember;

/**
 * Java model reader: Class visitor for @ID
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class IDVisitor extends JavaLanguageVisitor 
{	
	public IDVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void visit (Field field, LanguageMember member) 
	{
    	if (field.isAnnotationPresent(ID.class)) {
    		member.setKey(true);
    		if (member.isOptional()) {
    			member.setKey(false);
    			log(Level.SEVERE, "In field \"{0}\" of class \"{1}\": A field annotated with @ID cannot be annotated with @Optional.", 
    					new Object[]{field.getName(), field.getDeclaringClass().getCanonicalName()});
    		}
    	}
	}

}
