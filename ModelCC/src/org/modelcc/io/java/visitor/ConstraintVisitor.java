package org.modelcc.io.java.visitor;

import java.lang.reflect.Method;
import java.util.logging.Level;

import org.modelcc.Constraint;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaLanguageClass;

/**
 * Java model reader: Class visitor for @Constraint
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ConstraintVisitor extends JavaLanguageVisitor 
{	
	public ConstraintVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void visit (Class type, Method method, JavaLanguageClass element) 
	{
		boolean error = false;
		
		if (method.isAnnotationPresent(Constraint.class)) {
			if ( method.getReturnType()!=boolean.class 
				&& method.getReturnType()!=Boolean.class) {
				log(Level.SEVERE, "In method \"{0}\" of class \"{1}\": The @Constraint annotation can only be assigned to a method that returns boolean or Boolean.", new Object[]{method.getName(), type.getCanonicalName()});
				error = true;
			}
			if (method.getParameterTypes().length!=0) {
				log(Level.SEVERE, "In method \"{0}\" of class \"{1}\": The @Constraint annotation can only be assigned to a method with no parameters.", new Object[]{method.getName(), type.getCanonicalName()});
				error = true;
			}
			if (!error)
				element.addConstraintMethod(method);
		}
	}


}
