package org.modelcc.io.java.visitor;

import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageClass;

/**
 * Java model reader: Class visitor for @Associativity
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class CompositionVisitor extends JavaLanguageVisitor
{	
	public CompositionVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void startVisit(Class type, JavaLanguageClass element) 
	{
    	CompositionType composition = null;
    	
    	if (type.isAnnotationPresent(Composition.class))
    		composition = ((Composition) type.getAnnotation(Composition.class)).value();

    	element.setComposition(composition);
	}

}
