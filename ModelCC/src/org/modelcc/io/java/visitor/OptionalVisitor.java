package org.modelcc.io.java.visitor;

import java.lang.reflect.Field;

import org.modelcc.Optional;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.language.metamodel.LanguageMember;

/**
 * Java model reader: Class visitor for @Optional
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class OptionalVisitor extends JavaLanguageVisitor 
{	
	public OptionalVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void visit (Field field, LanguageMember member) 
	{
    	if (field.isAnnotationPresent(Optional.class))
    		member.setOptional(true);
	}

}
