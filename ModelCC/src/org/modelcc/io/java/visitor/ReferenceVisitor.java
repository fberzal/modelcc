package org.modelcc.io.java.visitor;

import java.lang.reflect.Field;

import org.modelcc.Reference;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.JavaLanguageVisitor;
import org.modelcc.language.metamodel.LanguageMember;

/**
 * Java model reader: Class visitor for @Reference
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ReferenceVisitor extends JavaLanguageVisitor 
{	
	public ReferenceVisitor(JavaModelReader reader)
	{
		super(reader);
	}
	
	@Override
	public void visit (Field field, LanguageMember member) 
	{
    	if (field.isAnnotationPresent(Reference.class)) {
    		member.setReference(true);
    	}
	}

}
