/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io.java;

import org.modelcc.io.ModelIOException;

/**
 * Class does not extend IModel.
 *
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public final class ClassDoesNotExtendIModelException extends ModelIOException
{
    /**
     * Constructor
     * @param string the message string
     */
    public ClassDoesNotExtendIModelException (String string) 
    {
        super(string);
    }

}
