package org.modelcc.io.java;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.modelcc.language.metamodel.LanguageElement;
import org.modelcc.lexer.recognizer.PatternRecognizer;

public class JavaLanguageMetadata extends JavaModelReaderMetadata<JavaLanguageClass>
{
	public Set<LanguageElement> elements = new HashSet<LanguageElement>();	
	public Map<Class,LanguageElement> classToElement = new HashMap<Class,LanguageElement>();
	public Map<JavaLanguageClass,LanguageElement> javaElementToElement = new HashMap<JavaLanguageClass,LanguageElement>();
	
	public LanguageElement start;
	
	public Set<PatternRecognizer> delimiters = new HashSet<PatternRecognizer>();	
	public Map<String,PatternRecognizer> pas = new HashMap<String,PatternRecognizer>();
	
	public Map<LanguageElement,LanguageElement> superclasses = new HashMap<LanguageElement,LanguageElement>();
	public Map<LanguageElement,Set<LanguageElement>> subclasses = new HashMap<LanguageElement,Set<LanguageElement>>();
	
	public Map<JavaLanguageClass,Integer> prePriorities = new HashMap<JavaLanguageClass,Integer>();
	public Map<JavaLanguageClass,Set<JavaLanguageClass>> prePrecedences = new HashMap<JavaLanguageClass,Set<JavaLanguageClass>>();
	public Map<LanguageElement,Set<LanguageElement>> precedences = new HashMap<LanguageElement,Set<LanguageElement>>();

	public Map<LanguageElement, Set<LanguageElement>> defaultElement = new HashMap<LanguageElement, Set<LanguageElement>>();
}
