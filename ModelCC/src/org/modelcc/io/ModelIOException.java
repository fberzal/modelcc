/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io;

import org.modelcc.ModelCCException;

/**
 * ModelCC IO exception.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ModelIOException extends ModelCCException
{
	public ModelIOException (String message)
	{
		super(message);
	}
	
	public ModelIOException (String message, Throwable cause)
	{
		super(message, cause);
	}
	
}
