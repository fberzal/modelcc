/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io;

import java.io.Serializable;

import org.modelcc.ModelCCProcess;
import org.modelcc.metamodel.Model;

/**
 * ModelCC model writer.
 */
public abstract class ModelWriter<M extends Model> extends ModelCCProcess implements Serializable 
{
    /**
     * Writes a model.
     * @param m the model to write.
     * @throws Exception
     */
    public abstract void write(M m) throws Exception;

}
