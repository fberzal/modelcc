/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io;

import java.io.Serializable;

import org.modelcc.ModelCCProcess;
import org.modelcc.metamodel.Model;

/**
 * ModelCC model reader.
 */
public abstract class ModelReader<M extends Model> extends ModelCCProcess implements Serializable 
{
    /**
     * Reads a model.
     * @return the read model
     * @throws Exception
     */
    public abstract M read() throws Exception;

}
