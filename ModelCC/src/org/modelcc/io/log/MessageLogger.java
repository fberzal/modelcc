/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.io.log;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

/**
 * Message logger.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class MessageLogger extends Logger
{    
	/**
     * Message list.
     */
    private List<Message> messages;
	
    /**
     * Constructor.
     */
    public MessageLogger() 
    {
        super("org.modelcc",null);
        
        messages = new ArrayList<Message>();
    }   
    
    /**
     * Log message.
     */
    @Override
    public void log (LogRecord record)
    {
    	Message message = new Message ( 
    			record.getLevel(), 
    			record.getSourceClassName(),
    			MessageFormat.format(record.getMessage(), record.getParameters()),
    			record.getThrown() );
    	
    	messages.add(message);
    }
    
    /**
     * Clear the message list
     */
    public void clear ()
    {
    	messages.clear();
    }
    
    /**
     * Get the message list.
     * @return the message list
     */
    public List<Message> getMessages ()
    {
    	return messages;
    }
    
    /**
     * Get message count.
     * @return Number of messages in the message list
     */
    public int getMessageCount ()
    {
    	return messages.size();
    }
    
    /**
     * Get message.
     * @param index Message index
     * @return the desired message
     */
    public Message getMessage (int index)
    {
    	return messages.get(index);
    }
}
