/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */
package org.modelcc.io.log;

import java.util.logging.Level;

/**
 * Log message.
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class Message 
{
	private String message;
	
	private Level level;
	private String source;
	private Throwable cause;
	
	// Constructors
	
	public Message (Level level, String source, String message)
	{
		this.level = level;
		this.source = source;
		this.message = message;
	}

	public Message (Level level, String source, String message, Throwable cause)
	{
		this.level = level;
		this.source = source;
		this.message = message;
		this.cause = cause;
	}

	// Getters
	
	public String getMessage ()
	{
		return message;
	}
	
	public Level getLevel ()
	{
		return level;
	}
	
	public String getSource ()
	{
		return source;
	}
	
	public Throwable getCause ()
	{
		return cause;
	}

	// toString
	
	@Override
	public String toString ()
	{
		return "["+source+"] " + message + ((cause!=null)?" - "+cause:"");
	}
}
