/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

/**
 * Composition type.
 */
public enum CompositionType
{
    /**
     * Undefined composition constraint
     */
  UNDEFINED,

    /**
     * Eager composition
     */
  EAGER,

    /**
     * Lazy composition
     */
  LAZY,

    /**
     * Explicit composition
     */
  EXPLICIT
}
