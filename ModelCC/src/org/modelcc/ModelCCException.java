/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc;

import java.io.Serializable;

/**
 * ModelCC exception
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ModelCCException extends Exception implements Serializable 
{
	public ModelCCException (String message)
	{
		super(message);
	}

	public ModelCCException (String message, Throwable cause)
	{
		super(message, cause);
	}
}
