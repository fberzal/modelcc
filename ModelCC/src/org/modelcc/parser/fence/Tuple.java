/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.parser.fence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.modelcc.language.syntax.Rule;

/**
 * Tuple (rule,{list of non-expanded symbols}) that represents valid reductions.
 * 
 * @author Luis Quesada (lquesada@modelcc.org), bug fix by Fernando Berzal (berzal@modelcc.org)
 */
public class Tuple implements Serializable {
    
    /**
     * The rule.
     */
    private Rule r;
    
    /**
     * The relevant rule.
     */
    private Rule relevant;
    
    /**
     * The list of symbols.
     */
    private List<ParsedSymbol> symbols;

    /**
     * Constructor
     * @param r Grammar rule
     */
    public Tuple(Rule r) 
    {
        this.r = r;
        this.symbols = new ArrayList<ParsedSymbol>();
        this.relevant = null;
    }

    /**
     * @return the rule
     */
    public Rule getRule() {
        return r;
    }

    /**
     * @return the symbols
     */
    public List<ParsedSymbol> getSymbols() 
    {
        return symbols;
    }
    
    public int getSymbolCount ()
    {
    	return symbols.size();
    }
    
    public ParsedSymbol getSymbol (int index)
    {
    	if (index<symbols.size())
    		return symbols.get(index);
    	else
    		return null;
    }
    
    public void addSymbols (List<ParsedSymbol> ss)
    {
    	for (ParsedSymbol s: ss)
    		if (!symbols.contains(s))
    			symbols.addAll(ss);
    }
    
    public void addSymbol (ParsedSymbol s)
    {
    	if (!symbols.contains(s))
    		symbols.add(s);
    }
    
    /**
     * @return the relevant rule.
     */
    public Rule getRelevant() {
        return relevant;
    }

    /**
     * @param relevant the relevant to set
     */
    public void setRelevant(Rule relevant) {
        this.relevant = relevant;
    }  
    
    
    // toString
    
    @Override
    public String toString ()
    {
    	return r + " [" + symbols.size() + "]";
    }
}
