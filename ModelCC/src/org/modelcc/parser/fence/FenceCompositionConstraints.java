package org.modelcc.parser.fence;

import java.util.Set;

import org.modelcc.language.syntax.Rule;
import org.modelcc.language.syntax.Symbol;
import org.modelcc.language.syntax.SyntaxConstraints;

/**
 * Fence composition constraints
 * 
 * @author Luis Quesada (lquesada@modelcc.org), refactored by Fernando Berzal (fberzal@modelcc.org)
 */

public class FenceCompositionConstraints 
{
	private SyntaxConstraints constraints;
	
	public FenceCompositionConstraints (SyntaxConstraints constraints)
	{
		this.constraints = constraints;
	}
	
	// Composition constraint
	
	public boolean inhibit (Rule r, Symbol s) 
	{
		boolean inhibited = false;

		Set<Rule> compc = constraints.getCompositionPrecedences(r);
		
		if (compc != null) {
			for (int j = 0;j < s.size();j++) {
				if (s.getContent(j).getRelevantRule() != null) {
					if (compc.contains(s.getContent(j).getRelevantRule())) {
						inhibited = true;
					}
				}
			}
		}
		
		return inhibited;
	}

}
