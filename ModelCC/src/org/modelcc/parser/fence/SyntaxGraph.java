/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.parser.fence;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.modelcc.language.syntax.Symbol;

/**
 * Syntax graph.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public final class SyntaxGraph implements Serializable {

    /**
     * Set of symbols used in this graph.
     */
    private Set<Symbol> symbols;

    /**
     * Set of root symbols in this graph.
     */
    private Set<Symbol> roots;

    
    /**
     * Default constructor.
     */
    public SyntaxGraph() 
    {
        this.symbols = new HashSet<Symbol>(256);
        this.roots = new HashSet<Symbol>();
    }

    /**
     * @return the roots symbols of this graph.
     */
    public Set<Symbol> getRoots() 
    {
        return Collections.unmodifiableSet(roots);
    }
    
    public void addStart (Symbol s)
    {
    	roots.add(s);
    }

    /**
     * @return the symbols used in this graph.
     */
    public Set<Symbol> getSymbols() 
    {
        return Collections.unmodifiableSet(symbols);
    }
    
    public void add (Symbol s)
    {
    	symbols.add(s);
    }
    
    public void remove (Symbol s)
    {
    	symbols.remove(s);
    	roots.remove(s);
    }

}
