/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.parser.fence;

import java.io.Serializable;
import java.util.Map;

import org.modelcc.language.syntax.ParserMetadata;
import org.modelcc.language.syntax.SyntaxSpecification;
import org.modelcc.lexer.LexicalGraph;

/**
 * Fence - SyntaxGraphParser with Lexical and Syntactic Ambiguity Support.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class Fence implements Serializable {

    /**
     * Perform syntactical analysis on a Lexical Graph.
     * @param fs the Fence specification.
     * @param lg the input lexical graph.
     * @return a syntax graph.
     */
    public SyntaxGraph parse(ParserMetadata metadata,SyntaxSpecification fs,LexicalGraph lg) 
    {
    	return parse(metadata,fs,lg,null);
    }
    
    /**
     * Perform syntactical analysis on a Lexical Graph.
     * @param fs the Fence specification.
     * @param lg the input lexical graph.
     * @param objectMetadata the object metadata warehouse
     * @return a syntax graph.
     */
    public SyntaxGraph parse(ParserMetadata metadata,SyntaxSpecification fs,LexicalGraph lg,Map<String,Map<Object,Object>> objectMetadata) 
    {
        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(fs.getGrammar(),lg);
        FenceConstraintEnforcer fce = new FenceConstraintEnforcer(metadata,fs.getConstraints(),objectMetadata);
        SyntaxGraph sg = fce.enforce(pg);
        return sg;
    }

}
