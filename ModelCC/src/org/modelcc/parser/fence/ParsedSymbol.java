/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.parser.fence;

import java.io.Serializable;
import java.util.Set;

import org.modelcc.language.syntax.InputSymbol;

/**
 * Parsed symbol.
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public final class ParsedSymbol extends InputSymbol implements Serializable 
{

    /**
     * Single type history
     */
    private Set<Object> singleTypeHistory;
    
    /**
     * Is token?
     */
    private boolean token;
    
    /**
     * Text.
     */
    private String string;
    
    /**
     * Constructor.
     * @param type the type of the token.
     * @param startIndex the start index.
     * @param endIndex the end index.
     */
    public ParsedSymbol(Object type,int startIndex,int endIndex) 
    {
    	super(type,startIndex,endIndex);
        this.singleTypeHistory = null;
        this.string = null;
        this.token = false;
    }

    /**
     * Constructor.
     * @param type the type of the token.
     * @param startIndex the start index.
     * @param endIndex the end index.
     * @param string the token text.
     */
    public ParsedSymbol(Object type,int startIndex,int endIndex,String string) 
    {
    	super(type,startIndex,endIndex);
        this.singleTypeHistory = null;
        this.string = string;
        this.token = true;
    }

    /**
     * Constructor.
     * @param type the type of the token.
     * @param startIndex the start index.
     * @param endIndex the end index.
     * @param singleTypeHistory the single type history.
     */
    public ParsedSymbol(Object type,int startIndex,int endIndex,Set<Object> singleTypeHistory) 
    {
    	super(type,startIndex,endIndex);
        this.singleTypeHistory = singleTypeHistory;
        this.token = false;
    }

    /**
     * Whether the symbol is a token or not.
     * @return true if it is a token, false if not
     */
    public boolean isToken() {
        return token;
    }


    /**
     * @return the singleTypeHistory
     */
    Set<Object> getSingleTypeHistory() {
        return singleTypeHistory;
    }

    /**
     * @param singleTypeHistory the singleTypeHistory to set
     */
    void setSingleTypeHistory(Set<Object> singleTypeHistory) {
        this.singleTypeHistory = singleTypeHistory;
    }

    /**
     * @return the string
     */
    public String getString() 
    {
 
        return string;
    }
}
