/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package org.modelcc.parser;

import org.modelcc.ModelCCException;

/**
 * Parser exception
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ParserException extends ModelCCException
{
	public ParserException (String message)
	{
		super(message);
	}    
    
	public ParserException (String message, Throwable cause)
	{
		super(message, cause);
	}    

}
