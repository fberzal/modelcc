/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package org.modelcc.parser;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.modelcc.metamodel.Model;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExps;
import org.modelcc.parser.fence.FenceParserFactory;

/**
 * ModelCC Parser Generator: Generic interface for parsers.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public abstract class ParserFactory implements Serializable 
{

	public abstract Parser createParser (Model m) throws ParserException;
	
    public abstract Parser createParser (Model m, Lexer lexer) throws ParserException;

    public abstract Parser createParser (Model m, Model skip) throws ParserException;
	
    public abstract Parser createParser (Model m, Set<PatternRecognizer> skip) throws ParserException;

    /**
     * Current parser factory
     */
    private static ParserFactory factory = new FenceParserFactory();

	
	public static ParserFactory getParserFactory ()
	{
		return ParserFactory.factory;
	}

	public static void setParserFactory (ParserFactory factory)
	{
		ParserFactory.factory = factory;
	}
	


    public final static Set<PatternRecognizer> WHITESPACE = new HashSet<PatternRecognizer>(Arrays.asList(new RegExpPatternRecognizer(RegExps.WHITESPACE)));

    /**
     * Creates a parser (convenience method)
     * @param m the model
     * @return the parser
     * @throws ParserException
     */
    public static Parser create (Model m) throws ParserException 
    {
        return factory.createParser(m);
    }

    /**
     * Creates a parser (convenience method)
     * @param m the model
     * @param lexer the lexer
     * @return the parser
     * @throws ParserException
     */
    public static Parser create (Model m, Lexer lexer) throws ParserException 
    {
    	return factory.createParser(m,lexer);
    }

    /**
     * Creates a parser (convenience method)
     * @param m the model
     * @param skip the skip model
     * @return the parser
     * @throws ParserException
     */
    public static Parser create (Model m, Model skip) throws ParserException 
    {
    	return factory.createParser(m,skip);
    }

    /**
     * Creates a parser (convenience method)
     * @param m the model
     * @param skip the skip set
     * @return the parser
     * @throws ParserException
     */
    public static Parser create (Model m, Set<PatternRecognizer> skip) throws ParserException 
    {
    	return factory.createParser(m,skip);
    }
	
}
