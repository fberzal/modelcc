/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.modelcc.io.ModelReader;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserException;
import org.modelcc.parser.ParserFactory;

import model.examples.inheritance.Basic;
import model.examples.inheritance.Inherit1;
import model.examples.inheritance.Inherit2;
import model.examples.inheritance.Inherit3;
import model.examples.inheritance.Inherit4;
import model.examples.inheritance.Inherit5;
import model.examples.inheritance.Parentheses;
import model.examples.keys.Keys10Lang;
import model.examples.keys.Keys11Lang;
import model.examples.keys.Keys1Lang;
import model.examples.keys.Keys2Lang;
import model.examples.keys.Keys3Lang;
import model.examples.keys.Keys4Lang;
import model.examples.keys.Keys5Lang;
import model.examples.keys.Keys6Lang;
import model.examples.keys.Keys7Lang;
import model.examples.keys.Keys8Lang;
import model.examples.keys.Keys9Lang;
import model.examples.positions.Position1;
import model.examples.positions.Position10;
import model.examples.positions.Position11;
import model.examples.positions.Position12;
import model.examples.positions.Position13;
import model.examples.positions.Position14;
import model.examples.positions.Position15;
import model.examples.positions.Position16;
import model.examples.positions.Position17;
import model.examples.positions.Position18;
import model.examples.positions.Position19;
import model.examples.positions.Position2;
import model.examples.positions.Position20;
import model.examples.positions.Position3;
import model.examples.positions.Position4;
import model.examples.positions.Position5;
import model.examples.positions.Position6;
import model.examples.positions.Position7;
import model.examples.positions.Position8;
import model.examples.positions.Position9;
import model.examples.positions.PositionFree1;
import model.examples.positions.PositionRef1;
import model.examples.test.DollarPrefix;
import model.examples.test.IncorrectDollarPrefix;
import model.examples.test.Test7;
import model.examples.test.Test7_1;
import model.examples.test.Test7_2;
import model.examples.test.Test9;
import model.examples.test.Test9B;
import model.examples.work.Ini;
import model.examples.work.Ini1;
import model.examples.work.Ini10;
import model.examples.work.Ini11;
import model.examples.work.Ini2;
import model.examples.work.Ini3;
import model.examples.work.Ini4;
import model.examples.work.Ini5;
import model.examples.work.Ini6;
import model.examples.work.Ini61;
import model.examples.work.Ini7;
import model.examples.work.Ini8;
import model.examples.work.Ini9;
import model.examples.work.Ino;

import test.languages.calculator.model.BinaryExpression;
import test.languages.calculator.model.Expression;
import test.languages.calculator.model.ExpressionGroup;
import test.languages.calculator.model.IntegerLiteral;

public class LanguageSpecificationFactoryTest 
{
    private Parser createParser(Class c)
    {
        ModelReader<LanguageModel> jmr = new JavaLanguageReader(c);
        LanguageModel m = null;
		try {
			m = jmr.read();
		} catch (Exception e1) {
			fail();
		}

		Set<PatternRecognizer> skip = new HashSet<PatternRecognizer>();
        skip.add(new RegExpPatternRecognizer("\\t"));
        skip.add(new RegExpPatternRecognizer(" "));
        skip.add(new RegExpPatternRecognizer("\n"));
        skip.add(new RegExpPatternRecognizer("\r"));
        
        Parser parser = null;

        try {
        	parser = ParserFactory.create(m,skip);
        } catch (ParserException exception){
        	fail();
        }
        
        return parser;
    }

    public Collection<Object> parseAll(Class cls, String input) 
    {
        Parser parser = createParser(cls);
        
        try {
        	return parser.parseAll(input);
        } catch (Exception e) {
        	return new HashSet<Object>();
        }
    }


    @Test
    public void ModelToLanguageSpecificationTest1a() 
    {
        assertEquals(1,parseAll(Expression.class,"3+5+5").size());
        assertEquals(1,parseAll(Expression.class,"3+5").size());
        assertEquals(1,parseAll(Expression.class,"3").size());
        assertEquals(1,parseAll(Expression.class,"3+(5+5)").size());
        assertEquals(1,parseAll(Expression.class,"3+5*5").size());
        assertEquals(1,parseAll(Expression.class,"3*5+5").size());
        assertEquals(1,parseAll(Expression.class,"3*(5+5)").size());
        assertEquals(1,parseAll(Expression.class,"3+2/6/2").size());
        assertEquals(1,parseAll(Expression.class,"3*5*1-5+6*12+5").size());
        assertEquals(1,parseAll(Expression.class,"3*5+1*5+6*12+5").size());
        assertEquals(1,parseAll(Expression.class,"(3*5+1*5)+(6*12+5)").size());
        assertEquals(1,parseAll(Expression.class,"++2").size());
        assertEquals(1,parseAll(Expression.class,"++2++3").size());
        assertEquals(1,parseAll(Expression.class,"3/5++2*5").size());
        assertEquals(1,parseAll(Expression.class,"(3/5++2*5)").size());
        assertEquals(1,parseAll(Expression.class,"(3/5++2*5)+(3/5+2*5)").size());
        assertEquals(1,parseAll(Expression.class,"3*2*5+-2").size());
    }

    @Test
    public void ModelToLanguageSpecificationTest2() 
    {
        Collection<Object> o;
        o = parseAll(Ini.class,"hello");
        assertEquals(1,o.size());
        Ini i = (Ini) o.iterator().next();
        assertEquals(Ino.class,i.a.getClass());
    }

    @Test
    public void ModelToLanguageSpecificationTest3() 
    {
        Collection<Object> o;
        o = parseAll(Ini1.class,"hello");
        assertEquals(1,o.size());
        Ini1 i = (Ini1) o.iterator().next();
        assertEquals(Ino[].class,i.a.getClass());
        assertEquals(1,i.a.length);
    }

    @Test
    public void ModelToLanguageSpecificationTest3a() 
    {
        Collection<Object> o;
        o = parseAll(Ini1.class,"hellohello");
        assertEquals(1,o.size());
        Ini1 i = (Ini1) o.iterator().next();
        assertEquals(Ino[].class,i.a.getClass());
        assertEquals(2,i.a.length);
    }

    @Test
    public void ModelToLanguageSpecificationTest4() 
    {
        Collection<Object> o;
        o = parseAll(Ini2.class,"hello");
        assertEquals(1,o.size());
        Ini2 i = (Ini2) o.iterator().next();
        assertEquals(ArrayList.class,i.a.getClass());
        assertEquals(1,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest4a() 
    {
        Collection<Object> o;
        o = parseAll(Ini1.class,"hellohello");
        assertEquals(1,o.size());
        Ini1 i = (Ini1) o.iterator().next();
        assertEquals(Ino[].class,i.a.getClass());
        assertEquals(2,i.a.length);
    }

    @Test
    public void ModelToLanguageSpecificationTest5() 
    {
        Collection<Object> o;
        o = parseAll(Ini3.class,"hello");
        assertEquals(1,o.size());
        Ini3 i = (Ini3) o.iterator().next();
        assertEquals(ArrayList.class,i.a.getClass());
        assertEquals(1,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest5a() 
    {
        Collection<Object> o;
        o = parseAll(Ini3.class,"hellohellohello");
        assertEquals(1,o.size());
        Ini3 i = (Ini3) o.iterator().next();
        assertEquals(ArrayList.class,i.a.getClass());
        assertEquals(3,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest6() 
    {
        Collection<Object> o;
        o = parseAll(Ini4.class,"hellohello");
        assertEquals(1,o.size());
        Ini4 i = (Ini4) o.iterator().next();
        assertEquals(ArrayList.class,i.a.getClass());
        assertEquals(2,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest7() 
    {
        Collection<Object> o;
        o = parseAll(Ini5.class,"hellohellohellohello");
        assertEquals(1,o.size());
        Ini5 i = (Ini5) o.iterator().next();
        assertEquals(HashSet.class,i.a.getClass());
        assertEquals(4,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest8() 
    {
        Collection<Object> o;
        o = parseAll(Ini6.class,"hellohellohellohello");
        assertEquals(1,o.size());
        Ini6 i = (Ini6) o.iterator().next();
        assertEquals(HashSet.class,i.a.getClass());
        assertEquals(4,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest9() {
        Collection<Object> o;
        o = parseAll(Ini61.class,"hellohellohellohello");
        assertEquals(1,o.size());
        Ini61 i = (Ini61) o.iterator().next();
        assertEquals(HashSet.class,i.a.getClass());
        assertEquals(1,i.a.size());
    }

    @Test
    public void ModelToLanguageSpecificationTest10() {
        Class c = Ini7.class;
        assertEquals(0,parseAll(c,"()").size());
        assertEquals(1,parseAll(c,"(-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+,-hello+)").size());
    }

    @Test
    public void ModelToLanguageSpecificationTest11() {
        Class c = Ini8.class;
        assertEquals(1,parseAll(c,"()").size());
        assertEquals(1,parseAll(c,"(-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+,-hello+)").size());
    }

    @Test
    public void ModelToLanguageSpecificationTest12() {
        Class c = Ini9.class;
        assertEquals(0,parseAll(c,"()").size());
        assertEquals(0,parseAll(c,"(-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+)").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(0,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+)").size());
        assertEquals(0,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+,-hello+)").size());
    }

    @Test
    public void ModelToLanguageSpecificationTest13() {
        Class c = Ini10.class;
        assertEquals(0,parseAll(c,"()-hello+").size());
        assertEquals(0,parseAll(c,"(-hello+)-hello+").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+)-hello+").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+)-hello+").size());
        assertEquals(1,parseAll(c,"(-hello+,-hello+,-hello+,-hello+)-hello+").size());
        assertEquals(0,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+)-hello+").size());
        assertEquals(0,parseAll(c,"(-hello+,-hello+,-hello+,-hello+,-hello+,-hello+)-hello+").size());
    }

    @Test
    public void ModelToLanguageSpecificationTest14() {
        Class c = Ini11.class;
        assertEquals(0,parseAll(c,"a()hellobcc").size());
        assertEquals(0,parseAll(c,"a(-hello+)hellobcc").size());
        assertEquals(1,parseAll(c,"a(-hello+,-hello+)hellobcc").size());
        assertEquals(1,parseAll(c,"a(-hello+,-hello+,-hello+)hellobcc").size());
        assertEquals(1,parseAll(c,"a(-hello+,-hello+,-hello+,-hello+)hellobcc").size());
        assertEquals(0,parseAll(c,"a(-hello+,-hello+,-hello+,-hello+,-hello+)hellobcc").size());
        assertEquals(0,parseAll(c,"a(-hello+,-hello+,-hello+,-hello+,-hello+,-hello+)hellobcc").size());
    }


    @Test
    public void DollarPrefixTest() {
        Collection<Object> col = parseAll(DollarPrefix.class,"$0");
        assertEquals(1,col.size());
        DollarPrefix obj = (DollarPrefix)col.iterator().next();
        assertEquals("$0",obj.toString());
    }
    

    @Test
    public void IncorrectDollarPrefixTest() {
        Collection<Object> col = parseAll(IncorrectDollarPrefix.class,"$0");
        assertEquals(0,col.size());
    }

    @Test
    public void AutorunTest1() {
        assertEquals(1,parseAll(Test7.class,"a").size());
        assertEquals(0,parseAll(Test7.class,"b").size());
        assertEquals(1,parseAll(Test7_1.class,"a").size());
        assertEquals(0,parseAll(Test7_1.class,"b").size());
        assertEquals(1,parseAll(Test7_2.class,"a").size());
        assertEquals(1,parseAll(Test7_2.class,"b").size());
    }


    @Test
    public void CompositionTest() {
        assertEquals(1,parseAll(Test9.class,"CC").size());
        assertEquals(1,parseAll(Test9.class,"CmCm").size());
        assertEquals(1,parseAll(Test9.class,"CmC").size());
        assertEquals(1,parseAll(Test9.class,"CCm").size());
        assertEquals(1,parseAll(Test9.class,"CCC").size());
    }

    @Test
    public void CompositionTest2() {
        assertEquals(1,parseAll(Test9B.class,"CC").size());
        assertEquals(1,parseAll(Test9B.class,"mCmC").size());
        assertEquals(1,parseAll(Test9B.class,"mCC").size());
        assertEquals(1,parseAll(Test9B.class,"CmC").size());
        assertEquals(1,parseAll(Test9B.class,"CCC").size());
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest1() {
        Class c = Keys1Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a1 refs a");
        Keys1Lang cc = (Keys1Lang) o.iterator().next();
        assertEquals(cc.keys1[0],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest2() {
        Class c = Keys1Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a1 refs a a");
        Keys1Lang cc = (Keys1Lang) o.iterator().next();
        assertEquals(cc.keys1[0],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest3() {
        Class c = Keys1Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a1 b4 refs a a b");
        Keys1Lang cc = (Keys1Lang) o.iterator().next();
        assertEquals(cc.keys1[0],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
        assertEquals(cc.keys1[1],cc.refs[2]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest4() {
        Class c = Keys2Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a1 refs a a b");
        assertFalse(o.iterator().hasNext());
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest5() {
        Class c = Keys3Lang.class;
        Collection<Object> o;
        o = parseAll(c,"data a1");
        assertTrue(o.iterator().hasNext());
    }

    @Test
    public void ModelToLanguageSpecificationReferencesTest6() {
        Class c = Keys3Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a data a1");
        assertTrue(o.iterator().hasNext());
        Keys3Lang cc = (Keys3Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[0],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest7() {
        Class c = Keys4Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a data a1");
        assertTrue(o.iterator().hasNext());
        Keys4Lang cc = (Keys4Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[0],cc.refs);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest8() {
        Class c = Keys4Lang.class;
        Collection<Object> o;
        o = parseAll(c,"b data a1 b2");
        assertTrue(o.iterator().hasNext());
        Keys4Lang cc = (Keys4Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest9() {
        Class c = Keys3Lang.class;
        Collection<Object> o;
        o = parseAll(c,"b a a data a1 b2");
        assertTrue(o.iterator().hasNext());
        Keys3Lang cc = (Keys3Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
        assertEquals(cc.keys1[0],cc.refs[2]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest10() {
        Class c = Keys3Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a a b data a1 b3");
        assertTrue(o.iterator().hasNext());
        Keys3Lang cc = (Keys3Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[0],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
        assertEquals(cc.keys1[1],cc.refs[2]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest11() {
        Class c = Keys3Lang.class;
        Collection<Object> o;
        o = parseAll(c,"a a data a1");
        assertTrue(o.iterator().hasNext());
        Keys3Lang cc = (Keys3Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[0],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest12() {
        Class c = Keys5Lang.class;
        Collection<Object> o;
        o = parseAll(c,"startref b endref data a1 b2");
        assertTrue(o.iterator().hasNext());
        Keys5Lang cc = (Keys5Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest13() {
        Class c = Keys6Lang.class;
        Collection<Object> o;
        o = parseAll(c,"startref kbc endref data kac1 kbc2");
        assertTrue(o.iterator().hasNext());
        Keys6Lang cc = (Keys6Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest14() {
        Class c = Keys7Lang.class;
        Collection<Object> o;
        o = parseAll(c,"startref kbc endref data kac1 kbc2");
        assertTrue(o.iterator().hasNext());
        Keys7Lang cc = (Keys7Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest15() {
        Class c = Keys7Lang.class;
        Collection<Object> o;
        o = parseAll(c,"startref kac kbc endref data kbc1 kac2");
        assertTrue(o.iterator().hasNext());
        Keys7Lang cc = (Keys7Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs[0]);
        assertEquals(cc.keys1[0],cc.refs[1]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest16() {
        Class c = Keys7Lang.class;
        Collection<Object> o;
        o = parseAll(c,"startref kac kbc kdc endref data kbc1 kac2");
        assertFalse(o.iterator().hasNext());
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest17() {
        Class c = Keys8Lang.class;
        Collection<Object> o;
        o = parseAll(c,"kbc1 kac2 refs kac");
        assertTrue(o.iterator().hasNext());
        Keys8Lang cc = (Keys8Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.keys1[1],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest18() {
        Class c = Keys9Lang.class;
        Collection<Object> o;
        o = parseAll(c,"[b,a,c] data [a,c,b]:1 [b,a,c]:2");
        assertTrue(o.iterator().hasNext());
        Keys9Lang cc = (Keys9Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.data[1],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest19() {
        Class c = Keys10Lang.class;
        Collection<Object> o;
        o = parseAll(c,"[c,b,a,c] data [d,a]:1 [b,c,a,c]:2");
        assertTrue(o.iterator().hasNext());
        Keys10Lang cc = (Keys10Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.data[1],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest20() {
        Class c = Keys10Lang.class;
        Collection<Object> o;
        o = parseAll(c,"[b,a,c] data [d,a]:1 [b,c,a,c]:2");
        assertTrue(o.iterator().hasNext());
        Keys10Lang cc = (Keys10Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.data[1],cc.refs[0]);
    }
    
    @Test
    public void ModelToLanguageSpecificationReferencesTest21() {
        Class c = Keys11Lang.class;
        Collection<Object> o;
        o = parseAll(c,"d1 data cval3endval1 val2endval1d");
        assertTrue(o.iterator().hasNext());
        Keys11Lang cc = (Keys11Lang) o.iterator().next();
        assertEquals(1,o.size());
        assertEquals(cc.data[1],cc.refs[0]);
    }
    
    
    @Test
    public void PositionTest1() {
        assertEquals(1,parseAll(Position1.class,"BA").size());
        assertEquals(0,parseAll(Position1.class,"AB").size());
        Position1 o = (Position1)parseAll(Position1.class,"B12A22").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
    }

    @Test
    public void PositionTest2() {
        assertEquals(1,parseAll(Position2.class,"BA").size());
        assertEquals(0,parseAll(Position2.class,"AB").size());
        Position2 o = (Position2)parseAll(Position2.class,"B12A22").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
    }

    @Test
    public void PositionTest3() {
        assertEquals(1,parseAll(Position3.class,"BAC").size());
        assertEquals(0,parseAll(Position3.class,"ABC").size());
        Position3 o = (Position3)parseAll(Position3.class,"B12A22").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
        o = (Position3)parseAll(Position3.class,"B12A22C1").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
        assertEquals("C1",o.c.value);
        o = (Position3)parseAll(Position3.class,"B12A22").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
        assertNull(o.c);
    }

    @Test
    public void PositionTest4() {
    	assertEquals(1,parseAll(Position4.class,"BAC").size());
    	assertEquals(1,parseAll(Position4.class,"ABC").size());
    	Position4 o = (Position4) parseAll(Position4.class,"B12A22").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	o = (Position4)parseAll(Position4.class,"B12A22C1").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	assertEquals("C1",o.c.value);
    	o = (Position4)parseAll(Position4.class,"B12A22").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	assertNull(o.c);
    	o = (Position4) parseAll(Position4.class,"A22B12").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	o = (Position4)parseAll(Position4.class,"A22B12C1").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	assertEquals("C1",o.c.value);
    	o = (Position4)parseAll(Position4.class,"A22B12").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	assertNull(o.c);    
    }
    
    @Test
    public void PositionTest5() {
        assertEquals(1,parseAll(Position5.class,"ACBCCCC").size());
        assertEquals(1,parseAll(Position5.class,"ACCCCBC").size());
        assertEquals(1,parseAll(Position5.class,"ACCCCCB").size());
        assertEquals(1,parseAll(Position5.class,"ABCCCCC").size());
        assertEquals(0,parseAll(Position5.class,"BACCCCC").size());
        Position5 o = (Position5) parseAll(Position5.class,"A22C1B12C2C3").iterator().next();
        assertEquals("B12",o.b.value);
        assertEquals("A22",o.a.value);
        assertEquals("C1",o.c[0].value);
        assertEquals("C2",o.c[1].value);
        assertEquals("C3",o.c[2].value);
    }

    @Test
    public void PositionTest6() {
    	assertEquals(1,parseAll(Position6.class,"ACCCCBC").size());
    	assertEquals(0,parseAll(Position6.class,"ACBCCCC").size());
    	assertEquals(0,parseAll(Position6.class,"ACCCCCB").size());
    	assertEquals(0,parseAll(Position6.class,"ABCCCCC").size());
    	Position5 o = (Position5) parseAll(Position5.class,"A22C1B12C2").iterator().next();
    	assertEquals("B12",o.b.value);
    	assertEquals("A22",o.a.value);
    	assertEquals("C1",o.c[0].value);
    	assertEquals("C2",o.c[1].value);
    }

    @Test
    public void PositionTest7() {
        assertEquals(1,parseAll(Position7.class,"ACCCCBC").size());
        assertEquals(1,parseAll(Position7.class,"ACBCCCC").size());
        assertEquals(2,parseAll(Position7.class,"ACCCCCB").size());
        assertEquals(2,parseAll(Position7.class,"ABCCCCC").size());
        assertEquals(0,parseAll(Position7.class,"BACCCCC").size());
    }

    @Test
    public void PositionTest8() {
        assertEquals(1,parseAll(Position8.class,"ACCBC").size());
        assertEquals(1,parseAll(Position8.class,"ACCCCBC").size());
        assertEquals(0,parseAll(Position8.class,"ACCCCCB").size());
        assertEquals(0,parseAll(Position8.class,"ABC").size());
        assertEquals(0,parseAll(Position8.class,"ACBC").size());
    }    

    @Test
    public void PositionTest9() {
        assertEquals(1,parseAll(Position9.class,"ACxyCxyCxyCxyzBwC").size());
        assertEquals(0,parseAll(Position9.class,"ACxyzBwC").size());
        assertEquals(0,parseAll(Position9.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(0,parseAll(Position9.class,"AzBwC").size());
    }    

    @Test
    public void PositionTest10() {
        assertEquals(1,parseAll(Position10.class,"ACxyCxyCxyCzBwxyC").size());
        assertEquals(0,parseAll(Position10.class,"ACxyzBwC").size());
        assertEquals(0,parseAll(Position10.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(0,parseAll(Position10.class,"AzBwC").size());
    }    

    @Test
    public void PositionTest11() {
        assertEquals(1,parseAll(Position11.class,"ACxyCxyCxyCxyzBwxyC").size());
        assertEquals(0,parseAll(Position11.class,"ACxyzBwxyC").size());
        assertEquals(0,parseAll(Position11.class,"ACxyCxyCxyCxyCxyzBw").size());
        assertEquals(0,parseAll(Position11.class,"AzBwC").size());
    }    

    @Test
    public void PositionTest12() {
        assertEquals(1,parseAll(Position12.class,"ACxyCxyCxyCzBwC").size());
        assertEquals(0,parseAll(Position12.class,"ACzBwC").size());
        assertEquals(0,parseAll(Position12.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(0,parseAll(Position12.class,"AzBwC").size());
    }    

    @Test
    public void PositionTest13() {
        assertEquals(1,parseAll(Position13.class,"ACxyCxyCxyCxyzBwC").size());
        assertEquals(1,parseAll(Position13.class,"ACxyCxyzBwCxyCxyC").size());
        assertEquals(1,parseAll(Position13.class,"ACxyzBwCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest14() {
        assertEquals(1,parseAll(Position14.class,"ACxyCxyCxyCzBwxyC").size());
        assertEquals(1,parseAll(Position14.class,"ACxyCzBwxyCxyCxyC").size());
        assertEquals(1,parseAll(Position14.class,"ACzBwxyCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest15() {
        assertEquals(1,parseAll(Position15.class,"ACxyCxyCxyCzBwC").size());
        assertEquals(1,parseAll(Position15.class,"ACxyCzBwCxyCxyC").size());
        assertEquals(1,parseAll(Position15.class,"ACzBwCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest16() {
        assertEquals(1,parseAll(Position16.class,"ACxyCxyCxyCxyzBwxyC").size());
        assertEquals(1,parseAll(Position16.class,"ACxyCxyzBwxyCxyCxyC").size());
        assertEquals(1,parseAll(Position16.class,"ACxyzBwxyCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest17() {
        assertEquals(1,parseAll(Position17.class,"ACxyCxyCxyCxyzBwC").size());
        assertEquals(1,parseAll(Position17.class,"ACxyCxyzBwCxyCxyC").size());
        assertEquals(1,parseAll(Position17.class,"ACxyzBwCxyCxyCxyC").size());
        assertEquals(1,parseAll(Position17.class,"AzBwCxyCxyCxyCxyC").size()); // TODO 2
        assertEquals(1,parseAll(Position17.class,"ACxyCxyCxyCxyCzBw").size()); // TODO 2
    }    

    @Test
    public void PositionTest18() {
        assertEquals(1,parseAll(Position18.class,"ACxyCxyCxyCzBwxyC").size());
        assertEquals(1,parseAll(Position18.class,"ACxyCzBwxyCxyCxyC").size());
        assertEquals(1,parseAll(Position18.class,"ACzBwxyCxyCxyCxyC").size());
        assertEquals(1,parseAll(Position18.class,"AzBwCxyCxyCxyCxyC").size());
        assertEquals(1,parseAll(Position18.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(1,parseAll(Position18.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(1,parseAll(Position18.class,"AzBwxyCxyCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest19() {
        assertEquals(1,parseAll(Position19.class,"ACxyCxyCxyCzBwC").size());
        assertEquals(1,parseAll(Position19.class,"ACxyCzBwCxyCxyC").size());
        assertEquals(1,parseAll(Position19.class,"ACzBwCxyCxyCxyC").size());
        assertEquals(2,parseAll(Position19.class,"AzBwCxyCxyCxyCxyC").size());
        assertEquals(2,parseAll(Position19.class,"ACxyCxyCxyCxyCzBw").size());
        assertEquals(0,parseAll(Position19.class,"AzBwxyCxyCxyCxyCxyC").size());
    }    

    @Test
    public void PositionTest20() {
        assertEquals(1,parseAll(Position20.class,"ACxyCxyCxyCxyzBwxyC").size());
        assertEquals(1,parseAll(Position20.class,"ACxyCxyzBwxyCxyCxyC").size());
        assertEquals(1,parseAll(Position20.class,"ACxyzBwxyCxyCxyCxyC").size());
        assertEquals(1,parseAll(Position20.class,"AzBwCxyCxyCxyCxyC").size());
        assertEquals(1,parseAll(Position20.class,"ACxyCxyCxyCxyCzBw").size());
    }    
    
    @Test
    public void PositionFreeTest1() {
        assertEquals(1,parseAll(PositionFree1.class,"ACCBC").size());
        assertEquals(1,parseAll(PositionFree1.class,"ACCCCBC").size());
        assertEquals(1,parseAll(PositionFree1.class,"CCBCA").size());
        assertEquals(1,parseAll(PositionFree1.class,"CCCCBCA").size());
    }    

    @Test
    public void PositionRefTest1() {
        assertEquals(1,parseAll(PositionRef1.class,"ID1A1 ID2A2   ID2 ID2 aID1 ID1").size());
        PositionRef1 o = (PositionRef1) parseAll(PositionRef1.class,"ID1A1 ID2A2   ID2 ID2 aID1 ID1").iterator().next();
        assertEquals("A1",o.objects[0].content.value);
        assertEquals("A2",o.objects[1].content.value);
        assertEquals(o.objects[1],o.reflist[0]);
        assertEquals(o.objects[1],o.reflist[1]);
        assertEquals(o.objects[0],o.reflist[2]);
        assertEquals(o.objects[0],o.ref);
    }    
    
    @Test
    public void InheritMembers() {
        assertEquals(1,parseAll(Basic.class,"A").size());
        assertEquals(1,parseAll(Parentheses.class,"(A)").size());
        assertEquals(1,parseAll(Basic.class,"(A)").size());
        assertEquals(0,parseAll(Parentheses.class,"A").size());
    }    

    @Test
    public void InheritTest1() {
        assertEquals(1,parseAll(Inherit1.class,"pA").size());
        assertEquals(1,parseAll(Inherit1.class,"A").size());
    }    

    @Test
    public void InheritTest2() {
        assertEquals(2,parseAll(Inherit2.class,"A").size());
    }    

    @Test
    public void InheritTest3() {
        assertEquals(1,parseAll(Inherit3.class,"AB").size());
    }    

    @Test
    public void InheritTest4() {
        assertEquals(1,parseAll(Inherit4.class,"BA").size());
    }    

    @Test
    public void InheritTest5() {
        assertEquals(1,parseAll(Inherit5.class,"A").size());
    }    
    
    @Test
    public void startAndEndIndexTest() 
    	throws ParserException
    {
    	Parser<Expression> parser = createParser(Expression.class);
    	
    	//                               0        10
    	//                               01234567890123456789
    	Expression exp2 = parser.parse("(3/5++2*5)+(3/5+2*5)");
        
        BinaryExpression be = (BinaryExpression)exp2;
        assertEquals(0,parser.getParsingMetadata(be).get("startIndex"));
        assertEquals(19,parser.getParsingMetadata(be).get("endIndex"));

        assertEquals(0,parser.getParsingMetadata(be.e1).get("startIndex"));
        assertEquals(9,parser.getParsingMetadata(be.e1).get("endIndex"));

        assertEquals(10,parser.getParsingMetadata(be.op).get("startIndex"));
        assertEquals(10,parser.getParsingMetadata(be.op).get("endIndex"));

        assertEquals(11,parser.getParsingMetadata(be.e2).get("startIndex"));
        assertEquals(19,parser.getParsingMetadata(be.e2).get("endIndex"));

        ExpressionGroup pe = (ExpressionGroup)(be.e1);

        assertEquals(0,parser.getParsingMetadata(pe).get("startIndex"));
        assertEquals(9,parser.getParsingMetadata(pe).get("endIndex"));

        BinaryExpression be1 = (BinaryExpression)(pe.e);
        
        assertEquals(1,parser.getParsingMetadata(be1).get("startIndex"));
        assertEquals(8,parser.getParsingMetadata(be1).get("endIndex"));

        assertEquals(1,parser.getParsingMetadata(be1.e1).get("startIndex"));
        assertEquals(3,parser.getParsingMetadata(be1.e1).get("endIndex"));

        assertEquals(4,parser.getParsingMetadata(be1.op).get("startIndex"));
        assertEquals(4,parser.getParsingMetadata(be1.op).get("endIndex"));

        assertEquals(5,parser.getParsingMetadata(be1.e2).get("startIndex"));
        assertEquals(8,parser.getParsingMetadata(be1.e2).get("endIndex"));

        BinaryExpression be2 = (BinaryExpression)(be1.e1);
        
        assertEquals(1,parser.getParsingMetadata(be2).get("startIndex"));
        assertEquals(3,parser.getParsingMetadata(be2).get("endIndex"));

        assertEquals(1,parser.getParsingMetadata(be2.e1).get("startIndex"));
        assertEquals(1,parser.getParsingMetadata(be2.e1).get("endIndex"));

        IntegerLiteral le = (IntegerLiteral)(be2.e1);
        
        assertEquals(1,parser.getParsingMetadata(le).get("startIndex"));
        assertEquals(1,parser.getParsingMetadata(le).get("endIndex"));
    }
}
