package test.modelcc.factory;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ConstraintFactoryTest.class,
	                   GrammarFactoryTest.class,
	                   LexicalSpecificationFactoryTest.class,
	                   LanguageSpecificationFactoryTest.class,
	                   KeyWrapperTest.class,
	                   ObjectWrapperTest.class } )
public class AllTests {

}