/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import org.junit.Test;
import org.modelcc.language.CyclicPrecedenceException;
import org.modelcc.language.syntax.SyntaxConstraintsFactory;
import org.modelcc.language.syntax.Rule;
import org.modelcc.language.syntax.RuleSymbol;

public class ConstraintFactoryTest 
{
    @Test(expected=CyclicPrecedenceException.class)
    public void testCyclicPrecedence1() throws Exception
    {
        Rule r1,r2;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
        
        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        cf.addCompositionPrecedences(r2,r1);
        cf.addCompositionPrecedences(r1,r2);
        
        cf.create();
    }

    @Test(expected=CyclicPrecedenceException.class)
    public void testCyclicPrecedence2() throws Exception
    {
        Rule r1,r2,r3;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        r3 = new Rule(new RuleSymbol("TestThree"));
        r3.add(new RuleSymbol("TestOne"));

        cf.addCompositionPrecedences(r2,r3);
        cf.addCompositionPrecedences(r3,r1);
        cf.addCompositionPrecedences(r1,r2);
           
        cf.create();
    }

    @Test
    public void testCyclicPrecedence3 () throws Exception
    {
        Rule r1,r2,r3;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        r3 = new Rule(new RuleSymbol("TestThree"));
        r3.add(new RuleSymbol("TestOne"));

        cf.addCompositionPrecedences(r3,r1);
        cf.addCompositionPrecedences(r1,r2);

        cf.create();
    }


    @Test(expected=CyclicPrecedenceException.class)
    public void testSelectionPrecedence1() throws Exception 
    {
        Rule r1,r2;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        cf.addSelectionPrecedences(r2,r1);
        cf.addSelectionPrecedences(r1,r2);

        cf.create();
    }

    @Test(expected=CyclicPrecedenceException.class)
    public void testSelectionPrecedence2() throws Exception
    {
        Rule r1,r2,r3;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        r3 = new Rule(new RuleSymbol("TestThree"));
        r3.add(new RuleSymbol("TestOne"));

        cf.addSelectionPrecedences(r2,r3);
        cf.addSelectionPrecedences(r3,r1);
        cf.addSelectionPrecedences(r1,r2);

        cf.create();
    }

    @Test
    public void testSelectionPrecedence3() throws Exception
    {
        Rule r1,r2,r3;

        SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

        r1 = new Rule(new RuleSymbol("TestOne"));
        r1.add(new RuleSymbol("Test"));

        r2 = new Rule(new RuleSymbol("TestTwo"));
        r2.add(new RuleSymbol("TestOne"));

        r3 = new Rule(new RuleSymbol("TestThree"));
        r3.add(new RuleSymbol("TestOne"));

        cf.addSelectionPrecedences(r3,r1);
        cf.addSelectionPrecedences(r1,r2);

        cf.create();
    }

}
