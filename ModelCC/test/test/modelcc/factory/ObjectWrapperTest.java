/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.junit.Test;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.syntax.ObjectWrapper;

import model.examples.container.IntData;
import model.examples.container.LibArray;
import model.examples.container.LibList;
import model.examples.container.LibSet;

import test.languages.arithmetic.model.*;

public class ObjectWrapperTest 
{
	public IntegerLiteral getLiteral(int val,LanguageModel m,Map<Object,ObjectWrapper> map) 
	{
		IntegerLiteral il = new IntegerLiteral();
		il.value = val;
		map.put(il,new ObjectWrapper(il,m,val));
		return il;
	}

	public BinaryOperator getBinaryOperator(char opc,LanguageModel m,Map<Object,ObjectWrapper> map) 
	{
		BinaryOperator op = null;
		switch (opc) {
		case '+':
			op = new AdditionOperator();
		break;
		case '-':
			op = new SubstractionOperator();
			break;
		case '/':
			op = new DivisionOperator();
			break;
		case '*':
			op = new MultiplicationOperator();
			break;
		}

		map.put(op,new ObjectWrapper(op,m,opc));
		return op;
	}

	public BinaryExpression getBinaryExpression(Expression e1,BinaryOperator op,Expression e2) 
	{
		BinaryExpression b;
		b = new BinaryExpression();
		b.e1 = e1;
		b.op = op;
		b.e2 = e2;
		return b;
	}

	public IntData getIntData(int val,LanguageModel m,Map<Object,ObjectWrapper> map) 
	{
		IntData il = new IntData();
		il.value = val;
		map.put(il,new ObjectWrapper(il,m,val));
		return il;
	}


	public LanguageModel getModel (Class cls)
	{
		LanguageModel m = null;
		
		try {
			m = JavaLanguageReader.read(cls);
		} catch (Exception ex) {
			fail();
		}
		
		return m;
	}
	
	
	@Test
	public void objectWrapperTest1() 
	{
		LanguageModel m = getModel(Expression.class);
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		IntegerLiteral il1 = getLiteral(1,m,map);
		IntegerLiteral il2 = getLiteral(2,m,map);
		IntegerLiteral il3 = getLiteral(2,m,map);

		ObjectWrapper ow1l = ObjectWrapper.createObjectWrapper(il1,m,map);
		ObjectWrapper ow2l = ObjectWrapper.createObjectWrapper(il2,m,map);
		ObjectWrapper ow3l = ObjectWrapper.createObjectWrapper(il3,m,map);

		assertEquals(ow2l,ow2l);
		assertEquals(ow3l,ow3l);
		assertEquals(ow2l,ow3l);
		assertFalse(ow2l.equals(ow1l));

		assertEquals(ow2l.hashCode(),ow3l.hashCode());
		assertFalse(ow2l.hashCode()==ow1l.hashCode());
	}
	
	
	@Test
	public void objectWrapperTest2() 
	{
		LanguageModel m = getModel(Expression.class);
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		IntegerLiteral il1 = getLiteral(1,m,map);
		IntegerLiteral il2 = getLiteral(2,m,map);
		IntegerLiteral il3 = getLiteral(1,m,map);
		IntegerLiteral il4 = getLiteral(2,m,map);

		BinaryOperator op1 = getBinaryOperator('+',m,map);
		BinaryOperator op2 = getBinaryOperator('+',m,map);
		BinaryOperator op3 = getBinaryOperator('-',m,map);

		BinaryExpression b1 = getBinaryExpression(il1,op1,il2);
		BinaryExpression b2 = getBinaryExpression(il3,op2,il4);
		BinaryExpression b3 = getBinaryExpression(il3,op2,il1);
		BinaryExpression b4 = getBinaryExpression(il1,op3,il2);

		ObjectWrapper ow1 = ObjectWrapper.createObjectWrapper(b1,m,map);
		ObjectWrapper ow2 = ObjectWrapper.createObjectWrapper(b2,m,map);
		ObjectWrapper ow3 = ObjectWrapper.createObjectWrapper(b3,m,map);
		ObjectWrapper ow4 = ObjectWrapper.createObjectWrapper(b4,m,map);

		assertEquals(ow1,ow2);
		assertFalse(ow3.equals(ow2));
		assertFalse(ow4.equals(ow1));

		assertEquals(ow1.hashCode(),ow2.hashCode());
		assertFalse(ow3.hashCode()==ow2.hashCode());
		assertFalse(ow4.hashCode()==ow1.hashCode());
	}

	
	@Test
	public void objectWrapperArrayTest() 
	{
		LanguageModel m = getModel(LibArray.class);
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		IntData il1 = getIntData(1,m,map);
		IntData il2 = getIntData(2,m,map);
		IntData il3 = getIntData(3,m,map);
		IntData il4 = getIntData(4,m,map);
		IntData il1b = getIntData(1,m,map);
		IntData il2b = getIntData(2,m,map);
		IntData il3b = getIntData(3,m,map);
		IntData il4b = getIntData(4,m,map);

		LibArray list1 = new LibArray();
		list1.data = new IntData[] { il1, il2, il3, il4 };

		LibArray list2 = new LibArray();
		list2.data = new IntData[] { il1b, il2b, il3b, il4b };

		LibArray list3 = new LibArray();
		list3.data = new IntData[] { il1b, il3b, il2b, il4b };

		ObjectWrapper ow1 = ObjectWrapper.createObjectWrapper(list1,m,map);
		ObjectWrapper ow2 = ObjectWrapper.createObjectWrapper(list2,m,map);
		ObjectWrapper ow3 = ObjectWrapper.createObjectWrapper(list3,m,map);

		assertEquals(ow1,ow2);
		assertFalse(ow1.equals(ow3));

		assertEquals(ow1.hashCode(),ow2.hashCode());
		assertFalse(ow1.hashCode()==ow3.hashCode());
	}

	
	@Test
	public void objectWrapperListTest() 
	{
		LanguageModel m = getModel(LibList.class);
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		IntData il1 = getIntData(1,m,map);
		IntData il2 = getIntData(2,m,map);
		IntData il3 = getIntData(3,m,map);
		IntData il4 = getIntData(4,m,map);
		IntData il1b = getIntData(1,m,map);
		IntData il2b = getIntData(2,m,map);
		IntData il3b = getIntData(3,m,map);
		IntData il4b = getIntData(4,m,map);

		LibList list1 = new LibList();
		list1.data = new ArrayList<IntData>();
		list1.data.add(il1);
		list1.data.add(il2);
		list1.data.add(il3);
		list1.data.add(il4);

		LibList list2 = new LibList();
		list2.data = new ArrayList<IntData>();
		list2.data.add(il1b);
		list2.data.add(il2b);
		list2.data.add(il3b);
		list2.data.add(il4b);

		LibList list3 = new LibList();
		list3.data = new ArrayList<IntData>();
		list3.data.add(il1b);
		list3.data.add(il3b);
		list3.data.add(il2b);
		list3.data.add(il4b);

		ObjectWrapper ow1 = ObjectWrapper.createObjectWrapper(list1,m,map);
		ObjectWrapper ow2 = ObjectWrapper.createObjectWrapper(list2,m,map);
		ObjectWrapper ow3 = ObjectWrapper.createObjectWrapper(list3,m,map);

		assertEquals(ow1,ow2);
		assertFalse(ow1.equals(ow3));

		assertEquals(ow1.hashCode(),ow2.hashCode());
		assertFalse(ow1.hashCode()==ow3.hashCode());
	}


	@Test
	public void objectWrapperSetTest() 
	{
		LanguageModel m = getModel(LibSet.class);
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		IntData il1 = getIntData(1,m,map);
		IntData il2 = getIntData(2,m,map);
		IntData il3 = getIntData(3,m,map);
		IntData il4 = getIntData(4,m,map);
		IntData il1b = getIntData(1,m,map);
		IntData il2b = getIntData(2,m,map);
		IntData il3b = getIntData(3,m,map);
		IntData il4b = getIntData(4,m,map);

		LibSet list1 = new LibSet();
		list1.data = new HashSet<IntData>();
		list1.data.add(il1);
		list1.data.add(il2);
		list1.data.add(il3);
		list1.data.add(il4);

		LibSet list2 = new LibSet();
		list2.data = new HashSet<IntData>();
		list2.data.add(il1b);
		list2.data.add(il2b);
		list2.data.add(il3b);
		list2.data.add(il4b);

		LibSet list3 = new LibSet();
		list3.data = new HashSet<IntData>();
		list3.data.add(il1b);
		list3.data.add(il3b);
		list3.data.add(il2b);
		list3.data.add(il4b);

		LibSet list4 = new LibSet();
		list4.data = new HashSet<IntData>();
		list4.data.add(il1b);
		list4.data.add(il3b);
		list4.data.add(il2b);

		ObjectWrapper ow1 = ObjectWrapper.createObjectWrapper(list1,m,map);
		ObjectWrapper ow2 = ObjectWrapper.createObjectWrapper(list2,m,map);
		ObjectWrapper ow3 = ObjectWrapper.createObjectWrapper(list3,m,map);
		ObjectWrapper ow4 = ObjectWrapper.createObjectWrapper(list4,m,map);

		assertEquals(ow1,ow2);
		assertEquals(ow1,ow3);
		assertFalse(ow1.equals(ow4));

		assertEquals(ow1.hashCode(),ow2.hashCode());
		assertEquals(ow1.hashCode(),ow3.hashCode());
		assertFalse(ow1.hashCode()==ow4.hashCode());
	}
}
