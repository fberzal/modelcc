/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import org.junit.Test;
import org.modelcc.language.NullElementException;
import org.modelcc.language.syntax.GrammarFactory;
import org.modelcc.language.syntax.Rule;
import org.modelcc.language.syntax.RuleSymbol;

public class GrammarFactoryTest 
{
    @Test
    public void testGrammar() throws Exception
    {
        Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15;

        GrammarFactory gf = new GrammarFactory();

        //Statement ::= InputStatement
        r1 = new Rule(new RuleSymbol("Statement"));
        r1.add(new RuleSymbol("InputStatement"));

        //Statement ::= OutputStatement
        r2 = new Rule(new RuleSymbol("Statement"));
        r2.add(new RuleSymbol("OutputStatement"));

        //InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
        r3 = new Rule(new RuleSymbol("InputStatement"));
        r3.add(new RuleSymbol("input"));
        r3.add(new RuleSymbol("LeftParenthesis"));
        r3.add(new RuleSymbol("Identifier"));
        r3.add(new RuleSymbol("RightParenthesis"));
        r3.add(new RuleSymbol("Semicolon"));

        //OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
        r4 = new Rule(new RuleSymbol("OutputStatement"));
        r4.add(new RuleSymbol("output"));
        r4.add(new RuleSymbol("LeftParenthesis"));
        r4.add(new RuleSymbol("Expression"));
        r4.add(new RuleSymbol("RightParenthesis"));
        r4.add(new RuleSymbol("Semicolon"));

        //Expression ::= Expression Operator1 Expression
        r5 = new Rule(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Operator1"));
        r5.add(new RuleSymbol("Expression"));

        //Expression ::= Expression Operator2 Expression
        r6 = new Rule(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Operator2"));
        r6.add(new RuleSymbol("Expression"));

        //Expression ::= LeftParenthesis Expression RightParenthesis
        r7 = new Rule(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("LeftParenthesis"));
        r7.add(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("RightParenthesis"));

        //Expression ::= Operator1 Expression
        r8 = new Rule(new RuleSymbol("Expression"));
        r8.add(new RuleSymbol("Operator1"));
        r8.add(new RuleSymbol("Expression"));

        //Expression ::= Integer
        r9 = new Rule(new RuleSymbol("Expression"));
        r9.add(new RuleSymbol("Integer"));

        //Expression ::= Real
        r10 = new Rule(new RuleSymbol("Expression"));
        r10.add(new RuleSymbol("Real"));

        //Start ::= main LeftBracket StatementList RightBracket
        r11 = new Rule(new RuleSymbol("Start"));
        r11.add(new RuleSymbol("main"));
        r11.add(new RuleSymbol("LeftBracket"));
        r11.add(new RuleSymbol("StatementList"));
        r11.add(new RuleSymbol("RightBracket"));

        //StatementList ::=
        r12 = new Rule(new RuleSymbol("StatementList"));

        //StatementList ::= StatementListAny
        r13 = new Rule(new RuleSymbol("StatementList"));
        r13.add(new RuleSymbol("StatementListAny"));

        //StatementListAny ::= Statement
        r14 = new Rule(new RuleSymbol("StatementListAny"));
        r14.add(new RuleSymbol("Statement"));

        //StatementListAny ::= Statement StatementListAny
        r15 = new Rule(new RuleSymbol("StatementListAny"));
        r15.add(new RuleSymbol("Statement"));
        r15.add(new RuleSymbol("StatementListAny"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.addRule(r6);
        gf.addRule(r7);
        gf.addRule(r8);
        gf.addRule(r9);
        gf.addRule(r10);
        gf.addRule(r11);
        gf.addRule(r12);
        gf.addRule(r13);
        gf.addRule(r14);
        gf.addRule(r15);
        gf.setStartType("Start");

        gf.create();
    }

    @Test(expected=NullElementException.class)
    public void testNullLeftElement() throws Exception
    {
        Rule r1;

        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule((RuleSymbol)null);
        r1.add(new RuleSymbol("Test"));

        gf.addRule(r1);
        gf.setStartType("Start");
        
        gf.create();
    }

    @Test(expected=NullElementException.class)
    public void testNullRightElement() throws Exception
    {
        Rule r1;
     
        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Test"));
        r1.add(null);

        gf.addRule(r1);
        gf.setStartType("Start");

        gf.create();
    }
}
