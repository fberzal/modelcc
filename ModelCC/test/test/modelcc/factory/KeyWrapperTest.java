/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.syntax.ObjectWrapper;

import model.examples.keys.CharData;
import model.examples.keys.ComplexKey;
import model.examples.keys.IntData;
import model.examples.keys.Keys1;
import model.examples.keys.Keys2;

public class KeyWrapperTest 
{
	public IntData getIntData(int val,LanguageModel m,Map<Object,ObjectWrapper> map) {
		IntData il = new IntData();
		il.value = val;
		map.put(il,new ObjectWrapper(il,m,val));
		return il;
	}

	public CharData getCharData(char val,LanguageModel m,Map<Object,ObjectWrapper> map) {
		CharData il = new CharData();
		il.value = val;
		map.put(il,new ObjectWrapper(il,m,val));
		return il;
	}

	@Test
	public void keyWrapperTest1() {
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		LanguageModel m = null;
		try {
			m = JavaLanguageReader.read(Keys1.class);
		} catch (Exception ex) {
			fail();
		}
		IntData il1 = getIntData(1,m,map);
		IntData il2 = getIntData(2,m,map);
		IntData il3 = getIntData(3,m,map);
		getIntData(4,m,map);
		getIntData(1,m,map);
		getIntData(2,m,map);
		IntData il3b = getIntData(3,m,map);
		getIntData(4,m,map);
		CharData a1 = getCharData('1',m,map);
		CharData a2 = getCharData('2',m,map);
		getCharData('3',m,map);
		getCharData('4',m,map);
		CharData a1b = getCharData('1',m,map);
		getCharData('2',m,map);
		getCharData('3',m,map);
		getCharData('4',m,map);

		Keys1 k1 = new Keys1(a1,il1);
		Keys1 k2 = new Keys1(a1,il2);
		Keys1 k3 = new Keys1(a1b,il3);
		Keys1 k4 = new Keys1(a2,il3b);

		ObjectWrapper kw1 = ObjectWrapper.createKeyWrapper(k1, m, map);
		ObjectWrapper kw2 = ObjectWrapper.createKeyWrapper(k2, m, map);
		ObjectWrapper kw3 = ObjectWrapper.createKeyWrapper(k3, m, map);
		ObjectWrapper kw4 = ObjectWrapper.createKeyWrapper(k4, m, map);

		assertEquals(kw1.hashCode(),kw2.hashCode());
		assertEquals(kw1.hashCode(),kw3.hashCode());
		assertFalse(kw1.hashCode()==kw4.hashCode());

		assertEquals(kw1,kw2);
		assertEquals(kw1,kw3);
		assertFalse(kw1.equals(kw4));
	}

	@Test
	public void keyWrapperTest2() {
		Map<Object,ObjectWrapper> map = new HashMap<Object,ObjectWrapper>();

		LanguageModel m = null;
		try {
			m = JavaLanguageReader.read(Keys2.class);
		} catch (Exception ex) {
			fail();
		}

		IntData il1 = getIntData(1,m,map);
		IntData il2 = getIntData(2,m,map);
		IntData il3 = getIntData(3,m,map);
		IntData il4 = getIntData(4,m,map);
		IntData il1b = getIntData(1,m,map);
		IntData il2b = getIntData(2,m,map);
		getIntData(3,m,map);
		getIntData(4,m,map);
		CharData a1 = getCharData('1',m,map);
		getCharData('2',m,map);
		CharData a3 = getCharData('3',m,map);
		getCharData('4',m,map);
		getCharData('1',m,map);
		getCharData('2',m,map);
		getCharData('3',m,map);
		getCharData('4',m,map);

		IntData[] data1 = new IntData[]{ il1,  il2,  il1, il3};
		IntData[] data2 = new IntData[]{ il1b, il2b, il1, il3};
		IntData[] data3 = new IntData[]{ il1,  il2,  il1, il2};
		IntData[] data4 = new IntData[]{ il1,  il2,  il1};

		ComplexKey ck1 = new ComplexKey(data1);
		ComplexKey ck2 = new ComplexKey(data2);
		ComplexKey ck3 = new ComplexKey(data3);
		ComplexKey ck4 = new ComplexKey(data4);

		Keys2 k1 = new Keys2(ck1,il1,a1);
		Keys2 k2 = new Keys2(ck2,il1b,a3);
		Keys2 k3 = new Keys2(ck3,il1,a1);
		Keys2 k4 = new Keys2(ck1,il2,a1);
		Keys2 k5 = new Keys2(ck4,il1,a1);
		Keys2 k6 = new Keys2(ck4,il4,a1);

		ObjectWrapper kw1 = ObjectWrapper.createKeyWrapper(k1, m,map);
		ObjectWrapper kw2 = ObjectWrapper.createKeyWrapper(k2, m, map);
		ObjectWrapper kw3 = ObjectWrapper.createKeyWrapper(k3, m, map);
		ObjectWrapper kw4 = ObjectWrapper.createKeyWrapper(k4, m, map);
		ObjectWrapper kw5 = ObjectWrapper.createKeyWrapper(k5, m,map);
		ObjectWrapper kw6 = ObjectWrapper.createKeyWrapper(k6, m, map);

		assertEquals(kw1.hashCode(),kw2.hashCode());
		assertFalse(kw1.hashCode()==kw3.hashCode());
		assertFalse(kw1.hashCode()==kw4.hashCode());
		assertFalse(kw1.hashCode()==kw5.hashCode());
		assertFalse(kw1.hashCode()==kw6.hashCode());

		assertEquals(kw1,kw2);
		assertFalse(kw1.equals(kw3));
		assertFalse(kw1.equals(kw4));
		assertFalse(kw1.equals(kw5));
		assertFalse(kw1.equals(kw6));
	}
}
