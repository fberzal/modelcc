/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.factory;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.Before;
import org.modelcc.language.CyclicPrecedenceException;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;


public class LexicalSpecificationFactoryTest 
{
    TokenSpecification m1,m2,m3,m4,m5,m6;
    LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
	
    @Before
    public void createLSF ()
	{
		lsf = new LexicalSpecificationFactory();
		
        m1 = new TokenSpecification("Space",new RegExpPatternRecognizer(" +"));
        m2 = new TokenSpecification("Tab",new RegExpPatternRecognizer("\\t+"));
        m3 = new TokenSpecification("NewLine",new RegExpPatternRecognizer("\\n|\\r+"));
        m4 = new TokenSpecification("Integer2",new RegExpPatternRecognizer("(-|\\+)?[0-9][0-9]"));
        m5 = new TokenSpecification("Integer",new RegExpPatternRecognizer("(-|\\+)?[0-9]+"));
        m6 = new TokenSpecification("Operator",new RegExpPatternRecognizer("\\+"));

        lsf.skipTokenSpecification(m1);
        lsf.skipTokenSpecification(m2);
        lsf.skipTokenSpecification(m3);
        lsf.addTokenSpecification(m4);
        lsf.addTokenSpecification(m5);
        lsf.addTokenSpecification(m6);
	}

    
    @Test
    public void testLexicalSpecificationFactory() throws Exception
    {
        lsf.addPrecedence(m5,m4);

        LexicalSpecification ls = lsf.create();

        assertTrue(ls.getTokenSpecifications().contains(m1));
        assertTrue(ls.getTokenSpecifications().contains(m2));
        assertTrue(ls.getTokenSpecifications().contains(m3));
        assertTrue(ls.getTokenSpecifications().contains(m4));
        assertTrue(ls.getTokenSpecifications().contains(m5));
        assertTrue(ls.getTokenSpecifications().contains(m6));
        
        assertTrue(ls.getPrecedences().get(m5).contains(m4));
    }

    @Test(expected=CyclicPrecedenceException.class)
    public void testCyclicPrecedence() throws Exception
    {
    	lsf.addPrecedence(m1,m4);
    	lsf.addPrecedence(m4,m1);
    	lsf.addPrecedence(m5,m6);

    	lsf.create();
    }

    @Test(expected=CyclicPrecedenceException.class)
    public void testCyclicPrecedence2() throws Exception
    {
    	lsf.addPrecedence(m1,m4);
    	lsf.addPrecedence(m2,m1);
    	lsf.addPrecedence(m4,m2);

   		lsf.create();
    }

}
