package test.modelcc.parser;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { FenceConstraintEnforcerTest.class,
					   FenceGrammarParserTest.class})
public class AllTests {

}