/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.parser;

import static org.junit.Assert.*;

import java.io.StringReader;

import org.junit.Test;
import org.modelcc.language.NullElementException;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.language.syntax.Grammar;
import org.modelcc.language.syntax.GrammarFactory;
import org.modelcc.language.syntax.ParserMetadata;
import org.modelcc.language.syntax.Rule;
import org.modelcc.language.syntax.RuleSymbol;
import org.modelcc.language.syntax.SyntaxSpecification;
import org.modelcc.language.syntax.SyntaxSpecificationFactory;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;
import org.modelcc.lexer.lamb.LambLexer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.fence.Fence;
import org.modelcc.parser.fence.FenceGrammarParser;
import org.modelcc.parser.fence.ParsedGraph;
import org.modelcc.parser.fence.SyntaxGraph;

/**
 * Fence test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class FenceGrammarParserTest extends FenceTest
{
	@Test 
    public void testChainedSelectionPrecedence() throws Exception
    {
        // Lexis

        LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
        
        TokenSpecification m1,m2,m3;
        
        m1 = new TokenSpecification("A",new RegExpPatternRecognizer("abc"));
        m2 = new TokenSpecification("B",new RegExpPatternRecognizer("abc"));
        m3 = new TokenSpecification("C",new RegExpPatternRecognizer("abc"));

        lsf.addTokenSpecification(m1);
        lsf.addTokenSpecification(m2);
        lsf.addTokenSpecification(m3);

        LexicalSpecification ls = lsf.create();

        // Syntax
        
        SyntaxSpecificationFactory ssf = new SyntaxSpecificationFactory();
        Rule r1,r2,r3;

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("A"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("B"));

        r3 = new Rule(new RuleSymbol("Start"));
        r3.add(new RuleSymbol("C"));

        ssf.addRule(r1);
        ssf.addRule(r2);
        ssf.addRule(r3);
        ssf.setStartType("Start");

        ssf.addSelectionPrecedence(r1, r2);
        ssf.addSelectionPrecedence(r2, r3);

        SyntaxSpecification ss  = ssf.create();

        // Parse "abc"
        
        String input = "abc";
        StringReader sr = new StringReader(input);

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        Fence f = new Fence();
        SyntaxGraph sg = f.parse( new ParserMetadata(null), ss, lg);

        assertEquals(4,sg.getSymbols().size());
        assertEquals(2,sg.getRoots().size());
        assertNotNull(searchSymbol(sg,0,2,"A"));
        assertNull(searchSymbol(sg,0,3,"B"));
        assertNotNull(searchSymbol(sg,0,2,"C"));
        assertNotNull(searchSymbol(sg,0,2,"Start"));
        assertEquals(2,countSymbols(sg,0,2,"Start"));
    }
    
    
    @Test 
    public void testAnalysis1() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2;
        String input = "main main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Statement ::= InputStatement
        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("main"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(7,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,5,8,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
        assertNotNull(searchSymbol(pg,5,8,"Start"));
        assertNotNull(searchSymbol(pg,0,8,"Start"));
        assertTrue(pg.getFollowing(searchSymbol(pg,0,3,"main")).contains(searchSymbol(pg,5,8,"Start")));
        assertTrue(pg.getFollowing(searchSymbol(pg,0,3,"main")).contains(searchSymbol(pg,5,8,"main")));
        assertTrue(pg.getPreceding(searchSymbol(pg,5,8,"Start")).contains(searchSymbol(pg,0,3,"main")));
        assertTrue(pg.getPreceding(searchSymbol(pg,5,8,"main")).contains(searchSymbol(pg,0,3,"main")));
        assertTrue(pg.getPreceding(searchSymbol(pg,5,8,"main")).contains(searchSymbol(pg,0,3,"Start")));
        assertTrue(pg.getPreceding(searchSymbol(pg,5,8,"main")).contains(searchSymbol(pg,0,3,"main")));
        assertTrue(pg.getFollowing(searchSymbol(pg,0,3,"Start")).contains(searchSymbol(pg,5,8,"Start")));
        assertTrue(pg.getFollowing(searchSymbol(pg,0,3,"main")).contains(searchSymbol(pg,5,8,"main")));
        assertNull(pg.getFollowing(searchSymbol(pg,5,8,"main")));
        assertNull(pg.getFollowing(searchSymbol(pg,5,8,"Start")));
        assertNull(pg.getFollowing(searchSymbol(pg,0,8,"Start")));
        assertNull(pg.getPreceding(searchSymbol(pg,0,3,"main")));
        assertNull(pg.getPreceding(searchSymbol(pg,0,3,"Start")));
        assertNull(pg.getPreceding(searchSymbol(pg,0,8,"Start")));
    }

    
    @Test 
    public void testAnalysis2() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15;
        String input = "main { input(a); input(b); input(cd); output(5+1+(2*5)+3*5+3); }";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Statement ::= InputStatement
        r1 = new Rule(new RuleSymbol("Statement"));
        r1.add(new RuleSymbol("InputStatement"));

        //Statement ::= OutputStatement
        r2 = new Rule(new RuleSymbol("Statement"));
        r2.add(new RuleSymbol("OutputStatement"));

        //InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
        r3 = new Rule(new RuleSymbol("InputStatement"));
        r3.add(new RuleSymbol("input"));
        r3.add(new RuleSymbol("LeftParenthesis"));
        r3.add(new RuleSymbol("Identifier"));
        r3.add(new RuleSymbol("RightParenthesis"));
        r3.add(new RuleSymbol("Semicolon"));

        //OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
        r4 = new Rule(new RuleSymbol("OutputStatement"));
        r4.add(new RuleSymbol("output"));
        r4.add(new RuleSymbol("LeftParenthesis"));
        r4.add(new RuleSymbol("Expression"));
        r4.add(new RuleSymbol("RightParenthesis"));
        r4.add(new RuleSymbol("Semicolon"));

        //Expression ::= Expression Operator1 Expression
        r5 = new Rule(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Operator1"));
        r5.add(new RuleSymbol("Expression"));

        //Expression ::= Expression Operator2 Expression
        r6 = new Rule(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Operator2"));
        r6.add(new RuleSymbol("Expression"));

        //Expression ::= LeftParenthesis Expression RightParenthesis
        r7 = new Rule(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("LeftParenthesis"));
        r7.add(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("RightParenthesis"));

        //Expression ::= Operator1 Expression
        r8 = new Rule(new RuleSymbol("Expression"));
        r8.add(new RuleSymbol("Operator1"));
        r8.add(new RuleSymbol("Expression"));

        //Expression ::= Integer
        r9 = new Rule(new RuleSymbol("Expression"));
        r9.add(new RuleSymbol("Integer"));

        //Expression ::= Real
        r10 = new Rule(new RuleSymbol("Expression"));
        r10.add(new RuleSymbol("Real"));

        //Start ::= main LeftBracket StatementList RightBracket
        r11 = new Rule(new RuleSymbol("Start"));
        r11.add(new RuleSymbol("main"));
        r11.add(new RuleSymbol("LeftBracket"));
        r11.add(new RuleSymbol("StatementList"));
        r11.add(new RuleSymbol("RightBracket"));

        //StatementList ::=
        r12 = new Rule(new RuleSymbol("StatementList"));

        //StatementList ::= StatementListAny
        r13 = new Rule(new RuleSymbol("StatementList"));
        r13.add(new RuleSymbol("StatementListAny"));

        //StatementListAny ::= Statement
        r14 = new Rule(new RuleSymbol("StatementListAny"));
        r14.add(new RuleSymbol("Statement"));

        //StatementListAny ::= Statement StatementListAny
        r15 = new Rule(new RuleSymbol("StatementListAny"));
        r15.add(new RuleSymbol("Statement"));
        r15.add(new RuleSymbol("StatementListAny"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.addRule(r6);
        gf.addRule(r7);
        gf.addRule(r8);
        gf.addRule(r9);
        gf.addRule(r10);
        gf.addRule(r11);
        gf.addRule(r12);
        gf.addRule(r13);
        gf.addRule(r14);
        gf.addRule(r15);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        //assertEquals(1,pg.getStart().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,5,5,"LeftBracket"));
        assertNotNull(searchSymbol(pg,7,11,"input"));
        assertNotNull(searchSymbol(pg,12,12,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,13,13,"Identifier"));
        assertNotNull(searchSymbol(pg,14,14,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,15,15,"Semicolon"));
        assertNotNull(searchSymbol(pg,17,21,"input"));
        assertNotNull(searchSymbol(pg,22,22,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,23,23,"Identifier"));
        assertNotNull(searchSymbol(pg,24,24,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,25,25,"Semicolon"));
        assertNotNull(searchSymbol(pg,27,31,"input"));
        assertNotNull(searchSymbol(pg,32,32,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,33,34,"Identifier"));
        assertNotNull(searchSymbol(pg,35,35,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,36,36,"Semicolon"));
        assertNotNull(searchSymbol(pg,38,43,"output"));
        assertNotNull(searchSymbol(pg,44,44,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,45,45,"Integer"));
        assertNotNull(searchSymbol(pg,46,46,"Operator1"));
        assertNotNull(searchSymbol(pg,47,47,"Integer"));
        assertNotNull(searchSymbol(pg,48,48,"Operator1"));
        assertNotNull(searchSymbol(pg,49,49,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,50,50,"Integer"));
        assertNotNull(searchSymbol(pg,51,51,"Operator2"));
        assertNotNull(searchSymbol(pg,52,52,"Integer"));
        assertNotNull(searchSymbol(pg,53,53,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,54,54,"Operator1"));
        assertNotNull(searchSymbol(pg,55,55,"Integer"));
        assertNotNull(searchSymbol(pg,56,56,"Operator2"));
        assertNotNull(searchSymbol(pg,57,57,"Integer"));
        assertNotNull(searchSymbol(pg,58,58,"Operator1"));
        assertNotNull(searchSymbol(pg,59,59,"Integer"));
        assertNotNull(searchSymbol(pg,60,60,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,61,61,"Semicolon"));
        assertNotNull(searchSymbol(pg,63,63,"RightBracket"));

        assertNotNull(searchSymbol(pg,45,45,"Expression"));
        assertNotNull(searchSymbol(pg,47,47,"Expression"));
        assertNotNull(searchSymbol(pg,50,50,"Expression"));
        assertNotNull(searchSymbol(pg,52,52,"Expression"));
        assertNotNull(searchSymbol(pg,55,55,"Expression"));
        assertNotNull(searchSymbol(pg,57,57,"Expression"));
        assertNotNull(searchSymbol(pg,59,59,"Expression"));
        assertNotNull(searchSymbol(pg,45,47,"Expression"));
        assertNotNull(searchSymbol(pg,50,52,"Expression"));
        assertNotNull(searchSymbol(pg,49,53,"Expression"));
        assertNotNull(searchSymbol(pg,55,57,"Expression"));
        assertNotNull(searchSymbol(pg,57,59,"Expression"));
        assertNotNull(searchSymbol(pg,47,53,"Expression"));
        assertNotNull(searchSymbol(pg,49,55,"Expression"));
        assertNotNull(searchSymbol(pg,55,59,"Expression"));
        assertNotNull(searchSymbol(pg,45,53,"Expression"));
        assertNotNull(searchSymbol(pg,47,55,"Expression"));
        assertNotNull(searchSymbol(pg,49,57,"Expression"));
        assertNotNull(searchSymbol(pg,45,55,"Expression"));
        assertNotNull(searchSymbol(pg,47,57,"Expression"));
        assertNotNull(searchSymbol(pg,49,59,"Expression"));
        assertNotNull(searchSymbol(pg,45,57,"Expression"));
        assertNotNull(searchSymbol(pg,47,59,"Expression"));
        assertNotNull(searchSymbol(pg,45,59,"Expression"));

        assertNotNull(searchSymbol(pg,7,15,"InputStatement"));
        assertNotNull(searchSymbol(pg,17,25,"InputStatement"));
        assertNotNull(searchSymbol(pg,27,36,"InputStatement"));
        assertNotNull(searchSymbol(pg,38,61,"OutputStatement"));

        assertNotNull(searchSymbol(pg,7,15,"Statement"));
        assertNotNull(searchSymbol(pg,17,25,"Statement"));
        assertNotNull(searchSymbol(pg,27,36,"Statement"));
        assertNotNull(searchSymbol(pg,38,61,"Statement"));

        assertNotNull(searchSymbol(pg,7,15,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,25,"StatementListAny"));
        assertNotNull(searchSymbol(pg,27,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,38,61,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,25,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,27,61,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,61,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,61,"StatementListAny"));

        assertNotNull(searchSymbol(pg,0,63,"Start"));
    }

    @Test 
    public void testAnalysis3() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15;
        String input = "main { input(a); input(b); input(cd); output(5); }";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Statement ::= InputStatement
        r1 = new Rule(new RuleSymbol("Statement"));
        r1.add(new RuleSymbol("InputStatement"));

        //Statement ::= OutputStatement
        r2 = new Rule(new RuleSymbol("Statement"));
        r2.add(new RuleSymbol("OutputStatement"));

        //InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
        r3 = new Rule(new RuleSymbol("InputStatement"));
        r3.add(new RuleSymbol("input"));
        r3.add(new RuleSymbol("LeftParenthesis"));
        r3.add(new RuleSymbol("Identifier"));
        r3.add(new RuleSymbol("RightParenthesis"));
        r3.add(new RuleSymbol("Semicolon"));

        //OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
        r4 = new Rule(new RuleSymbol("OutputStatement"));
        r4.add(new RuleSymbol("output"));
        r4.add(new RuleSymbol("LeftParenthesis"));
        r4.add(new RuleSymbol("Expression"));
        r4.add(new RuleSymbol("RightParenthesis"));
        r4.add(new RuleSymbol("Semicolon"));

        //Expression ::= Expression Operator1 Expression
        r5 = new Rule(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Operator1"));
        r5.add(new RuleSymbol("Expression"));

        //Expression ::= Expression Operator2 Expression
        r6 = new Rule(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Operator2"));
        r6.add(new RuleSymbol("Expression"));

        //Expression ::= LeftParenthesis Expression RightParenthesis
        r7 = new Rule(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("LeftParenthesis"));
        r7.add(new RuleSymbol("Expression"));
        r7.add(new RuleSymbol("RightParenthesis"));

        //Expression ::= Operator1 Expression
        r8 = new Rule(new RuleSymbol("Expression"));
        r8.add(new RuleSymbol("Operator1"));
        r8.add(new RuleSymbol("Expression"));

        //Expression ::= Integer
        r9 = new Rule(new RuleSymbol("Expression"));
        r9.add(new RuleSymbol("Integer"));

        //Expression ::= Real
        r10 = new Rule(new RuleSymbol("Expression"));
        r10.add(new RuleSymbol("Real"));

        //Start ::= main LeftBracket StatementList RightBracket
        r11 = new Rule(new RuleSymbol("Start"));
        r11.add(new RuleSymbol("main"));
        r11.add(new RuleSymbol("LeftBracket"));
        r11.add(new RuleSymbol("StatementList"));
        r11.add(new RuleSymbol("RightBracket"));

        //StatementList ::=
        r12 = new Rule(new RuleSymbol("StatementList"));

        //StatementList ::= StatementListAny
        r13 = new Rule(new RuleSymbol("StatementList"));
        r13.add(new RuleSymbol("StatementListAny"));

        //StatementListAny ::= Statement
        r14 = new Rule(new RuleSymbol("StatementListAny"));
        r14.add(new RuleSymbol("Statement"));

        //StatementListAny ::= Statement StatementListAny
        r15 = new Rule(new RuleSymbol("StatementListAny"));
        r15.add(new RuleSymbol("Statement"));
        r15.add(new RuleSymbol("StatementListAny"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.addRule(r6);
        gf.addRule(r7);
        gf.addRule(r8);
        gf.addRule(r9);
        gf.addRule(r10);
        gf.addRule(r11);
        gf.addRule(r12);
        gf.addRule(r13);
        gf.addRule(r14);
        gf.addRule(r15);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(1,pg.getStart().size());
        assertEquals(59,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Identifier"));
        assertNotNull(searchSymbol(pg,5,5,"LeftBracket"));
        assertNotNull(searchSymbol(pg,7,11,"input"));
        assertNotNull(searchSymbol(pg,7,11,"Identifier"));
        assertNotNull(searchSymbol(pg,12,12,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,13,13,"Identifier"));
        assertNotNull(searchSymbol(pg,14,14,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,15,15,"Semicolon"));
        assertNotNull(searchSymbol(pg,17,21,"input"));
        assertNotNull(searchSymbol(pg,17,21,"Identifier"));
        assertNotNull(searchSymbol(pg,22,22,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,23,23,"Identifier"));
        assertNotNull(searchSymbol(pg,24,24,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,25,25,"Semicolon"));
        assertNotNull(searchSymbol(pg,27,31,"input"));
        assertNotNull(searchSymbol(pg,27,31,"Identifier"));
        assertNotNull(searchSymbol(pg,32,32,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,33,34,"Identifier"));
        assertNotNull(searchSymbol(pg,35,35,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,36,36,"Semicolon"));
        assertNotNull(searchSymbol(pg,38,43,"output"));
        assertNotNull(searchSymbol(pg,38,43,"Identifier"));
        assertNotNull(searchSymbol(pg,44,44,"LeftParenthesis"));
        assertNotNull(searchSymbol(pg,45,45,"Integer"));
        assertNotNull(searchSymbol(pg,46,46,"RightParenthesis"));
        assertNotNull(searchSymbol(pg,47,47,"Semicolon"));
        assertNotNull(searchSymbol(pg,49,49,"RightBracket"));

        assertNotNull(searchSymbol(pg,45,45,"Expression"));
        assertNotNull(searchSymbol(pg,44,46,"Expression"));


        assertNotNull(searchSymbol(pg,7,15,"InputStatement"));
        assertNotNull(searchSymbol(pg,17,25,"InputStatement"));
        assertNotNull(searchSymbol(pg,27,36,"InputStatement"));
        assertNotNull(searchSymbol(pg,38,47,"OutputStatement"));

        assertNotNull(searchSymbol(pg,7,15,"Statement"));
        assertNotNull(searchSymbol(pg,17,25,"Statement"));
        assertNotNull(searchSymbol(pg,27,36,"Statement"));
        assertNotNull(searchSymbol(pg,38,47,"Statement"));

        assertNotNull(searchSymbol(pg,7,15,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,25,"StatementListAny"));
        assertNotNull(searchSymbol(pg,27,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,38,47,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,25,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,27,47,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,36,"StatementListAny"));
        assertNotNull(searchSymbol(pg,17,47,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,47,"StatementListAny"));

        assertNotNull(searchSymbol(pg,7,15,"StatementList"));
        assertNotNull(searchSymbol(pg,17,25,"StatementList"));
        assertNotNull(searchSymbol(pg,27,36,"StatementList"));
        assertNotNull(searchSymbol(pg,38,47,"StatementList"));

        assertNotNull(searchSymbol(pg,7,25,"StatementList"));
        assertNotNull(searchSymbol(pg,17,36,"StatementList"));
        assertNotNull(searchSymbol(pg,27,47,"StatementList"));

        assertNotNull(searchSymbol(pg,7,36,"StatementList"));
        assertNotNull(searchSymbol(pg,17,47,"StatementList"));

        assertNotNull(searchSymbol(pg,7,47,"StatementList"));

        assertNotNull(searchSymbol(pg,0,49,"Start"));    
    }

    
    @Test 
    public void testAnalysis4() throws Exception 
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3,r4,r5,r6;
        String input = "5+1+2";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Expression ::= Expression Operator1 Expression
        r1 = new Rule(new RuleSymbol("Expression"));
        r1.add(new RuleSymbol("Expression"));
        r1.add(new RuleSymbol("Operator1"));
        r1.add(new RuleSymbol("Expression"));

        //Expression ::= Expression Operator2 Expression
        r2 = new Rule(new RuleSymbol("Expression"));
        r2.add(new RuleSymbol("Expression"));
        r2.add(new RuleSymbol("Operator2"));
        r2.add(new RuleSymbol("Expression"));

        //Expression ::= LeftParenthesis Expression RightParenthesis
        r3 = new Rule(new RuleSymbol("Expression"));
        r3.add(new RuleSymbol("LeftParenthesis"));
        r3.add(new RuleSymbol("Expression"));
        r3.add(new RuleSymbol("RightParenthesis"));

        //Expression ::= Operator1 Expression
        r4 = new Rule(new RuleSymbol("Expression"));
        r4.add(new RuleSymbol("Operator1"));
        r4.add(new RuleSymbol("Expression"));

        //Expression ::= Integer
        r5 = new Rule(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Integer"));

        //Expression ::= Real
        r6 = new Rule(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Real"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.addRule(r6);
        gf.setStartType("Expression");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(1,pg.getStart().size());
        assertEquals(16,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,0,"Integer"));
        assertNotNull(searchSymbol(pg,1,1,"Operator1"));
        assertNotNull(searchSymbol(pg,2,2,"Integer"));
        assertNotNull(searchSymbol(pg,1,2,"Integer"));
        assertNotNull(searchSymbol(pg,3,3,"Operator1"));
        assertNotNull(searchSymbol(pg,4,4,"Integer"));
        assertNotNull(searchSymbol(pg,3,4,"Integer"));
        assertNotNull(searchSymbol(pg,0,0,"Expression"));
        assertNotNull(searchSymbol(pg,1,2,"Expression"));
        assertNotNull(searchSymbol(pg,2,2,"Expression"));
        assertNotNull(searchSymbol(pg,3,4,"Expression"));
        assertNotNull(searchSymbol(pg,1,4,"Expression"));
        assertNotNull(searchSymbol(pg,4,4,"Expression"));
        assertNotNull(searchSymbol(pg,0,2,"Expression"));
        assertNotNull(searchSymbol(pg,2,4,"Expression"));
        assertNotNull(searchSymbol(pg,4,4,"Expression"));
    }

    
    @Test 
    public void testAnalysis5() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3,r4,r5,r6;
        String input = "5+1+2+1";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Expression ::= Expression Operator1 Expression
        r1 = new Rule(new RuleSymbol("Expression"));
        r1.add(new RuleSymbol("Expression"));
        r1.add(new RuleSymbol("Operator1"));
        r1.add(new RuleSymbol("Expression"));

        //Expression ::= Expression Operator2 Expression
        r2 = new Rule(new RuleSymbol("Expression"));
        r2.add(new RuleSymbol("Expression"));
        r2.add(new RuleSymbol("Operator2"));
        r2.add(new RuleSymbol("Expression"));

        //Expression ::= LeftParenthesis Expression RightParenthesis
        r3 = new Rule(new RuleSymbol("Expression"));
        r3.add(new RuleSymbol("LeftParenthesis"));
        r3.add(new RuleSymbol("Expression"));
        r3.add(new RuleSymbol("RightParenthesis"));

        //Expression ::= Operator1 Expression
        r4 = new Rule(new RuleSymbol("Expression"));
        r4.add(new RuleSymbol("Operator1"));
        r4.add(new RuleSymbol("Expression"));

        //Expression ::= Integer
        r5 = new Rule(new RuleSymbol("Expression"));
        r5.add(new RuleSymbol("Integer"));

        //Expression ::= Real
        r6 = new Rule(new RuleSymbol("Expression"));
        r6.add(new RuleSymbol("Real"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.addRule(r6);
        gf.setStartType("Expression");

        Grammar g = null;
        
        try {
            g = gf.create();
        } catch (NullElementException ex) {
            fail();
        }

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(1,pg.getStart().size());
        assertEquals(26,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,0,"Integer"));
        assertNotNull(searchSymbol(pg,1,1,"Operator1"));
        assertNotNull(searchSymbol(pg,2,2,"Integer"));
        assertNotNull(searchSymbol(pg,1,2,"Integer"));
        assertNotNull(searchSymbol(pg,3,3,"Operator1"));
        assertNotNull(searchSymbol(pg,4,4,"Integer"));
        assertNotNull(searchSymbol(pg,3,4,"Integer"));
        assertNotNull(searchSymbol(pg,5,5,"Operator1"));
        assertNotNull(searchSymbol(pg,6,6,"Integer"));
        assertNotNull(searchSymbol(pg,5,6,"Integer"));
        assertNotNull(searchSymbol(pg,0,0,"Expression"));
        assertNotNull(searchSymbol(pg,1,2,"Expression"));
        assertNotNull(searchSymbol(pg,2,2,"Expression"));
        assertNotNull(searchSymbol(pg,3,4,"Expression"));
        assertNotNull(searchSymbol(pg,1,4,"Expression"));
        assertNotNull(searchSymbol(pg,4,4,"Expression"));
        assertNotNull(searchSymbol(pg,0,2,"Expression"));
        assertNotNull(searchSymbol(pg,2,4,"Expression"));
        assertNotNull(searchSymbol(pg,4,4,"Expression"));
        assertNotNull(searchSymbol(pg,3,6,"Expression"));
        assertNotNull(searchSymbol(pg,1,6,"Expression"));
        assertNotNull(searchSymbol(pg,4,6,"Expression"));
        assertNotNull(searchSymbol(pg,2,6,"Expression"));
        assertNotNull(searchSymbol(pg,4,6,"Expression"));
        assertNotNull(searchSymbol(pg,5,6,"Expression"));
        assertNotNull(searchSymbol(pg,6,6,"Expression"));
    }

    
    @Test 
    public void testEmptyRule1() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3;
        String input = "main main";
        StringReader sr = new StringReader(input);
        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Separator"));
        r2.add(new RuleSymbol("main"));

        r3 = new Rule(new RuleSymbol("Separator"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(7,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,5,8,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
        assertNotNull(searchSymbol(pg,5,8,"Start"));
        assertNotNull(searchSymbol(pg,0,8,"Start"));
    }

    
    @Test 
    public void testEmptyRuleMatch() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2;
        String input = "";
        StringReader sr = new StringReader(input);
        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("Separator"));

        r2 = new Rule(new RuleSymbol("Separator"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(1,pg.getStart().size());
    }

    
    @Test 
    public void testEmptyRule2() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3;
        String input = "main main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("main"));
        r2.add(new RuleSymbol("Separator"));

        r3 = new Rule(new RuleSymbol("Separator"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(7,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,5,8,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
        assertNotNull(searchSymbol(pg,5,8,"Start"));
        assertNotNull(searchSymbol(pg,0,8,"Start"));

    }

    @Test 
    public void testChainedEmptyRule() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3,r4,r5;
        String input = "main main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("main"));
        r2.add(new RuleSymbol("Separator"));

        r3 = new Rule(new RuleSymbol("Separator"));
        r3.add(new RuleSymbol("Separator2"));
        r3.add(new RuleSymbol("Separator3"));

        r4 = new Rule(new RuleSymbol("Separator2"));
        r4.add(new RuleSymbol("Separator3"));

        r5 = new Rule(new RuleSymbol("Separator3"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);
        gf.addRule(r4);
        gf.addRule(r5);
        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(7,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,5,8,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
        assertNotNull(searchSymbol(pg,5,8,"Start"));
        assertNotNull(searchSymbol(pg,0,8,"Start"));

    }

    
    @Test 
    public void testCyclicRule1() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3;
        String input = "main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Object"));
        r1.add(new RuleSymbol("main"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Object"));

        r3 = new Rule(new RuleSymbol("Object"));
        r3.add(new RuleSymbol("Start"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);

        gf.setStartType("Start");

        Grammar g = gf.create();
        
        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(4,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
        assertNotNull(searchSymbol(pg,0,3,"Object"));
    }

    @Test 
    public void testCyclicRule2() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3;
        String input = "main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));
        r1.add(new RuleSymbol("Empty"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));

        r3 = new Rule(new RuleSymbol("Empty"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);

        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(3,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
    }
    

    @Test 
    public void testCyclicRule3() throws Exception 
    {
        LexicalSpecification ls = lexicalSpecification();
        Rule r1,r2,r3;
        String input = "main";
        StringReader sr = new StringReader(input);

        GrammarFactory gf = new GrammarFactory();

        //Statement ::= InputStatement
        r1 = new Rule(new RuleSymbol("Start"));
        r1.add(new RuleSymbol("main"));
        r1.add(new RuleSymbol("Empty"));

        r2 = new Rule(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Start"));
        r2.add(new RuleSymbol("Empty"));

        r3 = new Rule(new RuleSymbol("Empty"));

        gf.addRule(r1);
        gf.addRule(r2);
        gf.addRule(r3);

        gf.setStartType("Start");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(3,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
        assertNotNull(searchSymbol(pg,0,3,"Start"));
    }

    
    @Test 
    public void testSingleSymbolTest() throws Exception
    {
        LexicalSpecification ls = lexicalSpecification();
        String input = "main";
        StringReader sr = new StringReader(input);
        GrammarFactory gf = new GrammarFactory();
        
        //Statement ::= InputStatement

        gf.setStartType("main");

        Grammar g = gf.create();

        Lexer lamb = new LambLexer(ls);
        LexicalGraph lg = lamb.scan(sr);

        FenceGrammarParser fgp = new FenceGrammarParser();
        ParsedGraph pg = fgp.parse(g, lg);

        assertEquals(1,pg.getStart().size());
        assertEquals(2,pg.getSymbols().size());
        assertNotNull(searchSymbol(pg,0,3,"main"));
    }

}