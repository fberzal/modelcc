/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.parser;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;

import org.junit.Test;
import org.modelcc.AssociativityType;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.language.syntax.ParserMetadata;
import org.modelcc.language.syntax.SyntaxConstraintsFactory;
import org.modelcc.language.syntax.SyntaxConstraints;
import org.modelcc.language.syntax.Grammar;
import org.modelcc.language.syntax.GrammarFactory;
import org.modelcc.language.syntax.Rule;
import org.modelcc.language.syntax.RuleSymbol;
import org.modelcc.language.syntax.Symbol;
import org.modelcc.language.syntax.SymbolBuilder;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;
import org.modelcc.lexer.lamb.LambLexer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.fence.FenceConstraintEnforcer;
import org.modelcc.parser.fence.FenceGrammarParser;
import org.modelcc.parser.fence.ParsedGraph;
import org.modelcc.parser.fence.SyntaxGraph;

/**
 * Fence constraint enforcement test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class FenceConstraintEnforcerTest extends FenceTest
{
	private LexicalGraph getLexicalGraph (LexicalSpecification ls, String input)
	{
		StringReader sr = new StringReader(input);
		Lexer lamb = new LambLexer(ls);
		LexicalGraph lg = lamb.scan(sr);

		return lg;
	}
	
	private SyntaxGraph getSyntaxGraph (Grammar g, SyntaxConstraints c, LexicalGraph lg)
	{
		FenceGrammarParser fgp = new FenceGrammarParser();
		ParsedGraph pg = fgp.parse(g, lg);

		FenceConstraintEnforcer fce = new FenceConstraintEnforcer( new ParserMetadata(null), c);
		SyntaxGraph sg = fce.enforce(pg);		
		
		return sg;
	}



	@Test 
	public void testConstraintEnforcement1() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("main"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(4,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());
		assertNotNull(searchSymbol(sg,0,3,"main"));
		assertNotNull(searchSymbol(sg,0,3,"Start"));
		assertNotNull(searchSymbol(sg,5,8,"main"));
		assertNull(searchSymbol(sg,5,8,"Start"));
		assertNotNull(searchSymbol(sg,0,8,"Start"));
		assertTrue(sg.getRoots().contains(searchSymbol(sg,0,8,"Start")));
	}
	
	
	@Test 
	public void testConstraintEnforcement2() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Statement"));
		r1.add(new RuleSymbol("InputStatement"));

		//Statement ::= OutputStatement
		r2 = new Rule(new RuleSymbol("Statement"));
		r2.add(new RuleSymbol("OutputStatement"));

		//InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
		r3 = new Rule(new RuleSymbol("InputStatement"));
		r3.add(new RuleSymbol("input"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Identifier"));
		r3.add(new RuleSymbol("RightParenthesis"));
		r3.add(new RuleSymbol("Semicolon"));

		//OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
		r4 = new Rule(new RuleSymbol("OutputStatement"));
		r4.add(new RuleSymbol("output"));
		r4.add(new RuleSymbol("LeftParenthesis"));
		r4.add(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("RightParenthesis"));
		r4.add(new RuleSymbol("Semicolon"));

		//Expression ::= Expression Operator1 Expression
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Operator1"));
		r5.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Operator2"));
		r6.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("LeftParenthesis"));
		r7.add(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("Operator1"));
		r8.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r10 = new Rule(new RuleSymbol("Expression"));
		r10.add(new RuleSymbol("Real"));

		//Start ::= main LeftBracket StatementList RightBracket
		r11 = new Rule(new RuleSymbol("Start"));
		r11.add(new RuleSymbol("main"));
		r11.add(new RuleSymbol("LeftBracket"));
		r11.add(new RuleSymbol("StatementList"));
		r11.add(new RuleSymbol("RightBracket"));

		//StatementList ::=
		r12 = new Rule(new RuleSymbol("StatementList"));

		//StatementList ::= StatementListAny
		r13 = new Rule(new RuleSymbol("StatementList"));
		r13.add(new RuleSymbol("StatementListAny"));

		//StatementListAny ::= Statement
		r14 = new Rule(new RuleSymbol("StatementListAny"));
		r14.add(new RuleSymbol("Statement"));

		//StatementListAny ::= Statement StatementListAny
		r15 = new Rule(new RuleSymbol("StatementListAny"));
		r15.add(new RuleSymbol("Statement"));
		r15.add(new RuleSymbol("StatementListAny"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.addRule(r10);
		gf.addRule(r11);
		gf.addRule(r12);
		gf.addRule(r13);
		gf.addRule(r14);
		gf.addRule(r15);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main { input(a); input(b); input(cd); output(5); }";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
		assertEquals(38,sg.getSymbols().size());
		assertNotNull(searchSymbol(sg,0,3,"main"));
		assertNotNull(searchSymbol(sg,5,5,"LeftBracket"));
		assertNotNull(searchSymbol(sg,7,11,"input"));
		assertNotNull(searchSymbol(sg,12,12,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,13,13,"Identifier"));
		assertNotNull(searchSymbol(sg,14,14,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,15,15,"Semicolon"));
		assertNotNull(searchSymbol(sg,17,21,"input"));
		assertNotNull(searchSymbol(sg,22,22,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,23,23,"Identifier"));
		assertNotNull(searchSymbol(sg,24,24,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,25,25,"Semicolon"));
		assertNotNull(searchSymbol(sg,27,31,"input"));
		assertNotNull(searchSymbol(sg,32,32,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,33,34,"Identifier"));
		assertNotNull(searchSymbol(sg,35,35,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,36,36,"Semicolon"));
		assertNotNull(searchSymbol(sg,38,43,"output"));
		assertNotNull(searchSymbol(sg,44,44,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,45,45,"Integer"));
		assertNotNull(searchSymbol(sg,46,46,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,47,47,"Semicolon"));
		assertNotNull(searchSymbol(sg,49,49,"RightBracket"));

		assertNotNull(searchSymbol(sg,45,45,"Expression"));

		assertNotNull(searchSymbol(sg,7,15,"InputStatement"));
		assertEquals(5,searchSymbol(sg,7,15,"InputStatement").size());
		assertEquals("input",searchSymbol(sg,7,15,"InputStatement").getElement(0).getType());
		assertEquals("LeftParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(1).getType());
		assertEquals("Identifier",searchSymbol(sg,7,15,"InputStatement").getElement(2).getType());
		assertEquals("RightParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(3).getType());
		assertEquals("Semicolon",searchSymbol(sg,7,15,"InputStatement").getElement(4).getType());
		assertNotNull(searchSymbol(sg,17,25,"InputStatement"));
		assertNotNull(searchSymbol(sg,27,36,"InputStatement"));
		assertNotNull(searchSymbol(sg,38,47,"OutputStatement"));

		assertNotNull(searchSymbol(sg,7,15,"Statement"));
		assertNotNull(searchSymbol(sg,17,25,"Statement"));
		assertNotNull(searchSymbol(sg,27,36,"Statement"));
		assertNotNull(searchSymbol(sg,38,47,"Statement"));

		assertNull(searchSymbol(sg,7,15,"StatementListAny"));
		assertNull(searchSymbol(sg,17,25,"StatementListAny"));
		assertNull(searchSymbol(sg,27,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,38,47,"StatementListAny"));

		assertNull(searchSymbol(sg,7,25,"StatementListAny"));
		assertNull(searchSymbol(sg,17,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,27,47,"StatementListAny"));

		assertNull(searchSymbol(sg,7,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,17,47,"StatementListAny"));

		assertNotNull(searchSymbol(sg,7,47,"StatementListAny"));

		assertNotNull(searchSymbol(sg,7,47,"StatementList"));

		assertNotNull(searchSymbol(sg,0,49,"Start"));
		assertEquals(4,searchSymbol(sg,0,49,"Start").size());
		assertEquals("main",searchSymbol(sg,0,49,"Start").getElement(0).getType());
		assertEquals("LeftBracket",searchSymbol(sg,0,49,"Start").getElement(1).getType());
		assertEquals("StatementList",searchSymbol(sg,0,49,"Start").getElement(2).getType());
		assertEquals("RightBracket",searchSymbol(sg,0,49,"Start").getElement(3).getType());
	}

	
	@Test 
	public void testConstraintEnforcement3() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "5";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(2,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,0,0,"Expression"));
	}


	@Test 
	public void testAssociativity1() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();

		cf.setAssociativity("Operator1",AssociativityType.RIGHT_TO_LEFT);
		
		SyntaxConstraints c = cf.create();

		String input = "5+3+2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(10,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,1,1,"Operator1"));
		assertNotNull(searchSymbol(sg,2,2,"Integer"));
		assertNotNull(searchSymbol(sg,3,3,"Operator1"));
		assertNotNull(searchSymbol(sg,4,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Integer"));
		assertNull(searchSymbol(sg,3,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Expression"));

		assertNull(searchSymbol(sg,3,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,0,"Expression"));
		assertNotNull(searchSymbol(sg,2,2,"Expression"));
		assertNotNull(searchSymbol(sg,4,4,"Expression"));

		assertNull(searchSymbol(sg,0,2,"Expression"));
		assertNotNull(searchSymbol(sg,2,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,4,"Expression"));

		assertNull(searchSymbol(sg,1,4,"Expression"));

		assertEquals(searchSymbol(sg,1,1,"Operator1"),searchSymbolFirstContent(sg,0,4,"Expression",searchSymbol(sg,0,0,"Expression")).getContent(1));
		assertEquals(searchSymbol(sg,2,4,"Expression"),searchSymbolFirstContent(sg,0,4,"Expression",searchSymbol(sg,0,0,"Expression")).getContent(2));
	}


	@Test 
	public void testAssociativity2() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.setAssociativity("Operator1",AssociativityType.NON_ASSOCIATIVE);
		SyntaxConstraints c = cf.create();

		String input = "5+3+2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
	}


	@Test public void testPrecedence1() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addCompositionPrecedences(r2, r1);
		SyntaxConstraints c = cf.create();

		String input = "5+3*2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(10,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,1,1,"Operator1"));
		assertNotNull(searchSymbol(sg,2,2,"Integer"));
		assertNotNull(searchSymbol(sg,3,3,"Operator2"));
		assertNotNull(searchSymbol(sg,4,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Integer"));
		assertNull(searchSymbol(sg,3,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Expression"));

		assertNull(searchSymbol(sg,3,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,0,"Expression"));
		assertNotNull(searchSymbol(sg,2,2,"Expression"));
		assertNotNull(searchSymbol(sg,4,4,"Expression"));

		assertNotNull(searchSymbol(sg,2,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,4,"Expression"));

		assertNull(searchSymbol(sg,1,4,"Expression"));

		assertEquals(searchSymbol(sg,3,3,"Operator2"),searchSymbolFirstContent(sg,2,4,"Expression",searchSymbol(sg,2,2,"Expression")).getContent(1));
		assertEquals(searchSymbol(sg,4,4,"Expression"),searchSymbolFirstContent(sg,2,4,"Expression",searchSymbol(sg,2,2,"Expression")).getContent(2));
	}

	
	@Test 
	public void testPrecedence2() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addCompositionPrecedences(r1, r2);
		SyntaxConstraints c = cf.create();

		String input = "5+3*2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(10,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,1,1,"Operator1"));
		assertNotNull(searchSymbol(sg,2,2,"Integer"));
		assertNotNull(searchSymbol(sg,3,3,"Operator2"));
		assertNotNull(searchSymbol(sg,4,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Integer"));
		assertNull(searchSymbol(sg,3,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Expression"));

		assertNull(searchSymbol(sg,3,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,0,"Expression"));
		assertNotNull(searchSymbol(sg,2,2,"Expression"));
		assertNotNull(searchSymbol(sg,4,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,2,"Expression"));

		assertNotNull(searchSymbol(sg,0,4,"Expression"));

		assertNull(searchSymbol(sg,1,4,"Expression"));

		assertEquals(searchSymbol(sg,3,3,"Operator2"),searchSymbolFirstContent(sg,0,4,"Expression",searchSymbol(sg,0,2,"Expression")).getContent(1));
		assertEquals(searchSymbol(sg,4,4,"Expression"),searchSymbolFirstContent(sg,0,4,"Expression",searchSymbol(sg,0,2,"Expression")).getContent(2));
	}

	
	@Test 
	public void testPrecedence3() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("BinaryExpression1"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("BinaryExpression2"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("ParenthesizedExpression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("UnaryExpression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		//Expression ::= BinaryExpression1
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("BinaryExpression1"));

		//Expression ::= BinaryExpression2
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("BinaryExpression2"));

		//Expression ::= ParenthesizedExpression
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("ParenthesizedExpression"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addCompositionPrecedences(r2, r1);
		SyntaxConstraints c = cf.create();

		String input = "5+3*2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(12,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,1,1,"Operator1"));
		assertNotNull(searchSymbol(sg,2,2,"Integer"));
		assertNotNull(searchSymbol(sg,3,3,"Operator2"));
		assertNotNull(searchSymbol(sg,4,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Integer"));
		assertNull(searchSymbol(sg,3,4,"Integer"));
		assertNull(searchSymbol(sg,1,2,"Expression"));
		assertNull(searchSymbol(sg,3,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,0,"Expression"));
		assertNotNull(searchSymbol(sg,2,2,"Expression"));
		assertNotNull(searchSymbol(sg,4,4,"Expression"));
		assertNotNull(searchSymbol(sg,2,4,"Expression"));
		assertNotNull(searchSymbol(sg,2,4,"BinaryExpression2"));
		assertNotNull(searchSymbol(sg,0,4,"Expression"));
		assertNotNull(searchSymbol(sg,0,4,"BinaryExpression1"));

		assertNull(searchSymbol(sg,1,4,"Expression"));

		assertEquals(searchSymbol(sg,3,3,"Operator2"),searchSymbolFirstContent(sg,2,4,"BinaryExpression2",searchSymbol(sg,2,2,"Expression")).getContent(1));
		assertEquals(searchSymbol(sg,4,4,"Expression"),searchSymbolFirstContent(sg,2,4,"BinaryExpression2",searchSymbol(sg,2,2,"Expression")).getContent(2));
	}


	@Test 
	public void testPrecedence4() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9;
		GrammarFactory gf = new GrammarFactory();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("BinaryExpression1"));
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("BinaryExpression2"));
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("ParenthesizedExpression"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("UnaryExpression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		//Expression ::= BinaryExpression1
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("BinaryExpression1"));

		//Expression ::= BinaryExpression2
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("BinaryExpression2"));

		//Expression ::= ParenthesizedExpression
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("ParenthesizedExpression"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addCompositionPrecedences(r1, r2);
		SyntaxConstraints c = cf.create();

		String input = "2+3*2";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
		assertEquals(12,sg.getSymbols().size());

		assertNotNull(searchSymbol(sg,0,0,"Integer"));
		assertNotNull(searchSymbol(sg,1,1,"Operator1"));
		assertNotNull(searchSymbol(sg,2,2,"Integer"));
		assertNotNull(searchSymbol(sg,3,3,"Operator2"));
		assertNotNull(searchSymbol(sg,4,4,"Integer"));

		assertNull(searchSymbol(sg,1,2,"Integer"));
		assertNull(searchSymbol(sg,3,4,"Integer"));
		assertNull(searchSymbol(sg,1,2,"Expression"));
		assertNull(searchSymbol(sg,3,4,"Expression"));

		assertNotNull(searchSymbol(sg,0,0,"Expression"));
		assertNotNull(searchSymbol(sg,2,2,"Expression"));
		assertNotNull(searchSymbol(sg,4,4,"Expression"));
		assertNotNull(searchSymbol(sg,0,2,"Expression"));
		assertNotNull(searchSymbol(sg,0,2,"BinaryExpression1"));
		assertNotNull(searchSymbol(sg,0,4,"Expression"));
		assertNotNull(searchSymbol(sg,0,4,"BinaryExpression2"));

		assertNull(searchSymbol(sg,1,4,"Expression"));

		assertEquals(searchSymbol(sg,3,3,"Operator2"),searchSymbolFirstContent(sg,0,4,"BinaryExpression2",searchSymbol(sg,0,2,"Expression")).getContent(1));
		assertEquals(searchSymbol(sg,4,4,"Expression"),searchSymbolFirstContent(sg,0,4,"BinaryExpression2",searchSymbol(sg,0,2,"Expression")).getContent(2));
	}

	
	@Test 
	public void testSelectionPrecedence1() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Statement"));
		r1.add(new RuleSymbol("InputStatement"));

		//Statement ::= OutputStatement
		r2 = new Rule(new RuleSymbol("Statement"));
		r2.add(new RuleSymbol("OutputStatement"));

		//InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
		r3 = new Rule(new RuleSymbol("InputStatement"));
		r3.add(new RuleSymbol("input"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Identifier"));
		r3.add(new RuleSymbol("RightParenthesis"));
		r3.add(new RuleSymbol("Semicolon"));

		//OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
		r4 = new Rule(new RuleSymbol("OutputStatement"));
		r4.add(new RuleSymbol("output"));
		r4.add(new RuleSymbol("LeftParenthesis"));
		r4.add(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("RightParenthesis"));
		r4.add(new RuleSymbol("Semicolon"));

		//Expression ::= Expression Operator1 Expression
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Operator1"));
		r5.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Operator2"));
		r6.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("LeftParenthesis"));
		r7.add(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("Operator1"));
		r8.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r10 = new Rule(new RuleSymbol("Expression"));
		r10.add(new RuleSymbol("Real"));

		//Start ::= main LeftBracket StatementList RightBracket
		r11 = new Rule(new RuleSymbol("Start"));
		r11.add(new RuleSymbol("main"));
		r11.add(new RuleSymbol("LeftBracket"));
		r11.add(new RuleSymbol("StatementList"));
		r11.add(new RuleSymbol("RightBracket"));

		//StatementList ::=
		r12 = new Rule(new RuleSymbol("StatementList"));

		//StatementList ::= StatementListAny
		r13 = new Rule(new RuleSymbol("StatementList"));
		r13.add(new RuleSymbol("StatementListAny"));

		//StatementListAny ::= Statement
		r14 = new Rule(new RuleSymbol("StatementListAny"));
		r14.add(new RuleSymbol("Statement"));

		//StatementListAny ::= Statement StatementListAny
		r15 = new Rule(new RuleSymbol("StatementListAny"));
		r15.add(new RuleSymbol("Statement"));
		r15.add(new RuleSymbol("StatementListAny"));

		//Statement ::= FunctionCallStatement
		r16 = new Rule(new RuleSymbol("Statement"));
		r16.add(new RuleSymbol("FunctionCallStatement"));

		//OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
		r17 = new Rule(new RuleSymbol("FunctionCallStatement"));
		r17.add(new RuleSymbol("Identifier"));
		r17.add(new RuleSymbol("LeftParenthesis"));
		r17.add(new RuleSymbol("Expression"));
		r17.add(new RuleSymbol("RightParenthesis"));
		r17.add(new RuleSymbol("Semicolon"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.addRule(r10);
		gf.addRule(r11);
		gf.addRule(r12);
		gf.addRule(r13);
		gf.addRule(r14);
		gf.addRule(r15);
		gf.addRule(r16);
		gf.addRule(r17);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addSelectionPrecedences(r4, r17);
		SyntaxConstraints c = cf.create();

		String input = "main { input(a); input(b); input(cd); output(5); }";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(38,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());
		assertNotNull(searchSymbol(sg,0,3,"main"));
		assertNotNull(searchSymbol(sg,5,5,"LeftBracket"));
		assertNotNull(searchSymbol(sg,7,11,"input"));
		assertNotNull(searchSymbol(sg,12,12,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,13,13,"Identifier"));
		assertNotNull(searchSymbol(sg,14,14,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,15,15,"Semicolon"));
		assertNotNull(searchSymbol(sg,17,21,"input"));
		assertNotNull(searchSymbol(sg,22,22,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,23,23,"Identifier"));
		assertNotNull(searchSymbol(sg,24,24,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,25,25,"Semicolon"));
		assertNotNull(searchSymbol(sg,27,31,"input"));
		assertNotNull(searchSymbol(sg,32,32,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,33,34,"Identifier"));
		assertNotNull(searchSymbol(sg,35,35,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,36,36,"Semicolon"));
		assertNotNull(searchSymbol(sg,38,43,"output"));
		assertNotNull(searchSymbol(sg,44,44,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,45,45,"Integer"));
		assertNotNull(searchSymbol(sg,46,46,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,47,47,"Semicolon"));
		assertNotNull(searchSymbol(sg,49,49,"RightBracket"));
		assertNotNull(searchSymbol(sg,45,45,"Expression"));
		assertNotNull(searchSymbol(sg,7,15,"InputStatement"));
		assertEquals(5,searchSymbol(sg,7,15,"InputStatement").size());
		assertEquals("input",searchSymbol(sg,7,15,"InputStatement").getElement(0).getType());
		assertEquals("LeftParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(1).getType());
		assertEquals("Identifier",searchSymbol(sg,7,15,"InputStatement").getElement(2).getType());
		assertEquals("RightParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(3).getType());
		assertEquals("Semicolon",searchSymbol(sg,7,15,"InputStatement").getElement(4).getType());
		assertNotNull(searchSymbol(sg,17,25,"InputStatement"));
		assertNotNull(searchSymbol(sg,27,36,"InputStatement"));
		assertNotNull(searchSymbol(sg,38,47,"OutputStatement"));
		assertNotNull(searchSymbol(sg,7,15,"Statement"));
		assertNotNull(searchSymbol(sg,17,25,"Statement"));
		assertNotNull(searchSymbol(sg,27,36,"Statement"));
		assertNotNull(searchSymbol(sg,38,47,"Statement"));
		assertNull(searchSymbol(sg,7,15,"StatementListAny"));
		assertNull(searchSymbol(sg,17,25,"StatementListAny"));
		assertNull(searchSymbol(sg,27,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,38,47,"StatementListAny"));
		assertNull(searchSymbol(sg,7,25,"StatementListAny"));
		assertNull(searchSymbol(sg,17,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,27,47,"StatementListAny"));
		assertNull(searchSymbol(sg,7,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,17,47,"StatementListAny"));
		assertNotNull(searchSymbol(sg,7,47,"StatementListAny"));
		assertNotNull(searchSymbol(sg,7,47,"StatementList"));
		assertNotNull(searchSymbol(sg,0,49,"Start"));
		assertEquals(4,searchSymbol(sg,0,49,"Start").size());
		assertEquals("main",searchSymbol(sg,0,49,"Start").getElement(0).getType());
		assertEquals("LeftBracket",searchSymbol(sg,0,49,"Start").getElement(1).getType());
		assertEquals("StatementList",searchSymbol(sg,0,49,"Start").getElement(2).getType());
		assertEquals("RightBracket",searchSymbol(sg,0,49,"Start").getElement(3).getType());
	}


	@Test 
	public void testSelectionPrecedence2() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9,r10,r11,r12,r13,r14,r15,r16,r17;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Statement"));
		r1.add(new RuleSymbol("InputStatement"));

		//Statement ::= OutputStatement
		r2 = new Rule(new RuleSymbol("Statement"));
		r2.add(new RuleSymbol("OutputStatement"));

		//InputStatement ::= input LeftParenthesis Identifier RightParenthesis Semicolon
		r3 = new Rule(new RuleSymbol("InputStatement"));
		r3.add(new RuleSymbol("input"));
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Identifier"));
		r3.add(new RuleSymbol("RightParenthesis"));
		r3.add(new RuleSymbol("Semicolon"));

		//OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
		r4 = new Rule(new RuleSymbol("OutputStatement"));
		r4.add(new RuleSymbol("output"));
		r4.add(new RuleSymbol("LeftParenthesis"));
		r4.add(new RuleSymbol("Expression"));
		r4.add(new RuleSymbol("RightParenthesis"));
		r4.add(new RuleSymbol("Semicolon"));

		//Expression ::= Expression Operator1 Expression
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Operator1"));
		r5.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Operator2"));
		r6.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("LeftParenthesis"));
		r7.add(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("Operator1"));
		r8.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r10 = new Rule(new RuleSymbol("Expression"));
		r10.add(new RuleSymbol("Real"));

		//Start ::= main LeftBracket StatementList RightBracket
		r11 = new Rule(new RuleSymbol("Start"));
		r11.add(new RuleSymbol("main"));
		r11.add(new RuleSymbol("LeftBracket"));
		r11.add(new RuleSymbol("StatementList"));
		r11.add(new RuleSymbol("RightBracket"));

		//StatementList ::=
		r12 = new Rule(new RuleSymbol("StatementList"));

		//StatementList ::= StatementListAny
		r13 = new Rule(new RuleSymbol("StatementList"));
		r13.add(new RuleSymbol("StatementListAny"));

		//StatementListAny ::= Statement
		r14 = new Rule(new RuleSymbol("StatementListAny"));
		r14.add(new RuleSymbol("Statement"));

		//StatementListAny ::= Statement StatementListAny
		r15 = new Rule(new RuleSymbol("StatementListAny"));
		r15.add(new RuleSymbol("Statement"));
		r15.add(new RuleSymbol("StatementListAny"));

		//Statement ::= FunctionCallStatement
		r16 = new Rule(new RuleSymbol("Statement"));
		r16.add(new RuleSymbol("FunctionCallStatement"));

		//OutputStatement ::= output LeftParenthesis Expression RightParenthesis Semicolon
		r17 = new Rule(new RuleSymbol("FunctionCallStatement"));
		r17.add(new RuleSymbol("Identifier"));
		r17.add(new RuleSymbol("LeftParenthesis"));
		r17.add(new RuleSymbol("Expression"));
		r17.add(new RuleSymbol("RightParenthesis"));
		r17.add(new RuleSymbol("Semicolon"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.addRule(r10);
		gf.addRule(r11);
		gf.addRule(r12);
		gf.addRule(r13);
		gf.addRule(r14);
		gf.addRule(r15);
		gf.addRule(r16);
		gf.addRule(r17);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addSelectionPrecedences(r2, r16);
		SyntaxConstraints c = cf.create();

		String input = "main { input(a); input(b); input(cd); output(5); }";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(38,sg.getSymbols().size());
		assertEquals(1,sg.getRoots().size());
		assertNotNull(searchSymbol(sg,0,3,"main"));
		assertNotNull(searchSymbol(sg,5,5,"LeftBracket"));
		assertNotNull(searchSymbol(sg,7,11,"input"));
		assertNotNull(searchSymbol(sg,12,12,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,13,13,"Identifier"));
		assertNotNull(searchSymbol(sg,14,14,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,15,15,"Semicolon"));
		assertNotNull(searchSymbol(sg,17,21,"input"));
		assertNotNull(searchSymbol(sg,22,22,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,23,23,"Identifier"));
		assertNotNull(searchSymbol(sg,24,24,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,25,25,"Semicolon"));
		assertNotNull(searchSymbol(sg,27,31,"input"));
		assertNotNull(searchSymbol(sg,32,32,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,33,34,"Identifier"));
		assertNotNull(searchSymbol(sg,35,35,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,36,36,"Semicolon"));
		assertNotNull(searchSymbol(sg,38,43,"output"));
		assertNotNull(searchSymbol(sg,44,44,"LeftParenthesis"));
		assertNotNull(searchSymbol(sg,45,45,"Integer"));
		assertNotNull(searchSymbol(sg,46,46,"RightParenthesis"));
		assertNotNull(searchSymbol(sg,47,47,"Semicolon"));
		assertNotNull(searchSymbol(sg,49,49,"RightBracket"));
		assertNotNull(searchSymbol(sg,45,45,"Expression"));
		assertNotNull(searchSymbol(sg,7,15,"InputStatement"));
		assertEquals(5,searchSymbol(sg,7,15,"InputStatement").size());
		assertEquals("input",searchSymbol(sg,7,15,"InputStatement").getElement(0).getType());
		assertEquals("LeftParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(1).getType());
		assertEquals("Identifier",searchSymbol(sg,7,15,"InputStatement").getElement(2).getType());
		assertEquals("RightParenthesis",searchSymbol(sg,7,15,"InputStatement").getElement(3).getType());
		assertEquals("Semicolon",searchSymbol(sg,7,15,"InputStatement").getElement(4).getType());
		assertNotNull(searchSymbol(sg,17,25,"InputStatement"));
		assertNotNull(searchSymbol(sg,27,36,"InputStatement"));
		assertNotNull(searchSymbol(sg,38,47,"OutputStatement"));
		assertNotNull(searchSymbol(sg,7,15,"Statement"));
		assertNotNull(searchSymbol(sg,17,25,"Statement"));
		assertNotNull(searchSymbol(sg,27,36,"Statement"));
		assertNotNull(searchSymbol(sg,38,47,"Statement"));
		assertNull(searchSymbol(sg,7,15,"StatementListAny"));
		assertNull(searchSymbol(sg,17,25,"StatementListAny"));
		assertNull(searchSymbol(sg,27,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,38,47,"StatementListAny"));
		assertNull(searchSymbol(sg,7,25,"StatementListAny"));
		assertNull(searchSymbol(sg,17,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,27,47,"StatementListAny"));
		assertNull(searchSymbol(sg,7,36,"StatementListAny"));
		assertNotNull(searchSymbol(sg,17,47,"StatementListAny"));
		assertNotNull(searchSymbol(sg,7,47,"StatementListAny"));
		assertNotNull(searchSymbol(sg,7,47,"StatementList"));
		assertNotNull(searchSymbol(sg,0,49,"Start"));
		assertEquals(4,searchSymbol(sg,0,49,"Start").size());
		assertEquals("main",searchSymbol(sg,0,49,"Start").getElement(0).getType());
		assertEquals("LeftBracket",searchSymbol(sg,0,49,"Start").getElement(1).getType());
		assertEquals("StatementList",searchSymbol(sg,0,49,"Start").getElement(2).getType());
		assertEquals("RightBracket",searchSymbol(sg,0,49,"Start").getElement(3).getType());
	}

	
	@Test 
	public void testChainedSelectionPrecedence() throws Exception 
	{
		TokenSpecification m1,m2,m3;
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
		Rule r1,r2,r3;

		m1 = new TokenSpecification("A",new RegExpPatternRecognizer("abc"));
		m2 = new TokenSpecification("B",new RegExpPatternRecognizer("abc"));
		m3 = new TokenSpecification("C",new RegExpPatternRecognizer("abc"));

		lsf.addTokenSpecification(m1);
		lsf.addTokenSpecification(m2);
		lsf.addTokenSpecification(m3);

		LexicalSpecification ls = lsf.create();

		GrammarFactory gf = new GrammarFactory();

		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("A"));

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("B"));

		r3 = new Rule(new RuleSymbol("Start"));
		r3.add(new RuleSymbol("C"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.addSelectionPrecedences(r1, r2);
		cf.addSelectionPrecedences(r2, r3);
		SyntaxConstraints c = cf.create();

		String input = "abc";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(4,sg.getSymbols().size());
		assertEquals(2,sg.getRoots().size());
		assertNotNull(searchSymbol(sg,0,2,"A"));
		assertNull(searchSymbol(sg,0,3,"B"));
		assertNotNull(searchSymbol(sg,0,2,"C"));
		assertNotNull(searchSymbol(sg,0,2,"Start"));
		assertEquals(2,countSymbols(sg,0,2,"Start"));
	}


	@Test 
	public void testEmptySymbol() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("Separator"));

		r2 = new Rule(new RuleSymbol("Separator"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
	}


	@Test 
	public void testEmptySymbol2() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));
		r1.add(new RuleSymbol("Separator"));

		r2 = new Rule(new RuleSymbol("Separator"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
	}


	@Test 
	public void testOptional1() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));
		r1.add(new RuleSymbol("Empty"));

		r2 = new Rule(new RuleSymbol("Empty"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();
		
		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main 3";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
	}

	
	@Test 
	public void testOptional2() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));
		r1.add(new RuleSymbol("Empty"));

		r2 = new Rule(new RuleSymbol("Empty"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "3 main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
	}    



	@Test 
	public void testOptional3() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("Empty"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Empty"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main 3";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
	}

	
	@Test 
	public void testOptional4() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("Empty"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Empty"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "3 main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
	}        

	
	// Builders
	
	private class CountSymbolBuilder extends SymbolBuilder {

		private int count = 0;

		@Override
		public boolean build(Symbol t,ParserMetadata data) {
			count++;
			return true;
		}

		public int getCount() {
			return count;
		}

	}
	
	
	@Test 
	public void testBuilder1() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("main2"));
		r2.setBuilder(new SymbolBuilder() {
			@Override
			public boolean build(Symbol t, ParserMetadata data) {
				return false;
			}
		});

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
	}          


	@Test 
	public void testBuilder2() throws Exception 
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("main2"));
		r2.setBuilder(new SymbolBuilder() {
			@Override
			public boolean build(Symbol t, ParserMetadata data) {
				return true;
			}
		});

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(1,sg.getRoots().size());
	}      


	@Test 
	public void testPostBuilder() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		CountSymbolBuilder csb = new CountSymbolBuilder();

		r1 = new Rule(new RuleSymbol("Interm"));
		r1.add(new RuleSymbol("main"));
		r1.setPostBuilder(new SymbolBuilder() {
			@Override
			public boolean build(Symbol t, ParserMetadata data) {
				return false;
			}
		});

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("Interm"));
		r2.setBuilder(csb);


		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertEquals(0,sg.getRoots().size());
		assertEquals(1,csb.getCount());
	}          
	

	@Test 
	public void testCountBuilder() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2;
		GrammarFactory gf = new GrammarFactory();

		CountSymbolBuilder csb = new CountSymbolBuilder();

		//Statement ::= InputStatement
		r1 = new Rule(new RuleSymbol("Start"));
		r1.add(new RuleSymbol("main"));

		r2 = new Rule(new RuleSymbol("Start"));
		r2.add(new RuleSymbol("main2"));
		r2.setBuilder(csb);

		gf.addRule(r1);
		gf.addRule(r2);
		gf.setStartType("Start");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		SyntaxConstraints c = cf.create();

		String input = "main";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertNotNull(sg);
		assertEquals(0,csb.getCount());
	}      
	

	@Test 
	public void testBoundBuilder() throws Exception
	{
		LexicalSpecification ls = lexicalSpecification();
		Rule r1,r2,r3,r4,r5,r6,r7,r8,r9;
		GrammarFactory gf = new GrammarFactory();

		CountSymbolBuilder csb = new CountSymbolBuilder();

		//Expression ::= Expression Operator1 Expression
		r1 = new Rule(new RuleSymbol("BinaryExpression1"));
		r1.setBuilder(csb);
		r1.add(new RuleSymbol("Expression"));
		r1.add(new RuleSymbol("Operator1"));
		r1.add(new RuleSymbol("Expression"));

		//Expression ::= Expression Operator2 Expression
		r2 = new Rule(new RuleSymbol("BinaryExpression2"));
		r2.setBuilder(csb);
		r2.add(new RuleSymbol("Expression"));
		r2.add(new RuleSymbol("Operator2"));
		r2.add(new RuleSymbol("Expression"));

		//Expression ::= LeftParenthesis Expression RightParenthesis
		r3 = new Rule(new RuleSymbol("ParenthesizedExpression"));
		r3.setBuilder(csb);
		r3.add(new RuleSymbol("LeftParenthesis"));
		r3.add(new RuleSymbol("Expression"));
		r3.add(new RuleSymbol("RightParenthesis"));

		//Expression ::= Operator1 Expression
		r4 = new Rule(new RuleSymbol("UnaryExpression"));
		r4.add(new RuleSymbol("Operator1"));
		r4.add(new RuleSymbol("Expression"));

		//Expression ::= Integer
		r5 = new Rule(new RuleSymbol("Expression"));
		r5.add(new RuleSymbol("Integer"));

		//Expression ::= Real
		r6 = new Rule(new RuleSymbol("Expression"));
		r6.add(new RuleSymbol("Real"));

		//Expression ::= BinaryExpression1
		r7 = new Rule(new RuleSymbol("Expression"));
		r7.add(new RuleSymbol("BinaryExpression1"));

		//Expression ::= BinaryExpression2
		r8 = new Rule(new RuleSymbol("Expression"));
		r8.add(new RuleSymbol("BinaryExpression2"));

		//Expression ::= ParenthesizedExpression
		r9 = new Rule(new RuleSymbol("Expression"));
		r9.add(new RuleSymbol("ParenthesizedExpression"));

		gf.addRule(r1);
		gf.addRule(r2);
		gf.addRule(r3);
		gf.addRule(r4);
		gf.addRule(r5);
		gf.addRule(r6);
		gf.addRule(r7);
		gf.addRule(r8);
		gf.addRule(r9);
		gf.setStartType("Expression");

		Grammar g = gf.create();

		SyntaxConstraintsFactory cf = new SyntaxConstraintsFactory();
		cf.setAssociativity("Operator1", AssociativityType.LEFT_TO_RIGHT);
		cf.setAssociativity("Operator2", AssociativityType.LEFT_TO_RIGHT);
		SyntaxConstraints c = cf.create();

		String input = "5+3+2+1+2+1+1+1+5+3+2+1+2+1+1+1+5+3+2+1+2+1+1+1+5+3+2+1+2+1+1+1+5+3+2+1+2+1+1+1";
		LexicalGraph lg = getLexicalGraph(ls,input);
		SyntaxGraph sg = getSyntaxGraph(g,c,lg);

		assertNotNull(sg);
		assertEquals(39,csb.getCount()); // vs. 780 
	}      
}
