/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.parser;

import java.util.Iterator;

import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.language.syntax.Symbol;
import org.modelcc.language.syntax.InputSymbol;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.fence.ParsedGraph;
import org.modelcc.parser.fence.ParsedSymbol;
import org.modelcc.parser.fence.SyntaxGraph;

/**
 * Fence parser test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class FenceTest 
{
	protected static InputSymbol searchSymbol(ParsedGraph sg,int start,int end,Object type) 
	{
		InputSymbol s;
		Iterator<ParsedSymbol> ite;
		for (ite = sg.getSymbols().iterator();ite.hasNext();) {
			s = ite.next();
			if (s.getStartIndex()==start && s.getEndIndex()==end && s.getType()==type)
				return s;
		}
		return null;
	}

	protected static Symbol searchSymbol(SyntaxGraph sg,int start,int end,Object type) 
	{
		Symbol s;
		Iterator<Symbol> ite;
		for (ite = sg.getSymbols().iterator();ite.hasNext();) {
			s = ite.next();
			if (s.getStartIndex()==start && s.getEndIndex()==end && s.getType()==type)
				return s;
		}
		return null;
	}

	
	protected static int countSymbols(ParsedGraph sg,int start,int end,Object type) 
	{
		InputSymbol s;
		Iterator<ParsedSymbol> ite;
		int count = 0;
		for (ite = sg.getSymbols().iterator();ite.hasNext();) {
			s = ite.next();
			if (s.getStartIndex()==start && s.getEndIndex()==end && s.getType()==type)
				count++;
		}
		return count;
	}

	protected static int countSymbols(SyntaxGraph sg,int start,int end,Object type) 
	{
		Symbol s;
		Iterator<Symbol> ite;
		int count = 0;
		for (ite = sg.getSymbols().iterator();ite.hasNext();) {
			s = ite.next();
			if (s.getStartIndex()==start && s.getEndIndex()==end && s.getType()==type)
				count++;
		}
		return count;
	}


	public static Symbol searchSymbolFirstContent(SyntaxGraph sg,int start,int end,Object type,Symbol first) 
	{
		Symbol s;
		Iterator<Symbol> ite;
		for (ite = sg.getSymbols().iterator();ite.hasNext();) {
			s = ite.next();
			if (s.size()>0)
				if (s.getStartIndex()==start && s.getEndIndex()==end && s.getType()==type && s.getContent(0) == first)
					return s;
		}
		return null;
	}


	// Lexical specification for test language

	protected LexicalSpecification lexicalSpecification () throws Exception
	{
        LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
        TokenSpecification m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19;

        m1 = new TokenSpecification("Space",new RegExpPatternRecognizer(" +"));
        m2 = new TokenSpecification("Tab",new RegExpPatternRecognizer("\\t+"));
        m3 = new TokenSpecification("NewLine",new RegExpPatternRecognizer("\\n|\\r+"));
        m4 = new TokenSpecification("Integer",new RegExpPatternRecognizer("(-|\\+)?[0-9]+"));
        m5 = new TokenSpecification("Real",new RegExpPatternRecognizer("(-|\\+)?[0-9]+\\.[0-9]*"));
        m6 = new TokenSpecification("Point",new RegExpPatternRecognizer("\\."));
        m7 = new TokenSpecification("Identifier",new RegExpPatternRecognizer("[a-z]+"));
        m8 = new TokenSpecification("Operator1",new RegExpPatternRecognizer("-|\\+"));
        m9 = new TokenSpecification("Operator2",new RegExpPatternRecognizer("\\*|\\/"));
        m10 = new TokenSpecification("Slash",new RegExpPatternRecognizer("\\/"));
        m11 = new TokenSpecification("Colon",new RegExpPatternRecognizer(":"));
        m12 = new TokenSpecification("Semicolon",new RegExpPatternRecognizer(";"));
        m13 = new TokenSpecification("LeftParenthesis",new RegExpPatternRecognizer("\\("));
        m14 = new TokenSpecification("RightParenthesis",new RegExpPatternRecognizer("\\)"));
        m15 = new TokenSpecification("LeftBracket",new RegExpPatternRecognizer("\\{"));
        m16 = new TokenSpecification("RightBracket",new RegExpPatternRecognizer("\\}"));
        m17 = new TokenSpecification("main",new RegExpPatternRecognizer("main"));
        m18 = new TokenSpecification("input",new RegExpPatternRecognizer("input"));
        m19 = new TokenSpecification("output",new RegExpPatternRecognizer("output"));

        lsf.skipTokenSpecification(m1);
        lsf.skipTokenSpecification(m2);
        lsf.skipTokenSpecification(m3);
        lsf.addTokenSpecification(m4);
        lsf.addTokenSpecification(m5);
        lsf.addTokenSpecification(m6);
        lsf.addTokenSpecification(m7);
        lsf.addTokenSpecification(m8);
        lsf.addTokenSpecification(m9);
        lsf.addTokenSpecification(m10);
        lsf.addTokenSpecification(m11);
        lsf.addTokenSpecification(m12);
        lsf.addTokenSpecification(m13);
        lsf.addTokenSpecification(m14);
        lsf.addTokenSpecification(m15);
        lsf.addTokenSpecification(m16);
        lsf.addTokenSpecification(m17);
        lsf.addTokenSpecification(m18);
        lsf.addTokenSpecification(m19);

        LexicalSpecification ls = lsf.create();

        return ls;
	}
	
}