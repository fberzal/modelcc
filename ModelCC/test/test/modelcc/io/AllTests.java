package test.modelcc.io;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { 
	JavaModelReaderTest.class,
	MessageLoggerTest.class,
    ReaderCharSequenceTest.class  } )
public class AllTests {

}