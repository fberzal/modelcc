package test.modelcc.io;

import static org.junit.Assert.*;

import java.util.logging.Level;

import org.junit.Test;
import org.modelcc.io.log.Message;
import org.modelcc.io.log.MessageLogger;

/**
 * Message logger JUnit test cases
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class MessageLoggerTest 
{
	@Test
	public void testConstructor() 
	{
		MessageLogger logger = new MessageLogger();
		
		assertEquals (0, logger.getMessageCount());
	}
		
	@Test
	public void testMessageString ()
	{
		MessageLogger logger = new MessageLogger();
		
		logger.log(Level.WARNING, "Warning message");
		
		assertEquals (1, logger.getMessageCount());

		Message message = logger.getMessage(0);
		
		assertEquals ( "Warning message", message.getMessage() );
		assertEquals ( Level.WARNING, message.getLevel() );
		assertEquals ( "test.modelcc.io.MessageLoggerTest", message.getSource() );
		assertNull ( message.getCause() );
		assertEquals ( "[test.modelcc.io.MessageLoggerTest] Warning message", message.toString());
	}

	@Test
	public void testMessageParameters ()
	{
		MessageLogger logger = new MessageLogger();
		
		logger.log(Level.WARNING, "{0} by {1}", new Object[]{"Message", "sender"});
		
		assertEquals (1, logger.getMessageCount());

		Message message = logger.getMessage(0);
		
		assertEquals ( "Message by sender", message.getMessage() );
		assertEquals ( Level.WARNING, message.getLevel() );
		assertEquals ( "test.modelcc.io.MessageLoggerTest", message.getSource() );
		assertNull ( message.getCause() );
		assertEquals ( "[test.modelcc.io.MessageLoggerTest] Message by sender", message.toString());
	}

	@Test
	public void testMessageException ()
	{
		MessageLogger logger = new MessageLogger();
		
		logger.log(Level.WARNING, "Exception", new UnsupportedOperationException() );
		
		assertEquals (1, logger.getMessageCount());

		Message message = logger.getMessage(0);
		
		assertEquals ( "Exception", message.getMessage() );
		assertEquals ( Level.WARNING, message.getLevel() );
		assertEquals ( "test.modelcc.io.MessageLoggerTest", message.getSource() );
		assertEquals ( UnsupportedOperationException.class, message.getCause().getClass() );
		assertEquals ( "[test.modelcc.io.MessageLoggerTest] Exception - java.lang.UnsupportedOperationException", message.toString());
	}
	
}
