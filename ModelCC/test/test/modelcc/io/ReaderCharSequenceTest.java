/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.io;

import static org.junit.Assert.assertEquals;

import java.io.StringReader;

import org.junit.Test;
import org.modelcc.io.file.ReaderCharSequence;

/**
 * Reader char sequence test suite
 * 
 * @author Fernando Berzal (fberzal@modelcc.org)
 */
public class ReaderCharSequenceTest 
{

	public ReaderCharSequence create (String string)
	{
		return new ReaderCharSequence( new StringReader(string) );
	}
	
	@Test
	public void testEmptyString ()
	{
		ReaderCharSequence rcs = create("");
		
		assertEquals('\uFFFF', rcs.charAt(0));
		assertEquals(-1, rcs.lineAt(0));
		assertEquals(0, rcs.length());
	}
	
	@Test
	public void testSingleLine ()
	{
		String string = "123456789";
		ReaderCharSequence rcs = create(string);
		
		for (int i=0; i<string.length(); i++) {
			assertEquals(string.charAt(i), rcs.charAt(i));
			assertEquals(1, rcs.lineAt(i));
		}
	
		assertEquals(-1, rcs.lineAt(9));
		assertEquals(9, rcs.length());
		
		for (int i=0; i<string.length(); i++) {
			assertEquals(string.charAt(i), rcs.charAt(i));
			assertEquals(1, rcs.lineAt(i));
		}
	}

	@Test
	public void testTwoLines ()
	{
		String string = "1\n2";
		ReaderCharSequence rcs = create(string);

		assertEquals(string.charAt(0), rcs.charAt(0));
		assertEquals(1, rcs.lineAt(0));

		assertEquals(string.charAt(1), rcs.charAt(1));
		assertEquals(1, rcs.lineAt(1));

		assertEquals(string.charAt(2), rcs.charAt(2));
		assertEquals(2, rcs.lineAt(2));
	
		assertEquals('\uFFFF', rcs.charAt(3));
		assertEquals(-1, rcs.lineAt(3));
		
		assertEquals(3, rcs.length());
	}

	@Test
	public void testSeveralLines ()
	{
		String string = "1\n2\n3\n4\n5\n6\n7\n8\n9\n";
		ReaderCharSequence rcs = create(string);

		for (int i=1; i<10; i++) {
			assertEquals(string.charAt(2*i-2), rcs.charAt(2*i-2));
			assertEquals(i, rcs.lineAt(2*i-2));
			assertEquals(string.charAt(2*i-1), rcs.charAt(2*i-1));
			assertEquals(i, rcs.lineAt(2*i-1));
		}

		assertEquals('\uFFFF', rcs.charAt(string.length()));
		assertEquals(-1, rcs.lineAt(string.length()));
		
		assertEquals(string.length(), rcs.length());
	}

	@Test
	public void testSeveralLinesWithPruning()
	{
		String string = "1\n2\n3\n4\n5\n6\n7\n8\n9\n";
		ReaderCharSequence rcs = create(string);

		for (int i=1; i<10; i++) {
			assertEquals(string.charAt(2*i-2), rcs.charAt(2*i-2));
			assertEquals(i, rcs.lineAt(2*i-2));
			assertEquals(string.charAt(2*i-1), rcs.charAt(2*i-1));
			assertEquals(i, rcs.lineAt(2*i-1));
			
			rcs.prune(2*i);
		}

		assertEquals('\uFFFF', rcs.charAt(string.length()));
		assertEquals(-1, rcs.lineAt(string.length()));
		
		assertEquals(string.length(), rcs.length());
	}
	
}
