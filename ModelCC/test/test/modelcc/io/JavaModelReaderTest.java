/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.io;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Filter;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.junit.Test;
import org.modelcc.AssociativityType;
import org.modelcc.CompositionType;
import org.modelcc.Position;
import org.modelcc.io.ModelReader;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.io.java.ClassDoesNotExtendIModelException;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.io.java.Reflection;
import org.modelcc.io.log.Message;
import org.modelcc.language.metamodel.LanguageElement;
import org.modelcc.language.metamodel.SimpleLanguageElement;
import org.modelcc.language.metamodel.MemberCollection;
import org.modelcc.language.metamodel.MemberCollectionType;
import org.modelcc.language.metamodel.CompositeLanguageElement;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.language.metamodel.LanguageMember;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserException;
import org.modelcc.parser.ParserFactory;

import model.examples.inheritance.NestedInherits;
import model.examples.test.*;
import model.examples.warnings.*;
import model.examples.wrong.*;

import test.languages.arithmetic.model.*;

public class JavaModelReaderTest 
{
    private class CountFilter implements Filter {

        boolean show;

        private int count;

        public CountFilter(boolean show) {
            this.show = show;
        }

        @Override
		public boolean isLoggable(LogRecord record) {
            if (record.getLevel() == Level.SEVERE) {
                count++;
            }
            return show;
        }

        int getCount() { return count; }
    }

    
    private boolean checkPrec(LanguageModel m,Class c1,Class c2) 
    {
        if (m.getPrecedences().get(m.getClassToElement().get(c1)) != null)
            if (m.getPrecedences().get(m.getClassToElement().get(c1)).contains(m.getClassToElement().get(c2)))
                return true;
        return false;
            
    }

    private LanguageModel modelGen(Class cls) 
    {
        ModelReader<LanguageModel> jmr = new JavaLanguageReader(cls);
        LanguageModel m = null;

        try {
            m = jmr.read();
        } catch (Exception ex) {
            fail();
        }
        return m;
    }

    private List<Message> modelWarnings(Class cl) 
    {
        JavaModelReader jmr = new JavaLanguageReader(cl);
        try {
            jmr.read();
        } catch (Exception ex) {
            fail();
        }
        return jmr.getMessages();
    }

    @Test
    public void runTimeFindSubClassesCheck1()
    	throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
    {
    	Class[] args = new Class[] { String.class,  Class.class};
    	Method m = Reflection.class.getDeclaredMethod("findSubclasses", args);
    
        m.setAccessible(true);
        
        Set<Class> classes = (Set<Class>) m.invoke(Reflection.class,Expression.class.getPackage().getName(),Expression.class);

        assertTrue(classes.contains(UnaryExpression.class));
        assertTrue(classes.contains(BinaryExpression.class));
        assertTrue(classes.contains(ExpressionGroup.class));
        assertTrue(classes.contains(IntegerLiteral.class));
        assertTrue(classes.contains(RealLiteral.class));
        assertTrue(classes.contains(LiteralExpression.class));
        assertEquals(6,classes.size());
    }

    @Test
    public void runTimeFindSubClassesCheck2() 
        throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
    {
        Class[] args = new Class[] { String.class, Class.class};
        Method m = Reflection.class.getDeclaredMethod("findSubclasses", args);

        m.setAccessible(true);

        Set<Class> classes = (Set<Class>) m.invoke(Reflection.class,LiteralExpression.class.getPackage().getName(),LiteralExpression.class);

        assertTrue(classes.contains(IntegerLiteral.class));
        assertTrue(classes.contains(RealLiteral.class));
        assertEquals(2,classes.size());
    }

    @Test
    public void runTimeFindSubClassesCheck3() 
    	throws NoSuchMethodException, SecurityException, IllegalAccessException, IllegalArgumentException, InvocationTargetException 
    {
        Class[] args = new Class[] { String.class, Class.class};
        Method m = Reflection.class.getDeclaredMethod("findSubclasses", args);

        m.setAccessible(true);

        Set<Class> classes = (Set<Class>) m.invoke(Reflection.class,NestedInherits.A.class.getPackage().getName(),NestedInherits.A.class);

        assertTrue(classes.contains(NestedInherits.A.B.class));
        assertTrue(classes.contains(NestedInherits.C.class));
        assertEquals(2,classes.size());
    }


    @Test
    public void modelReadTest1() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(Expression.class);
        
        assertNotNull(m);
        assertEquals(0,c.getCount());
        assertEquals(15,m.getElements().size());
        CompositeLanguageElement ce;
        SimpleLanguageElement be;
        LanguageElement se;

        LanguageMember sc;
        be = (SimpleLanguageElement) m.getClassToElement().get(AdditionOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(AdditionOperator.class,be.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(DivisionOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(DivisionOperator.class,be.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(MultiplicationOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(MultiplicationOperator.class,be.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(SubstractionOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(SubstractionOperator.class,be.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(IntegerLiteral.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(IntegerLiteral.class,be.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNotNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(RealLiteral.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(RealLiteral.class,be.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNotNull(be.getValueField());
        assertNull(be.getSetupMethod());

        ce = (CompositeLanguageElement) m.getClassToElement().get(BinaryExpression.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(BinaryExpression.class,ce.getElementClass());
        assertEquals(3,ce.getMembers().size());
        assertEquals(Position.AFTER,ce.getPositions().get(ce.getMembers().get(2)).getPosition()[0]);
        assertEquals(ce.getMembers().get(1),ce.getPositions().get(ce.getMembers().get(2)).getMember());
        sc = ce.getMembers().get(0);
        assertEquals("e1",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        sc = ce.getMembers().get(1);
        assertEquals("op",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        sc = ce.getMembers().get(2);
        assertEquals("e2",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());
        assertEquals(false,ce.isFreeOrder());
        assertEquals(AssociativityType.UNDEFINED,ce.getAssociativity());
        assertEquals(CompositionType.UNDEFINED,ce.getComposition());
        assertEquals(0,ce.getPrefix().size());
        assertEquals(0,ce.getSuffix().size());
        assertEquals(0,ce.getSeparator().size());
        assertNull(ce.getSetupMethod());

        se = m.getClassToElement().get(LiteralExpression.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(LiteralExpression.class,se.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,se.getAssociativity());
        assertEquals(0,se.getPrefix().size());
        assertEquals(0,se.getSuffix().size());
        assertEquals(0,se.getSeparator().size());
        assertNull(se.getSetupMethod());
        assertTrue(m.getSubelements().get(m.getClassToElement().get(LiteralExpression.class)).contains(m.getClassToElement().get(IntegerLiteral.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(LiteralExpression.class)).contains(m.getClassToElement().get(RealLiteral.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(IntegerLiteral.class)).equals(m.getClassToElement().get(LiteralExpression.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(RealLiteral.class)).equals(m.getClassToElement().get(LiteralExpression.class)));

        ce = (CompositeLanguageElement) m.getClassToElement().get(ExpressionGroup.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(ExpressionGroup.class,ce.getElementClass());
        assertEquals(1,ce.getMembers().size());
        sc = ce.getMembers().get(0);
        assertEquals("e",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        assertEquals(false,ce.isFreeOrder());
        assertEquals(AssociativityType.UNDEFINED,ce.getAssociativity());
        assertEquals(CompositionType.UNDEFINED,ce.getComposition());
        assertEquals(1,ce.getPrefix().size());
        assertEquals("\\(",((RegExpPatternRecognizer)ce.getPrefix().get(0)).getRegExp());
        assertEquals(1,ce.getSuffix().size());
        assertEquals("\\)",((RegExpPatternRecognizer)ce.getSuffix().get(0)).getRegExp());
        assertEquals(0,ce.getSeparator().size());
        assertNull(ce.getSetupMethod());

        ce = (CompositeLanguageElement) m.getClassToElement().get(UnaryExpression.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(UnaryExpression.class,ce.getElementClass());
        assertEquals(2,ce.getMembers().size());
        sc = ce.getMembers().get(0);
        assertEquals("op",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        sc = ce.getMembers().get(1);
        assertEquals("e",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        assertEquals(false,ce.isFreeOrder());
        assertEquals(AssociativityType.UNDEFINED,ce.getAssociativity());
        assertEquals(CompositionType.UNDEFINED,ce.getComposition());
        assertEquals(0,ce.getPrefix().size());
        assertEquals(0,ce.getSuffix().size());
        assertEquals(0,ce.getSeparator().size());
        assertNull(ce.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(MinusOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(MinusOperator.class,be.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        be = (SimpleLanguageElement) m.getClassToElement().get(PlusOperator.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(PlusOperator.class,be.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,be.getAssociativity());
        assertEquals(0,be.getPrefix().size());
        assertEquals(0,be.getSuffix().size());
        assertEquals(0,be.getSeparator().size());
        assertNotNull(be.getPatternRecognizer());
        assertNull(be.getValueField());
        assertNull(be.getSetupMethod());

        se = m.getClassToElement().get(BinaryOperator.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(BinaryOperator.class,se.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,se.getAssociativity());
        assertEquals(0,se.getPrefix().size());
        assertEquals(0,se.getSuffix().size());
        assertEquals(0,se.getSeparator().size());
        assertNull(se.getSetupMethod());
        assertTrue(m.getSubelements().get(m.getClassToElement().get(BinaryOperator.class)).contains(m.getClassToElement().get(AdditionOperator.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(BinaryOperator.class)).contains(m.getClassToElement().get(SubstractionOperator.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(BinaryOperator.class)).contains(m.getClassToElement().get(MultiplicationOperator.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(BinaryOperator.class)).contains(m.getClassToElement().get(DivisionOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(AdditionOperator.class)).equals(m.getClassToElement().get(BinaryOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(SubstractionOperator.class)).equals(m.getClassToElement().get(BinaryOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(DivisionOperator.class)).equals(m.getClassToElement().get(BinaryOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(MultiplicationOperator.class)).equals(m.getClassToElement().get(BinaryOperator.class)));

        se = m.getClassToElement().get(Expression.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(Expression.class,se.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,se.getAssociativity());
        assertEquals(0,se.getPrefix().size());
        assertEquals(0,se.getSuffix().size());
        assertEquals(0,se.getSeparator().size());
        assertNull(se.getSetupMethod());
        assertTrue(m.getSubelements().get(m.getClassToElement().get(Expression.class)).contains(m.getClassToElement().get(UnaryExpression.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(Expression.class)).contains(m.getClassToElement().get(BinaryExpression.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(Expression.class)).contains(m.getClassToElement().get(ExpressionGroup.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(UnaryExpression.class)).equals(m.getClassToElement().get(Expression.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(BinaryExpression.class)).equals(m.getClassToElement().get(Expression.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(ExpressionGroup.class)).equals(m.getClassToElement().get(Expression.class)));

        se =  m.getClassToElement().get(UnaryOperator.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(UnaryOperator.class,se.getElementClass());
        assertEquals(AssociativityType.UNDEFINED,se.getAssociativity());
        assertEquals(0,se.getPrefix().size());
        assertEquals(0,se.getSuffix().size());
        assertEquals(0,se.getSeparator().size());
        assertNull(se.getSetupMethod());
        assertTrue(m.getSubelements().get(m.getClassToElement().get(UnaryOperator.class)).contains(m.getClassToElement().get(MinusOperator.class)));
        assertTrue(m.getSubelements().get(m.getClassToElement().get(UnaryOperator.class)).contains(m.getClassToElement().get(PlusOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(MinusOperator.class)).equals(m.getClassToElement().get(UnaryOperator.class)));
        assertTrue(m.getSuperelements().get(m.getClassToElement().get(PlusOperator.class)).equals(m.getClassToElement().get(UnaryOperator.class)));
    }


    @Test
    public void modelReadTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(true);
        lg.setFilter(c);
        LanguageModel m = modelGen(Main.class);

        assertNotNull(m);
        assertEquals(0,c.getCount());
        assertEquals(7,m.getElements().size());
        CompositeLanguageElement ce;
        SimpleLanguageElement be;
        LanguageElement se;

        LanguageMember sc;
        LanguageMember scid;
        MemberCollection mc;
        be = (SimpleLanguageElement) m.getClassToElement().get(Test1.class);
        assertTrue(m.getElements().contains(be));
        assertEquals(Test1.class,be.getElementClass());
        assertEquals(AssociativityType.NON_ASSOCIATIVE,be.getAssociativity());
        assertEquals(2,be.getPrefix().size());
            assertEquals("a1",((RegExpPatternRecognizer)be.getPrefix().get(0)).getRegExp());
            assertEquals("b1",((RegExpPatternRecognizer)be.getPrefix().get(1)).getRegExp());
        assertEquals(2,be.getSuffix().size());
            assertEquals("c1",((RegExpPatternRecognizer)be.getSuffix().get(0)).getRegExp());
            assertEquals("d1",((RegExpPatternRecognizer)be.getSuffix().get(1)).getRegExp());
        assertEquals(2,be.getSeparator().size());
            assertEquals("e",((RegExpPatternRecognizer)be.getSeparator().get(0)).getRegExp());
            assertEquals("a1",((RegExpPatternRecognizer)be.getSeparator().get(1)).getRegExp());
            assertEquals(((RegExpPatternRecognizer)be.getPrefix().get(0)).getRegExp(),((RegExpPatternRecognizer)be.getSeparator().get(1)).getRegExp());
        assertNotNull(be.getPatternRecognizer());
        assertEquals("a", be.getValueField());
        assertNull(be.getSetupMethod());

        ce = (CompositeLanguageElement) m.getClassToElement().get(Main.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(Main.class,ce.getElementClass());
        assertEquals(1,ce.getKeyMembers().size());
        
        assertEquals(5,ce.getMembers().size());
        mc = (MemberCollection) ce.getMembers().get(0);
        assertEquals("tests1",mc.getID());
        assertEquals(MemberCollectionType.ARRAY,mc.getCollection());
        assertEquals(0,mc.getMinimumMultiplicity());
        assertEquals(1000,mc.getMaximumMultiplicity());
        assertEquals(true,mc.isOptional());
        assertEquals(false,mc.isKey());
        assertEquals(false,mc.isReference());
        assertEquals(2,mc.getPrefix().size());
            assertEquals("a",((RegExpPatternRecognizer)mc.getPrefix().get(0)).getRegExp());
            assertEquals("b",((RegExpPatternRecognizer)mc.getPrefix().get(1)).getRegExp());
        assertEquals(2,mc.getSuffix().size());
            assertEquals("c",((RegExpPatternRecognizer)mc.getSuffix().get(0)).getRegExp());
            assertEquals("d",((RegExpPatternRecognizer)mc.getSuffix().get(1)).getRegExp());
        assertEquals(2,mc.getSeparator().size());
            assertEquals("e",((RegExpPatternRecognizer)mc.getSeparator().get(0)).getRegExp());
            assertEquals("a",((RegExpPatternRecognizer)mc.getSeparator().get(1)).getRegExp());
            assertEquals(((RegExpPatternRecognizer)be.getSeparator().get(0)).getRegExp(),((RegExpPatternRecognizer)mc.getSeparator().get(0)).getRegExp());

        mc = (MemberCollection) ce.getMembers().get(1);
        assertEquals("tests2",mc.getID());
        assertEquals(MemberCollectionType.LIST,mc.getCollection());
        assertEquals(1,mc.getMinimumMultiplicity());
        assertEquals(-1,mc.getMaximumMultiplicity());
        assertEquals(false,mc.isOptional());
        assertEquals(false,mc.isKey());
        assertEquals(false,mc.isReference());
        assertNull(mc.getPrefix());
        assertNull(mc.getSuffix());
        assertNull(mc.getSeparator());

        mc = (MemberCollection) ce.getMembers().get(2);
        assertEquals("tests3",mc.getID());
        assertEquals(MemberCollectionType.LIST,mc.getCollection());
        assertEquals(0,mc.getMinimumMultiplicity());
        assertEquals(10,mc.getMaximumMultiplicity());
        assertEquals(false,mc.isOptional());
        assertEquals(false,mc.isKey());
        assertEquals(true,mc.isReference());
        assertNull(mc.getPrefix());
        assertNull(mc.getSuffix());
        assertNull(mc.getSeparator());

        mc = (MemberCollection) ce.getMembers().get(3);
        assertEquals("tests4",mc.getID());
        assertEquals(MemberCollectionType.SET,mc.getCollection());
        assertEquals(0,mc.getMinimumMultiplicity());
        assertEquals(-1,mc.getMaximumMultiplicity());
        assertEquals(false,mc.isOptional());
        assertEquals(false,mc.isKey());
        assertEquals(false,mc.isReference());
        assertNull(mc.getPrefix());
        assertNull(mc.getSuffix());
        assertNull(mc.getSeparator());

        sc = ce.getMembers().get(4);
        scid = ce.getKeyMembers().get(0);
        assertEquals(scid,sc);
        assertEquals("tests5",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(true,sc.isKey());
        assertEquals(true,sc.isReference());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        assertEquals(false,ce.isFreeOrder());
        assertEquals(AssociativityType.UNDEFINED,ce.getAssociativity());
        assertEquals(CompositionType.UNDEFINED,ce.getComposition());
        assertEquals(0,ce.getPrefix().size());
        assertEquals(0,ce.getSuffix().size());
        assertEquals(0,ce.getSeparator().size());
        assertEquals(1,ce.getConstraintMethods().size());
        assertEquals("run",ce.getConstraintMethods().get(0));
        assertEquals("setup",ce.getSetupMethod());

        se = m.getClassToElement().get(Test2.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(Test2.class,se.getElementClass());
        assertEquals(AssociativityType.LEFT_TO_RIGHT,se.getAssociativity());
        assertEquals(2,se.getPrefix().size());
            assertEquals("a",((RegExpPatternRecognizer)se.getPrefix().get(0)).getRegExp());
            assertEquals("b",((RegExpPatternRecognizer)se.getPrefix().get(1)).getRegExp());
        assertEquals(2,se.getSuffix().size());
            assertEquals("c",((RegExpPatternRecognizer)se.getSuffix().get(0)).getRegExp());
            assertEquals("d",((RegExpPatternRecognizer)se.getSuffix().get(1)).getRegExp());
        assertEquals(1,se.getSeparator().size());
            assertEquals("e",((RegExpPatternRecognizer)se.getSeparator().get(0)).getRegExp());
        assertEquals(1,se.getConstraintMethods().size());
        assertEquals("run",se.getConstraintMethods().get(0));

        ce = (CompositeLanguageElement) m.getClassToElement().get(Test3.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(Test3.class,ce.getElementClass());
        assertEquals(false,ce.isFreeOrder()); // ???
        assertEquals(AssociativityType.LEFT_TO_RIGHT,ce.getAssociativity());
        assertEquals(CompositionType.EAGER,ce.getComposition());
        assertEquals(2,se.getPrefix().size());
            assertEquals("a",((RegExpPatternRecognizer)se.getPrefix().get(0)).getRegExp());
            assertEquals("b",((RegExpPatternRecognizer)se.getPrefix().get(1)).getRegExp());
        assertEquals(2,se.getSuffix().size());
            assertEquals("c",((RegExpPatternRecognizer)se.getSuffix().get(0)).getRegExp());
            assertEquals("d",((RegExpPatternRecognizer)se.getSuffix().get(1)).getRegExp());
        assertEquals(1,se.getSeparator().size());
            assertEquals("e",((RegExpPatternRecognizer)se.getSeparator().get(0)).getRegExp());
        assertEquals(1,se.getConstraintMethods().size());
        assertEquals("run",se.getConstraintMethods().get(0));

        se = m.getClassToElement().get(Test4.class);
        assertTrue(m.getElements().contains(se));
        assertEquals(Test4.class,se.getElementClass());
        assertEquals(AssociativityType.RIGHT_TO_LEFT,se.getAssociativity());
        assertEquals(0,se.getPrefix().size());
        assertEquals(2,se.getSuffix().size());
            assertEquals("d",((RegExpPatternRecognizer)se.getSuffix().get(0)).getRegExp());
            assertEquals("c",((RegExpPatternRecognizer)se.getSuffix().get(1)).getRegExp());
        assertEquals(0,se.getSeparator().size());
        assertEquals(1,se.getConstraintMethods().size());
        assertTrue(se.getConstraintMethods().get(0).contains("run2"));

        ce = (CompositeLanguageElement) m.getClassToElement().get(Test5.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(Test5.class,ce.getElementClass());
        assertEquals(false,ce.isFreeOrder()); // ???
        assertEquals(AssociativityType.LEFT_TO_RIGHT,ce.getAssociativity());
        assertEquals(CompositionType.EAGER,ce.getComposition());
        assertEquals(0,ce.getPrefix().size());
        assertEquals(0,ce.getSuffix().size());
        assertEquals(1,ce.getSeparator().size());
            assertEquals("e",((RegExpPatternRecognizer)ce.getSeparator().get(0)).getRegExp());
        assertEquals(0,ce.getConstraintMethods().size());


        ce = (CompositeLanguageElement) m.getClassToElement().get(Test6.class);
        assertTrue(m.getElements().contains(ce));
        assertEquals(Test6.class,ce.getElementClass());
        assertEquals(1,ce.getMembers().size());

        sc =  ce.getMembers().get(0);
        assertEquals("two",sc.getID());
        assertEquals(false,sc.isOptional());
        assertEquals(false,sc.isKey());
        assertEquals(false,sc.isReference());
        assertNull(sc.getPrefix());
        assertNull(sc.getSuffix());
        assertNull(sc.getSeparator());

        assertEquals(false,ce.isFreeOrder());
        assertEquals(AssociativityType.RIGHT_TO_LEFT,ce.getAssociativity());
        assertEquals(CompositionType.LAZY,ce.getComposition());
        assertEquals(0,ce.getPrefix().size());
        assertEquals(0,ce.getSuffix().size());
        assertEquals(0,ce.getSeparator().size());
        assertEquals(0,ce.getConstraintMethods().size());

        assertFalse(checkPrec(m,Main.class,Test1.class));
        assertFalse(checkPrec(m,Main.class,Test2.class));
        assertFalse(checkPrec(m,Main.class,Test3.class));
        assertFalse(checkPrec(m,Main.class,Test4.class));
        assertFalse(checkPrec(m,Main.class,Test5.class));
        assertFalse(checkPrec(m,Main.class,Test6.class));

        assertFalse(checkPrec(m,Test1.class,Main.class));
        assertFalse(checkPrec(m,Test1.class,Test2.class));
        assertFalse(checkPrec(m,Test1.class,Test3.class));
        assertFalse(checkPrec(m,Test1.class,Test4.class));
        assertFalse(checkPrec(m,Test1.class,Test5.class));
        assertFalse(checkPrec(m,Test1.class,Test6.class));

        assertFalse(checkPrec(m,Test2.class,Main.class));
        assertFalse(checkPrec(m,Test2.class,Test1.class));
        assertFalse(checkPrec(m,Test2.class,Test3.class));
        assertFalse(checkPrec(m,Test2.class,Test4.class));
        assertTrue(checkPrec(m,Test2.class,Test5.class));
        assertFalse(checkPrec(m,Test2.class,Test6.class));

        assertFalse(checkPrec(m,Test3.class,Main.class));
        assertFalse(checkPrec(m,Test3.class,Test1.class));
        assertFalse(checkPrec(m,Test3.class,Test2.class));
        assertFalse(checkPrec(m,Test3.class,Test4.class));
        assertTrue(checkPrec(m,Test3.class,Test5.class));
        assertFalse(checkPrec(m,Test3.class,Test6.class));

        assertFalse(checkPrec(m,Test4.class,Main.class));
        assertFalse(checkPrec(m,Test4.class,Test1.class));
        assertTrue(checkPrec(m,Test4.class,Test2.class));
        assertTrue(checkPrec(m,Test4.class,Test3.class));
        assertTrue(checkPrec(m,Test4.class,Test5.class));
        assertTrue(checkPrec(m,Test4.class,Test6.class));

        assertFalse(checkPrec(m,Test5.class,Main.class));
        assertFalse(checkPrec(m,Test5.class,Test1.class));
        assertFalse(checkPrec(m,Test5.class,Test2.class));
        assertFalse(checkPrec(m,Test5.class,Test3.class));
        assertFalse(checkPrec(m,Test5.class,Test4.class));
        assertFalse(checkPrec(m,Test5.class,Test6.class));

        assertFalse(checkPrec(m,Test6.class,Main.class));
        assertFalse(checkPrec(m,Test6.class,Test1.class));
        assertTrue(checkPrec(m,Test6.class,Test2.class));
        assertTrue(checkPrec(m,Test6.class,Test3.class));
        assertFalse(checkPrec(m,Test6.class,Test4.class));
        assertTrue(checkPrec(m,Test6.class,Test5.class));

   }
    

    @Test
    public void modelWrongTest01() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass01.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
        SimpleLanguageElement be;
        be = (SimpleLanguageElement) m.getClassToElement().get(WrongClass01.class);
        assertNull(be.getValueField());
    }

    @Test
    public void modelWrongTest02() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass02.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
        SimpleLanguageElement be;
        be = (SimpleLanguageElement) m.getClassToElement().get(WrongClass02.class);
        assertNull(be.getSetupMethod());
    }

    @Test
    public void modelWrongTest02b() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass02b.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        SimpleLanguageElement be;
        be = (SimpleLanguageElement) m.getClassToElement().get(WrongClass02b.class);
        assertEquals(1,be.getConstraintMethods().size());
    }

    @Test
    public void modelWrongTest03() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass03.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongTest04() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass04.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        SimpleLanguageElement be;
        be = (SimpleLanguageElement) m.getClassToElement().get(WrongClass04.class);
        assertNull(m.getPrecedences().get(be));
    }

    @Test
    public void modelWrongTest05() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass05.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass05.class).getClass());
    }

    @Test
    public void modelWrongTest06() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass06.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass06.class).getClass());
    }

    @Test
    public void modelWrongTest07() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass07.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass07.class).getClass());
    }

    @Test
    public void modelWrongTest08() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass08.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass08.class).getClass());
    }

    @Test
    public void modelWrongTest09() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass09.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(0,((MemberCollection)((CompositeLanguageElement)m.getClassToElement().get(WrongClass09.class)).getMembers().get(0)).getMinimumMultiplicity());
    }

    @Test
    public void modelWrongTest10() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass10.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass10.class).getClass());
        assertEquals(-1,((MemberCollection)((CompositeLanguageElement)m.getClassToElement().get(WrongClass10.class)).getMembers().get(0)).getMaximumMultiplicity());
    }

    @Test
    public void modelWrongTest11() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass11.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass11.class).getClass());
        assertEquals(3,((MemberCollection)((CompositeLanguageElement)m.getClassToElement().get(WrongClass11.class)).getMembers().get(0)).getMinimumMultiplicity());
        assertEquals(-1,((MemberCollection)((CompositeLanguageElement)m.getClassToElement().get(WrongClass11.class)).getMembers().get(0)).getMaximumMultiplicity());
    }

    @Test
    public void modelWrongTest12() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass12.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass12.class).getClass());
    }

    @Test
    public void modelCorrectTest13() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(CorrectClass13.class);
        assertNotNull(m);
        assertEquals(0,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(CorrectClass13.class).getClass());
    }

    @Test
    public void modelWrongTest14() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass14.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass14.class).getClass());
        assertNull(m.getClassToElement().get(WrongClass14.class).getSetupMethod());
    }

    @Test
    public void modelWrongTest15() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass15.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass15.class).getClass());
        assertNull(m.getClassToElement().get(WrongClass15.class).getSetupMethod());
    }

    @Test
    public void modelWrongTest16() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass16.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass16.class).getClass());
        assertNull(m.getClassToElement().get(WrongClass16.class).getSetupMethod());
    }

    @Test
    public void modelWrongTest17() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass17.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass17.class).getClass());
        assertNull(m.getPrecedences().get(m.getClassToElement().get(WrongClass17.class)));
        assertNull(m.getPrecedences().get(m.getClassToElement().get(WrongClass17A.class)));
    }

    @Test
    public void modelWrongTest18() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass18.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass18.class).getClass());
        assertNull(m.getPrecedences().get(m.getClassToElement().get(WrongClass18.class)));
        assertNull(m.getPrecedences().get(m.getClassToElement().get(WrongClass18A.class)));
    }

    @Test
    public void modelWrongTest19() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass19.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
        assertEquals(CompositeLanguageElement.class,m.getClassToElement().get(WrongClass19.class).getClass());
    }

    @Test
    public void modelWrongTest20() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass20.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongTest21() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass21.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }


    @Test
    public void modelWrongPatternTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClass23.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongEmptyTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(model.examples.empty.multiple.StartPoint.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }
    
    @Test(expected=ClassDoesNotExtendIModelException.class)
    public void NotIModelTest()
    	throws Exception
    {
        JavaModelReader jmr = new JavaLanguageReader(WrongClass22.class);
        jmr.read();
    }

    @Test
    public void modelWrongPositionTest1() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition1.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongPositionTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition2.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongPositionTest3() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition3.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongPositionTest4() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition4.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }


    @Test
    public void modelWrongPositionTest5() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition5.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }


    @Test
    public void modelWrongPositionTest6() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition6.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }

    @Test
    public void modelWrongPositionTest7() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPosition7.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void modelWrongPositionClashTest1() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPositionClash1.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }

    @Test
    public void modelWrongPositionClashTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPositionClash2.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }
    @Test
    public void modelWrongPositionClashTest3() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPositionClash3.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }
    @Test
    public void modelWrongPositionClashTest4() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPositionClash4.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }
    @Test
    public void modelWrongPositionClashTest5() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(WrongClassPositionClash5.class);
        assertNotNull(m);
        assertEquals(2,c.getCount());
    }
    
    @Test
    public void AbstractTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(AbstNoSubClasses.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void AbstractTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(AbstNoSubClasses1.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    
    @Test
    public void OptionalIDTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(OptionalID.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    
    @Test
    public void IDNotIModelTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(IDNotIModel.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    
    @Test
    public void ReferenceNotIModelTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(ReferenceNotIModel.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test(expected=ParserException.class)
    public void EmptyPrefixTest() throws ParserException 
    {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(model.examples.empty.prefix.StartPoint.class);
        assertNotNull(m);
        Set<PatternRecognizer> se = new HashSet<PatternRecognizer>();
        se.add(new RegExpPatternRecognizer(" "));
        Parser<model.examples.empty.prefix.StartPoint> parser;
		parser = ParserFactory.create(m,se);

		parser.parseAll("ab");
    }

    @Test
    public void EmptyPrefixTest2() throws ParserException 
    {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(model.examples.empty.prefix2.StartPoint.class);
        assertNotNull(m);
        Set<PatternRecognizer> se = new HashSet<PatternRecognizer>();
        se.add(new RegExpPatternRecognizer(" "));
        Parser<model.examples.empty.prefix2.StartPoint> parser;
	
        parser = ParserFactory.create(m,se);
	
        Collection<model.examples.empty.prefix2.StartPoint> result = parser.parseAll("acb");
        assertEquals(1,result.size());
    }


    @Test
    public void PrefixLoopTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(model.examples.empty.prefixloop.StartPoint.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }

    @Test
    public void PrefixLoopTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(model.examples.empty.prefixloop2.StartPoint.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    
    @Test
    public void ReferenceNotIDTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(ReferenceNotID.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    
    @Test
    public void ReferenceNotID2Test() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(ReferenceNotID2.class);
        assertNotNull(m);
        assertEquals(1,c.getCount());
    }
    

    @Test
    public void AllOptionalTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(OptionalMain.class);
        assertNotNull(m);
        assertEquals(0,c.getCount());
    }
    
    @Test
    public void MultOptionalTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        LanguageModel m = modelGen(OptionalMult.class);
        assertNotNull(m);
        assertEquals(0,c.getCount());
    }
            
    @Test
    public void WarningExportHandlerTest() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        List<Message> warnings = modelWarnings(OptionalMult.class);
        
        assertEquals(0,warnings.size());
    }    
    
    @Test
    public void WarningExportHandlerTest2() {
        Logger lg = Logger.getLogger(JavaLanguageReader.class.getName());
        CountFilter c = new CountFilter(false);
        lg.setFilter(c);
        List<Message> warnings = modelWarnings(OptionalMult.class);
        List<Message> warnings2 = modelWarnings(OptionalMult.class);        
        assertEquals(0,warnings.size());
        assertEquals(0,warnings2.size());
    }

    @Test
    public void WarningExportHandlerTest3() 
    	throws Exception
    {
        JavaModelReader jmr = new JavaLanguageReader(OptionalMult.class);
        jmr.read();
        jmr.read();
        assertEquals(0,jmr.getMessages().size());
    }
    
    @Test
    public void SerializationTest() 
    {
        ModelReader<LanguageModel> jmr = new JavaLanguageReader(OptionalMult.class);
        LanguageModel m = null;

        try {
            m = jmr.read();
            assertNotNull(testSerialize(m));
        } catch (Exception ex) {
            Logger.getLogger(JavaModelReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static Object testSerialize(Object o) throws ClassNotFoundException 
    {
        if (o == null)
            return null;
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectOutputStream oos;
        try {
            oos = new ObjectOutputStream(out);
            oos.writeObject(o);
            oos.close();
        } catch (Exception e) {
        	fail();
        }
        assertTrue(out.toByteArray().length > 0);
        
        byte[] pickled = out.toByteArray();
        InputStream in = new ByteArrayInputStream(pickled);
        ObjectInputStream ois;
        try {
            ois = new ObjectInputStream(in);
            ois.readObject();
        } catch (IOException ex) {
        	fail();
        }

        return o;
    }
    

    @Test
    public void CloneTest() 
    {
        ModelReader<LanguageModel> jmr = new JavaLanguageReader(OptionalMult.class);
        LanguageModel m = null;

        try {
            m = jmr.read();
        } catch (Exception ex) {
            Logger.getLogger(JavaModelReaderTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        LanguageModel m2 = m.clone();
        assertNotSame(m.getClassToElement().get(OptionalMult.class),m2.getClassToElement().get(OptionalMult.class));
        assertNotSame(m.getClassToElement().get(OptionalPart.class),m2.getClassToElement().get(OptionalPart.class));
    }
}