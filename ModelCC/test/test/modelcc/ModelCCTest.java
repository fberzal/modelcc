package test.modelcc;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;

import org.modelcc.io.ModelReader;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;


/**
 * ModelCC generic test for JUnit
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ModelCCTest<T> 
{
	public T parse (String text)
			throws Exception
	{
	    Parser<T> parser = createParser();
	    T         model = parser.parse(text);

		return model;
	}

	public Collection<T> parseAll (String text)
			throws Exception
	{
	    Parser<T>     parser = createParser();
	    Collection<T> models = parser.parseAll(text);

		return models;
	}

	public Parser<T> createParser ()
		throws Exception
	{
		ModelReader<LanguageModel> modelReader = new JavaLanguageReader(getGenericClass());
		LanguageModel model = modelReader.read();
		Parser<T> parser = ParserFactory.create(model,ParserFactory.WHITESPACE);

		return parser;
	}

	private Class getGenericClass ()
	{
		ParameterizedType parameterizedType = (ParameterizedType) getClass().getGenericSuperclass();
		return (Class) parameterizedType.getActualTypeArguments()[0];
	}
}

