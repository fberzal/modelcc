package test.modelcc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { test.modelcc.annotations.AllTests.class,
	                   test.modelcc.factory.AllTests.class,
	                   test.modelcc.io.AllTests.class,
	                   test.modelcc.lexer.AllTests.class,
	                   test.modelcc.parser.AllTests.class,
	                   test.languages.AllTests.class } )
public class AllTests {

}