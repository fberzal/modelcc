/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.FreeOrder;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Prefix("0")
@FreeOrder
public class DelimitedEmptyString implements IModel {

	@Prefix("1")
	@Suffix("2")
	private	EmptyString password;

	public EmptyString getValue() {
		return password;
	}

}
