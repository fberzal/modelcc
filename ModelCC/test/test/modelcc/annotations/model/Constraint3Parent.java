/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.Constraint;
import org.modelcc.IModel;

public class Constraint3Parent implements IModel {

	int a=0;
	
	@Constraint
	public boolean test() {
		return a==4;
	}
}
