/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.Constraint;
import org.modelcc.IModel;
import org.modelcc.Value;

public class Constraint2 implements IModel {

    @Value int a;
    
    @Constraint boolean test() {
   		return a!=10;
    }

    @Constraint boolean test2() {
   		return a==9;
    }
}
