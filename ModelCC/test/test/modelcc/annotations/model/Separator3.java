/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;

public class Separator3 implements IModel {

	@Prefix("\\|?")
	@Suffix("\\|?")
	@Separator("\\|")
	X list[];
}
