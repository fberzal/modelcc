/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;

public class Setup implements IModel {
    
    public Setup1 a;
    
    public Setup1 b;
    
    public Setup1 c;
}
