/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Setup;

public class Setup1 implements IModel {
    
    public Setup2 a;
    
    public int count=0;
    
    @Setup
    void load() throws Exception {
        count++;
    }
}
