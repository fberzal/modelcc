/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Value;

@Prefix("a?")
public class Prefix2 implements IModel {

    @Value int a;
}
