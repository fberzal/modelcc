/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Value;

@Prefix("(?i)a")
public class Prefix1 implements IModel {

    @Value int a;
}
