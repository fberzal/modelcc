/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="") 
public class EmptyString implements IModel {

	@Value
	String value;

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return "OptionalString("+value+")";
	}

}
