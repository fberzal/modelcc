/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Setup;
import org.modelcc.Value;

public class Constraint3 extends Constraint3Parent implements IModel {

	@Value
	int b;
	
	@Setup
	public void setup() {
		a = 4;
	}
}
