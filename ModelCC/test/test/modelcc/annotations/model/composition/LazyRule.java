/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.IModel;
import org.modelcc.Optional;

@Composition(CompositionType.LAZY)
public class LazyRule implements IModel {
    @Optional
	public RuleLHS a;
    
    @Optional
    public RuleRHS b;
}
