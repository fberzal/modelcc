/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="a")
public class RuleLHS implements IModel {
}
