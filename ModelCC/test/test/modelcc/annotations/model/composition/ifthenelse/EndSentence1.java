/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition.ifthenelse;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="s")
public class EndSentence1 extends Sentence1 implements IModel {

}
