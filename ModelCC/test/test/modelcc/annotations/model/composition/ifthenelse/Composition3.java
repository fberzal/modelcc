/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition.ifthenelse;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Prefix("start")
@Suffix("end")
public class Composition3 implements IModel {
    public Sentence3 sent;
}
