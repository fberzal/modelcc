/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition.ifthenelse;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;

public class CondSentence1 extends Sentence1 implements IModel {

    @Prefix("if")
    public Expression e;

    public Sentence1 ifsentence;

    @Prefix("else")
    @Optional
    public Sentence1 elsesentence;
}
