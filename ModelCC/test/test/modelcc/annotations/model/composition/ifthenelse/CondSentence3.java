/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition.ifthenelse;

import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;

@Composition(CompositionType.LAZY)
public class CondSentence3 extends Sentence3 implements IModel {

    @Prefix("if")
    public Expression e;

    public Sentence3 ifsentence;

    @Prefix("else")
    @Optional
    public Sentence3 elsesentence;
}
