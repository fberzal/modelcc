/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition.ifthenelse;

import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;

@Composition(CompositionType.EAGER)
public class CondSentence2 extends Sentence2 implements IModel {

    @Prefix("if")
    public Expression e;

    public Sentence2 ifsentence;

    @Prefix("else")
    @Optional
    public Sentence2 elsesentence;
}
