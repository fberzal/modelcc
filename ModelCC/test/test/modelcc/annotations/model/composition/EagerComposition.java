/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.IModel;

public class EagerComposition implements IModel {
    public EagerRule[] rules;
}
