/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.IModel;

public class AmbiguousComposition implements IModel {
    public AmbiguousRule[] rules;
}
