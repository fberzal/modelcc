/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class AmbiguousRule implements IModel {
    @Optional
	public RuleLHS a;
    
    @Optional
    public RuleRHS b;
}
