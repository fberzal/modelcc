/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.composition;

import org.modelcc.IModel;

public class LazyComposition implements IModel {
    public LazyRule[] rules;
}
