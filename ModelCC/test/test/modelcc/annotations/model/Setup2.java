/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;

@Pattern(regExp="a")
public class Setup2 implements IModel {
     
    public int count=0;
    
    @Setup
    void load() {
        count++;
    }
}
