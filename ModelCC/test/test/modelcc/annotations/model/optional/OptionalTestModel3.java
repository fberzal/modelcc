/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;

public class OptionalTestModel3 implements IModel {
    
    private OptionalTestModel3Class1 a;
    
    public OptionalTestModel3Class1 getTest() {
        return a;
    }
}
