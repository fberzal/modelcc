/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;
import org.modelcc.Value;

public class OptionalTestModel1Class3 implements IModel {
    @Value
    int a;
}
