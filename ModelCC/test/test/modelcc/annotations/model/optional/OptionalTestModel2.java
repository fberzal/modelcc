/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalTestModel2 implements IModel {
    
    @Optional
    private OptionalTestModel1Class3 val;
            
    @Optional
    private OptionalTestModel1Class3 a;
    
}