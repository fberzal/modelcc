/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalTestModel3Class2 implements IModel {
    @Optional 
    OptionalTestModel3Class3 a;
    
    public OptionalTestModel3Class3 getTest() {
        return a;
    }

}
