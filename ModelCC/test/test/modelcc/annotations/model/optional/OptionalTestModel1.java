/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;

public class OptionalTestModel1 implements IModel {
    
    private OptionalTestModel1Class1 a;
    
    public OptionalTestModel1Class1 getTest() {
        return a;
    }
}
