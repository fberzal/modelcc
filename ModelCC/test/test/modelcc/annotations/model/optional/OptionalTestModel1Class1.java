/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model.optional;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalTestModel1Class1 implements IModel {
    
    @Optional
    OptionalTestModel1Class2 a;
}
