/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;
import org.modelcc.Value;

@Prefix("")
@Suffix("")
public class Prefix4 implements IModel {

    @Value int a;
}
