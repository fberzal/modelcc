/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="[a-z]*") 
public class ValueString implements IModel {

	@Value
	String value;

	public String getValue() {
		return value;
	}
	
	@Override
	public String toString() {
		return "String("+value+")";
	}

}
