/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations.model;

import org.modelcc.IModel;
import org.modelcc.Separator;

public class Separator1 implements IModel {

	@Separator("")
	X list[];
}
