/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.HashSet;
import java.util.Set;

import org.modelcc.io.ModelReader;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserException;
import org.modelcc.parser.ParserFactory;

public class AnnotationTest 
{
    protected Parser createParser(Class a) 
    {
        Parser parser = null;
        try {
            ModelReader<LanguageModel> jmr = new JavaLanguageReader(a);
            LanguageModel m = jmr.read();

            Set<PatternRecognizer> skip = new HashSet<PatternRecognizer>();
            skip.add(new RegExpPatternRecognizer("\\t"));
            skip.add(new RegExpPatternRecognizer(" "));
            skip.add(new RegExpPatternRecognizer("\n"));
            skip.add(new RegExpPatternRecognizer("\r"));
            
            parser = ParserFactory.create(m,skip);
        } catch (Exception ex) {
        	fail();
        } 
        return parser;
    }
    

    protected Object parse (Class source, String input) throws ParserException
    {
    	return createParser(source).parse(input);
    }
   
    protected void assertInterpretations (int matches, Class source, String input) throws ParserException 
    {
    	assertEquals(matches,createParser(source).parseAll(input).size());
    }
    
}
