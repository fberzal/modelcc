package test.modelcc.annotations;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.modelcc.annotations.model.Setup;

public class SetupTest extends AnnotationTest
{
    @Test
    public void testSetup() throws ParserException 
    {
    	Setup cc = (Setup) createParser(test.modelcc.annotations.model.Setup.class).parse("aaa");

    	assertNotNull(cc);
    	assertEquals(1,cc.a.count);
    	assertEquals(1,cc.b.count);
    	assertEquals(1,cc.c.count);
    	assertEquals(1,cc.a.a.count);
    	assertEquals(1,cc.b.a.count);
    	assertEquals(1,cc.c.a.count); 	
    }

}
