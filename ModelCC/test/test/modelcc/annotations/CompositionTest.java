/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.modelcc.annotations.model.composition.AmbiguousComposition;
import test.modelcc.annotations.model.composition.EagerComposition;
import test.modelcc.annotations.model.composition.LazyComposition;

public class CompositionTest extends AnnotationTest 
{
	@Test
	public void test1() throws ParserException
	{
		assertInterpretations(2, AmbiguousComposition.class, "ab");
	}

	@Test
	public void test2() throws ParserException
	{	
		assertInterpretations(1, EagerComposition.class, "ab");
		
		EagerComposition val = (EagerComposition) parse(EagerComposition.class, "ab");

		assertEquals(1,val.rules.length);
	}

	@Test
	public void test3() throws ParserException
	{
		assertInterpretations(1, LazyComposition.class, "ab");
		
		LazyComposition val = (LazyComposition) parse(LazyComposition.class, "ab");

		assertEquals(2,val.rules.length);
	}

}
