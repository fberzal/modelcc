/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.modelcc.annotations.model.DelimitedEmptyString;
import test.modelcc.annotations.model.DelimitedString;
import test.modelcc.annotations.model.EmptyString;
import test.modelcc.annotations.model.ValueString;

public class ValueTest extends AnnotationTest 
{
    
	@Test
	public void testDelimitedString() throws ParserException 
	{
		assertInterpretations(1,DelimitedString.class,"01a2");
		assertInterpretations(1,DelimitedString.class,"012");		
	}

	@Test
	public void testValueString() throws ParserException 
	{
		DelimitedString a = (DelimitedString) parse(DelimitedString.class,"01aasdfasdf2");
		ValueString value = a.getValue();
		assertEquals("aasdfasdf",value.getValue());			
	}

	@Test
	public void testEmptyValueString() throws ParserException 
	{
		DelimitedString a = (DelimitedString) parse(DelimitedString.class, "012");
		ValueString value = a.getValue();
		assertEquals("",value.getValue());
	}

	
	@Test
	public void testDelimitedEmptyString() throws ParserException 
	{
		assertInterpretations(1,DelimitedEmptyString.class,"012");	
	}

	@Test
	public void testValueEmptyString() throws ParserException 
	{
		DelimitedEmptyString a = (DelimitedEmptyString) parse(DelimitedEmptyString.class,"012");
		EmptyString value = a.getValue();
		assertEquals("",value.getValue());
	}

}
