/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.modelcc.parser.ParserException;

public class DelimiterTest extends AnnotationTest
{
 
    @Test
    public void testPrefix1() throws ParserException 
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix1.class).parseAll("a0").size());
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix1.class).parseAll("A0").size());
    }

    @Test(expected=ParserException.class)
    public void testPrefix1Error1() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix1.class).parseAll("0");
    }
    
    @Test(expected=ParserException.class)
    public void testPrefix1Error2() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix1.class).parseAll("a");
    }
  
    @Test
    public void testPrefix2() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix2.class).parseAll("a42").size());
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix2.class).parseAll("42").size());
    }
    
    @Test(expected=ParserException.class)
    public void testPrefix2Error1() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix2.class).parseAll("aa42");
    }
    
    @Test(expected=ParserException.class)
    public void testPrefix2Error2() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix2.class).parseAll("a");
    }

    @Test
    public void testPrefix3() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix3.class).parseAll("ab42").size());
    }
    
    @Test(expected=ParserException.class)
    public void testPrefix3Error() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix3.class).parseAll("42");
    }

    @Test
    public void testPrefix4() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Prefix4.class).parseAll("42").size());
    }
    
    @Test(expected=ParserException.class)
    public void testPrefix4Error() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Prefix4.class).parseAll("a");
    }

    @Test
    public void testSeparator1() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator1.class).parseAll("xxxx").size());
    }
    
    @Test(expected=ParserException.class)
    public void testSeparator1Error1() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Separator1.class).parseAll("xxxxb");
    }
    
    @Test(expected=ParserException.class)
    public void testSeparator1Error2() throws ParserException 
    {    
    	createParser(test.modelcc.annotations.model.Separator1.class).parseAll("xxOxx");
    }

    @Test
    public void testSeparator2() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator2.class).parseAll("xabxabx").size());
    }
    
    @Test(expected=ParserException.class)
    public void testSeparator2Error1() throws ParserException 
    {
		createParser(test.modelcc.annotations.model.Separator2.class).parseAll("xabxabxa");
    }
    
    @Test(expected=ParserException.class)
    public void testSeparator2Error2() throws ParserException 
    {    
		createParser(test.modelcc.annotations.model.Separator2.class).parseAll("xabxabxab");
	}

    @Test
    public void testSeparator3() throws ParserException
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator3.class).parseAll("x|x|x").size());
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator3.class).parseAll("|x|x|x").size());
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator3.class).parseAll("x|x|x|").size());
    	assertEquals(1,createParser(test.modelcc.annotations.model.Separator3.class).parseAll("|x|x|x|").size());
    }

}




