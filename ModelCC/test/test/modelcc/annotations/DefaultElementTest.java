/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.modelcc.io.ModelReader;
import org.modelcc.io.java.JavaLanguageReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

public class DefaultElementTest 
{
    @Test
    public void DefaultElementTest1() 
    	throws Exception
    {
    	ModelReader<LanguageModel> jmr = new JavaLanguageReader(model.examples.empty.basics.StartPoint.class);

    	LanguageModel m = jmr.read();

    	Set<PatternRecognizer> se = new HashSet<PatternRecognizer>();
    	se.add(new RegExpPatternRecognizer(" "));

    	Parser<model.examples.empty.basics.StartPoint> parser = ParserFactory.create(m,se);

    	model.examples.empty.basics.StartPoint result = parser.parse("ab");

    	assertNotNull(result);
    	assertNotNull(result.content);
    	assertNotNull(((model.examples.empty.basics.ContentBasics)result.content).val);
    	assertEquals("",((model.examples.empty.basics.ContentBasics)result.content).val);
    }
    

    @Test
    public void DefaultElementTest2() 
    	throws Exception
    {
    	ModelReader<LanguageModel> jmr = new JavaLanguageReader(model.examples.empty.choices.StartPoint.class);

    	LanguageModel m = jmr.read();

    	Set<PatternRecognizer> se = new HashSet<PatternRecognizer>();
    	se.add(new RegExpPatternRecognizer(" "));

    	Parser<model.examples.empty.choices.StartPoint> parser = ParserFactory.create(m,se);

    	model.examples.empty.choices.StartPoint result = parser.parse("ab");

    	assertNotNull(result);
    	assertNotNull(result.content); 
    	assertNotNull(((model.examples.empty.choices.ContentChoices2)result.content).val);
    	assertEquals("",((model.examples.empty.choices.ContentChoices2)result.content).val);
    }

    @Test
    public void DefaultElementTest3()
    	throws Exception
    {
    	ModelReader<LanguageModel> jmr = new JavaLanguageReader(model.examples.empty.complex.StartPoint.class);

    	LanguageModel m = jmr.read();

    	Set<PatternRecognizer> se = new HashSet<PatternRecognizer>();
    	se.add(new RegExpPatternRecognizer(" "));

    	Parser<model.examples.empty.complex.StartPoint> parser = ParserFactory.create(m,se);

    	model.examples.empty.complex.StartPoint result = parser.parse("ab");

    	assertNotNull(result);
    	assertNotNull(result.content);
    	assertNull(((model.examples.empty.complex.ContentComplex)result.content).something);
    	assertNotNull(((model.examples.empty.complex.ContentComplex)result.content).empty);
    }
}