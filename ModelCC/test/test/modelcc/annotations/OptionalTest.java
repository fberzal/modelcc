/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.modelcc.annotations.model.optional.OptionalTestModel1;
import test.modelcc.annotations.model.optional.OptionalTestModel2;
import test.modelcc.annotations.model.optional.OptionalTestModel3;

public class OptionalTest extends AnnotationTest 
{
	@Test
	public void test1() throws ParserException
	{
		assertInterpretations(1, OptionalTestModel1.class, "1");
	}

	@Test
	public void test2() throws ParserException
	{
		assertInterpretations(1, OptionalTestModel2.class, ""); // Fully optional composite
	}
	
	@Test
	public void test3() throws ParserException
	{
		OptionalTestModel3 cc = (OptionalTestModel3) parse(OptionalTestModel3.class, "1");
		assertNotNull(cc.getTest());
        assertNotNull(cc.getTest().getTest());
        assertNotNull(cc.getTest().getTest().getTest());
    }
	
}
