/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.modelcc.annotations.model.composition.ifthenelse.Composition1;
import test.modelcc.annotations.model.composition.ifthenelse.Composition2;
import test.modelcc.annotations.model.composition.ifthenelse.Composition3;
import test.modelcc.annotations.model.composition.ifthenelse.CondSentence2;
import test.modelcc.annotations.model.composition.ifthenelse.CondSentence3;

public class IfThenElseTest extends AnnotationTest 
{
	@Test
	public void test1() throws ParserException
	{
        assertInterpretations(1, Composition1.class, "start s end");
        assertInterpretations(1, Composition1.class, "start if e s end");
        assertInterpretations(1, Composition1.class, "start if e s else s end");
        assertInterpretations(1, Composition1.class, "start if e if e s else s else s end");
        assertInterpretations(2, Composition1.class, "start if e if e s else s end");
    }

	@Test
	public void test2() throws ParserException
	{
        assertInterpretations(1, Composition2.class, "start s end");
        assertInterpretations(1, Composition2.class, "start if e s end");
        assertInterpretations(1, Composition2.class, "start if e s else s end");
        assertInterpretations(1, Composition2.class, "start if e if e s else s else s end");
        assertInterpretations(1, Composition2.class, "start if e if e s else s end");

        Composition2 cc = (Composition2) parse(Composition2.class, "start if e  if e s else s end");
        assertNull(((CondSentence2)(cc.sent)).elsesentence);
        assertNotNull(((CondSentence2) ((CondSentence2)(cc.sent)).ifsentence).elsesentence);
    }

    @Test
    public void test3() throws ParserException 
    {
        assertInterpretations(1, Composition3.class, "start s end");
        assertInterpretations(1, Composition3.class, "start if e s end");
        assertInterpretations(1, Composition3.class, "start if e s else s end");
        assertInterpretations(1, Composition3.class, "start if e if e s else s else s end");
        assertInterpretations(1, Composition3.class, "start if e if e s else s end");

        Composition3 cc = (Composition3) parse(Composition3.class, "start if e  if e s else s end");
        assertNotNull(((CondSentence3)(cc.sent)).elsesentence);
        assertNull(((CondSentence3) ((CondSentence3)(cc.sent)).ifsentence).elsesentence);
    }
	
}
