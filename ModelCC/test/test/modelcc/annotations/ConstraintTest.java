/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.annotations;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import org.modelcc.parser.ParserException;

public class ConstraintTest extends AnnotationTest
{
    @Test
    public void testConstraint1() throws ParserException 
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Constraint1.class).parseAll("0").size());
    }

    @Test(expected=ParserException.class)
    public void testConstraint1Error() throws ParserException 
    {
    	createParser(test.modelcc.annotations.model.Constraint1.class).parseAll("10");
    }
    
    @Test
    public void testConstraint2() throws ParserException 
    {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Constraint2.class).parseAll("9").size());
    }

    @Test(expected=ParserException.class)
    public void testConstraint2Error() throws ParserException 
    {
		createParser(test.modelcc.annotations.model.Constraint2.class).parseAll("0").size();
    }

    @Test(expected=ParserException.class)
    public void testConstraint2Error2() throws ParserException 
    {
		createParser(test.modelcc.annotations.model.Constraint2.class).parseAll("10").size();
    }

    
    @Test
    public void ConstraintTest3() throws ParserException {
    	assertEquals(1,createParser(test.modelcc.annotations.model.Constraint3.class).parseAll("5").size());
    }
        
}




