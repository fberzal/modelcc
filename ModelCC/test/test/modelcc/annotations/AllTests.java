package test.modelcc.annotations;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ConstraintTest.class,
	                   DelimiterTest.class,
	                   CompositionTest.class,
	                   IfThenElseTest.class,
	                   OptionalTest.class,
	                   SetupTest.class,
	                   ValueTest.class,
	                   DefaultElementTest.class})
public class AllTests {

}