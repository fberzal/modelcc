/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.lexer.recognizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import org.modelcc.lexer.recognizer.MatchedObject;
import org.modelcc.lexer.recognizer.PatternRecognizer;

/**
 * Pattern recognizer test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class PatternRecognizerTest 
{
	class SimplePatternRecognizer extends PatternRecognizer 
	{
		private String str;
		
		public SimplePatternRecognizer (String str)
		{
			this.str = str;
		}
		
		@Override
		public MatchedObject read (CharSequence cs, int start) 
		{
			if (cs.subSequence(start,str.length()).equals(str))
				return new MatchedObject(1,str);
			else
				return null;
		}		
	}
	
	@Test
	public void testValidMatch() 
	{
		PatternRecognizer pr = new SimplePatternRecognizer("hello");
		
		MatchedObject match = pr.read("hello");
		
		assertEquals("hello",match.getText());
		assertEquals(new Integer(1),match.getObject());
	}

	@Test
	public void testInvalidMatch() 
	{
		PatternRecognizer pr = new SimplePatternRecognizer("hello");
		
		MatchedObject match = pr.read("hella");
		assertNull(match);
	}
}
