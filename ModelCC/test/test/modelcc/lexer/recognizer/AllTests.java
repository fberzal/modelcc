package test.modelcc.lexer.recognizer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { PatternRecognizerTest.class,
	                   RegExpPatternRecognizerTest.class })
public class AllTests {

}