/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.lexer.recognizer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Test;

import org.modelcc.lexer.recognizer.MatchedObject;
import org.modelcc.lexer.recognizer.PatternRecognizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

/**
 * Regular expression recognizer test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org)
 */
public class RegExpPatternRecognizerTest 
{
	@Test
	public void testMatch1() {
		PatternRecognizer repr = new RegExpPatternRecognizer("abc+");
		MatchedObject match = repr.read("abc");
		assertEquals("abc",match.getText());
	}

	@Test
	public void testMatch2() {
		PatternRecognizer repr = new RegExpPatternRecognizer("abc+");
		MatchedObject match = repr.read("abccc");
		assertEquals("abccc",match.getText());
	}

	@Test
	public void testMatch3() {
		PatternRecognizer repr = new RegExpPatternRecognizer("abc+");
		MatchedObject match = repr.read("abcccd");
		assertEquals("abccc",match.getText());
	}

	@Test
	public void testMatch4() {
		PatternRecognizer repr = new RegExpPatternRecognizer("abc+");
		MatchedObject match = repr.read("aabcccd");
		assertNull(match);
	}

	@Test
	public void testMatch5() {
		PatternRecognizer repr = new RegExpPatternRecognizer("(ab)+c*");
		MatchedObject match = repr.read("aabab");
		assertNull(match);
	}

	@Test
	public void testMatch6() {
		PatternRecognizer repr = new RegExpPatternRecognizer("\\{");
		MatchedObject match = repr.read("{");
		assertEquals("{",match.getText());
	}

	@Test
	public void testMatch7() {
		PatternRecognizer repr = new RegExpPatternRecognizer("\\\\\\{");
		MatchedObject match = repr.read("\\{");
		assertEquals("\\{",match.getText());
	}
}