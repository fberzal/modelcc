package test.modelcc.lexer;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { test.modelcc.lexer.recognizer.AllTests.class,
					   LambTest.class,
					   FlexTest.class})
public class AllTests {

}