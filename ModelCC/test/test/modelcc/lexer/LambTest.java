/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.modelcc.lexer;

import static org.junit.Assert.*;

import java.io.Reader;

import org.junit.Test;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;
import org.modelcc.lexer.Tokenizer;
import org.modelcc.lexer.lamb.Lamb;
import org.modelcc.lexer.lamb.LambLexer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

/**
 * Lamb test suite
 * 
 * @author Luis Quesada (lquesada@modelcc.org), refactored by Fernando Berzal (fberzal@modelcc.org)
 */
public class LambTest extends LexerTest
{

	@Override
	protected Lexer lexer (LexicalSpecification ls)
	{
		return new LambLexer(ls);
	}
	
	@Override 
	protected Tokenizer tokenizer (LexicalSpecification ls, Reader reader)
	{
		return new Lamb(ls,reader);
	}
	

	@Test
	public void testOverlap() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(4,lg.getTokens().size());
		assertEquals(2,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		assertEquals("25",readText(input,searchToken(lg,1,2,"Integer2")));
		assertNotNull(searchToken(lg,1,2,"Integer2"));
		assertEquals("3",readText(input,searchToken(lg,3,3,"Integer")));
		assertNotNull(searchToken(lg,3,3,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
	}


	@Test
	public void testPrecedence2() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
		
		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		lsf.addPrecedence(tsInteger2,tsInteger);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(3,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("25",readText(input,searchToken(lg,1,2,"Integer2")));
		assertNotNull(searchToken(lg,1,2,"Integer2"));
		assertEquals("3",readText(input,searchToken(lg,3,3,"Integer")));
		assertNotNull(searchToken(lg,3,3,"Integer"));
		assertNull(searchToken(lg,3,3,"Integer2"));
		assertNull(searchToken(lg,1,3,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
	}

	@Test
	public void testAnalysis() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		LexicalSpecification ls = lsf.create();

		String input = " 22 +5354 +";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(10,lg.getTokens().size());
		assertEquals(2,lg.getStart().size());
		assertEquals("22",readText(input,searchToken(lg,1,2,"Integer2")));
		assertNotNull(searchToken(lg,1,2,"Integer2"));
		assertEquals("22",readText(input,searchToken(lg,1,2,"Integer")));
		assertNotNull(searchToken(lg,1,2,"Integer"));
		assertEquals("+53",readText(input,searchToken(lg,4,6,"Integer2")));
		assertNotNull(searchToken(lg,4,6,"Integer2"));
		assertEquals("+5354",readText(input,searchToken(lg,4,8,"Integer")));
		assertNotNull(searchToken(lg,4,8,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
		assertEquals("53",readText(input,searchToken(lg,5,6,"Integer2")));
		assertNotNull(searchToken(lg,5,6,"Integer2"));
		assertEquals("5354",readText(input,searchToken(lg,5,8,"Integer")));
		assertNotNull(searchToken(lg,5,8,"Integer"));
		assertEquals("54",readText(input,searchToken(lg,7,8,"Integer2")));
		assertNotNull(searchToken(lg,7,8,"Integer2"));
		assertEquals("54",readText(input,searchToken(lg,7,8,"Integer")));
		assertNotNull(searchToken(lg,7,8,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,10,10,"Operator")));
		assertNotNull(searchToken(lg,10,10,"Operator"));
	}


	@Test
	public void testLink() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertFalse(lg.getFollowing(searchToken(lg,1,3,"Integer")).contains(searchToken(lg,3,3,"Integer")));
		assertTrue(lg.getFollowing(searchToken(lg,1,2,"Integer2")).contains(searchToken(lg,3,3,"Integer")));
		assertTrue(lg.getFollowing(searchToken(lg,3,3,"Integer")).contains(searchToken(lg,4,4,"Operator")));
		assertTrue(lg.getFollowing(searchToken(lg,1,3,"Integer")).contains(searchToken(lg,4,4,"Operator")));

		assertFalse(lg.getPreceding(searchToken(lg,3,3,"Integer")).contains(searchToken(lg,1,3,"Integer")));
		assertTrue(lg.getPreceding(searchToken(lg,3,3,"Integer")).contains(searchToken(lg,1,2,"Integer2")));
		assertTrue(lg.getPreceding(searchToken(lg,4,4,"Operator")).contains(searchToken(lg,3,3,"Integer")));
		assertTrue(lg.getPreceding(searchToken(lg,4,4,"Operator")).contains(searchToken(lg,1,3,"Integer")));
	}


	@Test
	public void testPrecedenceTransitivity() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger1);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger3);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		lsf.addPrecedence(tsInteger1,tsInteger2);
		lsf.addPrecedence(tsInteger1,tsInteger);
		lsf.addPrecedence(tsInteger2,tsInteger3);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(5,lg.getTokens().size());
		assertEquals(2,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer3")));
		assertNotNull(searchToken(lg,1,3,"Integer3"));
		assertEquals("2",readText(input,searchToken(lg,1,1,"Integer1")));
		assertNotNull(searchToken(lg,1,1,"Integer1"));
		assertEquals("5",readText(input,searchToken(lg,2,2,"Integer1")));
		assertNotNull(searchToken(lg,2,2,"Integer1"));
		assertEquals("3",readText(input,searchToken(lg,3,3,"Integer1")));
		assertNotNull(searchToken(lg,3,3,"Integer1"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));		
	}

	@Test
	public void testSkip1() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.skipTokenSpecification(tsSpaceInteger);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(1,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
	}

	@Test
	public void testSkip2() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.skipTokenSpecification(tsSpaceInteger);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		lsf.addPrecedence(tsSpace,tsSpaceInteger);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";

		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
	}


	@Test
	public void testSkip3() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		String input = " 253+";

		LexicalSpecification ls = lsf.create();
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(4,lg.getTokens().size());
		assertEquals(2,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		assertEquals("25",readText(input,searchToken(lg,1,2,"Integer2")));
		assertNotNull(searchToken(lg,1,2,"Integer2"));
		assertEquals("3",readText(input,searchToken(lg,3,3,"Integer")));
		assertNotNull(searchToken(lg,3,3,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
	}


	@Test
	public void testRemoveCheck() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsSpaceInteger);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);
		lsf.addTokenSpecification(tsAnything);

		lsf.removeTokenSpecification(tsAnything);

		lsf.addPrecedence(tsSpace,tsSpaceInteger);
		lsf.addPrecedence(tsSpaceInteger,tsSpace);
		lsf.removePrecedence(tsSpaceInteger,tsSpace);

		String input = " 253+     ";

		LexicalSpecification ls = lsf.create();
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("253",LambTest.readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(LambTest.searchToken(lg,1,3,"Integer"));
		assertEquals("+",LambTest.readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(LambTest.searchToken(lg,4,4,"Operator"));
	}


	@Test
	public void testChainedPrecedence() throws Exception 
	{
		TokenSpecification tsA = new TokenSpecification("A",new RegExpPatternRecognizer("abc"));
		TokenSpecification tsB = new TokenSpecification("B",new RegExpPatternRecognizer("abc"));
		TokenSpecification tsC = new TokenSpecification("C",new RegExpPatternRecognizer("abc"));

		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.addTokenSpecification(tsA);
		lsf.addTokenSpecification(tsB);
		lsf.addTokenSpecification(tsC);

		lsf.addPrecedence(tsA,tsB);
		lsf.addPrecedence(tsB,tsC);

		String input = "abc";

		LexicalSpecification ls = lsf.create();
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		assertEquals(2,lg.getStart().size());
		assertNotNull(searchToken(lg,0,2,"A"));
		assertEquals("abc",readText(input,searchToken(lg,0,2,"A")));
		assertNotNull(searchToken(lg,0,2,"C"));
		assertEquals("abc",readText(input,searchToken(lg,0,2,"C")));
	}
}