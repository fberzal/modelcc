package test.modelcc.lexer;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.Reader;
import java.io.StringReader;

import org.junit.Test;
import org.modelcc.language.lexis.LexicalSpecification;
import org.modelcc.language.lexis.LexicalSpecificationFactory;
import org.modelcc.language.lexis.TokenBuilder;
import org.modelcc.language.lexis.TokenSpecification;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexicalGraph;
import org.modelcc.lexer.Token;
import org.modelcc.lexer.Tokenizer;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

/**
 * Lexer test suite
 * 
 * @author Fernando Berzal (berzal@modelcc.org)
 */
public abstract class LexerTest 
{
	// Abstract method 
	
	protected abstract Lexer lexer (LexicalSpecification ls);
	
	protected abstract Tokenizer tokenizer (LexicalSpecification ls, Reader reader);
	
	// Convenience methods
	
	protected final LexicalGraph lexicalGraph (LexicalSpecification ls, String input)
	{
		StringReader sr = new StringReader(input);
		Lexer flex = lexer(ls);	
		LexicalGraph lg = flex.scan(sr);
		
		return lg;
	}

	protected final Tokenizer tokenizer (LexicalSpecification ls, String input)
	{
		return tokenizer (ls, new StringReader(input) );
	}
	

	protected static String readText (String input, Token t) 
	{
		return input.substring(t.getStartIndex(),t.getEndIndex()+1);
	}

	protected static Token searchToken (LexicalGraph lg, int start, int end, Object type) 
	{
		for (Token t: lg.getTokens()) {
			if (t.getStartIndex()==start && t.getEndIndex()==end && t.getType()==type)
				return t;
		}
		return null;
	}
	
	// Common token specifications
	
	protected static TokenSpecification tsSpace = new TokenSpecification ("Space", new RegExpPatternRecognizer(" +"));
	protected static TokenSpecification tsTab = new TokenSpecification("Tab", new RegExpPatternRecognizer("\\t+"));
    protected static TokenSpecification tsNewLine = new TokenSpecification("NewLine",new RegExpPatternRecognizer("\\n|\\r"));

    protected static TokenSpecification tsInteger = new TokenSpecification("Integer", new RegExpPatternRecognizer("(-|\\+)?[0-9]+"));
    protected static TokenSpecification tsInteger1 = new TokenSpecification("Integer1", new RegExpPatternRecognizer("(-|\\+)?[0-9]"));
    protected static TokenSpecification tsInteger2 = new TokenSpecification("Integer2", new RegExpPatternRecognizer("(-|\\+)?[0-9][0-9]"));
    protected static TokenSpecification tsInteger3 = new TokenSpecification("Integer3", new RegExpPatternRecognizer("(-|\\+)?[0-9][0-9][0-9]"));
    
    protected static TokenSpecification tsSpaceInteger = new TokenSpecification("Spaceinteger",new RegExpPatternRecognizer(" [0-9]+"));
    
    protected static TokenSpecification tsOperator = new TokenSpecification("Operator", new RegExpPatternRecognizer("\\+"));
    protected static TokenSpecification tsKeyword = new TokenSpecification("Keyword", new RegExpPatternRecognizer("main"));

    protected static TokenSpecification tsAnything = new TokenSpecification("Anything", new RegExpPatternRecognizer("."));
    
    // Common tests for different kinds of lexers
	
	@Test
	public final void testSinglePrecedence() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger2);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification(tsOperator);

		lsf.addPrecedence(tsInteger,tsInteger2);

		LexicalSpecification ls = lsf.create();

		String input = " 253+";
		
		// Lexical graph
		
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		assertNull(searchToken(lg,1,2,"Integer2"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
		
		// Tokenizer API
		
		Token token;
		Tokenizer tokenizer = tokenizer(ls,input);
				
		token = tokenizer.nextToken();
		
		assertEquals ( "Integer", token.getType() );
		assertEquals ( "253", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 1, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertEquals ( "Operator", token.getType() );
		assertEquals ( "+", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 4, token.getOffset() );
		
		token = tokenizer.nextToken();
		
		assertNull ( token );		
	}

	@Test 
	public void testTwoTokens() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsKeyword);

		LexicalSpecification ls = lsf.create();

		String input = "main main";

		// Lexical graph
		
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		
		// Tokenizer API
		
		Token token;
		Tokenizer tokenizer = tokenizer(ls,input);
				
		token = tokenizer.nextToken();
		
		assertEquals ( "Keyword", token.getType() );
		assertEquals ( "main", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 0, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertEquals ( "Keyword", token.getType() );
		assertEquals ( "main", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 5, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertNull ( token );		
	}

	
	@Test 
	public void testTwoLines() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsKeyword);

		LexicalSpecification ls = lsf.create();

		String input = "main\nmain";
		Token token;

		// Lexical graph
		
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		
		token = lg.getTokens().get(0);
		
		assertEquals("main", token.getString());
		assertEquals(0, token.getOffset());
		assertEquals(0, token.getStartIndex());
		assertEquals(3, token.getEndIndex());
		assertEquals(1, token.getLine());

		token = lg.getTokens().get(1);
		
		assertEquals("main", token.getString());
		assertEquals(5, token.getOffset());
		assertEquals(5, token.getStartIndex());
		assertEquals(8, token.getEndIndex());
		assertEquals(2, token.getLine());
		
		// Tokenizer API
		
		Tokenizer tokenizer = tokenizer(ls,input);
				
		token = tokenizer.nextToken();
		
		assertEquals ( "Keyword", token.getType() );
		assertEquals ( "main", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 0, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertEquals ( "Keyword", token.getType() );
		assertEquals ( "main", token.getString() );
		assertEquals ( 2, token.getLine() );
		assertEquals ( 5, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertNull ( token );		
	}
	

	// Token builders
	// --------------

	@Test
	public void testTokenBuilderFail() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		TokenBuilder tb = new TokenBuilder(null) {

			@Override
			public Object build(Token t) {
				return null;
			}
		};
		
		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification( new TokenSpecification("Operator", new RegExpPatternRecognizer("\\+"), tb) );

		String input = " 253+";

		LexicalSpecification ls = lsf.create();
		
		// Lexical graph
		
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(1,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		
		// Tokenizer API

		Token token;
		Tokenizer tokenizer = tokenizer(ls,input);
				
		token = tokenizer.nextToken();
		
		assertEquals ( "Integer", token.getType() );
		assertEquals ( "253", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 1, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertNull ( token );		
	}


	@Test
	public void testTokenBuilderSuccess() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		TokenBuilder tb = new TokenBuilder(null) {
			@Override
			public Object build(Token t) {
				return "";
			}
		};
		
		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification( new TokenSpecification("Operator", new RegExpPatternRecognizer("\\+"), tb));

		String input = " 253+";

		LexicalSpecification ls = lsf.create();
		
		// Lexical graph
		
		LexicalGraph lg = lexicalGraph(ls,input);

		assertEquals(2,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());
		assertEquals("253",readText(input,searchToken(lg,1,3,"Integer")));
		assertNotNull(searchToken(lg,1,3,"Integer"));
		assertEquals("+",readText(input,searchToken(lg,4,4,"Operator")));
		assertNotNull(searchToken(lg,4,4,"Operator"));
		
		// Tokenizer API

		Token token;
		Tokenizer tokenizer = tokenizer(ls,input);
				
		token = tokenizer.nextToken();
		
		assertEquals ( "Integer", token.getType() );
		assertEquals ( "253", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 1, token.getOffset() );

		token = tokenizer.nextToken();
		
		assertEquals ( "Operator", token.getType() );
		assertEquals ( "+", token.getString() );
		assertEquals ( 1, token.getLine() );
		assertEquals ( 4, token.getOffset() );
		
		token = tokenizer.nextToken();
		
		assertNull ( token );		
		
	}


	private class CountTokenBuilder extends TokenBuilder 
	{
		private int count = 0;
		
		public CountTokenBuilder ()
		{
			super(null);
		}

		@Override
		public Object build(Token t) {
			count++;
			return "";
		}

		public int getCount() {
			return count;
		}

	}

	@Test
	public void testTokenBuilderCountCheck() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		CountTokenBuilder tb = new CountTokenBuilder();

		lsf.skipTokenSpecification(tsSpace);
		lsf.skipTokenSpecification(tsTab);
		lsf.skipTokenSpecification(tsNewLine);
		lsf.addTokenSpecification(tsInteger);
		lsf.addTokenSpecification( new TokenSpecification("Operator", new RegExpPatternRecognizer("\\+"), tb));

		LexicalSpecification ls = lsf.create();

		String input = " 253+";
		
		// Lexical graph

		lexicalGraph(ls,input);
		
		assertEquals(1,tb.getCount());
	}	
	
	
	@Test
	public void testSkipSpace() throws Exception
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();
		
		lsf.addTokenSpecification( new TokenSpecification("(",new RegExpPatternRecognizer("\\(")) );
		lsf.addTokenSpecification( new TokenSpecification(")",new RegExpPatternRecognizer("\\)")) );
		lsf.skipTokenSpecification( new TokenSpecification("WHITESPACE", new RegExpPatternRecognizer("[ \n\r\t]+")) );

		String input = "( )";

		LexicalSpecification ls = lsf.create();
		LexicalGraph lg = lexicalGraph(ls,input);
		
		assertEquals(2,lg.getTokens().size());
		assertEquals(1,lg.getStart().size()); 

		assertNotNull(lg.getFollowing(lg.getStartToken()));
	}

	@Test
	public void testNoSkipSpace() throws Exception 
	{
		LexicalSpecificationFactory lsf = new LexicalSpecificationFactory();

		lsf.addTokenSpecification( new TokenSpecification("(",new RegExpPatternRecognizer("\\(")) );
		lsf.addTokenSpecification( new TokenSpecification(")",new RegExpPatternRecognizer("\\)")) );

		String input = "( )";

		LexicalSpecification ls = lsf.create();
		LexicalGraph lg = lexicalGraph(ls,input);
		
		assertEquals(1,lg.getTokens().size());
		assertEquals(1,lg.getStart().size());

		assertNull(lg.getFollowing(lg.getStartToken()));
	}

}
