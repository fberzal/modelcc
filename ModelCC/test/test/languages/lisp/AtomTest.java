package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.Atom;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class AtomTest extends ModelCCTest<Atom>
{
	@Test
	public void testValidAtoms()
		throws Exception
	{
		assertNotNull ( parse("pi"));
		assertNotNull ( parse("SYMBOL"));
		assertNotNull ( parse("2"));
		assertNotNull ( parse("1.234"));
		assertNotNull ( parse("\"string\"") );
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAtom()
		throws Exception
	{
		parse("2x");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAtom2()
		throws Exception
	{
		parse("'quoted'");
	}	
}
