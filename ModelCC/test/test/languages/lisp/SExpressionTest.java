package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.SExpression;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class SExpressionTest extends ModelCCTest<SExpression>
{
	@Test
	public void testValidSExpressions()
		throws Exception
	{
		assertNotNull ( parse("(+ 1 2 3 4)"));
		assertNotNull ( parse("(print \"Hello world\")"));
		assertNotNull ( parse("(lambda (arg) (+ arg 1))"));
		assertNotNull ( parse("(defun factorial (x) (if (zerop x) 1 (* x (factorial (- x 1)))))"));
		assertNotNull ( parse("(((S) (NP VP)) ((VP) (V)) ((VP) (V NP)))"));
	}
	
	// Invalid lists

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidSExpression()
		throws Exception
	{
		parse("((a)");
	}
	
}
