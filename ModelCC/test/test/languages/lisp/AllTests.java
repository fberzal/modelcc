package test.languages.lisp;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { NumberTest.class,
	                   StringTest.class,
	                   SymbolTest.class,
	                   AtomTest.class,
	                   ListTest.class,
	                   SExpressionTest.class })
public class AllTests {

}