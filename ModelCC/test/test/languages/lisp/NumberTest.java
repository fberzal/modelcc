package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.Number;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class NumberTest extends ModelCCTest<Number>
{
	@Test
	public void testValidNumbers()
		throws Exception
	{
		assertNotNull ( parse("1"));
		assertNotNull ( parse("1.234"));
		assertNotNull ( parse("-2"));
		assertNotNull ( parse("-2.34"));
	}
	
	// Invalid numbers

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidNumber1()
		throws Exception
	{
		parse("2x");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidNumber2()
		throws Exception
	{
		parse("symbol");
	}		
}
