package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.Symbol;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class SymbolTest extends ModelCCTest<Symbol>
{
	@Test
	public void testValidSymbols()
		throws Exception
	{
		assertNotNull(parse("defun"));
		assertNotNull(parse("pi"));
		assertNotNull(parse("i2s"));
		assertNotNull(parse("SYMBOL"));
		assertNotNull(parse("*"));
		assertNotNull(parse("+"));
		assertNotNull(parse("-"));
		assertNotNull(parse("/"));
	}
			
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidSymbol()
		throws Exception
	{
		parse("2x");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidSymbol2()
		throws Exception
	{
		parse("a b");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidSymbol3()
		throws Exception
	{
		parse("_");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidSymbol4()
		throws Exception
	{
		parse("\"string\"");
	}
	
}
