package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.List;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ListTest extends ModelCCTest<List>
{
	@Test
	public void testValidLists()
		throws Exception
	{
		assertNotNull ( parse("()"));
		assertNotNull ( parse("( )"));
		assertNotNull ( parse("(1 2)"));
		assertNotNull ( parse("(x y)"));
		
		assertNotNull ( parse("((1 2))"));
		assertNotNull ( parse("((1)(2))"));
		assertNotNull ( parse("(x (1 2))"));
		
		assertNotNull ( parse("(f arg1 arg2 arg3)"));
		assertNotNull ( parse("(car (cons A B))"));
		assertNotNull ( parse("(list 1 2 (list 3 4))"));
	}
	
	// Invalid lists

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidList()
		throws Exception
	{
		parse("((a)");
	}
	
}
