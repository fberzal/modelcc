/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.lisp.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="(\\+|-)?[0-9]+(\\.[0-9]*)?")
public class Number extends Atom implements IModel 
{
    @Value 
    double value;
    
    // Constructors
    
    public Number() {}
    
    public Number(double value)
    {
    	this.value = value;
    }
    
    // Getter
    
    public double value() 
    {
        return value;
    }
    
    // toString
    
    public String toString()
    {
    	return Double.toString(value);
    }

}
