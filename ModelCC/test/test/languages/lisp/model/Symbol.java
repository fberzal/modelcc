package test.languages.lisp.model;

import org.modelcc.*;

@Pattern(regExp="[a-zA-Z][a-zA-Z0-9_]*|\\+|\\-|\\*|\\/")
public class Symbol extends Atom implements IModel
{
	@Value
	private String symbol;
	
	// Constructors
	
	public Symbol () {}
	
	public Symbol (String atom)
	{
		this.symbol = atom;
	}
	
	// toString
	
	public String toString ()
	{
		return symbol;
	}
}
