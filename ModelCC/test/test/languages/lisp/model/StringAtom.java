package test.languages.lisp.model;

import org.modelcc.*;

@Pattern(regExp="\"(\\\\\"|[^\"\n])*\"")
public class StringAtom extends Atom implements IModel
{
	@Value
	private String val;
	
	@Setup
	public void setup() 
	{
		val = val.substring(1,val.length()-1);
		val = val.replace("\\\"","\"");
	}
		
	// toString
	
	public String toString ()
	{
		return "\"" + val.replace("\"","\\\"") + "\"";
	}
}
