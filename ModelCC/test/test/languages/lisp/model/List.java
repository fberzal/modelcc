package test.languages.lisp.model;

import org.modelcc.*;

@Prefix("\\(")
@Suffix("\\)")
public class List extends SExpression implements IModel
{
	// @Separator(" ")
	private SExpression elements[];
	
	// Getters
	
	public SExpression element (int i)
	{
		return elements[i];
	}
	
	public int size ()
	{
		if (elements!=null)
			return elements.length;
		else
			return 0;
	}
	
	// toString
	
	public String toString ()
	{
		String str = "(";

		if (elements!=null)
			for (int i=0; i<elements.length; i++) {
				str += elements[i].toString();
				
				if (i<elements.length-1)
					str += " ";
			}
		
		str += ")";
		
		return str;
	}
}
