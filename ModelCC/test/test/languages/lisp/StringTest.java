package test.languages.lisp;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.lisp.model.StringAtom;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class StringTest extends ModelCCTest<StringAtom>
{
	@Test
	public void testValidStrings()
		throws Exception
	{
		assertNotNull ( parse("\"\""));
		assertNotNull ( parse("\"abc\""));
		assertNotNull ( parse("\"Dr Chen\""));
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidList()
		throws Exception
	{
		parse("\"a");
	}
	
}
