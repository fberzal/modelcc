package test.languages.imperative;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.imperative.model.Function;
import test.languages.imperative.model.FunctionCallExpression;
import test.languages.imperative.model.Variable;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class FunctionTest extends ModelCCTest<Function>
{
	@Test
	public void testValidFunctions()
		throws Exception
	{
		assertNotNull(parse("function f begin end"));
		assertNotNull(parse("function f(x) begin end"));
		assertNotNull(parse("function f(a,b) begin end"));
		assertNotNull(parse("function f(a,b) var i,j,k; begin end"));
	}

	@Test
	public void testValidNestedFunctions()
		throws Exception
	{
		assertNotNull(parse("function f function g begin end begin end"));
		assertNotNull(parse("function f function g begin end function h begin end begin end"));
		assertNotNull(parse("function f function g function h begin end begin end begin end"));
	}
	
	@Test
	public void testValidFunctionCall()
		throws Exception
	{
		Model model = JavaModelReader.read(FunctionCallExpression.class);
		Parser<FunctionCallExpression> parser = ParserFactory.create(model);

		parser.add(new Function("f"));
		parser.add(new Function("g"));
		parser.add(new Variable("x"));

		assertNotNull(parser.parse("f()"));
		assertNotNull(parser.parse("f(x)"));
		assertNotNull(parser.parse("f(1,2)"));

		assertNotNull(parser.parse("f(g(x))"));
	}
	
	
	// Invalid functions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidFunction()
		throws Exception
	{
		parse("function x");  // Missing body
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidFunctionCall()
		throws Exception
	{
		Model model = JavaModelReader.read(FunctionCallExpression.class);
		Parser<FunctionCallExpression> parser = ParserFactory.create(model);

		parser.add(new Function("f"));
		parser.add(new Variable("x"));

		assertNotNull(parser.parse("f[]"));
	}
	
}
