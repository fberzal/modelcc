package test.languages.imperative;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Variable;
import test.languages.imperative.model.VariableExpression;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class VariableTest extends ModelCCTest<Variable>
{
	@Test
	public void testValidVariable()
		throws Exception
	{
		Parser<Variable> parser = createParser();
		
		assertNotNull (parser.parse("pi"));
		assertNotNull (parser.parse("CONSTANT"));
		assertNotNull (parser.parse("i2s"));
	}
	
	@Test
	public void testValidVariableExpression()
		throws Exception
	{
		Model model = JavaModelReader.read(VariableExpression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		parser.add(new Variable("pi"));

		assertNotNull(parser.parse("pi"));
	}

	@Test
	public void testValidArrayExpression()
		throws Exception
	{
		Model model = JavaModelReader.read(VariableExpression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		parser.add(new Variable("data"));
		parser.add(new Variable("index"));

		assertNotNull(parser.parse("data"));
		assertNotNull(parser.parse("data[0]"));
		assertNotNull(parser.parse("data[1,2]"));
		assertNotNull(parser.parse("data[index]"));
	}
	
	
	// Invalid variables

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariable()
		throws Exception
	{
		parse("2x");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariableExpression()
		throws Exception
	{
		Model model = JavaModelReader.read(VariableExpression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		// Undefined variable
		
		parser.parse("pi");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidArrayExpression()
		throws Exception
	{
		Model model = JavaModelReader.read(VariableExpression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		parser.add(new Variable("data"));
		parser.add(new Variable("index"));

		parser.parse("data[index");
	}
	
}
