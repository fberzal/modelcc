package test.languages.imperative;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Function;
import test.languages.imperative.model.FunctionCallExpression;
import test.languages.imperative.model.Variable;
import test.languages.imperative.model.VariableExpression;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ExpressionTest extends ModelCCTest<Expression>
{
	public static final double EPSILON = 1e-9;

	
	@Test
	public void testValidExpressions()
		throws Exception
	{
		assertEquals (3, parse("3").eval(), EPSILON);
		assertEquals (8, parse("3+5").eval(), EPSILON);
		assertEquals (13, parse("3+5+5").eval(), EPSILON);
		assertEquals (13, parse("3+(5+5)").eval(), EPSILON);
		assertEquals (4, parse("3-5+6").eval(), EPSILON);
		assertEquals (20, parse("3*5+5").eval(), EPSILON);
		assertEquals (30, parse("3*(5+5)").eval(), EPSILON);
		assertEquals (10.6, parse("3/5+(2*5)").eval(), EPSILON);
		assertEquals (28, parse("3*2*5+-2").eval(), EPSILON);
		assertEquals (28, parse("3+5*5").eval(), EPSILON);
		assertEquals (4.5, parse("3+6/2/2").eval(), EPSILON);
		assertEquals (87, parse("3*5*1-5+6*12+5").eval(), EPSILON);
	}

	@Test
	public void testValidVariableExpression()
		throws Exception
	{
		Model model = JavaModelReader.read(Expression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		parser.add(new Variable("pi"));
		
		Expression expression = parser.parse("pi");

		assertNotNull(expression);
		assertTrue(expression instanceof VariableExpression);
	}
	
	@Test
	public void testValidFunctionCallExpressions()
		throws Exception
	{
		Model model = JavaModelReader.read(Expression.class);
		Parser<Expression> parser = ParserFactory.create(model);

		parser.add(new Function("f"));
		parser.add(new Function("g"));
		parser.add(new Variable("x"));

		Expression expression = parser.parse("f()");
		
		assertNotNull(expression);
		assertTrue(expression instanceof FunctionCallExpression);
		
		assertNotNull(parser.parse("f()"));
		assertNotNull(parser.parse("f(x)"));
		assertNotNull(parser.parse("f(1,2)"));

		assertNotNull(parser.parse("f(g(x))"));
	}
		
	// Invalid expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("+");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariableExpression()
		throws Exception
	{
		parse("pi"); // Undefined variable
	}
	
}
