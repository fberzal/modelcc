package test.languages.imperative;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.imperative.model.Identifier;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class IdentifierTest extends ModelCCTest<Identifier>
{
	@Test
	public void testValidIdentifiers()
		throws Exception
	{
		assertNotNull(parse("pi"));
		assertNotNull(parse("CONSTANT"));
		assertNotNull(parse("i2s"));
	}
		
	// Invalid identifiers

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidIdentifier()
		throws Exception
	{
		parse("2x");
	}
}
