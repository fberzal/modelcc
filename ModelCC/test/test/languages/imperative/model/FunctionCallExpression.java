package test.languages.imperative.model;

import org.modelcc.*;

public class FunctionCallExpression extends Expression implements IModel
{
	@Reference
	private Function function;
	
	@Prefix("\\(")
	@Suffix("\\)")
	@Separator(",")
	private Expression arguments[];

	
	public double eval ()
	{
		throw new UnsupportedOperationException("Function calls not implemented yet.");
	}
	
	public String toString ()
	{
		return function.toString();
	}
}
