package test.languages.imperative.model;

import org.modelcc.*;

@Prefix("function")
public class Function implements IModel
{
	@ID
	private Identifier id;
	
	@Optional
	@Prefix("\\(")
	@Suffix("\\)")
	@Separator(",")
	private Variable parameters[];

	@Optional
	@Prefix("var")
	@Suffix(";")
	@Separator(",")
	private Variable variables[];

	@Optional
	private Function functions[];

	private Statement statement;
	
	// Constructors
	
	public Function ()
	{
	}
	
	public Function (String id)
	{
		setId(id);
	}
	
	// Getters & setters
	
	public void setId (String id)
	{
		this.id = new Identifier(id);
	}
	
	// Execution
	
	public double run ()
	{
		return statement.run();
	}
	
	
	// toString
	
	public String toString ()
	{
		return id.toString();
	}
}
