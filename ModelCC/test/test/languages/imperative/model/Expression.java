/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model;

import org.modelcc.IModel;

public abstract class Expression implements IModel {

    public abstract double eval();

}
