package test.languages.imperative.model;

import org.modelcc.*;

public class VariableExpression extends Expression implements IModel
{
	@Reference
	private Variable variable;
	
	@Optional
	@Prefix("\\[")
	@Suffix("\\]")
	@Separator(",")
	private Expression indexes[];

	
	public double eval ()
	{
		throw new UnsupportedOperationException("Variable evaluation not implemented yet.");
	}
	
	public void setValue (double value)
	{
		throw new UnsupportedOperationException("Variable assignment not implemented yet.");		
	}
	
	public String toString ()
	{
		return variable.toString();
	}
}
