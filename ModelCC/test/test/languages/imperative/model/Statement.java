/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model;

import org.modelcc.IModel;

public abstract class Statement implements IModel 
{
    public abstract double run ();
}
