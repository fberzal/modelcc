package test.languages.imperative.model;

import org.modelcc.*;

public class Variable implements IModel
{
	@ID
	private Identifier id;
	
	public Variable ()
	{
	}
	
	public Variable (String id)
	{
		this.id = new Identifier(id);
	}

	public Variable (Identifier id)
	{
		this.id = id;
	}
	
	
	public String toString ()
	{
		return id.toString();
	}
}
