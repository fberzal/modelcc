/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.IModel;
import org.modelcc.Priority;

import test.languages.imperative.model.Expression;

@Priority(precedes=BinaryExpression.class)
public class UnaryExpression extends Expression implements IModel {
    public UnaryOperator op;
    public Expression e;

    @Override
    public double eval() {
        return op.eval(e);
    }
}
