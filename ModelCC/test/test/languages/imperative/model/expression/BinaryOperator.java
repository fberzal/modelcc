/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.IModel;

import test.languages.imperative.model.Expression;

@Associativity(AssociativityType.LEFT_TO_RIGHT)
public abstract class BinaryOperator implements IModel {

    public abstract double eval(Expression e1,Expression e2);

}
