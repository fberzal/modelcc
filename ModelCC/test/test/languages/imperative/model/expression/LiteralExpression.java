/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.IModel;

import test.languages.imperative.model.Expression;

public abstract class LiteralExpression extends Expression implements IModel {

}
