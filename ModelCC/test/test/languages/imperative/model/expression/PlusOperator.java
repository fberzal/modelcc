/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.IModel;
import org.modelcc.Pattern;

import test.languages.imperative.model.Expression;

@Pattern(regExp="\\+")
public class PlusOperator extends UnaryOperator implements IModel {

    @Override
    public double eval(Expression e) {
        return e.eval();
    }

}
