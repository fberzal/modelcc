/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.IModel;

import test.languages.imperative.model.Expression;

public class BinaryExpression extends Expression implements IModel {
    public Expression e1;
    public BinaryOperator op;
    public Expression e2;

    @Override
    public double eval() {
        return op.eval(e1,e2);
    }
}
