/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.imperative.model.expression;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Priority;

import test.languages.imperative.model.Expression;

@Priority(value=4)
@Pattern(regExp="\\/")
public class DivisionOperator extends BinaryOperator implements IModel {

    @Override
    public double eval(Expression e1, Expression e2) {
        return e1.eval()/e2.eval();
    }

}
