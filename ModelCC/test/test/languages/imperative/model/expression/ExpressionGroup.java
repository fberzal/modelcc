/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.expression;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

import test.languages.imperative.model.Expression;

@Prefix("\\(")
@Suffix("\\)")
public class ExpressionGroup extends Expression implements IModel 
{
    public Expression e;

    @Override
    public double eval() {
        return e.eval();
    }
}
