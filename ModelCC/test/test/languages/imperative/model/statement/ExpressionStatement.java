/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Statement;

public class ExpressionStatement extends Statement 
{
	private Expression expression;
	
	// Execution
	
    public double run ()
    {
    	return expression.eval();
    }
}
