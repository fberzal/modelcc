/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import org.modelcc.Prefix;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Statement;
import test.languages.imperative.model.VariableExpression;

public class AssignmentStatement extends Statement 
{
	private VariableExpression lhs;
	
	@Prefix("=")	
	private Expression rhs;
	
	// Execution
	
    public double run ()
    {
    	double result = rhs.eval();
    	
    	lhs.setValue(result);

    	return result;
    }
}
