/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.Optional;
import org.modelcc.Prefix;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Statement;

@Prefix("if")
@Composition(CompositionType.EAGER)
public class ConditionalStatement extends Statement 
{
	private Expression condition;
	
	private Statement thenStatement;
	
	@Optional
	@Prefix("else")
	private Statement elseStatement;
	
	// Execution
	
    public double run ()
    {
    	if (condition.eval()!=0)
    		return thenStatement.run();
    	else if (elseStatement!=null)
    		return elseStatement.run();

    	return 0;
    }
}
