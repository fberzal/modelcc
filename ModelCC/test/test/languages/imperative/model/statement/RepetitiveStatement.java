/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import org.modelcc.Prefix;

import test.languages.imperative.model.Expression;
import test.languages.imperative.model.Statement;

@Prefix("while")
public class RepetitiveStatement extends Statement 
{
	private Expression condition;
	
	private Statement statement;
	
	// Execution
	
    public double run ()
    {
    	while (condition.eval()!=0)
    		statement.run();

    	return 0;
    }
}
