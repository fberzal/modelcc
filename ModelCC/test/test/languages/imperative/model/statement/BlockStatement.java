/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;

import test.languages.imperative.model.Statement;

@Prefix("begin")
@Suffix("end")
public class BlockStatement extends Statement 
{
	@Separator(";")
	private Statement statements[];
	
	// Execution
	
    public double run ()
    {
    	double result = 0.0;
    	
    	if (statements!=null)
    		for (Statement s: statements)
    			result = s.run();
    	
    	return result;
    }
}
