/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.imperative.model.statement;

import org.modelcc.Prefix;

import test.languages.calculator.model.Expression;
import test.languages.imperative.model.Statement;

@Prefix("return")
public class ReturnStatement extends Statement 
{
	private Expression expression;
	
	// Execution
	
    public double run ()
    {
    	return expression.eval();
    }
}
