package test.languages.imperative;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ExpressionTest.class,
	                   IdentifierTest.class,
	                   VariableTest.class,
	                   StatementTest.class,
	                   FunctionTest.class })
public class AllTests {

}