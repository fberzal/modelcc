package test.languages.imperative;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.imperative.model.Function;
import test.languages.imperative.model.Statement;
import test.languages.imperative.model.Variable;
import test.languages.imperative.model.statement.ExpressionStatement;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class StatementTest extends ModelCCTest<Statement>
{
	public static final double EPSILON = 1e-9;

	@Test
	public void testValidExpressions()
		throws Exception
	{
		assertEquals (3, parse("3").run(), EPSILON);
		assertEquals (8, parse("3+5").run(), EPSILON);
		assertEquals (13, parse("3+5+5").run(), EPSILON);
		assertEquals (13, parse("3+(5+5)").run(), EPSILON);
		assertEquals (4, parse("3-5+6").run(), EPSILON);
		assertEquals (20, parse("3*5+5").run(), EPSILON);
		assertEquals (30, parse("3*(5+5)").run(), EPSILON);
		assertEquals (10.6, parse("3/5+(2*5)").run(), EPSILON);
		assertEquals (28, parse("3*2*5+-2").run(), EPSILON);
		assertEquals (28, parse("3+5*5").run(), EPSILON);
		assertEquals (4.5, parse("3+6/2/2").run(), EPSILON);
		assertEquals (87, parse("3*5*1-5+6*12+5").run(), EPSILON);
	}

	@Test
	public void testValidVariables()
		throws Exception
	{
		Model model = JavaModelReader.read(Statement.class);
		Parser<Statement> parser = ParserFactory.create(model);

		parser.add(new Variable("pi"));
		
		Statement expression = parser.parse("pi");

		assertNotNull(expression);
		assertTrue(expression instanceof ExpressionStatement);
	
		//assertEquals ( 3, parser.parse("pi").run(), EPSILON);		
		//assertEquals ( 3, parser.parse("return pi").run(), EPSILON);
	}
	
	
	@Test
	public void testValidAssignments()
		throws Exception
	{
		Model model = JavaModelReader.read(Statement.class);
		Parser<Statement> parser = ParserFactory.create(model);

		parser.add(new Variable("a"));
		parser.add(new Variable("b"));
		parser.add(new Variable("c"));
		
		assertNotNull(parser.parse("a=3"));
		assertNotNull(parser.parse("b=3+5"));
		assertNotNull(parser.parse("c=3*5+5"));
	}
	
	@Test
	public void testValidCalls()
		throws Exception
	{
		Model model = JavaModelReader.read(Statement.class);
		Parser<Statement> parser = ParserFactory.create(model);

		parser.add(new Function("f"));
		parser.add(new Function("g"));
		parser.add(new Variable("x"));
		
		assertNotNull(parser.parse("f(x)"));
		assertNotNull(parser.parse("f(g(x))"));
	}
	

	@Test
	public void testValidBlocks()
		throws Exception
	{
		assertEquals ( 0, parse("begin end").run(), EPSILON);
		assertEquals ( 1, parse("begin 1 end").run(), EPSILON);
		assertEquals ( 2, parse("begin 1; 2 end").run(), EPSILON);

		assertEquals ( 0, parse("begin begin end end").run(), EPSILON);
		assertEquals ( 2, parse("begin begin 1; 2 end end").run(), EPSILON);
		assertEquals ( 2, parse("begin 1; begin 2 end end").run(), EPSILON);
		assertEquals ( 2, parse("begin 1; begin end; 2 end").run(), EPSILON);
		assertEquals ( 2, parse("begin begin end; 1; 2 end").run(), EPSILON);
		assertEquals ( 2, parse("begin begin 1 end; 2 end").run(), EPSILON);
		assertEquals ( 2, parse("begin begin 1; 2 end end").run(), EPSILON);
		
	}
	
	@Test
	public void testValidReturnStatements()
		throws Exception
	{
		assertEquals ( 3, parse("return 3").run(), EPSILON);
		assertEquals ( 8, parse("return 3+5").run(), EPSILON);
		assertEquals (20, parse("return 3*5+5").run(), EPSILON);
	}
	
	@Test
	public void testValidConditionalStatements()
		throws Exception
	{
		assertEquals ( 2, parse("if (1) 2").run(), EPSILON);
		assertEquals ( 0, parse("if (0) 2").run(), EPSILON);
		
		assertEquals ( 2, parse("if (1) 2 else 3").run(), EPSILON);
		assertEquals ( 3, parse("if (0) 2 else 3").run(), EPSILON);
		
		assertEquals ( 2, parse("if (1) if (1) 2 else 3 else 4").run(), EPSILON);
		assertEquals ( 3, parse("if (1) if (0) 2 else 3 else 4").run(), EPSILON);
		assertEquals ( 4, parse("if (0) if (1) 2 else 3 else 4").run(), EPSILON);
		assertEquals ( 4, parse("if (0) if (0) 2 else 3 else 4").run(), EPSILON);

		assertEquals ( 0, parse("if (1) begin end else 3").run(), EPSILON);
		assertEquals ( 3, parse("if (0) begin end else 3").run(), EPSILON);

		assertEquals ( 2, parse("if (1) begin if (1) 2 end else 3").run(), EPSILON);
		assertEquals ( 0, parse("if (1) begin if (0) 2 end else 3").run(), EPSILON);
		assertEquals ( 3, parse("if (0) begin if (1) 2 end else 3").run(), EPSILON);
		assertEquals ( 3, parse("if (0) begin if (0) 2 end else 3").run(), EPSILON);
	}

	@Test
	public void testValidRepetitiveStatements()
		throws Exception
	{
		assertNotNull( parse("while (1) begin end") );
		assertNotNull( parse("while (0) begin end") );
	}
	
	// Invalid statements

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("+");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAssignment()
		throws Exception
	{
		parse("2+3=a");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAssignment2()
		throws Exception
	{
		parse("x = "); // Missing RHS
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidBlock()
		throws Exception
	{
		assertEquals ( 2, parse("begin ; end").run(), EPSILON);
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariable()
		throws Exception
	{
		parse("x");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidConditionalStatement()
		throws Exception
	{
		parse("if (1) else 2"); // Missing then statement
		
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testinvalidRepetitiveStatements()
		throws Exception
	{
		parse("while (1) ;"); // Missing loop body
	}
}
