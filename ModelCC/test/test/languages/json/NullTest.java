package test.languages.json;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.json.model.JSONNull;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class NullTest extends ModelCCTest<JSONNull>
{
	@Test
	public void testValidNull()
		throws Exception
	{
		assertNotNull ( parse("null"));
		
		assertNull ( parse("null").value() );
	}
	
	// Invalid null

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidNull()
		throws Exception
	{
		parse("NULL");
	}
}
