package test.languages.json;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { BooleanTest.class,
	                   NumberTest.class,
	                   StringTest.class,
	                   NullTest.class,
	                   ArrayTest.class,
	                   ObjectTest.class })
public class AllTests {

}