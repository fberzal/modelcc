package test.languages.json;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.json.model.JSONString;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class StringTest extends ModelCCTest<JSONString>
{
	@Test
	public void testValidStrings()
		throws Exception
	{
		assertNotNull ( parse("\"\""));
		assertNotNull ( parse("\"abc\""));
	}
	
	// Invalid string

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidString()
		throws Exception
	{
		parse("\"a");
	}
	
	// Escaped string
	
	@Test
	public void testEscapedString()
		throws Exception
	{
		assertNotNull ( parse("\"a\\\"b\""));
		assertEquals ( 3, parse("\"a\\\"b\"").value().length() );
		assertEquals ( "a\"b", parse("\"a\\\"b\"").value() );
	}
	
	
}
