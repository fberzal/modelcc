package test.languages.json;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.json.model.JSONArray;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ArrayTest extends ModelCCTest<JSONArray>
{
	@Test
	public void testValidArrays()
		throws Exception
	{
		assertNotNull ( parse("[1,2,3]"));
		assertNotNull ( parse("[\"a\",\"b\",\"c\"]"));
	}
	
	// Invalid string

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidArray()
		throws Exception
	{
		parse("[\"a");
	}
	
	// Array values
	
	public final double EPSILON = 1e-12;
	
	@Test
	public void testArrayValues()
		throws Exception
	{
		JSONArray array = parse("[31,10,1976]");
		
		assertEquals ( 3, array.size() );
		assertEquals ( 31, (Double) array.value(0), EPSILON );
		assertEquals ( 10, (Double) array.value(1), EPSILON );
		assertEquals ( 1976, (Double) array.value(2), EPSILON );
	}
	
	
}
