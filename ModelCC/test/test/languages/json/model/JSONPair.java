package test.languages.json.model;

import org.modelcc.*;

public class JSONPair implements IModel
{
	JSONString name;
	
	@Prefix(":")
	JSONValue value;
	
	// Getters
	
	public String name ()
	{
		return name.value();
	}
	
	public JSONValue json ()
	{
		return value;
	}
	
	public Object value ()
	{
		return value.value();
	}
	
	// toString
	
	public String toString ()
	{
		return name.toString() + ": " + value.toString();
	}
}
