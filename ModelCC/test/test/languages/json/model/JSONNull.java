/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.json.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="null")
public class JSONNull extends JSONValue implements IModel 
{
    @Value 
    String value;
    
    // Constructors
    
    public JSONNull() {}
        
    // Getter
    
    public Object value() 
    {
        return null;
    }
    
    // toString
    
    public String toString()
    {
    	return "null";
    }

}
