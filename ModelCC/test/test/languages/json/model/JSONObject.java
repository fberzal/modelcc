package test.languages.json.model;

import org.modelcc.*;

import java.util.HashMap;
import java.util.Map;

@Prefix("\\{")
@Suffix("\\}")
public class JSONObject extends JSONValue<Map> implements IModel
{
	@Separator(",")
	private JSONPair elements[];
	
	// Getters
	
	public Map value ()
	{
		HashMap<String,JSONValue> map = new HashMap();
		
		if (elements!=null)
			for (int i=0; i<elements.length; i++)
				map.put(elements[i].name(), elements[i].json());
		
		return map;
	}
	
	public String name (int i)
	{
		return elements[i].name();
	}
	
	
	public Object value (int i)
	{
		return json(i).value();
	}
	
	public Object value (String name)
	{
		JSONValue json = json(name);
		
		if (json!=null)
			return json.value();
		else
			return null;
	}
	
	
	public JSONValue json (int i)
	{
		return elements[i].json();
	}
	
	public JSONValue json (String name)
	{
		if (elements!=null)
			for (int i=0; i<elements.length; i++)
				if (elements[i].name().equals(name))
					return elements[i].json();

		return null;
	}
	
	public int size ()
	{
		if (elements!=null)
			return elements.length;
		else
			return 0;
	}
	
	// toString
	
	public String toString ()
	{
		String str = "{";

		if (elements!=null)
			for (int i=0; i<elements.length; i++) {
				str += elements[i].toString();
				
				if (i<elements.length-1)
					str += ",";
			}
		
		str += "}";
		
		return str;
	}
}
