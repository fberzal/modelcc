/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.json.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="(\\+|-)?[0-9]+(\\.[0-9]*)?")
public class JSONNumber extends JSONValue<Double> implements IModel 
{
    @Value 
    double value;
    
    // Constructors
    
    public JSONNumber() {}
    
    public JSONNumber(double value)
    {
    	this.value = value;
    }
    
    // Getter
    
    public Double value() 
    {
        return value;
    }
    
    public double doubleValue()
    {
    	return value;
    }
    
    public int intValue()
    {
    	return (int) value;
    }
    
    // toString
    
    public String toString()
    {
    	return Double.toString(value);
    }

}
