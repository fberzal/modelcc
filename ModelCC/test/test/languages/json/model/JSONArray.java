package test.languages.json.model;

import org.modelcc.*;

import java.util.Arrays;
import java.util.List;

@Prefix("\\[")
@Suffix("\\]")
public class JSONArray extends JSONValue<List> implements IModel
{
	@Separator(",")
	private JSONValue elements[];
	
	// Getters
	
	public List value ()
	{
		return Arrays.asList(elements);
	}
	
	public Object value (int i)
	{
		return elements[i].value();
	}
	
	public JSONValue json (int i)
	{
		return elements[i];
	}
	
	public int size ()
	{
		if (elements!=null)
			return elements.length;
		else
			return 0;
	}
	
	// toString
	
	public String toString ()
	{
		String str = "[";

		if (elements!=null)
			for (int i=0; i<elements.length; i++) {
				str += elements[i].toString();
				
				if (i<elements.length-1)
					str += ",";
			}
		
		str += "]";
		
		return str;
	}
}
