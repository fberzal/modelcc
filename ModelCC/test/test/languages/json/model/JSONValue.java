package test.languages.json.model;

import org.modelcc.*;

public abstract class JSONValue<T> implements IModel
{
	public abstract T value();
}
