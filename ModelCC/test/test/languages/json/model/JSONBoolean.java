/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.json.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="true|false")
public class JSONBoolean extends JSONValue<Boolean> implements IModel 
{
    @Value 
    boolean value;
    
    // Constructors
    
    public JSONBoolean() {}
    
    public JSONBoolean(boolean value)
    {
    	this.value = value;
    }
    
    // Getter
    
    public Boolean value() 
    {
        return value;
    }
    
    // toString
    
    public String toString()
    {
    	return Boolean.toString(value);
    }

}
