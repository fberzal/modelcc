package test.languages.json.model;

import org.modelcc.*;

@Pattern(regExp="\"(\\\\\"|[^\"])*\"")
public class JSONString extends JSONValue<String> implements IModel
{
	@Value
	private String val;
	
	@Setup
	public void setup() 
	{
		val = val.substring(1,val.length()-1);
		val = val.replace("\\\"","\"");
	}
	
	// Getters
	
	public String value ()
	{
		return val;
	}
	
	// toString
	
	public String toString ()
	{
		return "\"" + val.replace("\"","\\\"") + "\"";
	}
}
