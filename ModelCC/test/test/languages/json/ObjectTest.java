package test.languages.json;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.json.model.JSONArray;
import test.languages.json.model.JSONObject;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ObjectTest extends ModelCCTest<JSONObject>
{
	private String record = 
		" { \"firstName\": \"John\","
		+ " \"lastName\": \"Smith\","
		+ " \"age\": 33,"
		+ " \"address\": {"
		+ "    \"street\": \"21 2nd Street\","
		+ "    \"city\": \"New York\","
		+ "    \"state\": \"NY\","
		+ "    \"postalCode\": \"10021\""
		+ " },"
		+ " \"phone\": \"212 555-1239\","
		+ " \"email\": [ \"jsmith@acm.org\", \"jsmith@computer.org\" ]"
		+ "}";
	
	@Test
	public void testValidObject()
		throws Exception
	{
		JSONObject object = parse(record);
		
		assertNotNull ( object );
		assertEquals (6, object.size() );
		
		assertEquals ( "firstName", object.name(0) );
		assertEquals ( "John", object.value(0));
		assertEquals ( "John", object.value("firstName"));
		
		assertEquals ( "lastName", object.name(1) );
		assertEquals ( "Smith", object.value(1));
		assertEquals ( "Smith", object.value("lastName"));

		assertEquals ( "age", object.name(2) );
		assertEquals ( 33, ((Double)object.value(2)).intValue());
		assertEquals ( 33, ((Double)object.value("age")).intValue());

		assertEquals ( "address", object.name(3) );
		JSONObject address = (JSONObject) object.json(3); // == object.json("address");

		assertEquals ( 4, address.size() );
		assertEquals ( "21 2nd Street", address.value("street") );
		assertEquals ( "New York", address.value("city") );
		assertEquals ( "NY", address.value("state") );
		assertEquals ( "10021", address.value("postalCode") );

		assertEquals ( "phone", object.name(4) );
		assertEquals ( "212 555-1239", object.value(4));
		assertEquals ( "212 555-1239", object.value("phone"));

		assertEquals ( "email", object.name(5) );
		JSONArray array = (JSONArray) object.json(5); // == object.json("email");
		assertEquals ( 2, array.size() );
		assertEquals ( "jsmith@acm.org", array.value(0) );
		assertEquals ( "jsmith@computer.org", array.value(1) );
	}		

	
	// Invalid object

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidObject()
		throws Exception
	{
		parse("{\"a");
	}
	
	// JSON-RPC
	
	@Test
	public void testRPC()
		throws Exception
	{
		String request = "{\"jsonrpc\": \"2.0\", \"method\": \"subtract\", \"params\": [42, 23], \"id\": 1}";
		String response = "{\"jsonrpc\": \"2.0\", \"result\": 19, \"id\": 1}";
		
		JSONObject jsonRequest = parse(request);
		JSONObject jsonResponse = parse(response);
		
		assertEquals("2.0", jsonRequest.value("jsonrpc"));
		assertEquals("2.0", jsonResponse.value("jsonrpc"));
		
		assertEquals(1, ((Double)jsonRequest.value("id")).intValue());
		assertEquals(1, ((Double)jsonResponse.value("id")).intValue());
		
		assertEquals("subtract", jsonRequest.value("method"));
		
		JSONArray parameters = (JSONArray) jsonRequest.json("params");
		
		assertEquals(2, parameters.size());
		assertEquals(42, ((Double)parameters.value(0)).intValue());
		assertEquals(23, ((Double)parameters.value(1)).intValue());
		
		assertEquals(19, ((Double)jsonResponse.value("result")).intValue());
		
	}	
}
