package test.languages.json;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.json.model.JSONBoolean;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class BooleanTest extends ModelCCTest<JSONBoolean>
{
	@Test
	public void testValidBoolean()
		throws Exception
	{
		assertNotNull ( parse("true"));
		assertNotNull ( parse("false"));
		
		assertTrue ( parse("true").value() );
		assertFalse ( parse("false").value() );
	}
	
	// Invalid boolean

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidBoolean()
		throws Exception
	{
		parse("0");
	}
}
