package test.languages.calculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.calculator.model.Constant;
import test.languages.calculator.model.Expression;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ExpressionTest extends ModelCCTest<Expression>
{
	public static final double EPSILON = 1e-9;

	@Test
	public void testTutorialExample()
		throws Exception
	{
		// Read the model.
		Model model = JavaModelReader.read(Expression.class);
		// Create the parser.
		Parser<Expression> parser = ParserFactory.create(model);
		// Define a constant
		parser.add(new Constant("pi", 3.1415927));
		// Use the predefined constant in JUnit tests for arithmetic expressions
		assertEquals(3.1415927, parser.parse("pi").eval(), EPSILON);
		assertEquals(2*3.1415927, parser.parse("2*pi").eval(), EPSILON);		
	}	

	@Test
	public void testInvoiceExample()
		throws Exception
	{
		// Read the model.
		Model model = JavaModelReader.read(Expression.class);
		// Create the parser.
		Parser<Expression> parser = ParserFactory.create(model);
		// Define a constant
		parser.add(new Constant("price", 10));
		parser.add(new Constant("units", 2));
		parser.add(new Constant("tax", 0.21));
		parser.add(new Constant("shipping", 4.99));
		// Use the predefined constant in JUnit tests for arithmetic expressions
		assertEquals(10*2*0.21+4.99, parser.parse("price*units*tax+shipping").eval(), EPSILON);		
	}	
	
	@Test
	public void testValidExpressions()
		throws Exception
	{
		assertEquals (3, parse("3").eval(), EPSILON);
		assertEquals (8, parse("3+5").eval(), EPSILON);
		assertEquals (13, parse("3+5+5").eval(), EPSILON);
		assertEquals (13, parse("3+(5+5)").eval(), EPSILON);
		assertEquals (4, parse("3-5+6").eval(), EPSILON);
		assertEquals (20, parse("3*5+5").eval(), EPSILON);
		assertEquals (30, parse("3*(5+5)").eval(), EPSILON);
		assertEquals (10.6, parse("3/5+(2*5)").eval(), EPSILON);
		assertEquals (28, parse("3*2*5+-2").eval(), EPSILON);
		assertEquals (28, parse("3+5*5").eval(), EPSILON);
		assertEquals (4.5, parse("3+6/2/2").eval(), EPSILON);
		assertEquals (87, parse("3*5*1-5+6*12+5").eval(), EPSILON);
	}

	
	@Test
	public void testConstant()
		throws Exception
	{
		Parser<Expression> parser = createParser();

		parser.add( new Constant("pi", 3.1415927));

		assertEquals (3.1415927, parser.parse("pi").eval(), EPSILON);
		assertEquals (2*3.1415927, parser.parse("2*pi").eval(), EPSILON);
	}

	@Test
	public void testConstantChange()
		throws Exception
	{
		Parser<Expression> parser = createParser();
		Constant constant = new Constant("rate",3.0);
		
		parser.add(constant);
		assertEquals (3.0, parser.parse("rate").eval(), EPSILON);
		
		constant.setValue(5.0);
		assertEquals (5.0, parser.parse("rate").eval(), EPSILON);
	}
	
	// Invalid expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("+");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidConstant()
		throws Exception
	{
		// Undefined pi constant
		parse("2*pi");
	}
	
}
