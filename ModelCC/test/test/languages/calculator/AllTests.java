package test.languages.calculator;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ExpressionTest.class,
	                   ConstantTest.class,
	                   ExpressionTest.class })
public class AllTests {

}