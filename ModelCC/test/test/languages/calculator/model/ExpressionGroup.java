/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.calculator.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Prefix("\\(")
@Suffix("\\)")
public class ExpressionGroup extends Expression implements IModel 
{
    public Expression e;

    @Override
    public double eval() {
        return e.eval();
    }
}
