package test.languages.calculator.model;

import org.modelcc.*;

@Pattern(regExp="[a-zA-Z_]([a-zA-Z_]|[0-9])*")
public class Identifier implements IModel
{
	@Value
	private String id;
	
	// Constructors
	
	public Identifier () {}
	
	public Identifier (String id)
	{
		this.id = id;
	}
	
	// toString
	
	public String toString ()
	{
		return id;
	}
}
