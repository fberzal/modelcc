package test.languages.calculator.model;

import org.modelcc.*;

public class Constant implements IModel
{
	@ID
	private Identifier id;

	@Prefix("=")
	private RealLiteral value;
	
	public Constant ()
	{
	}
	
	public Constant (String id, double value)
	{
		setId(id);
		setValue(value);
	}
	
	public void setId (String id)
	{
		this.id = new Identifier(id);
	}

	public void setValue (double value)
	{
		this.value = new RealLiteral(value);
	}
	
	public double getValue ()
	{
		if (value!=null)
			return value.eval();
		else
			return 0;
	}
	
	
	public String toString ()
	{
		return id + " = " + getValue();
	}
}
