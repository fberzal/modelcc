/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.calculator.model;

import org.modelcc.IModel;
import org.modelcc.Priority;

@Priority(precedes=BinaryExpression.class)
public class UnaryExpression extends Expression implements IModel {
    public UnaryOperator op;
    public Expression e;

    @Override
    public double eval() {
        return op.eval(e);
    }
}
