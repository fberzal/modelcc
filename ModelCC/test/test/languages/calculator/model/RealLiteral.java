/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.calculator.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="[0-9]+\\.[0-9]*")
public class RealLiteral extends LiteralExpression implements IModel {

    @Value 
    double value;
    
    public RealLiteral() {}
    
    public RealLiteral(double value)
    {
    	this.value = value;
    }
    
    @Override
    public double eval() {
        return value;
    }

}
