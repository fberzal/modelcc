/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.calculator.model;

import org.modelcc.IModel;

public abstract class LiteralExpression extends Expression implements IModel {

}
