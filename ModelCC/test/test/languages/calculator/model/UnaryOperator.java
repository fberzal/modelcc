/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.calculator.model;

import org.modelcc.IModel;

public abstract class UnaryOperator implements IModel {

    public abstract double eval(Expression e);

}
