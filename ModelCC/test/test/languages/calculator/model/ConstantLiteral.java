package test.languages.calculator.model;

import org.modelcc.*;

public class ConstantLiteral extends LiteralExpression implements IModel
{
	@Reference
	private Constant constant;

	
	public double eval ()
	{
		return constant.getValue();
	}
	
	public String toString ()
	{
		return constant.toString();
	}
}
