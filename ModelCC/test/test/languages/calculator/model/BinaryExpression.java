/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package test.languages.calculator.model;

import org.modelcc.IModel;

public class BinaryExpression extends Expression implements IModel {
    public Expression e1;
    public BinaryOperator op;
    public Expression e2;

    @Override
    public double eval() {
        return op.eval(e1,e2);
    }
}
