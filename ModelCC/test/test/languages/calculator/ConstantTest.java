package test.languages.calculator;

import static org.junit.Assert.*;

import org.junit.Test;
import org.modelcc.parser.Parser;

import test.languages.calculator.model.Constant;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ConstantTest extends ModelCCTest<Constant>
{
	public static final double EPSILON = 1e-9;

	@Test
	public void testValidConstant()
		throws Exception
	{
		Parser<Constant> parser = createParser();
		
		assertEquals (3.1415927, parser.parse("pi = 3.1415927").getValue(), EPSILON);
		assertEquals (2.7182818, parser.parse("e = 2.7182818").getValue(), EPSILON);
	}
	
	// Invalid constants

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidConstant()
		throws Exception
	{
		parse("2x = pi");
	}
}
