package test.languages.arithmetic;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.arithmetic.model.RealLiteral;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class RealLiteralTest extends ModelCCTest<RealLiteral>
{
	public static final double EPSILON = 1e-9;

	// Valid expressions
	
	private String literals[] = new String[] {
		"10.",
		"-10.",
		"1000.0",
        "-1000.0",
        "3.7",
        "0.38",
        "+137.42",
        "-137.42",
        "-0.0013742",
        "1e9",
        "1e+9",
        "1e-9",
        "1.2e3",
        "1.2E3",
        "1.2e+3",
        "1.2E+3",
        "1.2e-3",
        "1.2E-3"
	};

	private double values[] = new double[] {
		10,
		-10,
		1000,
	    -1000,
		3.7,
		0.38,
		137.42,
		-137.42,
		-0.0013742,
		1e9,
		1e+9,
		1e-9,
		1200,
		1200,
		1200,
		1200,
		0.0012,
		0.0012
	};

	@Test
	public void testValidLiterals()
		throws Exception
	{
		RealLiteral expression;
		
		for (int i=0; i<literals.length; i++) {
			expression = parse(literals[i]);	
			assertEquals( values[i], expression.eval(), EPSILON );
		}
	}

	// Invalid literal expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidLiteral1()
		throws Exception
	{
		parse("+");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidLiteral2()
		throws Exception
	{
		parse(".39");
	}	
}
