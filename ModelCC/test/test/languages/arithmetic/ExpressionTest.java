package test.languages.arithmetic;

import static org.junit.Assert.*;

import java.io.StringReader;

import org.junit.Test;
import org.modelcc.io.java.JavaModelReader;
import org.modelcc.language.metamodel.LanguageModel;
import org.modelcc.lexer.Lexer;
import org.modelcc.lexer.LexerFactory;
import org.modelcc.lexer.LexicalGraph;
import org.modelcc.lexer.Token;
import org.modelcc.lexer.Tokenizer;
import org.modelcc.lexer.TokenizerFactory;
import org.modelcc.lexer.flex.Flex;
import org.modelcc.lexer.lamb.Lamb;
import org.modelcc.metamodel.Model;
import org.modelcc.parser.Parser;
import org.modelcc.parser.ParserFactory;

import test.languages.arithmetic.model.BinaryExpression;
import test.languages.arithmetic.model.Expression;
import test.languages.arithmetic.model.ExpressionGroup;
import test.languages.arithmetic.model.IntegerLiteral;
import test.languages.arithmetic.model.LiteralExpression;
import test.languages.arithmetic.model.RealLiteral;
import test.languages.arithmetic.model.UnaryExpression;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ExpressionTest extends ModelCCTest<Expression>
{
	public static final double EPSILON = 1e-9;
	
	@Test
	public void testTutorialExample()
		throws Exception
	{
		// Read the model.
		Model model = JavaModelReader.read(Expression.class);
		// Generate the parser.
		Parser<Expression> parser = ParserFactory.create(model);
		// Parse the input string and instantiate the corresponding expression.
		Expression expr = parser.parse("10/(2+3)*0.5+1");
		// Evaluate the expression.
		double value = expr.eval();

		assertEquals(2.0, value, EPSILON);
	}

	@Test
	public void testValidExpressions()
		throws Exception
	{
		assertEquals (3, parse("3").eval(), EPSILON);
		assertEquals (8, parse("3+5").eval(), EPSILON);
		assertEquals (13, parse("3+5+5").eval(), EPSILON);
		assertEquals (13, parse("3+(5+5)").eval(), EPSILON);
		assertEquals (4, parse("3-5+6").eval(), EPSILON);
		assertEquals (20, parse("3*5+5").eval(), EPSILON);
		assertEquals (30, parse("3*(5+5)").eval(), EPSILON);
		assertEquals (10.6, parse("3/5+(2*5)").eval(), EPSILON);
		assertEquals (28, parse("3*2*5+-2").eval(), EPSILON);
		assertEquals (28, parse("3+5*5").eval(), EPSILON);
		assertEquals (4.5, parse("3+6/2/2").eval(), EPSILON);
		assertEquals (87, parse("3*5*1-5+6*12+5").eval(), EPSILON);
	}

	// Invalid expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("+");
	}
	
	// Expression tokens at the lexer level
	
	@Test
	public void testLexer()
		throws Exception
	{
		// Read the model and analyze its lexical structure.
		LanguageModel model = JavaModelReader.read(Expression.class);
		Lexer lexer = LexerFactory.create(model);
		
		assertEquals(1, countTokens(lexer, "1", IntegerLiteral.class));
		assertEquals(1, countTokens(lexer, "1", RealLiteral.class));
		assertEquals(2, countTokens(lexer, "1", LiteralExpression.class)); // IntegerLiteral & RealLiteral
		
		assertEquals(2, countTokens(lexer, "2*3", IntegerLiteral.class));
		assertEquals(4, countTokens(lexer, "2*3", LiteralExpression.class)); // IntegerLiteral & RealLiteral
		
		assertEquals(2, countTokens(lexer, "2.2*3.3", IntegerLiteral.class)); // Lexical ambiguity discarded by the parser
		assertEquals(2, countTokens(lexer, "2.2*3.3", RealLiteral.class));
		assertEquals(4, countTokens(lexer, "2.2*3.3", LiteralExpression.class));
	}	

	private int countTokens (Lexer lexer, String input, Class type)
	{
		int count = 0;
		
		StringReader sr = new StringReader(input);
		LexicalGraph lg = lexer.scan(sr);		

		for (Token token: lg.getTokens()) {
			// Do whatever you want with your token string using token.getValue()
			if (type.isAssignableFrom( token.getTokenClass() ))
				count++; 
		}
		
		return count;
	}

	// Expression tokens at the tokenizer level

	@Test
	public void testTokenizerFlex()
		throws Exception
	{
		// Read the model and analyze its lexical structure.
		LanguageModel model = JavaModelReader.read(Expression.class);
		TokenizerFactory factory = new TokenizerFactory(model, Flex.class);
			
		assertEquals(1, countTokens(factory, "1", LiteralExpression.class));
		assertEquals(2, countTokens(factory, "2*3", LiteralExpression.class));
		assertEquals(0, countTokens(factory, "2.2*3.3", IntegerLiteral.class)); // Integer discarded by greedy Flex lexer
		assertEquals(2, countTokens(factory, "2.2*3.3", RealLiteral.class));
	}	

	@Test
	public void testTokenizerLamb()
		throws Exception
	{
		// Read the model and analyze its lexical structure.
		LanguageModel model = JavaModelReader.read(Expression.class);
		TokenizerFactory factory = new TokenizerFactory(model, Lamb.class);
			
		assertEquals(2, countTokens(factory, "1", LiteralExpression.class));    // Integer & Real
		assertEquals(1, countTokens(factory, "1", IntegerLiteral.class));
		assertEquals(1, countTokens(factory, "1", RealLiteral.class)); 
		assertEquals(4, countTokens(factory, "2*3", LiteralExpression.class));  // Integer & Real
		assertEquals(2, countTokens(factory, "2*3", IntegerLiteral.class));
		assertEquals(2, countTokens(factory, "2*3", RealLiteral.class)); 
		assertEquals(2, countTokens(factory, "2.2*3.3", IntegerLiteral.class)); 
		assertEquals(2, countTokens(factory, "2.2*3.3", RealLiteral.class));
	}	

	private int countTokens (TokenizerFactory factory, String input, Class type) 
			throws Exception
	{
		int count = 0;
		
		StringReader sr = new StringReader(input);
		Tokenizer tokenizer = factory.create(sr);
		
		Token token = tokenizer.nextToken();
		
		while (token!=null) {
			// Do whatever you want with your token string using token.getValue()
			if (type.isAssignableFrom( token.getTokenClass() ))
				count++;
			
			token = tokenizer.nextToken();
		}
		
		return count;
	}

	// Expression elements at the syntactic level (i.e. traversing the object graph)

	@Test
	public void testObjectGraph()
		throws Exception
	{
		assertEquals(1, countElements(parse("3"), LiteralExpression.class));
		assertEquals(2, countElements(parse("2*3"), LiteralExpression.class));
		assertEquals(0, countElements(parse("2.2*3.3"), IntegerLiteral.class)); 
		assertEquals(2, countElements(parse("2.2*3.3"), RealLiteral.class));
	}	

	private int countElements (Expression expression, Class type) 
	{
		int count = 0;
		
		Class expressionType = expression.getClass();
		
		if (type.isAssignableFrom(expressionType))
			count++;
		
		if (expression instanceof ExpressionGroup)
			count += countElements( ((ExpressionGroup)expression).e, type);
		else if (expression instanceof UnaryExpression)
			count += countElements( ((UnaryExpression)expression).e, type);
		else if (expression instanceof BinaryExpression)
			count += countElements( ((BinaryExpression)expression).e1, type)
			       + countElements( ((BinaryExpression)expression).e2, type);
		
		return count;
	}
	
}
