/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="-")
public class MinusOperator extends UnaryOperator implements IModel {

    @Override
    public double eval(Expression e) {
        return -e.eval();
    }

}
