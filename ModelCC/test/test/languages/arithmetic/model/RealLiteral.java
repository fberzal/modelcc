/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;
import org.modelcc.Value;

public class RealLiteral extends LiteralExpression implements IModel {

    @Value public double value;

    @Override
    public double eval() {
        return value;
    }

}
