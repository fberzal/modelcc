/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Priority;

@Priority(value=5)
@Pattern(regExp="\\+")
public class AdditionOperator extends BinaryOperator implements IModel {

    @Override
    public double eval(Expression e1, Expression e2) {
        return e1.eval()+e2.eval();
    }
    
    @Override
    public String toString ()
    {
    	return "+";
    }

}
