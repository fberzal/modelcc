/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;
import org.modelcc.Value;

public class IntegerLiteral extends LiteralExpression implements IModel 
{
    @Value public int value;

    @Override
    public double eval() {
        return value;
    }

    @Override
    public String toString ()
    {
    	return Integer.toString(value);
    }
    
}
