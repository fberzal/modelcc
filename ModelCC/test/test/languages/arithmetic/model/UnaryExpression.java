/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;

public class UnaryExpression extends Expression implements IModel {
    public UnaryOperator op;
    public Expression e;

    @Override
    public double eval() {
        return op.eval(e);
    }
}
