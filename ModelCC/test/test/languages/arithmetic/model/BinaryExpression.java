/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.arithmetic.model;

import org.modelcc.IModel;
import org.modelcc.Position;

public class BinaryExpression extends Expression implements IModel {
	
    public Expression e1;

    public BinaryOperator op;
    
    @Position(element="op",position=Position.AFTER)
    public Expression e2;

    @Override
    public double eval() {
        return op.eval(e1,e2);
    }
    
    @Override
    public String toString() 
    {
    	return e1.toString()+op.toString()+e2.toString();
    }
}
