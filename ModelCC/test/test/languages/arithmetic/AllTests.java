package test.languages.arithmetic;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { ExpressionTest.class,
	                   RealLiteralTest.class })
public class AllTests {

}