package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.AWKPattern;
import test.languages.awk.model.AWKRegularExpressionPattern;
import test.modelcc.ModelCCTest;

public class AWKPatternTest extends ModelCCTest<AWKPattern> 
{
	@Test
	public void testValidSimplePattern()
		throws Exception
	{
		AWKPattern expression = parse("/foo/");
		
		assertEquals("/foo/", expression.toString());
		assertTrue( expression instanceof AWKRegularExpressionPattern );
	}

	@Test
	public void testValidRegularExpressionPattern()
		throws Exception
	{
		AWKPattern expression = parse("/[a-zA-Z][a-zA-Z0-9_]+/");
		
		assertEquals("/[a-zA-Z][a-zA-Z0-9_]+/", expression.toString());
		assertTrue( expression instanceof AWKRegularExpressionPattern );
	}
	
	@Test
	public void testValidEmptyPattern()
		throws Exception
	{
		AWKPattern expression = parse("//");
		
		assertEquals("//", expression.toString());
		assertTrue( expression instanceof AWKRegularExpressionPattern );
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidPattern()
		throws Exception
	{
		parse("/abc");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidPattern2()
		throws Exception
	{
		parse("/aa/aa/");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidPattern3()
		throws Exception
	{
		parse("/");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidPattern4()
		throws Exception
	{
		parse("aaa");
	}
	
}
