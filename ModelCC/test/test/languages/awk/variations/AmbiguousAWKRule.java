package test.languages.awk.variations;

import org.modelcc.IModel;
import org.modelcc.Optional;

import test.languages.awk.model.*;

public class AmbiguousAWKRule implements IModel 
{
	@Optional
	private AWKPattern pattern;
	
	@Optional
	private AWKAction action;

	
	public AWKPattern getPattern ()
	{
		return pattern;
	}
	
	public AWKAction getAction ()
	{
		return action;
	}
}
