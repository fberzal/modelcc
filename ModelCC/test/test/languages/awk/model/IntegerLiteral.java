package test.languages.awk.model;

import org.modelcc.IModel;
import org.modelcc.Value;

public class IntegerLiteral implements IModel
{
	@Value
	int value;
}
