package test.languages.awk.model;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Composition;
import org.modelcc.CompositionType;

@Composition(CompositionType.EAGER)
public class AWKRule implements IModel 
{
	@Optional
	private AWKPattern pattern;
	
	@Optional
	private AWKAction action;

	
	public AWKPattern getPattern ()
	{
		return pattern;
	}
	
	public AWKAction getAction ()
	{
		return action;
	}
}
