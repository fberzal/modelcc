package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.AWKStatement;
import test.modelcc.ModelCCTest;

public class AWKStatementTest extends ModelCCTest<AWKStatement>
{
	@Test
	public void testValidPrintStatement()
		throws Exception
	{
		AWKStatement expression = parse("print $1");
		
		assertEquals("print $1", expression.toString());
		assertTrue( expression instanceof AWKStatement );
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidPrintStatement()
		throws Exception
	{
		parse("print 0");
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidStatement()
		throws Exception
	{
		parse("$0");
	}
}
