package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.AWKExpression;
import test.languages.awk.model.AWKField;
import test.modelcc.ModelCCTest;

public class AWKExpressionTest extends ModelCCTest<AWKExpression>
{
	@Test
	public void testValidFieldExpression()
		throws Exception
	{
		AWKExpression expression = parse("$1");
		
		assertEquals("$1", expression.toString());
		assertTrue( expression instanceof AWKField );
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("0");
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidRegularExpression()
		throws Exception
	{
		parse("/abc");
	}
}
