package test.languages.awk;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { AWKExpressionTest.class,
					   AWKFieldTest.class,
					   AWKStatementTest.class,
					   AWKPatternTest.class,
					   AWKActionTest.class,
					   AWKRuleTest.class,
					   AWKProgramTest.class,
					   VerySimpleTest.class,
					   AmbiguousAWKProgramTest.class})
public class AllTests {

}