package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.*;
import test.modelcc.ModelCCTest;

public class AWKRuleTest extends ModelCCTest<AWKRule>
{
	@Test
	public void testValidRule()
		throws Exception
	{
		AWKRule rule = parse("/[a-zA-Z][a-zA-Z0-9_]+/ { print $1 }");
		
		AWKPattern pattern = rule.getPattern();
		
		assertEquals("/[a-zA-Z][a-zA-Z0-9_]+/", pattern.toString());

		AWKAction action = rule.getAction();
		
		assertEquals("{ print $1 }", action.toString());
		assertTrue(action.getStatement() instanceof AWKPrintStatement);
		
		AWKPrintStatement print = (AWKPrintStatement) action.getStatement();
		
		assertEquals("$1", print.getArgument().toString() );
		assertTrue ( print.getArgument() instanceof AWKField );
	}

	@Test
	public void testNullAction()
		throws Exception
	{
		AWKRule rule = parse("/[a-zA-Z][a-zA-Z0-9_]+/");
		
		AWKPattern pattern = rule.getPattern();
		
		assertEquals("/[a-zA-Z][a-zA-Z0-9_]+/", pattern.toString());

		assertNull(rule.getAction());
	}

	@Test
	public void testNullPattern()
		throws Exception
	{
		AWKRule rule = parse("{ print $1 }");
		
		assertNull(rule.getPattern());

		AWKAction action = rule.getAction();
		
		assertEquals("{ print $1 }", action.toString());
		assertTrue(action.getStatement() instanceof AWKPrintStatement);
		
		AWKPrintStatement print = (AWKPrintStatement) action.getStatement();
		
		assertEquals("$1", print.getArgument().toString() );
		assertTrue ( print.getArgument() instanceof AWKField );
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidRule()
		throws Exception
	{
		parse("{ abc");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidRule2()
		throws Exception
	{
		parse("/aaaa/ /aa/");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidRule3()
		throws Exception
	{
		parse("{ $1 } { $1 }");
	}
	
}

