package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.*;
import test.modelcc.ModelCCTest;

public class AWKActionTest extends ModelCCTest<AWKAction>
{
	@Test
	public void testPrintAction()
		throws Exception
	{
		AWKAction action = parse("{ print $0 }");
		
		assertEquals("{ print $0 }", action.toString());
		assertTrue(action.getStatement() instanceof AWKPrintStatement);
		
		AWKPrintStatement print = (AWKPrintStatement) action.getStatement();
		
		assertEquals("$0", print.getArgument().toString() );
		assertTrue ( print.getArgument() instanceof AWKField );
	}

	@Test
	public void testEmptyAction()
		throws Exception
	{
		AWKAction action = parse("{ }");

		assertNull(action.getStatement());		
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAction()
		throws Exception
	{
		parse("{ abc");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAction2()
		throws Exception
	{
		parse("{ 0 }");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAction3()
		throws Exception
	{
		parse("$0");
	}

}

