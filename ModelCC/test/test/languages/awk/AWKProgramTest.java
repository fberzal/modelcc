package test.languages.awk;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.awk.model.*;
import test.modelcc.ModelCCTest;

public class AWKProgramTest extends ModelCCTest<AWKProgram>
{
	@Test
	public void testValidProgram()
		throws Exception
	{
		AWKProgram program = parse("// { print $0 }");
		
		assertEquals(1, program.getRules().length); 
		
		AWKAction action = program.getRule(0).getAction();
		
		assertEquals("{ print $0 }", action.toString());
		assertTrue(action.getStatement() instanceof AWKPrintStatement);
		
		AWKPrintStatement print = (AWKPrintStatement) action.getStatement();
		
		assertEquals("$0", print.getArgument().toString() );
		assertTrue ( print.getArgument() instanceof AWKField );
	}

	@Test
	public void testValidPatternProgram()
		throws Exception
	{
		AWKProgram program = parse("/[a-zA-Z][a-zA-Z0-9_]+/");
		
		assertEquals(1, program.getRules().length); 
		
		assertNotNull(program.getRule(0).getPattern());
		assertNull(program.getRule(0).getAction());
	}
	
	@Test
	public void testValidActionProgram()
		throws Exception
	{
		AWKProgram program = parse("{ print $1 }");
		
		assertEquals(1, program.getRules().length); 
		assertNull(program.getRule(0).getPattern());
		assertNotNull(program.getRule(0).getAction());
	}

	@Test
	public void testValid2PatternProgram()
		throws Exception
	{
		AWKProgram program = parse("/aaaa/ \n /aa/");
		
		assertEquals(2, program.getRules().length); 
		assertNotNull(program.getRule(0).getPattern());
		assertNull(program.getRule(0).getAction());
		assertNotNull(program.getRule(1).getPattern());
		assertNull(program.getRule(1).getAction());
	}

	@Test
	public void testValid2ActionProgram()
		throws Exception
	{
		AWKProgram program = parse("{ print $0 } \n { print $0 }");
		
		assertEquals(2, program.getRules().length); 
		assertNull(program.getRule(0).getPattern());
		assertNotNull(program.getRule(0).getAction());
		assertNull(program.getRule(1).getPattern());
		assertNotNull(program.getRule(1).getAction());
	}
	
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProgram()
		throws Exception
	{
		parse("{ abc");
	}

	
}

