/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.types.model.DecimalModel;

public class DecimalParseTest 
{    
    public static final double EPSILON = 1e-9;

    @Test
    public void testDecimalModel() 
    {
        assertEquals(10.,DecimalModel.parse("10"), EPSILON);
        assertEquals(-10.,DecimalModel.parse("-10"), EPSILON);
        assertEquals(1000.,DecimalModel.parse("10e2"), EPSILON);
        assertEquals(-1000.,DecimalModel.parse("-10e2"), EPSILON);
        assertEquals(37.,DecimalModel.parse("+37E+0"), EPSILON);
        assertEquals(37.,DecimalModel.parse("+37e-0"), EPSILON);
        assertEquals(38.,DecimalModel.parse("+380e-1"), EPSILON);
        assertEquals(40.,DecimalModel.parse("4e+1"), EPSILON);
        assertEquals(46000.,DecimalModel.parse("460E2"), EPSILON);
        assertEquals(0.3,DecimalModel.parse(".3"), EPSILON);
        assertEquals(3,DecimalModel.parse(".3e1"), EPSILON);
        assertEquals(3,DecimalModel.parse("3.e0"), EPSILON);

        assertEquals(137.42,DecimalModel.parse("+1.3742e2"), EPSILON);
        assertEquals(-137.42,DecimalModel.parse("-1.3742E+2"), EPSILON);
        assertEquals(-0.0013742,DecimalModel.parse("-1.3742e-3"), EPSILON);
    }
}
