/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.modelcc.parser.ParserException;

import test.languages.types.model.StringModel;
import test.modelcc.ModelCCTest;

public class StringModelTest extends ModelCCTest<StringModel>
{
    public void assertMatches (int matches, String input)
    	throws Exception
    {
    	try {
    		int nmatches = parseAll(input).size();
			assertEquals(matches,nmatches);
		} catch (ParserException e) {
			assertEquals(matches,0);
		}
    }

    @Test
	public void StringMatchingTest() 
		throws Exception
	{
		assertMatches(1,"");
		assertMatches(1,"a");
		assertMatches(1,"a$1!$&)=!)");
		assertMatches(1,"\"a$1!$&)=!)\"");
		assertMatches(0,"\"a$1!\"$&)=!)\"");
		assertMatches(1,"a+");
		assertMatches(1,"+8\"919");
		assertMatches(1,"-");
		assertMatches(1,"aasdiof");
		
		// Whitespaces
		assertMatches(1," ");
		assertMatches(1,"    asdiof");
		assertMatches(0,"a;asdf");
		assertMatches(0,"a;");
		assertMatches(1,"a\n");
		assertMatches(0,"a\nad");
		assertMatches(1,"a\r");
	}
	
	@Test
	public void StringParseTest()
		throws Exception
	{
		assertEquals("testvalue",parse("testvalue").getValue());
		assertEquals("testvalue",parse("     testvalue    ").getValue());
		assertEquals("   testvalue   ",parse("\"   testvalue   \"").getValue());
		assertEquals("   testv\"alue   ",parse("\"   testv\\\"alue   \"").getValue());
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void StringParseExceptionTest()
		throws Exception
	{
		parse("string1 string2");
	}
	
}
