/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.types.model.IntegerModel;
import test.modelcc.ModelCCTest;

public class IntegerModelTest extends ModelCCTest<IntegerModel>
{
	@Test
	public void IntegerTest() 
		throws Exception
	{
		assertEquals( 3,parse("3").intValue());
		assertEquals(-3,parse("-3").intValue());
		assertEquals( 3,parse("+3").intValue());
	}
}
