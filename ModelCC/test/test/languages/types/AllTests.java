package test.languages.types;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { DecimalParseTest.class,
	                   StringModelTest.class,
	                   IntegerModelTest.class,
	                   DecimalModelTest.class})
public class AllTests {

}