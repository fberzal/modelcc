/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Value;

/**
 * Boolean Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class BooleanModel implements IModel,Serializable 
{    
    /**
     * Value.
     */
    @Value
    protected Boolean val;

	public BooleanModel() {
	}

	public BooleanModel(Boolean val) {
		this.val = val;
	}

    public boolean booleanValue() {
        return val.booleanValue();
    }

	@Override
	public String toString() 
	{
		if (val)
			return val.toString();
		else
			return null;
	}	
}
