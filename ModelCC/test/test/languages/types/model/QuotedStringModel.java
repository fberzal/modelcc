/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;
import org.modelcc.Value;

/**
 * Quoted String Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
@Pattern(regExp="\"(\\\\\"|[^\"])*\"")
public class QuotedStringModel extends StringModel implements IModel,Serializable {

	@Value
	protected String val;

	public QuotedStringModel() 
	{
	}

	public QuotedStringModel(String val) 
	{
		this.val = val;
	}

	@Setup
	public void fix() 
	{
		val = val.substring(1,val.length()-1);
		val = val.replace("\\\"","\"");
	}
	
	@Override
	public String getValue() 
	{
		return val;
	}
	
	@Override
	public String toString() 
	{
		return "\""+val.replace("\"","\\\"")+"\"";
	}
}