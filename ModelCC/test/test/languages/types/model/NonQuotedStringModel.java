/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

/**
 * Non-Quoted String Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
@Pattern(regExp="([a-zA-Z0-9_\\-\\!\\@\\$\\#\\`\\~\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}\\\\\\|\\:\\'\\,\\.\\<\\>\\/\\?]([a-zA-Z0-9_\\-\\!\\@\\$\\#\\`\\~\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}\\\\\\|\\:\\'\\\"\\,\\.\\<\\>\\/\\?]*[a-zA-Z0-9_\\-\\!\\@\\$\\#\\`\\~\\%\\^\\&\\*\\(\\)\\=\\+\\[\\]\\{\\}\\\\\\|\\:\\'\\,\\.\\<\\>\\/\\?])?)?")
public class NonQuotedStringModel extends StringModel implements IModel,Serializable 
{
	@Value
	protected String val;

	public NonQuotedStringModel() 
	{
	}

	public NonQuotedStringModel(String val) 
	{
		this.val = val;
	}

	@Override
	public String getValue() 
	{
		return val;
	}
	
	@Override
	public String toString() 
	{
		return val;
	}
}