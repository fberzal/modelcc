/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;

/**
 * Number model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public abstract class NumberModel extends Number implements IModel,Serializable 
{    
	public abstract Number getValue();
	

    @Override
    public final int intValue() 
    {
        return getValue().intValue();
    }

    @Override
    public final long longValue() 
    {
        return getValue().longValue();
    }

    @Override
    public final float floatValue() 
    {
        return getValue().floatValue();
    }

    @Override
    public final double doubleValue() 
    {
        return getValue().doubleValue();
    }

    @Override
    public final byte byteValue() 
    {
        return getValue().byteValue();
    }
	
}
