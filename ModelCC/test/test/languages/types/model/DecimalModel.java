/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;

/**
 * Decimal Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class DecimalModel extends NumberModel implements IModel,Serializable 
{
    private Double val;
    
    
    public DecimalModel() 
    {
    }
    
	public DecimalModel(Double val) 
	{
		this.val = val;
	}

	public DecimalModel(Float val) 
	{
		this.val = val.doubleValue();
	}

	@Override
	public final Number getValue ()
	{
		return val;
	}

	public final void setValue (Number number)
	{
		val = number.doubleValue();
	}
    
	@Override
	public String toString() 
	{
		return Double.toString(val);
	}

	// Real numbers in scientific notation
	
    public static double parse(String text) 
    {
        int pos = -1;
        pos = text.indexOf('e');
        if (pos == -1)
            pos = text.indexOf('E');
        
        String m;
        String e;
        if (pos != -1) {
            m = text.substring(0,pos);
            e = text.substring(pos+1);
        }
        else {
            m = text;
            e = "0";
        }
        if (m.charAt(0)=='+')
            m = m.substring(1);
        if (e.charAt(0)=='+')
            e = e.substring(1);
        double ret = Double.parseDouble(m)*Math.pow(10, Double.parseDouble(e));
        return ret;
    }
}