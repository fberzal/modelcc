/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;

/**
 * Integer Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public class IntegerModel extends NumberModel implements IModel,Serializable 
{    
    private Long val;

    
	public IntegerModel() 
	{
	}
	
	public IntegerModel(Long val) 
	{
		this.val = val;
	}

	public IntegerModel(Integer val) 
	{
		this.val = val.longValue();
	}
	
	
	@Override
	public final Number getValue ()
	{
		return val;
	}
	
	public final void setValue (Number number)
	{
		this.val = number.longValue();
	}
    
	@Override
	public String toString() 
	{
		return Long.toString(val);
	}
}
