/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;
import org.modelcc.Value;

/**
 * Signed Integer Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
@Pattern(regExp="(-|\\+)[0-9]+")
public class SignedIntegerModel extends IntegerModel implements IModel,Serializable 
{    
    public SignedIntegerModel() 
    {
    }
    
	public SignedIntegerModel(Long val) 
	{
		setValue(val);
	}

	public SignedIntegerModel(Integer val) 
	{
		setValue(val);
	}
	

    /**
     * Token value.
     */
    @Value
    String textValue;
	
	@Setup
	void run() 
	{
		String stripped;
		if (textValue.startsWith("+"))
			stripped = textValue.substring(1);
		else
			stripped = textValue;
		
	    setValue(Long.parseLong(stripped));
	}
}
