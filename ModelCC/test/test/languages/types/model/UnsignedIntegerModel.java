/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;
import org.modelcc.Value;

/**
 * Unsigned Integer Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
@Pattern(regExp="[0-9]+")
public class UnsignedIntegerModel extends IntegerModel implements IModel,Serializable 
{    
    public UnsignedIntegerModel() 
    {
    }
    
	public UnsignedIntegerModel(Long val) 
	{
		setValue(val);
	}

	public UnsignedIntegerModel(Integer val) 
	{
		setValue(val);
	}
	
    /**
     * Token value.
     */
    @Value
    String textValue;
	
	@Setup
	void run() 
	{
		setValue(Long.parseLong(textValue));
	}
}
