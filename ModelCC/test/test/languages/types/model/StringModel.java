/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;

/**
 * String Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
public abstract class StringModel implements IModel,Serializable 
{    
	public abstract String getValue();

	
	public final boolean isInteger() 
	{
	    try {
	        Integer.parseInt(getValue());
	        return true;
	    } catch (NumberFormatException nfe) {}
	    return false;
	}
	
	public final boolean isDecimal() 
	{
	    try {
	        Double.parseDouble(getValue());
	        return true;
	    } catch (NumberFormatException nfe) {}
	    return false;
	}


	public final IntegerModel toInteger() 
	{
		return new IntegerModel(Long.parseLong(getValue()));
	}

	public final DecimalModel toDecimal() 
	{
		return new DecimalModel(Double.parseDouble(getValue()));
	}
}
