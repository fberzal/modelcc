/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types.model;

import java.io.Serializable;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;
import org.modelcc.Value;

/**
 * Signed Decimal Model.
 * 
 * @author Luis Quesada (lquesada@modelcc.org) & Fernando Berzal (fberzal@modelcc.org)
 */
@Pattern(regExp="(-|\\+)([0-9]*\\.[0-9]+|[0-9]+\\.[0-9]*)((e|E)(-|\\+)?[0-9]+)?")
public class SignedDecimalModel extends DecimalModel implements IModel,Serializable 
{    
    public SignedDecimalModel() 
    {
    }    

	public SignedDecimalModel(Double val) 
	{
		setValue(val);
	}

	public SignedDecimalModel(Float val) 
	{
		setValue(val);
	}	

    /**
     * Token value.
     */
    @Value
    String textValue;
	
	@Setup
	void run() 
	{
	    setValue(parse(textValue));
	}
}
