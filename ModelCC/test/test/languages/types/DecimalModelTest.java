/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package test.languages.types;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.types.model.DecimalModel;
import test.modelcc.ModelCCTest;

public class DecimalModelTest extends ModelCCTest<DecimalModel>
{
	public static final double EPSILON = 1e-9;

	@Test
	public void DecimalTest() 
		throws Exception
	{
		assertEquals(3.0, parse("3.").doubleValue(), EPSILON);
		assertEquals(0.3, parse(".3").doubleValue(), EPSILON);
		assertEquals(3.0, parse("+3.").doubleValue(), EPSILON);
		assertEquals(0.3, parse("+.3").doubleValue(), EPSILON);
		assertEquals(-3.0, parse("-3.").doubleValue(), EPSILON);
		assertEquals(-0.3, parse("-.3").doubleValue(), EPSILON);
		assertEquals(124.53, parse("1.2453e2").doubleValue(), EPSILON);
		assertEquals(0.012453, parse("1.2453E-2").doubleValue(), EPSILON);
	}    
}
