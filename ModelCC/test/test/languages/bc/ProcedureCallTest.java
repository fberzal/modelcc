package test.languages.bc;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bc.model.*;
import test.modelcc.ModelCCTest;


public class ProcedureCallTest extends ModelCCTest<ProcedureCallStatement>
{
	@Test
	public void testProcedureCallWithoutArguments()
		throws Exception
	{
		ProcedureCallStatement call = parse("id()");
		
		assertEquals ( "id", call.getProcedureId().toString());
		assertEquals ( 0, call.getArguments().length);
	}

	@Test
	public void testProcedureCallWithArgument()
		throws Exception
	{
		ProcedureCallStatement call = parse("id(arg)");
		
		assertEquals ( "id", call.getProcedureId().toString());
		assertEquals ( 1, call.getArguments().length);
		assertTrue ( call.getArgument(0) instanceof Identifier);
		assertEquals ( "arg", call.getArgument(0).toString());
	}

	@Test
	public void testProcedureCall1()
		throws Exception
	{
		ProcedureCallStatement call = parse("id(1)");
		
		assertEquals ( "id", call.getProcedureId().toString());
		assertEquals ( 1, call.getArguments().length);
		assertTrue ( call.getArgument(0) instanceof IntegerLiteral);
		assertEquals ( "1", call.getArgument(0).toString());
	}
	
	@Test
	public void testProcedureCall2()
		throws Exception
	{
		ProcedureCallStatement call = parse("id(1,2)");
		
		assertEquals ( "id", call.getProcedureId().toString());
		assertEquals ( 2, call.getArguments().length);
		assertEquals ( "1", call.getArgument(0).toString());
		assertEquals ( "2", call.getArgument(1).toString());
	}	


	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProcedureCall1()
		throws Exception
	{
		parse("id(");
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProcedureCall2()
		throws Exception
	{
		parse("id)");
	}
	
		
}
