package test.languages.bc;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bc.model.*;
import test.modelcc.ModelCCTest;


public class ProcedureTest extends ModelCCTest<Procedure>
{
	@Test
	public void testEmptyProcedure ()
		throws Exception
	{
		Procedure proc = parse("procedimiento id(a:int;b:int); inicio fin");
		
		assertEquals ( 2, proc.getParameters().length);
		assertNull ( proc.getBlock().getStatements() );
	}

	@Test
	public void testValidProcedure ()
		throws Exception
	{
		Procedure proc = parse("procedimiento id(a:int;b:int); inicio a := 2; fin");
		
		assertEquals ( 2, proc.getParameters().length);
		assertEquals ( 1, proc.getBlock().getStatements().length);
	}	



	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProcedure1()
		throws Exception
	{
		parse("procedimiento id(a:int;b:int); inicio a := 2 fin");
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProcedureCall2()
		throws Exception
	{
		parse("procedimiento id(a:int;b:int) inicio a := 2; fin");
	}
	
		
}
