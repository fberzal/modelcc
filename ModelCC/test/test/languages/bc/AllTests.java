package test.languages.bc;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { StringLiteralTest.class,
	                   ProcedureCallTest.class,
	                   ProcedureTest.class,
	                   ProgramTest.class,
					   StatementTest.class })
public class AllTests {

}