package test.languages.bc;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bc.model.*;
import test.modelcc.ModelCCTest;


public class ProgramTest extends ModelCCTest<Program>
{
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testNullProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNull ( program.getEntryPoint().getBlock() );
	}
	
	@Test
	public void testEmptyProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio fin.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
	}

	@Test
	public void testHelloProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio salida 'Hola'; fin.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertNull( program.getEntryPoint().getBlock().getVariables() );
		assertEquals( 1, program.getEntryPoint().getBlock().getStatements().length );
	}

	@Test
	public void testVarProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio var x: tipo; entrada x; salida x; fin.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertEquals( 1, program.getEntryPoint().getBlock().getVariables().length );
		assertEquals( "x", program.getEntryPoint().getBlock().getVariable(0).getId().toString() );
		assertEquals( 2, program.getEntryPoint().getBlock().getStatements().length );
	}

	@Test
	public void testVarsProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio var x: tipo; y: tipo; entrada x; salida x; fin.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertEquals( 2, program.getEntryPoint().getBlock().getVariables().length );
		assertEquals( "x", program.getEntryPoint().getBlock().getVariable(0).getId().toString() );
		assertEquals( "y", program.getEntryPoint().getBlock().getVariable(1).getId().toString() );
		assertEquals( 2, program.getEntryPoint().getBlock().getStatements().length );
	}

	@Test
	public void testNestedProgram()
		throws Exception
	{
		Program program = parse ( "procedimiento compuesto; "
	                            + "inicio "
				                + "  var x: tipo; y: tipo;"
				                + "  procedimiento simple; inicio fin; "
	                            + "  entrada x;"
				                + "  salida x;"
				                + "fin.");
		
		assertEquals( "compuesto", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertEquals( 2, program.getEntryPoint().getBlock().getVariables().length );
		assertEquals( 2, program.getEntryPoint().getBlock().getStatements().length );
		assertEquals( "entrada x", program.getEntryPoint().getBlock().getStatement(0).toString() );
		assertEquals( "salida x", program.getEntryPoint().getBlock().getStatement(1).toString() );
		assertEquals( 1, program.getEntryPoint().getBlock().getProcedures().length );
		assertEquals( "simple", program.getEntryPoint().getBlock().getProcedure(0).getId().toString() );
	}

	@Test
	public void testParameters()
		throws Exception
	{
		Program program = parse ( "procedimiento compuesto; "
	                            + "inicio "
				                + "  var x: tipo; y: tipo;"
				                + "  procedimiento simple (a: tipo; b:tipo); inicio fin; "
	                            + "  entrada x;"
				                + "  salida x;"
				                + "fin.");
		
		assertEquals( "compuesto", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertEquals( 2, program.getEntryPoint().getBlock().getVariables().length );
		assertEquals( 2, program.getEntryPoint().getBlock().getStatements().length );
		assertEquals( "entrada x", program.getEntryPoint().getBlock().getStatement(0).toString() );
		assertEquals( "salida x", program.getEntryPoint().getBlock().getStatement(1).toString() );
		assertEquals( 1, program.getEntryPoint().getBlock().getProcedures().length );
		
		Procedure procedure = program.getEntryPoint().getBlock().getProcedure(0);
		
		assertEquals( "simple", procedure.getId().toString() );
		assertEquals( 2, procedure.getParameters().length );
		assertEquals( "a", procedure.getParameter(0).getId().toString() );
		assertEquals( "b", procedure.getParameter(1).getId().toString() );
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testEmptyVarProgram()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio var entrada x; salida x; fin.");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertNull (program.getEntryPoint().getBlock().getVariables() );
		assertEquals( 2, program.getEntryPoint().getBlock().getStatements().length );
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProgram1()
		throws Exception
	{
		Program program = parse("procedimiento simple; inicio fin;");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNotNull ( program.getEntryPoint().getBlock() );
		assertNull ( program.getEntryPoint().getBlock().getVariables() );
		assertNull ( program.getEntryPoint().getBlock().getStatements() );
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidProgram2()
		throws Exception
	{
		Program program = parse("procedimiento simple;");
		
		assertEquals( "simple", program.getEntryPoint().getId().toString() );
		assertNull ( program.getEntryPoint().getBlock() );
	}
	
}
