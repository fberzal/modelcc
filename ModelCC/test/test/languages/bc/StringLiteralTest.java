package test.languages.bc;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bc.model.*;
import test.modelcc.ModelCCTest;


public class StringLiteralTest extends ModelCCTest<StringLiteral>
{
	@Test
	public void testEmptyStringLiteral()
		throws Exception
	{
		StringLiteral literal = parse("''");
		
		assertEquals ( "''", literal.toString() );
	}

	@Test
	public void testValidStringLiteral()
		throws Exception
	{
		StringLiteral literal = parse("'afjoa\" \n2'");
		
		assertEquals ( "'afjoa\" \n2'", literal.toString() );
	}

	@Test
	public void testEscapedStringLiteral()
		throws Exception
	{
		StringLiteral literal = parse("'af''joa\" \n2'");
		
		assertEquals ( "'af''joa\" \n2'", literal.toString() );
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidStringLiteral()
		throws Exception
	{
		parse("' str");
	}

	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidStringLiteral2()
		throws Exception
	{
		parse("' ' '");
	}
	
		
}
