package test.languages.prolog;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { AtomTest.class,
	                   VariableTest.class,
	                   NumberTest.class,
	                   TermTest.class,
	                   ListTest.class,
	                   StringTest.class,
	                   CompoundTermTest.class,
	                   ClauseTest.class })
public class AllTests {

}