package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.Clause;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ClauseTest extends ModelCCTest<Clause>
{
	@Test
	public void testValidFacts()
		throws Exception
		
	{
		assertNotNull ( parse("cat(tom)."));
		assertNotNull ( parse("cat(X)."));
		assertNotNull ( parse("cat(_)."));

		assertNotNull ( parse("mother_child(marge,bart)."));
		assertNotNull ( parse("father_child(homer,bart)."));
	}

	@Test
	public void testValidRules()
		throws Exception
		
	{
		assertNotNull ( parse("cat(tom) :- true."));
		
		assertNotNull ( parse("animal(X) :- cat(X)."));
		
		assertNotNull ( parse("parent_child(X, Y) :- father_child(X, Y)."));
		assertNotNull ( parse("parent_child(X, Y) :- mother_child(X, Y)."));

		assertNotNull ( parse("sibling(X, Y) :- parent_child(Z, X), parent_child(Z, Y)."));		
	}
	
	// Invalid clauses

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidCompoundTerm()
		throws Exception
	{
		parse("atom");
	}
	
}
