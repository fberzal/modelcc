package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.Term;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class TermTest extends ModelCCTest<Term>
{
	@Test
	public void testValidTerms()
		throws Exception
	{
		assertNotNull ( parse("pi"));
		assertNotNull ( parse("VARIABLE"));
		assertNotNull ( parse("'string'"));
		assertNotNull ( parse("2"));
		assertNotNull ( parse("1.234"));
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidTerm()
		throws Exception
	{
		System.err.println(parse("2x"));
	}
	
}
