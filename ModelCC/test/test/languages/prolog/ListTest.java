package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.List;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ListTest extends ModelCCTest<List>
{
	@Test
	public void testValidLists()
		throws Exception
	{
		assertNotNull ( parse("[]"));
		assertNotNull ( parse("[_]"));
		assertNotNull ( parse("[1,2]"));
		
		assertNotNull ( parse("[[1,2]]"));
		assertNotNull ( parse("[[1],[2]]"));
		assertNotNull ( parse("[_,[1,2]]"));
		
		assertNotNull ( parse("[1|_]"));
		assertNotNull ( parse("[1,2|X]"));
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidList()
		throws Exception
	{
		parse("[a");
	}
	
}
