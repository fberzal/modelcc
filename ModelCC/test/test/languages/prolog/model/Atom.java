package test.languages.prolog.model;

import org.modelcc.*;

@Pattern(regExp="([a-z]([a-zA-Z_]|[0-9])*)|'(''|[^'])*'")
public class Atom extends Term implements IModel
{
	@Value
	private String atom;
	
	// Constructors
	
	public Atom () {}
	
	public Atom (String atom)
	{
		this.atom = atom;
	}
	
	// toString
	
	public String toString ()
	{
		return atom;
	}
}
