package test.languages.prolog.model;

import org.modelcc.*;

@Prefix("\\[")
@Suffix("\\]")
public class List extends Term implements IModel
{
	@Separator(",")
	private Term elements[];

	@Optional
	@Prefix("\\|")
	private Term tail;
	
	// Getters
	
	public Term element (int i)
	{
		return elements[i];
	}
	
	public int size ()
	{
		if (elements!=null)
			return elements.length;
		else
			return 0;
	}
	
	public Term tail ()
	{
		return tail;
	}
	
	// toString
	
	public String toString ()
	{
		String str = "[";

		if (elements!=null)
			for (int i=0; i<elements.length; i++) {
				str += elements[i].toString();
				
				if (i<elements.length-1)
					str += ",";
			}
		
		if (tail!=null)
			str += "|" + tail.toString();
		
		str += ")";
		
		return str;
	}
}
