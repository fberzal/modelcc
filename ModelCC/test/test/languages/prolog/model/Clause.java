package test.languages.prolog.model;

import org.modelcc.*;

@Suffix(".")
public class Clause implements IModel
{
	private Term head;
	
	@Optional
	@Prefix(":-")
	@Separator(",")
	private Term body[];
	
	// Getters
	
	public Term head ()
	{
		return head;
	}
	
	public Term[] body ()
	{
		return body;
	}
	
	public boolean isFact ()
	{
		return (body==null) || (body.length==0);
	}
	
	public boolean isRule ()
	{
		return (body!=null) && (body.length>0);
	}
	
	// toString
	
	public String toString ()
	{
		String str = head.toString();

		if (isRule()) {
			str += " :- ";
			for (int i=0; i<body.length; i++) {
				str += body[i].toString();

				if (i<body.length-1)
					str += ", ";
			}
		}
		
		str += ".";
		
		return str;
	}
}
