package test.languages.prolog.model;

import org.modelcc.*;

@Pattern(regExp="[A-Z_]([a-zA-Z_]|[0-9])*")
public class Variable extends Term implements IModel
{
	@Value
	private String id;
	
	// Constructors
	
	public Variable () {}
	
	public Variable (String id)
	{
		this.id = id;
	}
	
	// toString
	
	public String toString ()
	{
		return id;
	}
}
