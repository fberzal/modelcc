package test.languages.prolog.model;

import org.modelcc.*;

public class CompoundTerm extends Term implements IModel
{
	private Atom functor;
	
	@Prefix("\\(")
	@Suffix("\\)")
	@Separator(",")
	private Term arguments[];
	
	// Getters
	
	public Atom functor ()
	{
		return functor;
	}
	
	public Term argument (int i)
	{
		return arguments[i];
	}
	
	public int arity ()
	{
		if (arguments!=null)
			return arguments.length;
		else
			return 0;
	}
	
	// toString
	
	public String toString ()
	{
		String str = functor.toString() + "(";

		if (arguments!=null)
			for (int i=0; i<arguments.length; i++) {
				str += arguments[i].toString();
				
				if (i<arguments.length-1)
					str += ",";
			}
		
		str += ")";
		
		return str;
	}
}
