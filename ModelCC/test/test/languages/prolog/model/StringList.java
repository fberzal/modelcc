package test.languages.prolog.model;

import org.modelcc.*;

@Pattern(regExp="\"(\\\\\"|[^\"])*\"")
public class StringList extends Term implements IModel
{
	@Value
	private String val;
	
	@Setup
	public void setup() 
	{
		val = val.substring(1,val.length()-1);
		val = val.replace("\\\"","\"");
	}
	
	// Getters
	
	public Term element (int i)
	{
		return new Number(val.charAt(i));
	}
	
	public int size ()
	{
		if (val!=null)
			return val.length();
		else
			return 0;
	}
	
	// toString
	
	public String toString ()
	{
		return "\"" + val.replace("\"","\\\"") + "\"";
	}
}
