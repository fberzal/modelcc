package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.StringList;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class StringTest extends ModelCCTest<StringList>
{
	@Test
	public void testValidStrings()
		throws Exception
	{
		assertNotNull ( parse("\"\""));
		assertNotNull ( parse("\"abc\""));
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidList()
		throws Exception
	{
		parse("\"a");
	}
	
}
