package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.Atom;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class AtomTest extends ModelCCTest<Atom>
{
	@Test
	public void testValidAtoms()
		throws Exception
	{
		assertNotNull(parse("pi"));
		assertNotNull(parse("i2s"));
		parse("'string'");           // Quoted atom
	}
			
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAtom()
		throws Exception
	{
		parse("2x");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAtom2()
		throws Exception
	{
		parse("VARIABLE");
	}
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidAtom3()
		throws Exception
	{
		parse("_");  // Anonymous variable
	}
	
}
