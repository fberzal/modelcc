package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.CompoundTerm;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class CompoundTermTest extends ModelCCTest<CompoundTerm>
{
	@Test
	public void testValidCompoundTerms()
		throws Exception
		
	{
		assertNotNull ( parse("f(x)"));
		assertNotNull ( parse("truck_year('Mazda', 1986)"));
		assertNotNull ( parse("'Person_Friends'(zelda,[tom,jim])"));
	}
	
	// Invalid atoms

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidCompoundTerm()
		throws Exception
	{
		parse("atom");
	}
	
}
