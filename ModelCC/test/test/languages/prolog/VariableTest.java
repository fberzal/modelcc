package test.languages.prolog;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.prolog.model.Variable;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class VariableTest extends ModelCCTest<Variable>
{
	@Test
	public void testValidVariables()
		throws Exception
	{
		assertNotNull ( parse("VAR"));
		assertNotNull ( parse("_OTHER"));
		assertNotNull ( parse("_"));
	}
	
	// Invalid variables

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariable1()
		throws Exception
	{
		parse("2x");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidVariable2()
		throws Exception
	{
		parse("atom");
	}		
}
