package test.languages;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
@RunWith(Suite.class)
@Suite.SuiteClasses( { test.languages.types.AllTests.class,
                       test.languages.arithmetic.AllTests.class,
					   test.languages.calculator.AllTests.class,
	                   test.languages.bool.AllTests.class,
					   test.languages.imperative.AllTests.class,
					   test.languages.prolog.AllTests.class,
					   test.languages.lisp.AllTests.class,
					   test.languages.json.AllTests.class,
	                   test.languages.awk.AllTests.class,
					   test.languages.bc.AllTests.class})
public class AllTests {

}