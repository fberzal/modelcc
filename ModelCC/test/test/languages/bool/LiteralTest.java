package test.languages.bool;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bool.model.Literal;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class LiteralTest extends ModelCCTest<Literal>
{

	@Test
	public void testValidLiterals()
		throws Exception
	{
		assertEquals ("1", parse("1").getValue().toString());
		assertEquals ("12", parse("12").getValue().toString());
		assertEquals ("123", parse("123").getValue().toString());
	}

	// Invalid literal expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidLiteral0()
		throws Exception
	{
		parse("-1");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidLiteral1()
		throws Exception
	{
		parse("+");
	}

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidLiteral2()
		throws Exception
	{
		parse(".39");
	}	
}
