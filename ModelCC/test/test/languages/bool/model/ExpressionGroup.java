package test.languages.bool.model;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Prefix("\\(")
@Suffix("\\)")
public class ExpressionGroup<T> extends BooleanExpression<T> implements IModel 
{
    BooleanExpression<T> e;

    @Override
    public boolean eval() 
    {
        return e.eval();
    }

    public BooleanExpression<T> getE() {
        return e;
    }

}
