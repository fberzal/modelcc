package test.languages.bool.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp = "[0-9]+")
public class Literal<T> extends LiteralExpression<T> implements IModel {

    @Value
    T value;

    @Override
    public boolean eval() 
    {
        return (Integer.parseInt(value.toString()) % 2 == 0)? true: false;
    }

    @Override
    public T getValue() {
        return value;
    }

}
