package test.languages.bool.model;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.IModel;

@Associativity(AssociativityType.LEFT_TO_RIGHT)
public abstract class BinaryOperator<T> implements IModel 
{
    public abstract boolean eval(BooleanExpression<T> e1, BooleanExpression<T> e2);
}
