package test.languages.bool.model;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Priority;

@Priority(value = 2)
@Pattern(regExp = "OR")
public class OrOperator<T> extends BinaryOperator<T> implements IModel 
{
    @Override
    public boolean eval(BooleanExpression<T> e1, BooleanExpression<T> e2) 
    {
        return e1.eval() || e2.eval();
    }

}
