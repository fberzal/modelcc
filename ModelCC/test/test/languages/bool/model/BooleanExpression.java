package test.languages.bool.model;

import org.modelcc.IModel;

public abstract class BooleanExpression<T> implements IModel 
{
    public abstract boolean eval();
}
