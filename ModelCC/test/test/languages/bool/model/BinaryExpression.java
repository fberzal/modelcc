package test.languages.bool.model;

import org.modelcc.IModel;

public class BinaryExpression<T> extends BooleanExpression<T> implements IModel 
{
    BooleanExpression<T> e1;
    BinaryOperator<T> op;
    BooleanExpression<T> e2;

    @Override
    public boolean eval() 
    {
        return op.eval(e1, e2);
    }

    public BooleanExpression<T> getE1() {
        return e1;
    }

    public BooleanExpression<T> getE2() {
        return e2;
    }

}
