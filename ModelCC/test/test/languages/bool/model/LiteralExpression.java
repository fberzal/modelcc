package test.languages.bool.model;

import org.modelcc.IModel;

public abstract class LiteralExpression<T> extends BooleanExpression<T> implements IModel {

    public abstract T getValue();
}
