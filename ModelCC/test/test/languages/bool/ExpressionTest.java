package test.languages.bool;

import static org.junit.Assert.*;

import org.junit.Test;

import test.languages.bool.model.*;
import test.modelcc.ModelCCTest;

/**
 * ModelCC test suite
 * 
 * @author Fernando Berzal (berzal@acm.org)
 */
public class ExpressionTest extends ModelCCTest<BooleanExpression>
{
	@Test
	public void testValidExpressions()
		throws Exception
	{
	    assertTrue(parse("(12998 AND 8) OR 5").eval());
	    assertFalse(parse("(12999 AND 8) OR 5").eval());
	    assertTrue(parse("(12999 AND 8) OR 6").eval());
	}

	@Test
	public void testWhitespace()
		throws Exception
	{
	    assertTrue(parse("(12998AND8)OR5").eval());
	    assertTrue(parse(" ( 12998  AND  8 )  OR  5").eval());
	}
	
	// Invalid expressions

	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidExpression()
		throws Exception
	{
		parse("AND");
	}
	
	
	@Test(expected=org.modelcc.parser.ParserException.class)
	public void testInvalidCaseSensitiveExpression()
		throws Exception
	{
		parse("1 and 2");
	}
	
}
