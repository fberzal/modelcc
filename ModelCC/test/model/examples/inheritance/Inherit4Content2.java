/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;
import org.modelcc.Position;

public class Inherit4Content2 extends Inherit4Content1 implements IModel {
	@Position(element="a",position=Position.BEFORE)
	B b;
}
