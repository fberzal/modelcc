/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;

public class Inherit3Content2 extends Inherit3Content1 implements IModel {
	B b;
}
