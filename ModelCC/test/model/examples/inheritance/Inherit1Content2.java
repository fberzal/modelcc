/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;
import org.modelcc.Prefix;

@Prefix("p")
public class Inherit1Content2 extends Inherit1Content1 implements IModel {
}
