/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;

public class Inherit5Content2 extends Inherit5Content1 implements IModel {
}
