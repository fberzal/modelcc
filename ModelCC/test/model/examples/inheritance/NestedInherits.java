/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

public class NestedInherits {
    public class A {
        public class B extends A {

        }
    }
    public class C extends A {

    }
}
