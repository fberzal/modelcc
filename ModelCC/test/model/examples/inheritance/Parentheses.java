/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Prefix("\\(")
@Suffix("\\)")
public class Parentheses extends Basic implements IModel {
}
