/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.inheritance;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="A")
public class A implements IModel {
	
	
}
