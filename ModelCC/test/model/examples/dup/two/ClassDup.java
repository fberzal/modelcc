/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.dup.two;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="b")
public class ClassDup implements IModel 
{
}
