/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.dup;

import org.modelcc.IModel;

public class Main implements IModel 
{
	model.examples.dup.one.ClassDup ok1;
	
	model.examples.dup.one.ClassDup2 ok2;

	model.examples.dup.two.ClassDup ok3;
	
	model.examples.dup.two.ClassDup2[] ok4;
}
