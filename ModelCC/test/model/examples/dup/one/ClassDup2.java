/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.dup.one;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="a")
public class ClassDup2 implements IModel 
{
}
