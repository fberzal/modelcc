/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.ID;
import org.modelcc.IModel;

public class Keys2 implements IModel {
    @ID public ComplexKey key;
    
    @ID public IntData keynum;
    
    public CharData value;
    
    public Keys2() {
        
    }
    
    public Keys2(ComplexKey key,IntData keynum,CharData value) {
        this.key = key;
        this.keynum = keynum;
        this.value = value;
    }
    
}
