/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;

public class CompositeData implements IModel {

    @ID
    @Prefix("\\[")
    @Suffix("\\]")
    @Separator(",")
    public CharData[] key;
    
    @Prefix(":")
    IntData val;
}
