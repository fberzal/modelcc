/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;
import org.modelcc.Suffix;

public class Keys5Lang implements IModel {

    @Optional
    @Prefix("startref") @Suffix("endref") @Reference public Keys1 refs;
    
    @Prefix("data") 
    public Keys1[] keys1;
    
    
}
