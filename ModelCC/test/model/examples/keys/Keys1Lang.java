/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys1Lang implements IModel {

    public Keys1[] keys1;
    
    @Optional
    @Prefix("refs")
    @Reference public Keys1[] refs;
    
}
