/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;
import org.modelcc.Suffix;

public class Keys7Lang implements IModel {

    @Optional
    @Multiplicity(minimum=1)
    @Prefix("startref") @Suffix("endref") @Reference public Keys1b[] refs;
    
    @Prefix("data") 
    public Keys1b[] keys1;
    
    
}
