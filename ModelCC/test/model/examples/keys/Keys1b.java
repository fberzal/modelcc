/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

public class Keys1b implements IModel {

    @Prefix("k")
    @Suffix("c")
    @ID public CharData key;
    
    public IntData val;
    
    public Keys1b() {
        
    }

    public Keys1b(CharData key,IntData val) {
        this.key = key;
        this.val = val;
    }
    
}
