/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import java.util.Set;

import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;

public class CompositeDataSet implements IModel {

    @ID
    @Prefix("\\[")
    @Suffix("\\]")
    @Separator(",")
    public Set<CharData> key;
    
    @Prefix(":")
    IntData val;
}
