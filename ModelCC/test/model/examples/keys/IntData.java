/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Value;

public class IntData implements IModel {
    
    @Value public int value;
    
    public String toString ()
    {
    	return Integer.toString(value);
    }
    
}
