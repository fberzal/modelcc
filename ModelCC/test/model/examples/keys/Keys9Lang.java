/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys9Lang implements IModel {

    @Reference public Keys1b[] refsbla;

    @Reference public CompositeData[] refs;
    
    @Prefix("data")
    public CompositeData[] data;
    
}
