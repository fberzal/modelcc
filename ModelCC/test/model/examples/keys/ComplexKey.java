/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;

public class ComplexKey implements IModel {
    public IntData[] data;
    
    public ComplexKey() {
        
    }
    
    public ComplexKey(IntData[] data) {
        this.data = data;
    }
    
    public String toString ()
    {
    	String result = "";
    	
    	for (int i=0; i<data.length; i++)
    		result += data[i].toString()+":";
    	
    	return result;
    }
}
