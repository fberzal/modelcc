/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.FreeOrder;
import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@FreeOrder
public class FreeKey implements IModel {

    @ID CharData key1;

    @ID IntData key2;
    
    @Prefix("val")
    @Suffix("endval")
    IntData val;
    
    
    public String toString ()
    {
    	return key1.toString()+":"+key2.toString();
    }
}
