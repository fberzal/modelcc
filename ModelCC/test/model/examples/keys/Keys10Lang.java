/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys10Lang implements IModel {

    @Reference public CompositeDataSet[] refs;
    
    @Prefix("data")
    public CompositeDataSet[] data;
    
}
