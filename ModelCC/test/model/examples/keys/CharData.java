/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="[a-z]")
public class CharData implements IModel {
   
   @Value public char value;
   
   public String toString ()
   {
	   return Character.toString(value);
   }
   
}
