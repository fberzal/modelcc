/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys3Lang implements IModel {

    @Optional
    @Multiplicity(minimum=1)
    @Reference public Keys1[] refs;
    
    @Prefix("data") 
    public Keys1[] keys1;
    
    
}
