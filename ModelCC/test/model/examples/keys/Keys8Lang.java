/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys8Lang implements IModel {

    public Keys1b[] keys1;
    
    @Prefix("refs") @Optional
    @Reference public Keys1b[] refs;
    
}
