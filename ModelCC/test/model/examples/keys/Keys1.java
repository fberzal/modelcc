/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.ID;
import org.modelcc.IModel;

public class Keys1 implements IModel {

    @ID public CharData key;
    
    public IntData val;
    
    public Keys1() {
        
    }

    public Keys1(CharData key,IntData val) {
        this.key = key;
        this.val = val;
    }
    
    public String toString ()
    {
    	return key.toString();
    }
    
}
