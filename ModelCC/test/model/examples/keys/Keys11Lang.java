/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.keys;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class Keys11Lang implements IModel {

	@Reference public FreeKey[] refs;
    
    @Prefix("data")
    public FreeKey[] data;
    
}
