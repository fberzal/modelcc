/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.work;

import java.util.Set;

import org.modelcc.Constraint;
import org.modelcc.IModel;

public class Ini5 implements IModel {

    public Set<Ino> a;

    @Constraint
    private boolean run() {
        return true;
    }
}
