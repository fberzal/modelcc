/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.work;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;

@Pattern(regExp="hello")
@Separator(",")
@Prefix("-")
@Suffix("\\+")
public class Ino2 implements IModel {

    int a = 0;

}
