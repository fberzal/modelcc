/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.work;

import java.util.Set;

import org.modelcc.Constraint;
import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

public class Ini9 implements IModel {

	@Multiplicity(minimum=2,maximum=4)
    @Prefix("\\(")
    @Suffix("\\)")
    public Set<Ino2> a;

    @Constraint
    private boolean run() {
        return true;
    }
}
