/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.work;

import java.util.HashSet;

import org.modelcc.Constraint;
import org.modelcc.IModel;

public class Ini6 implements IModel {

    public HashSet<Ino> a;

    @Constraint
    private boolean run() {
        return true;
    }
}
