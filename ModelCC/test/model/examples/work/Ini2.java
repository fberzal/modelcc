/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.work;

import java.util.List;

import org.modelcc.Constraint;
import org.modelcc.IModel;

public class Ini2 implements IModel {

    public List<Ino> a;

    @Constraint
    private boolean run() {
        return true;
    }
}
