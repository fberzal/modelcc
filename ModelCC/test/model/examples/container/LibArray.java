/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.container;

import org.modelcc.IModel;

public class LibArray implements IModel 
{
    public IntData[] data;
}
