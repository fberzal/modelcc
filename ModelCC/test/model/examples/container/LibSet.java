/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.container;

import java.util.Set;

import org.modelcc.IModel;

public class LibSet implements IModel 
{
    public Set<IntData> data;
}
