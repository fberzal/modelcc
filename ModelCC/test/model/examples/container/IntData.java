/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.container;

import org.modelcc.IModel;
import org.modelcc.Value;

public class IntData implements IModel 
{    
    @Value public int value;   
}
