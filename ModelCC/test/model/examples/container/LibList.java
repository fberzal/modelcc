/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */

package model.examples.container;

import java.util.List;

import org.modelcc.IModel;

public class LibList implements IModel 
{
    public List<IntData> data;
}
