/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.multiple;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="d?")
public class ContentChoices1 extends ContentChoices implements IModel {

	@Value
	public String val;
	
}
