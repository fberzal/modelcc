/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.multiple;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="c?")
public class ContentChoices2 extends ContentChoices implements IModel {

	@Value
	public String val;
	
}
