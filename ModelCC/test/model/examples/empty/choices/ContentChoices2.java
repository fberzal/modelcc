/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.choices;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="")
public class ContentChoices2 extends ContentChoices implements IModel {

	@Value
	public String val;
	
}
