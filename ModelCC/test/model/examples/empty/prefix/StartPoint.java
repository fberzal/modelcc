/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.prefix;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

public class StartPoint implements IModel {

	@Prefix("a")
	@Suffix("b")
	public Content content;
}
