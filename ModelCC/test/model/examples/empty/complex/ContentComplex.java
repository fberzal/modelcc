/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.complex;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class ContentComplex extends Content implements IModel {

	public Basic empty;
	
	@Optional
	public BasicSomething something;
}
