/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.empty.prefix2;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;

@Prefix("c")
public class ContentComplex extends Content implements IModel {

	@Optional
	public Content ct;
}
