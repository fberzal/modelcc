/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.simple;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Prefix;
import org.modelcc.Suffix;

@Pattern(regExp="e")
@Prefix("b")
@Suffix("a")
public class Cont implements IModel {

}
