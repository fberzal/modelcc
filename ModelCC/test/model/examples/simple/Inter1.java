/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.simple;

import org.modelcc.IModel;
import org.modelcc.Prefix;

@Prefix("a")
public class Inter1 extends Inter implements IModel {
  Simple simp;
}
