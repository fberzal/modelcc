/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.simple;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Prefix;

@Prefix("a")
public class Simple implements IModel {

    // No genera la optional parece
    @Optional
    Simple ot;

}
