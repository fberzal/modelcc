/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import java.util.Set;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;

public class WrongClass09 implements IModel {

	@Multiplicity(minimum=-5)
    Set<OKClass> aasdf2;

}
