/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Position;
import org.modelcc.SeparatorPolicy;

public class WrongClassPositionClash4 implements IModel {

	@Multiplicity(minimum=1)
	OKClass[] a;
	
	@Position(element="a",position={Position.BEFORE,Position.AFTER},separatorPolicy=SeparatorPolicy.REPLACE)
	OKClass c;
	
	@Position(element="a",position={Position.BEFORE,Position.AFTER},separatorPolicy=SeparatorPolicy.REPLACE)
	OKClass2 b;
	
}
