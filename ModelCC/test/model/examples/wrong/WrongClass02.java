/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;

@Pattern(regExp="a")
public class WrongClass02 implements IModel {

     @Setup void run1() {
     }

     @Setup boolean run2() {
         return true;
     }
}
