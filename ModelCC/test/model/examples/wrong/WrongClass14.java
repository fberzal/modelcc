/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Setup;

public class WrongClass14 implements IModel {

    OKClass aasdf2;

    @Setup String run() {
        return "a";
    }

}
