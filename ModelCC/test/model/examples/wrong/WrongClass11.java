/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import java.util.Set;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;

public class WrongClass11 implements IModel {

	@Multiplicity(minimum=3,maximum=1)
    Set<OKClass> aasdf2;

}
