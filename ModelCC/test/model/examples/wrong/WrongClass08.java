/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Value;

public class WrongClass08 implements IModel {

    @Value NotIModelClass aasdf;
    OKClass aasdf2;

}
