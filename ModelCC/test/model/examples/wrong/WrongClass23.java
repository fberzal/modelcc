/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="]]]]]]]]]]")
public class WrongClass23 implements IModel {

    private WrongClass23() {

    }

}
