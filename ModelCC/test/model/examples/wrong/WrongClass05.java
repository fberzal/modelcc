/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.lexer.recognizer.regexp.RegExpPatternRecognizer;

@Pattern(regExp="a",matcher=RegExpPatternRecognizer.class,args="a")
public class WrongClass05 implements IModel {

    OKClass aasdf;
    OKClass aasdf2;

}
