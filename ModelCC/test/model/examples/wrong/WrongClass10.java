/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import java.util.Set;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;

public class WrongClass10 implements IModel {

	@Multiplicity(maximum=-5)
    Set<OKClass> aasdf2;

}
