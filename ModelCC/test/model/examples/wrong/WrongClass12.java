/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;

public class WrongClass12 implements IModel {

	@Multiplicity(minimum=3,maximum=7)
    OKClass aasdf2;

}
