/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.Constraint;
import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="a")
public class WrongClass02b implements IModel {

     @Constraint void run1() {
     }

     @Constraint boolean run2() {
         return true;
     }
}
