/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Position;
import org.modelcc.SeparatorPolicy;

public class WrongClassPosition5 implements IModel {

	OKClass a;
	
	@Position(element="a",position=Position.WITHIN,separatorPolicy=SeparatorPolicy.REPLACE)
	OKClass2 b;
	
}
