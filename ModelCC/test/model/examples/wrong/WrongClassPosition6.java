/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Position;

public class WrongClassPosition6 implements IModel {

	@Position(element="b",position=Position.AFTER)
	OKClass a;
	
	@Position(element="a",position=Position.BEFORE)
	OKClass2 b;
	
}
