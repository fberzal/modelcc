/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Priority;

@Pattern(regExp="a")
@Priority(precedes=NotIModelClass.class)
public class WrongClass04 implements IModel {
    
}
