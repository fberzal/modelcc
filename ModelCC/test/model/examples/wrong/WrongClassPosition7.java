/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.wrong;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Position;
import org.modelcc.SeparatorPolicy;

public class WrongClassPosition7 implements IModel {

	@Optional
	OKClass[] a;
	
	@Position(element="a",position=Position.BEFORELAST,separatorPolicy=SeparatorPolicy.REPLACE)
	OKClass2 b;
	
}
