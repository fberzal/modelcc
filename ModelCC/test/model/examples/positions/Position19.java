/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Position;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.SeparatorPolicy;
import org.modelcc.Suffix;

public class Position19 implements IModel {

	public A a;

	@Multiplicity(minimum=3)
	@Separator(value={"x","y"})
	public C[] c;
	
	@Position(element="c",position={Position.BEFORE,Position.AFTER,Position.WITHIN},separatorPolicy=SeparatorPolicy.REPLACE)
	@Prefix("z")
	@Suffix("w")
	public B b;
    
}
