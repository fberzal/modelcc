/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Value;

public class Number implements IModel {
    @Value
	public int value;
    
    @Override
    public String toString ()
    {
    	return Integer.toString(value);
    }
}
