/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Prefix;

public class Obj implements IModel {

	@Prefix("ID")
	@ID
	public Number number;
	
	public A content;
	
	
	public String toString ()
	{
		return "ID"+number.toString()+ " " + content;
	}
}
