/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Position;

public class Position2 implements IModel {

	public A a;

	@Position(element="a",position=Position.BEFORE)
	public B b;
    
}
