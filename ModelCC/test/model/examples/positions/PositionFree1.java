/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.FreeOrder;
import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Position;

@FreeOrder
public class PositionFree1 implements IModel {

	public A a;

	@Multiplicity(minimum=3)
	public C[] c;
	
	@Position(element="c",position=Position.BEFORELAST)
	public B b;
    
}
