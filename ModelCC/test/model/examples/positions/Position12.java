/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Position;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.SeparatorPolicy;
import org.modelcc.Suffix;

public class Position12 implements IModel {

	public A a;

	@Multiplicity(minimum=3)
	@Separator(value={"x","y"})
	public C[] c;
	
	@Position(element="c",position=Position.BEFORELAST,separatorPolicy=SeparatorPolicy.REPLACE)
	@Prefix("z")
	@Suffix("w")
	public B b;
    
}
