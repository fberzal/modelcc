/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Position;

public class Position1 implements IModel {

	@Position(element="b",position=Position.AFTER)
	public A a;

	public B b;
    
}
