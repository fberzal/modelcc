/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Optional;
import org.modelcc.Position;

public class Position4 implements IModel {

	public A a;

	@Optional
	public C c;
	
	@Position(element="a",position={Position.BEFORE,Position.AFTER})
	public B b;
    
}
