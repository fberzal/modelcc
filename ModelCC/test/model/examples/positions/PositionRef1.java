/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.positions;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Position;
import org.modelcc.Prefix;
import org.modelcc.Reference;

public class PositionRef1 implements IModel {

	public Obj[] objects;
	
	@Reference
	@Multiplicity(minimum=1)
	public Obj[] reflist;    
	
	@Prefix("a")
	@Reference
	@Position(element="reflist",position=Position.WITHIN)
	public Obj ref;
	
	
	@Override
	public String toString()
	{
		String result = "";
		
		for (int i=0; i<objects.length; i++)
			result+=objects[i].toString();

		for (int i=0; i<reflist.length; i++)
			result+=reflist[i].toString();

		result+="a"+ref;
		
		return result;
	}
}
