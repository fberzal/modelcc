/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.Constraint;
import org.modelcc.FreeOrder;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Priority;
import org.modelcc.Separator;
import org.modelcc.Suffix;

@Associativity(AssociativityType.LEFT_TO_RIGHT)
@Composition(CompositionType.EAGER)
@Prefix({"a","b"})
@Suffix({"c","d"})
@Separator("e")
@Priority(value=5,precedes=Test5.class)
@FreeOrder
public class Test2 implements IModel {

    @Constraint
    boolean run() {
        return true;
    }

}
