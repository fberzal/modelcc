/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.Composition;
import org.modelcc.CompositionType;
import org.modelcc.Constraint;
import org.modelcc.FreeOrder;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Priority;
import org.modelcc.Separator;
import org.modelcc.Suffix;

@Associativity(AssociativityType.RIGHT_TO_LEFT)
@Composition(CompositionType.LAZY)
@Prefix({})
@Suffix({"d","c"})
@Separator({})
@Priority(value=2,precedes={Test6.class})
@FreeOrder(false)
public class Test4 extends Test2 implements IModel {

    @Constraint boolean run2() {
        return true;
    }
}
