/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.Associativity;
import org.modelcc.AssociativityType;
import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Separator;
import org.modelcc.Suffix;
import org.modelcc.Value;

@Prefix({"a1","b1"})
@Suffix({"c1","d1"})
@Separator({"e","a1"})
@Associativity(AssociativityType.NON_ASSOCIATIVE)
public class Test1 implements IModel {

    @Value int a;
}
