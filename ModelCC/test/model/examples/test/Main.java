/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.modelcc.Constraint;
import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Optional;
import org.modelcc.Prefix;
import org.modelcc.Reference;
import org.modelcc.Separator;
import org.modelcc.Setup;
import org.modelcc.Suffix;

public class Main implements IModel {

    @Prefix({"a","b"})
    @Suffix({"c","d"})
    @Separator({"e","a"})
    @Multiplicity(minimum=0,maximum=1000)
    @Optional
    Test1[] tests1;

    @Multiplicity(minimum=1)
    List<Test1> tests2;

    @Multiplicity(maximum=10)
    @Reference
    List<Test3> tests3;

    Set<Test2> tests4;

    @ID
    @Reference
    Test5 tests5;

    
    Map<Test1,Integer> testsout1;

    Map<Test1,Test1> testsout2;

    Map<Integer,Test1> testsout3;

    Set<Set<Test1>> testsout4;

    @Constraint
    boolean run() {
        return true;
    }

    @Setup
    void setup() {
    	
    }
}
