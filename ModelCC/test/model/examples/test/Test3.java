/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.ID;
import org.modelcc.IModel;

public class Test3 extends Test2 implements IModel {

    @ID
    Test1 bua;
}
