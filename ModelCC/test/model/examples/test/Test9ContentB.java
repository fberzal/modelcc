/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="m?")
public class Test9ContentB implements IModel {
}
