/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.IModel;
import org.modelcc.Prefix;
import org.modelcc.Value;

@Prefix("$")
public class IncorrectDollarPrefix implements IModel {
	@Value
	int number;

	public String toString () {
		return "$"+number;
	}
}
