/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.Constraint;
import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Value;

@Pattern(regExp="a|b")
public class Test8_1 implements IModel {
    
    @Value
    String read;
    
    public Test8_1() {
        
    }
    
    @Constraint
    public Boolean run() {
        if (read.equals("a"))    
            return true;
        else
            return false;
    }
    
}
