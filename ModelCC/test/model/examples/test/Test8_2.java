/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.test;

import org.modelcc.IModel;
import org.modelcc.Pattern;
import org.modelcc.Setup;
import org.modelcc.Value;

@Pattern(regExp="a|b")
public class Test8_2 implements IModel {
    
    @Value
    String read;
    
    public Test8_2() {
        
    }
    
    @Setup
    public void run() {
    }
    
}
