/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalMain implements IModel {
    @Optional
    OptionalAll a;
}
