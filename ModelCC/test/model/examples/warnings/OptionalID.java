/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.ID;
import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalID implements IModel {
    @Optional
    @ID
    OptionalPart a;
}
