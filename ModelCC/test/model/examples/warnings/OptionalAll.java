/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;
import org.modelcc.Optional;

public class OptionalAll implements IModel {
    @Optional
    OptionalPart a;

    @Optional
    OptionalPart b;

    @Optional
    OptionalPart c;
}
