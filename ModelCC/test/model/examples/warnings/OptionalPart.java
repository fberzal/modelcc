/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;
import org.modelcc.Pattern;

@Pattern(regExp="a")
public class OptionalPart implements IModel {
}
