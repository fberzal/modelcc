/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;
import org.modelcc.Multiplicity;
import org.modelcc.Optional;

public class OptionalMult implements IModel {
    @Optional
    @Multiplicity(minimum=0)
    OptionalPart[] a;
}
