/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.ID;
import org.modelcc.IModel;

public class IDNotIModel implements IModel {
    
    OptionalPart b;
    
    @ID
    NotIModel a;
}
