/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.ID;
import org.modelcc.IModel;

public class CollectionID implements IModel {
    @ID
    OptionalPart[] a;
}
