/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;

public abstract class AbstNoSubClasses implements IModel {
}
