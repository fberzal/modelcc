/*
 * ModelCC, distributed under ModelCC Shared Software License, www.modelcc.org
 */


package model.examples.warnings;

import org.modelcc.IModel;
import org.modelcc.Reference;

public class ReferenceNotID2 implements IModel {
    
    @Reference
    OptionalPart[] a;
}
